require 'spec_helper'

describe "Testing business rules" do
  before(:each) do
    # This is a double-load of bibliography since it is already loaded by spec_helper
    # I'm not changing it for now, since it does test a real world case. -SD
    load File.join(File.dirname(__FILE__), 'bibliography_models.rb')
    load File.join(File.dirname(__FILE__), 'bibliography_schema.rb')
    # TODO: Don't make the model be included for the tests to run
    Object.send(:include, Bibliography)
    
    bibliography_classes = [Bibliography::Journal, Bibliography::JournalIssue, Bibliography::JournalArticle, Bibliography::Book, Bibliography::Person, Bibliography::Journal, Bibliography::Citation_Contributor_Author]
    bibliography_classes.each(&:delete)
    
    @journal          = Bibliography::Journal.create(:title => "JAMA")
    @journal2         = Bibliography::Journal.create(:title => "CACM")
    @journal_issue    = Bibliography::JournalIssue.create(:title => "Special Issue")
    @journal_article1 = Bibliography::JournalArticle.create(:title => "The effect of chocolate on mental health")
    @journal_article2 = Bibliography::JournalArticle.create(:title => "Article Two")
    @journal_article3 = Bibliography::JournalArticle.create(:title => "Article Three")
    @book1            = Bibliography::Book.create(:title => "The RSpec Book")
    @book2            = Bibliography::Book.create(:title => "The Pickaxe Book")
    @person1          = Bibliography::Person.create(:given_name => 'author', :surname => 'one')
    @person2          = Bibliography::Person.create(:given_name => 'author', :surname => 'two')
    @person3          = Bibliography::Person.create(:given_name => 'author', :surname => 'three')
    @martian1         = Bibliography::Martian.create(:given_name => 'martian', :surname => 'one')
    @martian2         = Bibliography::Martian.create(:given_name => 'martian', :surname => 'two')
  end
  
  after(:each) do
    Object.send(:remove_const, :Bibliography)
  end

  after(:all) do
    load File.join(File.dirname(__FILE__), 'bibliography_models.rb')
    # TODO: Don't make the model be included for the tests to run
    Object.send(:include, Bibliography)
  end

  
  describe "an initial check to make sure that we are getting clean copies of the Sequel::Model classes" do  
    it "should define a new method on a class from the model" do
      Bibliography::Journal.send(:define_method, :a_unique_method_name) {}
      Bibliography::Journal.new.should respond_to(:a_unique_method_name)
    end
  
    it "should no longer have the new method on the class from the model" do
      Bibliography::Journal.new.should_not respond_to(:a_unique_method_name)
    end
  end
  
  describe "for a transforming rule" do
    before :each do
      condition = lambda do |input|
          # Have to directly check the values because otherwise we end up calling the new method & get infinite recursion.
          if input.values
            (input.values[:issue] == nil || input.values[:issue] == '') && !input.contained_by.nil? && input.contained_by.is_a?(JournalIssue)
          else
            false
          end
        end
      transformation = lambda { |input| input.contained_by.title } 
      BusinessRules::RuleDsl.transform(:on_type => Bibliography::JournalArticle, :on_property => :issue, :when => condition, :how => transformation)
    end
  
    it "should transform the specified value when the condition is met" do
      @journal_article1.contained_by = @journal_issue
      @journal_article1.issue.should == "Special Issue"
    end
  
    it "should not transform the specified value when the condition isn't met" do
      @journal_article1.issue.should be_nil
    end
  
  end
  
  describe "for a prefilling rule" do
    before :each do
      BusinessRules::RuleDsl.prefill(:on_type => Bibliography::Citation, :on_property => :language, :containing_association => :contained_by)
    end
  
    it "should prefill the specified value when the value is empty and there is an object on the other end" do
      @journal.language = "en"
      @journal.save
      @journal.add_contain @journal_article1
      @journal_article1._get_property(:language).should == "en"
      @journal_article1.language.should == "en"
    end
  
    it "should get prefilling value from multiple levels up" do
      @journal.language = "en"
      @journal.save
      @journal.add_contain @journal_issue
      @journal_issue.add_contain @journal_article1  
      @journal_article1.language.should == "en"
    end
  
    it "should not prefill the specified value when the value isn't empty regardless of if there is an object on the other end"do
      @journal.language = "en"
      @journal.save
      @journal.add_contain @journal_article1
      @journal_article1.language = "fr"
      @journal_article1.language.should == "fr"
    end
  
    it "should not prefill when there isn't an object on the other end regardless of if the value is empty" do
      @journal.language = "en"
      @journal.save
      @journal_article1.language.should be_nil or be_empty
    end  
  end
  
  describe "for a prefilling rule when information is given about the containing object" do
    before :each do
      BusinessRules::RuleDsl.prefill(:on_type => Bibliography::JournalArticle, :on_property => :issue, :containing_association => :contained_by, :parent_property => :title, :parent_is => :JournalIssue)
    end
    it "should prefill when the parent type is met" do
      @journal_issue.add_contain(@journal_article1)
      @journal_article1.issue.should == "Special Issue"
    end
  
    it "should not prefill when the parent type is not met" do
      @journal.add_contain(@journal_article1)
      @journal_article1.issue.should be_nil or be_empty
    end
  end
  
  describe "for a value constraint rule" do
    describe "when there is a multiple value" do
      before :each do
          BusinessRules::RuleDsl.value_constraint(:on_type => Bibliography::Journal, :on_property => :contains) { |value| value.respond_to?(:title) && value.title == "The effect of chocolate on mental health" }
      end
      it "should allow an array containing all valid objects to be set to the property" do
        @journal.contains = [@journal_article1]
        @journal.contains.count.should == 1
        @journal.contains.should include(@journal_article1)
        @journal_article1.contained_by.should == @journal
      end
    
      it "should allow a valid object to be added to the property" do
        @journal.add_contain @journal_article1
        @journal.contains.count.should == 1
        @journal.contains.should include(@journal_article1)
        @journal_article1.contained_by.should == @journal
      end
    
      it "should not allow an array containing invalid objects to be set to the property" do
        expect { @journal.contains = [@journal_article1, @journal_article2] }.to raise_error(BusinessRules::RuleError)
      end
    
      it "should not allow an invalid object to be added to the property" do
        expect { @journal.add_contain @journal_article2 }.to raise_error(BusinessRules::RuleError)
        @journal_article2.contained_by.should be_nil
        @journal.contains.should be_empty
      end
    end
  
    describe "when there is a single value" do
      before :each do
        BusinessRules::RuleDsl.value_constraint(:on_type => Bibliography::JournalArticle, :on_property => :contained_by) { |value| value.respond_to?(:title) && value.title == "JAMA" }
      end
    
      it "should allow a valid object to be set to the property" do
        @journal_article1.contained_by = @journal
        @journal_article1.contained_by.should == @journal
        @journal.contains.count.should == 1
        @journal.contains.should include(@journal_article1)
      end
    
      it "should not allow an invalid object to be set to the property" do
        expect { @journal_article1.contained_by = @journal2 }.to raise_error(BusinessRules::RuleError)
        @journal_article1.contained_by.should be_nil
        @journal2.contains.should be_empty
      end
    end
  end
  
  describe "for a type constraint rule" do  
    describe "on a there is a multiple value" do
      before :each do
        BusinessRules::RuleDsl.type_constraint(:on_type => Bibliography::Journal, :on_property => :contains, :valid_types => [:JournalIssue, :JournalArticle])
      end
    
      it "should report the valid types as exactly what is in the rule" do
        @journal.contains_type.count.should == 2
        @journal.contains_type.should include(Bibliography::JournalIssue)
        @journal.contains_type.should include(Bibliography::JournalArticle)
      end
    
      it "should allow an array containing all valid objects to be set to the property" do
        @journal.contains = [@journal_article1, @journal_article2]
        @journal.contains.count.should == 2
        @journal.contains.should include(@journal_article1)
        @journal.contains.should include(@journal_article2)
        @journal_article1.contained_by.should == @journal
        @journal_article2.contained_by.should == @journal
      
      end
    
      it "should allow a valid object to be added to the property" do
        @journal.add_contain @journal_article1
        @journal.contains.count.should == 1
        @journal.contains.should include(@journal_article1)
        @journal_article1.contained_by.should == @journal
      end
    
      it "should not allow an array containing an invalid to be set to the property" do
        expect { @journal.contains = [@book1, @book2] }.to raise_error(BusinessRules::RuleError)
      end
    
      it "should not allow an invalid object to be added to the property" do
        expect { @journal.add_contain @book1 }.to raise_error(BusinessRules::RuleError)
        @book1.contained_by.should be_nil
        @journal.contains.should be_empty
      end
    end
  
    describe "when there is a single value" do    
      before :each do
        BusinessRules::RuleDsl.type_constraint(:on_type => Bibliography::JournalArticle, :on_property => :contained_by, :valid_types => [:Journal, :JournalIssue])
      end
    
      it "should report the valid types as exactly what is in the rule" do
        @journal_article1.contained_by_type.count.should == 2
        @journal_article1.contained_by_type.should include(Bibliography::Journal)
        @journal_article1.contained_by_type.should include(Bibliography::JournalIssue)
      end
    
      it "should allow a valid object to be set to the property" do
        @journal_article1.contained_by = @journal
        @journal_article1.contained_by.should == @journal
        @journal.contains.count.should == 1
        @journal.contains.should include(@journal_article1)
      end
    
      it "should not allow an invalid object to be set to the property" do
        expect { @journal_article1.contained_by = @book1 }.to raise_error(BusinessRules::RuleError)
        @journal_article1.contained_by.should be_nil
        @book1.contains.should be_empty
      end
    end
  end

  describe "making sure that we can limit the number of instances of one class associated to an instance of another class" do  
    before :each do
      BusinessRules::RuleDsl.multiplicity_constraint(Bibliography::Journal, :contains, :max => 3)
      BusinessRules::RuleDsl.multiplicity_constraint(Bibliography::Journal, :contains, :max => 1, :on_subtype => "Bibliography::Book")
      BusinessRules::RuleDsl.multiplicity_constraint(Bibliography::JournalIssue, :contains, :min => 1)
      BusinessRules::RuleDsl.multiplicity_constraint(Bibliography::JournalArticle, :authors, :max => 3, :min => 1)
      BusinessRules::RuleDsl.multiplicity_constraint(Bibliography::JournalArticle, :authors, :max => 1, :on_subtype => "Bibliography::Martian")
    end
    it "should limit the association to a maximum number" do
      @journal.add_contain(@journal_article1)
      @journal.add_contain(@journal_article2)
      @journal.add_contain(@book1)
      lambda {@journal.add_contain(@journal_article3)}.should raise_error(BusinessRules::RuleError, /max/i)
    end
    
    it "should limit the association to a maximum number of instances of a specific subtype" do
      @journal.add_contain(@book1)
      lambda {@journal.add_contain(@book2)}.should raise_error(BusinessRules::RuleError, /max/i)
    end
  
    it "should limit the association to a minimum number" do
      @journal_issue.add_contain(@journal_article1)
      lambda {@journal_issue.remove_contain(@journal_article1)}.should raise_error(BusinessRules::RuleError, /min/i)
    end
    
    it "should enforce min/max restrictions with many_to_many association types, including subtype contraints" do
      @journal_article1.add_author(@person1)
      lambda { @journal_article1.remove_author(@person1) }.should raise_error(BusinessRules::RuleError, /min/i)
      @journal_article1.add_author(@person2)
      @journal_article1.add_author(@martian1)
      lambda { @journal_article1.add_author(@person3)}.should raise_error(BusinessRules::RuleError, /max/i)
      lambda { @journal_article1.add_author(@martian2)}.should raise_error(BusinessRules::RuleError, /max/i)
      
      # Same test on second journal article
      @journal_article2.add_author(@person1)
      lambda { @journal_article2.remove_author(@person1) }.should raise_error(BusinessRules::RuleError, /min/i)
      @journal_article2.add_author(@person2)
      @journal_article2.add_author(@martian1)
      lambda { @journal_article2.add_author(@person3)}.should raise_error(BusinessRules::RuleError, /max/i)
      lambda { @journal_article2.add_author(@martian2)}.should raise_error(BusinessRules::RuleError, /max/i)
    end
    
    it "should enforce min/max restrictions when setting a many_to_many association from a join table entry or association class, including subtype contraints" do
      join = Bibliography::Citation_Contributor_Author.create
      join.authored_citation = @journal_article1
      join.author = @person1
      lambda { join.author = nil }.should raise_error(BusinessRules::RuleError, /min/i)

      join2 = Bibliography::Citation_Contributor_Author.create
      join2.authored_citation = @journal_article1
      join2.author = @person2
      
      join3 = Bibliography::Citation_Contributor_Author.create
      join3.authored_citation = @journal_article1
      join3.author = @martian1
      
      illegal_join1 = Bibliography::Citation_Contributor_Author.create
      lambda do
        illegal_join1.authored_citation = @journal_article1
        illegal_join1.author = @person3
      end.should raise_error(BusinessRules::RuleError, /max/i)
      # Remove hanging join
      illegal_join1.delete
      
      illegal_join2 = Bibliography::Citation_Contributor_Author.create
      lambda do
        illegal_join2.authored_citation = @journal_article1
        illegal_join2.author = @martian2
      end.should raise_error(BusinessRules::RuleError, /max/i)
      # Remove hanging join
      illegal_join2.delete
      
      # Same test on second journal article
      join = Bibliography::Citation_Contributor_Author.create
      join.authored_citation = @journal_article2
      join.author = @person1
      lambda { join.author = nil }.should raise_error(BusinessRules::RuleError, /min/i)
       
      join2 = Bibliography::Citation_Contributor_Author.create
      join2.authored_citation = @journal_article2
      join2.author = @person2
      
      join3 = Bibliography::Citation_Contributor_Author.create
      join3.authored_citation = @journal_article2
      join3.author = @martian1
      
      illegal_join1 = Bibliography::Citation_Contributor_Author.create
      lambda do
        illegal_join1.authored_citation = @journal_article2
        illegal_join1.author = @person3
      end.should raise_error(BusinessRules::RuleError, /max/i)
      # Remove hanging join
      illegal_join1.delete
      
      illegal_join2 = Bibliography::Citation_Contributor_Author.create
      lambda do
        illegal_join2.authored_citation = @journal_article2
        illegal_join2.author = @martian2
      end.should raise_error(BusinessRules::RuleError, /max/i)
      # Remove hanging join
      illegal_join2.delete
    end
  end
end