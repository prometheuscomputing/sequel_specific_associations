module Bibliography
  # ============= Begin class Citation =============
  class Citation < Sequel::Model
    set_dataset :citations
    plugin :boolean_readers
    plugin :specific_associations
    #abstract!
    many_to_many :Citation, :through => :Citation_Citation_References, :opp_getter => :referenced_by, :getter => :references
    many_to_many :Citation, :through => :Citation_Citation_References, :opp_getter => :references, :getter => :referenced_by
    many_to_many :Contributor, :through => :Citation_Contributor_Author, :opp_getter => :authored_citations, :getter => :authors
    many_to_one :Citation, :getter => :contained_by
    many_to_one :Citation, :getter => :original_language_information
    one_to_many :Citation, :opp_getter => :contained_by, :getter => :contains
    one_to_many :Citation, :opp_getter => :original_language_information, :getter => :translations
    
    attribute :title, String
    attribute :subtitle, String
    attribute :publication_type, String
    attribute :edition, String
    attribute :language, String
  end

  class Citation_Contributor_Author < Sequel::Model
    set_dataset :citation_contributor_authors
    plugin :boolean_readers
    plugin :specific_associations
    associates :Citation => :authored_citation; associates :Contributor => :author
  end

  class JournalIssue < Citation
    set_dataset :journal_issues
  end

  class Book < Citation
    set_dataset :books
  end
  
  class JournalArticle < Citation
    set_dataset :journal_articles
    
    attribute :issue, String
  end

  class Journal < Citation
    set_dataset :journals
  end
  
  class Contributor < Sequel::Model
    set_dataset :contributors
    plugin :boolean_readers
    plugin :specific_associations
    abstract!
    many_to_many :Citation, :through => :Citation_Contributor_Author, :opp_getter => :authors, :getter => :authored_citations
    
    attribute :given_name, String
    attribute :surname, String
  end

  class Person < Contributor
    set_dataset :bib_people
  end

  class Martian < Contributor
    set_dataset :martians
  end
end