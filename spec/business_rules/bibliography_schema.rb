module Bibliography
  # ============= Begin class Citation =============
  Citation.create_schema?
  Citation_Contributor_Author.create_schema?
  JournalIssue.create_schema?
  Book.create_schema?
  JournalArticle.create_schema?
  Journal.create_schema?
  Contributor.create_schema?
  Person.create_schema?
  Martian.create_schema?
end

# module Bibliography
#   # So as not to mess up the pretty indentation below -SD
#   INT_TYPE = SpecificAssociations::INT_TYPE
  
#   # ============= Begin class Citation =============
#   DB.create_table :citations do
#     String   :abbreviated_title
#     INT_TYPE :contained_by_id
#     String   :contained_by_class
#     String   :edition
#     String   :language
#     INT_TYPE :original_language_information_id
#     String   :original_language_information_class
#     String   :publication_type
#     String   :subtitle
#     String   :title
#     primary_key :id
#   end
#   # == Begin associated classes for class Citation ==
      
#   DB.create_table :citation_contributor_authors do
#     INT_TYPE :authored_citation_id
#     String   :authored_citation_class
#     INT_TYPE :author_id
#     String   :author_class
#     primary_key :id
#     index [:authored_citation_id, :authored_citation_class, :author_id, :author_class]
#   end


#   # == End associated classes for class Citation ==
#   # ============= End class Citation =============

#   # ============= Begin class JournalIssue =============
#   DB.create_table :journal_issues do
#     String   :volume
#     INT_TYPE :publisher_id
#     INT_TYPE :institution_id
#     INT_TYPE :description_id
#     String   :description_class
#     String   :abbreviated_title
#     INT_TYPE :contained_by_id
#     String   :contained_by_class
#     String   :edition
#     String   :language
#     INT_TYPE :original_language_information_id
#     String   :original_language_information_class
#     String   :publication_type
#     String   :subtitle
#     String   :title
#     primary_key :id
#   end
#   # ============= End class JournalIssue =============

#   # ============= Begin class Contributor =============
#   DB.create_table :contributors do
#     primary_key :id
#   end
#   # ============= End class Contributor =============

#   # ============= Begin class Book =============
#   DB.create_table :books do
#     INT_TYPE :volume
#     INT_TYPE :publisher_id
#     INT_TYPE :institution_id
#     INT_TYPE :description_id
#     String   :description_class
#     String   :abbreviated_title
#     INT_TYPE :contained_by_id
#     String   :contained_by_class
#     String   :edition
#     String   :language
#     INT_TYPE :original_language_information_id
#     String   :original_language_information_class
#     String   :publication_type
#     String   :subtitle
#     String   :title
#     primary_key :id
#   end
#   # ============= End class Book =============

#   # ============= Begin class Person =============
#   DB.create_table :bib_people do
#     String   :given_name
#     String   :surname
#     primary_key :id
#   end
#   # ============= End class Person =============
  
#   # ============= Begin class Martian =============
#   DB.create_table :martians do
#     String   :given_name
#     String   :surname
#     primary_key :id
#   end
#   # ============= End class Person =============
  
#   # ============= Begin class JournalArticle =============
#   DB.create_table :journal_articles do
#     String   :issue
#     String   :volume
#     INT_TYPE :publisher_id
#     INT_TYPE :institution_id
#     INT_TYPE :description_id
#     String   :description_class
#     String   :abbreviated_title
#     INT_TYPE :contained_by_id
#     String   :contained_by_class
#     String   :edition
#     String   :language
#     INT_TYPE :original_language_information_id
#     String   :original_language_information_class
#     String   :publication_type
#     String   :subtitle
#     String   :title
#     primary_key :id
#   end
#   # ============= End class JournalArticle =============

#   # ============= Begin class Journal =============
#   DB.create_table :journals do
#     INT_TYPE :publisher_id
#     INT_TYPE :institution_id
#     INT_TYPE :description_id
#     String   :description_class
#     String   :abbreviated_title
#     INT_TYPE :contained_by_id
#     String   :contained_by_class
#     String   :edition
#     String   :language
#     INT_TYPE :original_language_information_id
#     String   :original_language_information_class
#     String   :publication_type
#     String   :subtitle
#     String   :title
#     primary_key :id
#   end
#   # ============= End class Journal =============
# end