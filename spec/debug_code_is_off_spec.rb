describe "This failed because $DEBUG is left on." do  
  it "Rspec must pass with $DEBUG off before committing or releasing." do
    expect($DEBUG).to be false
  end
end
