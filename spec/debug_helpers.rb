require 'rainbow'
module Sequel::Plugins::SpecificAssociations::InstanceMethods
  # nice for debugging, depends on #dputscp
  def _cinfo(getter_or_info)
    info = getter_or_info.is_a?(Hash) ? getter_or_info : _get_info(getter_or_info)
    dputscp info
  end
end

class Sequel::Database
  def clear_db; tables.each { |table| self[table].delete }; end
end

class Sequel::Model
  def properties_report(verbose = false)
    # attributes_report(verbose)
    associations_report(verbose)
  end

  def attributes_report(verbose = false)
    primitive_attributes_report(verbose)
    complex_attributes_report(verbose)
  end

  def primitive_attributes_report(verbose = false)
    puts "-"*10 + " #{name}" + " -- #{turn_magenta('ATTRIBUTES')} " + "-"*10
    primitive_attributes.each do |getter, info|
      display_name = "  \"#{info[:display_name]}\"" if info[:display_name]
      puts Rainbow(info[:getter]).green + ":" + Rainbow(info[:class]).cyan + display_name.to_s
      rejects = [:diplay_name, :type, :class, :getter, :attribute]
      rejects += [:store_on_get, :store_value] unless verbose
      info.reject { |k,v| rejects.include?(k)}.each do |k,v|
        printf "  %-15s => %s\n", k, v
      end
    end
    nil
  end

  def complex_attributes_report(verbose = false)
    puts "-"*10 + " #{name}" + " -- #{turn_magenta('COMPLEX ATTRIBUTES')} " + "-"*10
    complex_attributes.each do |getter, info|
      display_name = "  \"#{info[:display_name]}\"" if info[:display_name]
      puts Rainbow(info[:getter]).green + ":" + Rainbow(info[:class]).cyan + display_name.to_s
      rejects = [:diplay_name, :type, :class, :getter, :attribute, :complex]
      rejects += [:store_on_get, :store_value] unless verbose
      info.reject { |k,v| rejects.include?(k)}.each do |k,v|
        printf "  %-15s => %s\n", k, v
      end
    end
    nil
  end

  def associations_report(verbose = false)
    puts "-"*10 + " #{name}" + " -- #{turn_magenta('ASSOCIATIONS')} " + "-"*10
    associations.each do |getter, info|
      display_name = "  \"#{info[:display_name]}\"" if info[:display_name]
      t = "  " + Rainbow(info[:type]).red
      puts Rainbow(info[:getter]).green + ":" + Rainbow(info[:class]).cyan + t + display_name.to_s
      rejects = [:type, :display_name, :class, :getter]
      rejects += [:alias, :store_on_get, :store_value, :holds_key, :association, :singular_getter, :opp_getter, :singular_opp_getter, :as] unless verbose
      info.reject { |k,v| rejects.include?(k)}.each do |k,v|
        if v == true && !verbose
          printf "  %-15s\n", k
        else
          printf "  %-15s => %s\n", k, v
        end
      end
    end
    nil
  end

  def report_property(getter, verbose = false)
    getter = getter.to_sym
    _, info = properties.find { |k,v| v[:getter] == getter }
    if info.nil?
      puts "Could not find #{getter} in #{properties.keys}"
      return
    end
    display_name = "  \"#{info[:display_name]}\"" if info[:display_name]
    t = "  " + Rainbow(info[:type]).red
    puts Rainbow(info[:getter]).green + ":" + Rainbow(info[:class]).cyan + t + display_name.to_s
    rejects = [:type, :display_name, :class, :getter]
    rejects += [:alias, :store_on_get, :store_value, :holds_key, :association, :attribute, :complex_attribute, :complex, :singular_getter, :opp_getter, :singular_opp_getter, :as] unless verbose
    info.reject { |k,v| rejects.include?(k)}.each do |k,v|
      if v == true && !verbose
        printf "  %-15s\n", k
      else
        printf "  %-15s => %s\n", k, v
      end
    end
    !!info
  end
  alias_method :property_report, :report_property
end