# These extensions are in addition to those specified in car_example/model_extensions and gui_director/model_extensions
# NOTE: Please consider adding new extensions to car_example/model_extensions instead of this file.
module Automotive
  class Vehicle
    # Test add hooks
    before_add_proc = proc { |new_address, vehicle| vehicle.instance_variable_set(:@new_address, new_address)}
    after_add_proc = proc { |new_address, vehicle| vehicle.instance_variable_set(:@new_address_line, "#{new_address.street_number} #{new_address.street_name}") }
    add_association_options :registered_at, :before_add => before_add_proc, :after_add => after_add_proc
    # Test remove hooks
    before_remove_proc1 = proc { |old_address, vehicle| vehicle.instance_variable_set(:@old_address, old_address) }
    before_remove_proc2 = proc { |old_address, vehicle| vehicle.instance_variable_set(:@old_address_line, "#{old_address.street_number} #{old_address.street_name}") }
    after_remove_proc = proc { |old_address, vehicle| vehicle.instance_variable_set(:@old_address_copy, old_address) }
    add_association_options :registered_at, :before_remove => [before_remove_proc1, before_remove_proc2], :after_remove => after_remove_proc 
  end
  class Part
    # Ensure that sub_parts is acyclic.
    # This is a bad example since composition should already imply acyclic behavior
    add_association_options(:sub_parts, :acyclic => true)
  end
end

module People
  class Person
    # Define prototypes alias association
    alias_association :prototypes, 'Automotive::Vehicle', :type => :one_to_many, :alias_of => :vehicles, :filter => [{:cost => 100..99999999}, Sequel.ilike(:vehicle_model,'%Proto%')]
    
    # Test multiple before_add hooks to ensure return values are correctly processed
    before_add_proc1 = proc { |new_occupying, person| true }
    # Never allow a person to occupy a 'DeathTrap' vehicle
    before_add_proc2 = proc { |new_occupying, person| new_occupying.vehicle_model != 'DeathTrap'}
    # Nil value is not 'false', and should not raise a hook failure
    before_add_proc3 = proc { |new_occupying, person| nil }
    add_association_options :occupying, :before_add => [before_add_proc1, before_add_proc2, before_add_proc3]
    
    def before_association_add(assoc, object)
      # Don't allow 'Persona Non Grata' to associate to anything
      return false if name == 'Persona Non Grata'
      super
    end
  end
  
  module Clown
    class Clown
      # Test adding a before_add hook to a subclass of a model that already has before_add hooks defined.
      before_add_proc4 = proc { |new_occupying, clown| clown.name += " on a unicycle!" if new_occupying.is_a?(People::Clown::Unicycle); true }
      add_association_options :occupying, :before_add => [before_add_proc4]
    end
  end
end 

module Geography
  class Country
    attr_accessor :recent_additions
    attr_accessor :recent_removals
    def before_association_add(assoc, object)
      return false if object.name == "BadState"
      super
    end
    def after_association_add(assoc, object)
      super
      @recent_additions ||= []
      @recent_additions << object
    end
    def before_association_remove(assoc, object)
      return false if object.name == "GoodState"
      super
    end
    def after_association_remove(assoc, object)
      super
      @recent_removals ||= []
      @recent_removals << object
    end
  end
end