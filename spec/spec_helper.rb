require 'fileutils'
require 'lodepath'
begin
  require 'rspec'
rescue LoadError
  require 'rubygems' unless ENV['NO_RUBYGEMS']
  gem 'rspec'
  require 'rspec'
end

tmp_dir = relative '../tmp'
FileUtils.mkdir_p(tmp_dir) unless File.exists?(tmp_dir)

if ENV['PERFORMANCE_PROFILE']
  require 'ruby-prof'
  profiler = RubyProf::Profile.new
  t = Time.now
  profiler.start
  at_exit do
    time_taken = Time.now - t
    puts "Time taken: #{time_taken}"
    output_prefix = 'profiler_output-'
    existing_files = Dir["#{tmp_dir}/#{output_prefix}[0-9]*"]
    if existing_files.any?
      highest_num = existing_files.collect{|f| f.match(/^.*\-(\d+)$/)[1].to_i}.max
      next_num = highest_num + 1
    else
      next_num = 1
    end
    output_dir_name = output_prefix + next_num.to_s
    output_dir = File.join(tmp_dir, output_dir_name)
    FileUtils.mkdir_p output_dir
    # Print time_taken to filename in output dir
    FileUtils.touch File.join(output_dir, time_taken.to_s + '.txt')
    result = profiler.stop
    # Flat text printer
    # printer = RubyProf::FlatPrinter.new(result)
    # File.open File.join(output_dir, "flat.txt"), 'w' do |file|
    #   printer.print(file, :min_percent => 0.1)
    # end

    # Save a callgrind trace
    # You can load this up in kcachegrind or a compatible tool.
    # printer = RubyProf::CallTreePrinter.new(result)
    # calltree_dir = File.join(output_dir, 'calltree')
    # FileUtils.mkdir_p(calltree_dir)
    # printer.print(:path => calltree_dir, :profile => 'ssa')

    # printer = RubyProf::GraphHtmlPrinter.new(result)
    # File.open File.join(output_dir, "graph.html"), 'w' do |file|
    #   printer.print(file)
    # end

    printer = RubyProf::CallStackPrinter.new(result)
    File.open File.join(output_dir, "callstack.html"), 'w' do |file|
      printer.print(file)
    end
    
    puts " printed."
  end
end

# Enable old syntax for Rspec 3.0
RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

require 'simplecov'
SimpleCov.start do
  add_filter 'spec/'
  add_filter 'tmp/'
end

$DEBUG = false
require_relative 'debug_helpers' if $DEBUG

require 'rubygems'
require 'sequel'


# Turn on change tracker for the following classes
# Note that carexample_generated models are already change-tracked, and thus not listed here
def setup_change_tracker
  $session = {Thread.current.object_id => {:username => 'username'}}
  ChangeTracker.session_hash = $session
end

def clear_db
  DB.tables.each do |table|
    DB[table].delete
  end
  # DB[:sqlite_sequence].delete
  true
end

def backup_db
  $db_backup = {}
  DB.tables.each do |table|
    $db_backup[table] = DB[table].all
  end
end

def restore_db
  $db_backup.each do |table, rows|
    DB[table].import(rows.first.keys, rows.collect { |r| r.values}) if rows.any?
  end
end

def reset_db
  clear_db
  ChangeTracker.cancel if defined?(ChangeTracker)
  restore_db
end

def load_model_definition_example
  load 'associations/model_definition_example.rb'
end

# To help with failed comparisons, override Time's inspect method to show milliseconds
class Time
  def inspect
    strftime "%Y-%m-%d %H:%M:%S:%L %z"
  end
end

def sequel_models_in(modules)
  models = []
  modules.each do |mod|
    consts = mod.constants.collect { |constant| mod.const_get(constant)}
    classes, submodules = consts.partition{ |c| c.is_a?(Class)}
    # Reject any non-modules from submodules
    submodules.reject!{ |m| !m.is_a?(Module)}
    models.concat(classes.select { |c| c.ancestors.include?(Sequel::Model)})
    models.concat(sequel_models_in(submodules))
  end
  models
end

def top_level_modules
  top_level_modules = CarExampleGenerated.top_level_modules.collect { |m| m.to_const }
  top_level_modules += [UseCaseMetaModelV4, Custom_Profile, Application, Recursive, Home, Work, InterfaceTest, PrimitiveExample, ClassRoom, SingletonExample, CompositionTest, College, DefinitionExampleModule, Qux]
end

# If available, use local library from sibling directory to SSA folder
def use_local_lib(libname, files = [])
  files << libname
  begin
    local_path = File.expand_path("../../#{libname}/lib", __dir__)
    $:.unshift local_path
    files.each { |filepath| require local_path + '/' + filepath }
    puts " USING LOCAL COPY OF #{libname} AT: #{local_path} ".center(120, "-")
  rescue LoadError => e
    $:.shift # Remove local_path from load path
    puts " COULDN'T LOAD LOCAL COPY OF #{libname}. LOADING GEM... ".center(120, '*')
    files.each { |filepath| require filepath }
  end
end

db_file = File.dirname(__FILE__) + '/test_sql.db'
File.delete(db_file) if File.exists?(db_file)

if defined? Java
  require File.join(File.dirname(__FILE__), %w[sqlitejdbc-v056.jar])
  DB = Sequel.connect("jdbc:sqlite:#{db_file}")
else  
  DB = Sequel.sqlite#(db_file)
  DB.foreign_keys = false
end

use_local_lib('alternate_inheritance')
use_local_lib('model_metadata')
$:.unshift(File.dirname(__FILE__) + '/../lib')
require 'sequel_specific_associations'

use_local_lib('sequel_change_tracker')
require 'associations/custom_profile'
require 'associations/custom_profile_extensions'
require 'business_rules/bibliography_models'
require 'business_rules/bibliography_schema'
require 'associations/usecasemetamodelv4_sequel'
require 'associations/usecasemetamodelv4_schema'
require 'associations/one_to_one_poly-poly_example'
require 'associations/ordered_example'
require 'associations/uniqueness_example'
require 'associations/naming_example'
require 'associations/namespace_example.rb'
require 'associations/complex_attribute_example.rb'
require 'associations/multiple_inheritance_example.rb'
require 'associations/interfaces_example.rb'
require 'associations/primitive_example.rb'
require 'associations/primitive_example2.rb'
require 'associations/singleton_example.rb'
require 'associations/composition_example.rb'
require 'associations/subsets_example.rb'
require 'associations/create_schema_example.rb'
require 'associations/recursive_example.rb'
require 'associations/art_class_example.rb'


# Run once so example can be used other places
load_model_definition_example

use_local_lib('gui_director')
require 'gui_director/options'
require 'gui_director/policy'

# Load carexample into test database
$DB_HASH = {}
$DB_HASH['CarExample'] = DB
require 'car_example_generated'
require CarExampleGenerated.driver_file
use_local_lib('car_example', ['car_example/model_extensions'])
require 'car_example/populate_data'
# Load additional, SSA-defined car example model extensions
require 'car_example_model_extensions'
require 'gui_director/model_extensions'
SimpleCov.command_name('normal')


puts "Using Sequel version: #{Sequel::VERSION}"
# puts "Using SQLite3 version: #{SQLite3::VERSION}"
backup_db
include UseCaseMetaModelV4
