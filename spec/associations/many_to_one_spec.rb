require 'spec_helper'

describe "domain object for a many to one" do
  # ============================================================
  # BEGIN polymorphic-to-polymorphic many-to-one section
  # ============================================================
  
  context "polymorphic-to-polymorphic relationship -" do
    context "setting the association" do
      before :each do
        Actor.delete; System.delete; Domain.delete
        @super_actor = Actor.create
        @sub_actor = Actor.create
        @system = System.create
        @domain = Domain.create # Unrelated class used for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = (@super_actor.add_actor_sub(@system))
      #   result.should be_an_instance_of(System)
      #   result = @system.method(:actor_super=).call(@super_actor)
      #   #result = (@system.actor_super = @super_actor)
      #   result.should be_an_instance_of(System)
      # end

      # it "should not commit changes necessary for the association until the model is saved" do
      #   model_to_save = @super_actor.add_actor_sub(@sub_actor)
      #   # model_to_save is @sub_actor here
      #   model_to_save.actor_super_id.should be_nil
      #   model_to_save.actor_super_class.should be_nil
      #   model_to_save.save
      #   model_to_save.actor_super_id.should == @super_actor.id
      #   model_to_save.actor_super_class.should == @super_actor.class.to_s
      #   
      #   
      #   @super_actor.actor_subs.should include(@sub_actor)
      #   @sub_actor.actor_super.should == @super_actor
      # end
    
      it "should add the element to the association via the first class" do
        @super_actor.add_actor_sub(@sub_actor)#.save
        @super_actor.actor_subs.should include(@sub_actor)
        @sub_actor.actor_super.should == @super_actor
        @super_actor.add_actor_sub(@system)#.save
        @super_actor.actor_subs.should include(@system)
        @super_actor.actor_subs.should include(@sub_actor)
        @system.actor_super.should == @super_actor
      end

      it "should add the element to the association via the second class" do
        @sub_actor.actor_super = @super_actor
        @sub_actor#.save
        @super_actor.actor_subs.should include(@sub_actor)
        @sub_actor.actor_super.should == @super_actor
        @system.actor_super = @super_actor
        @system#.save
        @super_actor.actor_subs.should include(@system)
        @system.actor_super.should == @super_actor
      end

      it "should reject an element that is not of the polymorphic type" do
        # Domain is not of type Actor
        lambda {@super_actor.add_actor_sub(@domain)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @super_actor.actor_subs.should_not include(@sub_actor)
        lambda {@sub_actor.actor_super = @domain
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
      end
    end

    context "removing the association" do
      before :each do
        Actor.delete; System.delete; Domain.delete
        @super_actor = Actor.create
        @sub_actor = Actor.create
        @system = System.create
        @domain = Domain.create # Unrelated class used for exclusion testing

        @super_actor.add_actor_sub(@sub_actor)#.save
        @super_actor.add_actor_sub(@system)#.save
        @super_actor.actor_subs.should include(@system)
        @super_actor.actor_subs.should include(@sub_actor)
        @system.actor_super.should == @super_actor
        @sub_actor.actor_super.should == @super_actor
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = (@super_actor.remove_actor_sub(@system))
      #   result.should be_an_instance_of(System)
      #   result = (@sub_actor.actor_super = nil)
      #   result.should be_an_instance_of(Actor)
      #   result.should == @sub_actor
      # end

      it "should remove the association via the first class" do
        @super_actor.remove_actor_sub(@sub_actor)#.save
        @sub_actor.actor_super.should be_nil
        @super_actor.actor_subs.should_not include(@sub_actor)
        @super_actor.actor_subs.should include(@system)
        @super_actor.remove_actor_sub(@system)#.save
        @system.actor_super.should be_nil
        @super_actor.actor_subs.should be_empty
      end

      it "should remove the association via the second class" do
        @sub_actor.actor_super = nil
        @sub_actor.actor_super.should be_nil
        @super_actor.actor_subs.should_not include(@sub_actor)
        @super_actor.actor_subs.should include(@system)
        @system.actor_super = nil
        @system.actor_super.should be_nil
        @super_actor.actor_subs.should be_empty
      end

      it "should remove all associations" do
        @super_actor.remove_all_actor_subs
        @super_actor.actor_subs.should be_empty
        @sub_actor.refresh; @system.refresh
        @sub_actor.actor_super.should be_nil
        @system.actor_super.should be_nil
      end

      it "should allow reassociation of classes after removing their association" do
        @system.actor_super = nil
        @system.actor_super = @sub_actor
        @system#.save
        @system.actor_super.should == @sub_actor
        @sub_actor.actor_subs.should include(@system)
      end

      it "should reject a non-existant assocition" do
        lambda {@super_actor.remove_actor_sub(@domain)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
      end
    end

    context "retrieving the element" do
      before :each do
        Actor.delete; System.delete; Domain.delete
        @super_actor = Actor.create
        @sub_actor = Actor.create
        @system = System.create
        @unused_system = System.create
        @domain = Domain.create # Unrelated class used for exclusion testing

        @super_actor.add_actor_sub(@sub_actor)#.save
        @super_actor.add_actor_sub(@system)#.save
        @super_actor.actor_subs.should include(@system)
        @super_actor.actor_subs.should include(@sub_actor)
        @system.actor_super.should == @super_actor
        @sub_actor.actor_super.should == @super_actor
      end

      it "should return the associated element via the first class" do
        @super_actor.actor_subs.should =~ [@sub_actor, @system]
      end

      it "should return the associated element via the second class" do
        @sub_actor.actor_super.should == @super_actor
        @system.actor_super.should == @super_actor
      end
      
      it "should ignore invalid association data when retrieving the association" do
        # Check that columns are as we expect
        @sub_actor[:actor_super_id].should == @super_actor.id
        @sub_actor[:actor_super_class].should == @super_actor.class.to_s
        @system[:actor_super_id].should == @super_actor.id
        @system[:actor_super_class].should == @super_actor.class.to_s
        
        # Mess up the association's data
        @sub_actor[:actor_super_id] = nil
        @sub_actor.save
        @system[:actor_super_class] = nil
        @system.save
        
        # Check that the associations evaluate to nil/empty
        @sub_actor.actor_super.should be_nil
        @system.actor_super.should be_nil
        @super_actor.actor_subs.should be_empty
      end
      
      it "should be able to count the currently associated and unassociated objects" do
        # Test to-many side
        @super_actor.actor_subs_count.should == 2
        # NOTE for this unassociated count, self is included.
        # If in future, self is not returned, these counts should be decremented
        @super_actor.actor_subs_unassociated_count.should == 2
        
        # Test to-one side
        @sub_actor.actor_super_count.should == 1
        @sub_actor.actor_super_unassociated_count.should == 3
        @system.actor_super_count.should == 1
        @system.actor_super_unassociated_count.should == 3
        @unused_system.actor_super_count.should == 0
        @unused_system.actor_super_unassociated_count.should == 4
      end
    end
  end
  # ============================================================
  # END polymorphic-to-polymorphic many-to-one section
  # ============================================================


  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic many-to-one section
  # ============================================================
  context "polymorphic-to-nonpolymorphic relationship -" do
    context "adding an element" do
      before :each do
        WorkUnit.delete; Step.delete; StepGroup.delete; Scenario.delete
        @wu = WorkUnit.create
        @invocation2 = Invocation.create
        @step = Step.create
        @step_group = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = (@wu.add_step(@step))
      #   result.should be_an_instance_of(Step)
      #   result = @step.method(:unit_of_work=).call(@wu)
      #   #result = (@step.work_unit = @wu)
      #   result.should be_an_instance_of(Step)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end

      it "should set the element for the association via the first class" do
        @step.work_unit = @wu
        @step#.save
        @step.work_unit.should == @wu
        @wu.steps.should include(@step)

        @step_group.work_unit = @wu
        @step_group#.save
        @step_group.work_unit.should == @wu
        @wu.steps.should include(@step)
        @wu.steps.should include(@step_group)
      end

      it "should set the element for the association via the second class" do
        @wu.add_step(@step)#.save
        @wu.steps.should include(@step)
        @step.work_unit.should == @wu

        @wu.add_step(@step_group)#.save
        @wu.steps.should include(@step)
        @wu.steps.should include(@step_group)
        @step_group.work_unit.should == @wu
      end

      it "should reject an element that is not of the polymorphic type" do
        lambda {@step.work_unit = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.work_unit.should be_nil
        lambda {@wu.add_step(@scenario)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @wu.steps.should_not include(@scenario)
        lambda {@step.work_unit = @step_group
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.work_unit.should be_nil
        lambda {@wu.add_step(@wu)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @wu.steps.should_not include(@wu)
      end
    end

    context "removing the association" do
      before :each do
        WorkUnit.delete; Step.delete; StepGroup.delete; Scenario.delete
        @wu = WorkUnit.create
        @step = Step.create
        @step_group = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing

        @step.work_unit = @wu
        @step#.save
        @step.work_unit.should == @wu
        @step_group.work_unit = @wu
        @step_group#.save
        @step_group.work_unit.should == @wu
        @wu.steps.should include(@step)
        @wu.steps.should include(@step_group)    
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = (@step.remove_unit_of_work(@wu))
      #   result.should be_an_instance_of(Step)
      #   result = (@wu.remove_step(@step_group))
      #   result.should be_an_instance_of(StepGroup)
      # end
      
      it "should remove the association via the first class" do
        @step.work_unit = nil
        @step.work_unit.should be_nil
        @wu.steps.should_not include(@step)
        @wu.steps.should include(@step_group)

        @step_group.work_unit = nil
        @step_group.work_unit.should be_nil
        @wu.steps.should_not include(@step_group)
      end

      it "should remove the association via the second class" do
        @wu.remove_step(@step)#.save
        @wu.steps.should_not include(@step)
        @wu.steps.should include(@step_group)
        @step.work_unit.should be_nil

        @wu.remove_step(@step_group)#.save
        @wu.steps.should_not include(@step_group)
        @step_group.work_unit.should be_nil
      end

      it "should allow reassociation if the association is removed" do
        @wu.remove_step(@step)#.save
        @wu.steps.should_not include(@step)
        @wu.steps.should include(@step_group)
        @step.work_unit.should be_nil

        @wu.add_step(@step)#.save
        @wu.steps.should include(@step)
        @step.work_unit.should == @wu
      end
    end

    context "retrieving the element (or unassociated elements)" do
      before :each do
        WorkUnit.delete; Step.delete; StepGroup.delete; Scenario.delete
        @wu          = WorkUnit.create
        @unused_wu   = WorkUnit.create
        @step        = Step.create
        @unused_step = Step.create
        @step_group  = StepGroup.create
        @scenario    = Scenario.create # Unrelated class for exclusion testing
        
        @step.work_unit = @wu
        @step.work_unit.should == @wu
        @step_group.work_unit = @wu
        @step_group.work_unit.should == @wu
      end
      
      it "should return the associated element via the first class" do
        @step.work_unit.should == @wu
        @step_group.work_unit.should == @wu
      end
      
      it "should return the associated element via the second class" do
        @wu.steps.should =~ [@step, @step_group]
      end
      
      it "should ignore invalid association data when retrieving the association" do
        # Check that columns are as we expect
        @step[:work_unit_id].should == @wu.id
        
        # Mess up the association's data
        @step[:work_unit_id] = nil
        @step.save
        
        # Check that the associations evaluate to nil/empty
        @step.work_unit.should be_nil
        @step_group.work_unit.should == @wu
        @wu.steps.should =~ [@step_group]
      end

      it "should return an array of all unassociated elements via the first class" do
        @step.work_unit_unassociated.should =~ [@unused_wu]
        @step_group.work_unit_unassociated.should =~ [@unused_wu]
      end
      
      it "should return an array of all unassociated elements via the second class" do
        @wu.steps_unassociated.should =~ [@unused_step]
      end
    end
  end
  # ============================================================
  # END polymorphic-to-nonpolymorphic many-to-one section
  # ============================================================


  # ============================================================
  # BEGIN nonpolymorphic-to-polymorphic many-to-one section
  # ============================================================
  context "nonpolymorphic-to-polymorphic relationship -" do
    context "adding an element" do
      before :each do
        reset_db
        @query = Query.create
        @query2 = Query.create
        @step = Step.create
        @step_group = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = (@step.add_query_effect(@query))
      #   result.should be_an_instance_of(Query)
      #   result = @query.method(:step_effect=).call(@step)
      #   #result = (@query.step_effect = @step)
      #   result.should be_an_instance_of(Query)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end

      it "should set the element for the association via the first class" do
        @query.step_effect = @step
        @query#.save
        @query.step_effect.should == @step
        @step.query_effects.should include(@query)
      
        @query2.step_effect = @step_group
        @query2#.save
        @query2.step_effect.should == @step_group
        @step_group.query_effects.should include(@query2)
      end

      it "should set the element for the association via the second class" do
        @step.add_query_effect(@query)#.save
        @step.add_query_effect(@query2)#.save
      
        @step.query_effects.should include(@query)
        @step.query_effects.should include(@query2)

        @query.step_effect.should == @step
        @query2.step_effect.should == @step
      end

      it "should reject an element that is not of the correct type" do
        lambda {@query.step_effect = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @query.step_effect.should be_nil
        lambda {@step.add_query_effect(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.query_effects.should_not include(@scenario)
        lambda {@step_group.add_query_effect(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step_group.query_effects.should_not include(@scenario)
      end
    end

    context "removing the association" do
      before :each do
        reset_db
        @query = Query.create
        @query2 = Query.create
        @step = Step.create
        @step_group = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
        
        @step.add_query_effect(@query)#.save
        @step.add_query_effect(@query2)#.save
        @step.query_effects.should include(@query)
        @step.query_effects.should include(@query2)
        @query.step_effect.should == @step
        @query2.step_effect.should == @step
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = (@step.remove_query_effects(@query))
      #   result.should be_an_instance_of(Query)
      #   result = (@query2.remove_step_effect(@step))
      #   result.should be_an_instance_of(Query)
      # end
      
      it "should remove the association via the first class" do
        @query.step_effect = nil
        @query.step_effect.should be_nil
        @step.query_effects.should_not include(@query)
      end
      
      it "should remove the association via the second class" do
        @step.remove_query_effect(@query)#.save
        @step.query_effects.should_not include(@query)
        @query.step_effect.should be_nil
      end
      
      it "should allow reassociation if the association is removed" do
        @query.step_effect = nil
        @query.method(:step_effect=).call(@step)#.save
        @query.step_effect.should == @step
        @step.query_effects.should include(@query)
      end
    end
    
    context "retrieving the element (or unassociated elements)" do
      before :each do
        reset_db
        @query = Query.create
        @query2 = Query.create
        @unused_query = Query.create
        @step = Step.create
        @step_group = StepGroup.create
        @unused_step = Step.create # Used to test unassociated getter
        @scenario = Scenario.create # Unrelated class for exclusion testing
        
        @extended_scenario = Extension_Scenario.create
        @step.scenario = @scenario
        
        #FINDME Tyler
        
        @step.add_query_effect(@query)#.save
        @step.add_query_effect(@query2)#.save
      end
      
      it "should return the associated element via the first class" do
        @query.step_effect.should == @step
        @query2.step_effect.should == @step
      end
      
      it "should return the associated element via the second class" do
        @step.query_effects.should include(@query)
        @step.query_effects.should include(@query2)
      end

      it "should return an array of all unassociated elements via the first class" do
        @query.step_effect_unassociated.should =~ [@step_group, @unused_step]
        @query2.step_effect_unassociated.should =~ [@step_group, @unused_step]
      end
      
      it "should return an array of all unassociated elements via the second class" do
        @step.query_effects_unassociated.should =~ [@unused_query]
      end
      
      it "should return an array of all unassociated elements via the second class" do
        @step.scenario.should == @scenario
        @step.scenario_unassociated.should =~ [@extended_scenario]
      end
    end
  end

  # ============================================================
  # END nonpolymorphic-to-polymorphic many-to-one section
  # ============================================================


  # ============================================================
  # BEGIN nonpolymorphic-to-nonpolymorphic many-to-one section
  # ============================================================
  context "nonpolymorphic-to-nonpolymorphic relationship -" do
    context "adding an element" do
      before :each do
        User.delete; Sign_Off.delete; Scenario.delete
        @user = User.create
        @sign_off = Sign_Off.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      end
      
      it 'should be able to associate new objects' do
        u = User.new
        s = Sign_Off.new
        u.add_sign_off(s)
        u.sign_offs.should include(s)
        s.user.should == u
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = @user.add_sign_off(@sign_off)
      #   result.should be_an_instance_of(Sign_Off)
      #   result = @sign_off.method(:user=).call(@user)
      #   #result = (@sign_off.user = @user)
      #   result.should be_an_instance_of(Sign_Off)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end
    
      it "should set the element for the association via the first class" do
        @user.add_sign_off(@sign_off)
        # @user.save; @sign_off.save
        @user.sign_offs.should include(@sign_off)
        (@sign_off.user == @user).should == true
      end

      it "should set the element for the association via the second class" do
        @sign_off.user = @user
        # @user.save; @sign_off.save
        @user.sign_offs.should include(@sign_off)
        (@sign_off.user == @user).should == true
      end

      it "should reject an element that is not of the class type" do
        # Scenario is not of type Param or Query
        lambda {@user.add_sign_off(@user)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @user.sign_offs.should be_empty
        lambda {@sign_off.user = @sign_off
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @sign_off.user.should be_nil
        lambda {@user.add_sign_off(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @user.sign_offs.should be_empty
        lambda {@sign_off.user = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @sign_off.user.should be_nil
      end
    end
  
    context "removing an element" do
      before :each do
        User.delete; Sign_Off.delete; Scenario.delete
        @user = User.create
        @sign_off = Sign_Off.create
        @scenario = Scenario.create # Unrelated class for exclusion testing

        @user.add_sign_off(@sign_off)
        # @user.save; @sign_off.save
        @user.sign_offs.should include(@sign_off)
        (@sign_off.user == @user).should == true
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = @user.remove_sign_off(@sign_off).save
      #   result.should be_an_instance_of(Sign_Off)
      #   
      #   #Re-add association
      #   @user.add_sign_off(@sign_off).save
      #   
      #   result = @sign_off.remove_user(@user).save
      #   result.should be_an_instance_of(Sign_Off)
      # end

      it "should remove the association via the first class" do
        @user.remove_sign_off(@sign_off)
        # @user.save; @sign_off.save
        @user.sign_offs.should be_empty
        @sign_off.user.should be_nil
      end
    
      it "should remove the association via the second class" do
        @sign_off.user = nil
        # @user.save; @sign_off.save
        @user.sign_offs.should be_empty
        @sign_off.user.should be_nil
      end
    
      it "should reject a non-existant association" do
        @user.remove_sign_off(@sign_off)
        # @user.save; @sign_off.save
        @user.sign_offs.should be_empty
        @sign_off.user.should be_nil
        lambda {@user.remove_sign_off(@sign_off)
          }.should raise_error(Sequel::Error, /Could not find .* association/i)
        lambda {@user.remove_sign_off(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
      end
    end
  
    context "retrieving the element" do
      before :each do
        User.delete; Sign_Off.delete; Scenario.delete
        @user = User.create
        @sign_off = Sign_Off.create
        @scenario = Scenario.create # Unrelated class for exclusion testing

        @user.add_sign_off(@sign_off)
        # @user.save; @sign_off.save
        @user.sign_offs.should include(@sign_off)
        (@sign_off.user == @user).should == true
      end

      it "should return the associated elements via the first class" do
        @user.sign_offs.should include(@sign_off)
      end

      it "should return the associated element via the second class" do
        (@sign_off.user == @user).should == true
      end
    end
  end
  # ============================================================
  # END nonpolymorphic-to-nonpolymorphic many-to-one section
  # ============================================================

  # ============================================================
  # BEGIN non-composition many-to-one section
  # ============================================================
  context "non-composition relationship -" do
    # Check for specific bug causing this scenario to break
    context "retrieving unassociated elements" do
      before :each do
        reset_db
        ChangeTracker.start
        @car = Automotive::Car.create
        @rs1 = Automotive::RepairShop.create
        @rs2 = Automotive::RepairShop.create
        ChangeTracker.commit
      end
      
      it "should retrieve unassociated elements when there is no current association" do
        @car.being_repaired_by_unassociated.should =~ [@rs1, @rs2]
        @car.being_repaired_by_unassociated_count.should == 2
      end
      
      it "should retrieve unassociated elements when there is a current association" do
        ChangeTracker.start
        @car.being_repaired_by = @rs1
        ChangeTracker.commit
        @car.being_repaired_by_unassociated.should =~ [@rs2]
        @car.being_repaired_by_unassociated_count.should == 1
      end
      
      it "should retrieve unassociated elements when the object is not yet saved" do
        car2 = Automotive::Car.new
        car2.being_repaired_by_unassociated.should =~ [@rs1, @rs2]
        car2.being_repaired_by_unassociated_count.should == 2
      end
    end
  end
  # ============================================================
  # END non-composition many-to-one section
  # ============================================================
  
  # ============================================================
  # BEGIN non-composition one-to-many section
  # ============================================================
  context "non-composition relationship -" do
    # Check for specific bug causing this scenario to break
    context "retrieving unassociated elements" do
      before :each do
        reset_db
        ChangeTracker.start
        @o = People::Ownership.create
        @c1 = Automotive::Component.create
        @c2 = Automotive::Component.create
        ChangeTracker.commit
      end
      
      it "should retrieve unassociated elements when there is no current association" do
        @o.purchased_components_unassociated.should =~ [@c1, @c2]
        @o.purchased_components_unassociated_count.should == 2
      end
      
      it "should retrieve unassociated elements when there is a current association" do
        ChangeTracker.start
        @c1.ownership = @o
        ChangeTracker.commit
        @o.purchased_components_unassociated.should =~ [@c2]
        @o.purchased_components_unassociated_count.should == 1
      end
      
      it "should retrieve unassociated elements when the object is not yet saved" do
        o2 = People::Ownership.new
        o2.purchased_components_unassociated.should =~ [@c1, @c2]
        o2.purchased_components_unassociated_count.should == 2
      end
    end
  end
  # ============================================================
  # END non-composition one-to-many section
  # ============================================================
end
