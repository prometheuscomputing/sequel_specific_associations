require 'spec_helper'

describe "domain object with derived attributes" do
  it "should return correct information about all of the Model's derived attributes and their types" do
    attributes = People::Person.attributes
    # Test derived association
    attributes.keys.should include(:work_performance)
    work_performance_attr_info = attributes[:work_performance]
    work_performance_attr_info[:derived].should == true
    work_performance_attr_info[:column].to_s.should == "work_performance"
    work_performance_attr_info[:getter].to_s.should == "work_performance"
    work_performance_attr_info[:class].should == 'Gui_Builder_Profile::File'
  end
  
  it "should not automatically define methods for derived attributes (left to the user)" do
    People::Person.derived_attribute(:has_children, TrueClass)
    People::Person.attributes.should include(:has_children)
    p = People::Person.new
    p.should_not respond_to(:has_children)
    p.should_not respond_to(:has_children=)
    
    # Cleanup by removing derived attr
    People::Person.remove_immediate_property(:has_children)
  end
end
