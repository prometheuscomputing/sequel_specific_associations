module SequelDateTimePatch
  # typecast_value_datetime does not like a java.util.Date object, so we need
  # to convert it before we get there.  DB should extend this module so that it
  # goes here before it reaches Datebase.typecast_value_datetime
  def typecast_value_datetime(value)
    if value.kind_of?(java::util::Date)
      super DateTime.parse(value.to_s)
    else
      super
    end
  end
end

module SequelDatasetPatch
  # Split out from fetch rows to allow processing of JDBC result sets
  # that don't come from issuing an SQL string.
  def process_result_set(result)
    # get column names
    meta = result.getMetaData
    cols = []
    i = 0
    meta.getColumnCount.times{cols << [output_identifier(meta.getColumnLabel(i+=1)), i]}
    @columns = cols.map { |c| c.at(0)}
    row = {}
    blk = if @convert_types
      lambda{ |n, i| 
        table = meta.getTableName i
        unless table.nil? || table.to_s.empty? || table.to_s == "sqlite_master"
          col = n
          # puts DB.schema(table).inspect
          column_info = DB.schema(table).select { |c| c[0] == col }.first[1]
          column_type = column_info[:type]
        end
      
        if defined?(column_type) && column_type == :datetime
          obj = result.getObject(i)
          row[n] = obj.nil? ? nil : Time.parse(obj)
        else
          row[n] = convert_type(result.getObject(i))
        end
      }
    else
      lambda{ |n, i| row[n] = result.getObject(i)}
    end
    # get rows
    while result.next
      row = {}
      cols.each(&blk)
      yield row
    end
  end
end

DB.extend(SequelDateTimePatch) if defined?(Java)
Sequel::JDBC::SQLite::Dataset.send(:include, SequelDatasetPatch) if defined?(Java)

module SequelModifications
  module InstanceMethods
    if defined?(Java)
      def after_initialize
        super
        fix_java_datetimes
      end # End after_initialize hook
      
      def refresh
        return_val = super
        fix_java_datetimes
        return_val
      end
      
      private
      def fix_java_datetimes
        db_schema.each do |column, info_hash|
          next unless info_hash[:type] == :datetime
          val = self[column]
          if val && ([String, Java::java.lang.String].any?{ |k| val.kind_of?(k)})
            format = Java::java.text.SimpleDateFormat.new("yyyy-MM-dd HH:mm:ss.SSSSSS")
            new_value = format.parse(val)
            self[column] = new_value if new_value
          end
        end
      end
    end # End if defined?(Java)
  end # End InstanceMethods module
end

class Time
  # Extend the comparision operator so we can compare Java Date objects to
  # Ruby Time objects
  alias_method :orig_compare, :<=> unless public_method_defined?(:orig_compare)
  def <=> other
    defined?(Java) && other.kind_of?(java.util.Date) && \
      other = Time.at(other.time/1000)
    orig_compare other
  end
end

class Sequel::Model
  plugin SequelModifications
  # plugin :timestamps # must add to each column individually
end
module UseCaseMetaModelV4
  # ============= Begin class WorkUnit =============
  class WorkUnit < Sequel::Model
    set_dataset :work_units
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    one_to_many :Step
  end
  # ============= End class WorkUnit =============

  # ============= Begin class Delta =============
  class Delta < Sequel::Model
    set_dataset :deltas
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :Predicate, :one_to_one => true, :getter => :predicate_guard
    many_to_one :Extension_Scenario
    many_to_one :Step, :getter => :step_resuming_step
    many_to_one :Step, :getter => :step_starting_step
    many_to_many :Step, :through => :Delta_Step_Replacements, :opp_getter => :delta_replacements, :getter => :step_replacements
  end
  # == Begin associated classes for class Delta ==
      
  class Delta_Step_Replacements < Sequel::Model
    set_dataset :delta_step_replacements
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    associates :Delta => :delta_replacement; associates :Step => :step_replacement
  end
  # == End associated classes for class Delta ==
  # ============= End class Delta =============

  # ============= Begin class User =============
  class User < Sequel::Model
    set_dataset :users
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    one_to_many :Sign_Off
    one_to_many :UseCase, :opp_getter => :user_champion, :getter => :usecase_champion
  end
  # ============= End class User =============

  # ============= Begin class UC_Component =============
  class UC_Component < Sequel::Model
    set_dataset :uc_components
    plugin :boolean_readers
    plugin :specific_associations
    abstract!
    #plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :getter => :description
    one_to_many :Assumption
    one_to_many :Issue
    one_to_many :NonFunctionalRequirement
    one_to_many :Business_Rule
    many_to_many :Reference, :through => :Reference_UC_Component_Association
    many_to_many :Category, :through => :Category_UC_Component_Association
    many_to_many :Actor, :through => :Stakeholder, :opp_getter => :uc_component_stakeholder, :getter => :actor_stakeholder
  end

  # == Begin associated classes for class UC_Component ==
  class Reference_UC_Component_Association < Sequel::Model
    set_dataset :reference_uc_component_associations
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    associates :Reference; associates :UC_Component
  end

  class Category_UC_Component_Association < Sequel::Model
    set_dataset :category_uc_component_associations
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    associates :Category; associates :UC_Component
  end

  class Stakeholder < Sequel::Model
    set_dataset :stakeholders
    plugin :boolean_readers
    plugin :specific_associations
    association_class!
    #plugin :change_tracker
    associates :Actor => :actor_stakeholder; associates :UC_Component => :uc_component_stakeholder
    many_to_one :RichText, :one_to_one => true, :getter => :interest
  end
  # == End associated classes for class UC_Component ==
  # ============= End class UC_Component =============

  # ============= Begin class Named =============
  class Named < UC_Component
    set_dataset :nameds
    attribute :name, String
    abstract!
  end
  # ============= End class Named =============

  # ============= Begin class Assumption =============
  class Assumption < UC_Component
    set_dataset :assumptions
    many_to_one :UC_Component
  end
  # ============= End class Assumption =============

  # ============= Begin class Codeable =============
  class Codeable < Named
    set_dataset :codeables
    many_to_one :Code, :one_to_one => true
  end
  # ============= End class Codeable =============

  # ============= Begin class Constrained =============
  class Constrained < Named
    set_dataset :constraineds
    abstract!
    many_to_one :Algorithm, :one_to_one => true
    one_to_many :Param
    many_to_many :Constraint, :through => :Constrained_Constraint_Constraints, :opp_getter => :constrained_constraints, :getter => :constraint_constraints
    many_to_many :Actor, :through => :Goal, :opp_getter => :constrained_goals, :getter => :actor_goals
  end

  # == Begin associated classes for class Constrained ==
  class Constrained_Constraint_Constraints < Sequel::Model
    set_dataset :constrained_constraint_constraints
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    associates :Constrained => :constrained_constraint; associates :Constraint => :constraint_constraint
  end

  class Goal < Sequel::Model
    set_dataset :goals
    plugin :boolean_readers
    plugin :specific_associations
    association_class!
    #plugin :change_tracker
    associates :Actor => :actor_goal; associates :Constrained => :constrained_goal
    many_to_one :RichText, :one_to_one => true, :getter => :description
  end
  # == End associated classes for class Constrained ==
  # ============= End class Constrained =============

  # ============= Begin class Constraint =============
  class Constraint < Codeable
    set_dataset :constraints
    abstract!
    many_to_many :Constrained, :through => :Constrained_Constraint_Constraints, :opp_getter => :constraint_constraints, :getter => :constrained_constraints
  end
  # ============= End class Constraint =============

  # ============= Begin class Scenario =============
  class Scenario < Constrained
    set_dataset :scenarios
    many_to_one :UseCase
    one_to_many :Step
  end
  # ============= End class Scenario =============

  # ============= Begin class RichText =============
  class RichText < Sequel::Model
    set_dataset :ucmm_rich_texts
    plugin :boolean_readers
    plugin :specific_associations
    attribute :content, String
    attribute :markup_language, String
    #plugin :change_tracker
  end
  # ============= End class RichText =============

  # ============= Begin class Clause_Holder =============
  class Clause_Holder < Constraint
    set_dataset :clause_holders
    abstract!
    one_to_many :Clause
  end
  # ============= End class Clause_Holder =============

  # ============= Begin class Code =============
  class Code < Sequel::Model
    set_dataset :ucmm_codes
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :getter => :code
    one_to_many :Codeable, :one_to_one => true, :getter => :codeable
  end
  # ============= End class Code =============

  # ============= Begin class Actor =============
  class Actor < Named
    set_dataset :actors
    many_to_one :Actor, :opp_getter => :actor_subs, :getter => :actor_super, :foo => :bar
    one_to_many :Actor, :opp_getter => :actor_super, :getter => :actor_subs
    one_to_many :Step, :opp_getter => :actor_actor, :getter => :step_actor
    one_to_many :Regulation, :opp_getter => :actor_agency, :getter => :regulation_agency
    one_to_many :Contract, :opp_getter => :actor_to, :getter => :contract_to
    one_to_many :Contract, :opp_getter => :actor_from, :getter => :contract_from
    many_to_many :Constrained, :through => :Goal, :opp_getter => :actor_goals, :getter => :constrained_goals
    many_to_many :UC_Component, :through => :Stakeholder, :opp_getter => :actor_stakeholder, :getter => :uc_component_stakeholder
  end
  # ============= End class Actor =============

  # ============= Begin class Issue =============
  class Issue < UC_Component
    set_dataset :issues
    many_to_one :RichText, :one_to_one => true, :getter => :workarounds
    many_to_one :UC_Component
  end
  # ============= End class Issue =============

  # ============= Begin class Contract =============
  class Contract < Clause_Holder
    set_dataset :contracts
    many_to_one :Actor, :getter => :actor_to
    many_to_one :Actor, :getter => :actor_from
  end
  # ============= End class Contract =============

  # ============= Begin class Invocation =============
  class Invocation < Sequel::Model
    set_dataset :invocations
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :Param_Use, :one_to_one => true
    one_to_many :Step, :one_to_one => true, :getter => :step
    many_to_one :UseCase, :getter => :usecase_provider
  end
  # ============= End class Invocation =============

  # ============= Begin class Param =============
  class Param < Sequel::Model
    set_dataset :params
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :Query, :one_to_one => true, :getter => :query_type
    many_to_one :Constrained
    many_to_many :Query, :through => :Param_Use, :opp_getter => :param_param_use, :getter => :query_param_use
  end

  # == Begin associated classes for class Param ==
  class Param_Use < Sequel::Model
    set_dataset :param_uses
    plugin :boolean_readers
    plugin :specific_associations
    association_class!
    #plugin :change_tracker
    associates :Param => :param_param_use; associates :Query => :query_param_use
    one_to_many :Invocation, :one_to_one => true, :getter => :invocation
  end
  # == End associated classes for class Param ==
  # ============= End class Param =============

  # ============= Begin class Business_Rule =============
  class Business_Rule < Codeable
    set_dataset :business_rules
    many_to_one :UC_Component
  end
  # ============= End class Business_Rule =============

  # ============= Begin class Extension_Scenario =============
  class Extension_Scenario < Scenario
    set_dataset :extension_scenarios
    one_to_many :Delta
  end
  # ============= End class Extension_Scenario =============

  # ============= Begin class Clause =============
  class Clause < Codeable
    set_dataset :clauses
    many_to_one :Clause_Holder
  end
  # ============= End class Clause =============

  # ============= Begin class Ontology =============
  class Ontology < Sequel::Model
    set_dataset :ontologies
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    one_to_many :Domain
    one_to_many :Definition, :opp_getter => :ontology_glossary, :getter => :definition_glossary
  end
  # ============= End class Ontology =============

  # ============= Begin class Category =============
  class Category < Sequel::Model
    set_dataset :categories
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :getter => :description
    many_to_one :Category, :getter => :category_parent
    one_to_many :Category, :opp_getter => :category_parent, :getter => :category_subcat
    many_to_many :UC_Component, :through => :Category_UC_Component_Association
  end
  # ============= End class Category =============

  # ============= Begin class Definition =============
  class Definition < Sequel::Model
    set_dataset :definitions
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :RichText, :one_to_one => true, :getter => :definition
    many_to_one :Ontology, :getter => :ontology_glossary
  end
  # ============= End class Definition =============

  # ============= Begin class Step =============
  class Step < Constrained
    set_dataset :steps
    many_to_one :Invocation, :one_to_one => true
    many_to_one :WorkUnit
    many_to_one :Scenario
    many_to_one :Actor, :getter => :actor_actor
    many_to_one :StepGroup, :getter => :steps_substep_for
    many_to_one :Step, :getter => :step_variant_for
    one_to_many :Query, :opp_getter => :step_effect, :getter => :query_effects
    one_to_many :Delta, :opp_getter => :step_resuming_step, :getter => :delta_resuming_step
    one_to_many :Delta, :opp_getter => :step_starting_step, :getter => :delta_starting_step
    one_to_many :Step, :opp_getter => :step_variant_for, :getter => :step_variants
    many_to_many :Delta, :through => :Delta_Step_Replacements, :opp_getter => :step_replacements, :getter => :delta_replacements
  end
  # ============= End class Step =============

  # ============= Begin class Predicate =============
  class Predicate < Constraint
    set_dataset :predicates
    one_to_many :Delta, :one_to_one => true, :opp_getter => :predicate_guard, :getter => :delta_guard
  end
  # ============= End class Predicate =============

  # ============= Begin class StepGroup =============
  class StepGroup < Step
    set_dataset :step_groups
    one_to_many :Step, :opp_getter => :steps_substep_for, :getter => :step_substeps
  end
  # ============= End class StepGroup =============

  # ============= Begin class NonFunctionalRequirement =============
  class NonFunctionalRequirement < UC_Component
    set_dataset :non_functional_requirements
    many_to_one :UC_Component
  end
  # ============= End class NonFunctionalRequirement =============

  # ============= Begin class Domain =============
  class Domain < Constrained
    set_dataset :domains
    many_to_one :Ontology
    many_to_many :System, :through => :Domain_System_Association
  end
  # == Begin associated classes for class Domain ==
  class Domain_System_Association < Sequel::Model
    set_dataset :domain_system_associations
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    associates :Domain; associates :System
  end
  # == End associated classes for class Domain ==

  # ============= End class Domain =============

  # ============= Begin class Reference =============
  class Reference < UC_Component
    set_dataset :references
    many_to_many :UC_Component, :through => :Reference_UC_Component_Association
  end
  # ============= End class Reference =============

  # ============= Begin class Query =============
  class Query < Codeable
    set_dataset :queries
    one_to_many :Param, :one_to_one => true, :opp_getter => :query_type, :getter => :param_type
    many_to_one :Step, :getter => :step_effect
    many_to_many :Param, :through => :Param_Use, :opp_getter => :query_param_use, :getter => :param_param_use
  end
  # ============= End class Query =============

  # ============= Begin class System =============
  class System < Actor
    set_dataset :systems
    one_to_many :UseCase
    many_to_many :Domain, :through => :Domain_System_Association
  end
  # ============= End class System =============

  # ============= Begin class Algorithm =============
  class Algorithm < Codeable
    set_dataset :algorithms
    one_to_many :Constrained, :one_to_one => true, :getter => :constrained
  end
  # ============= End class Algorithm =============

  # ============= Begin class Regulation =============
  class Regulation < Clause_Holder
    set_dataset :regulations
    many_to_one :Actor, :getter => :actor_agency
  end
  # ============= End class Regulation =============

  # ============= Begin class UseCase =============
  class UseCase < Constrained
    set_dataset :use_cases
    many_to_one :RichText, :one_to_one => true, :getter => :narrative
    many_to_one :System
    many_to_one :UseCase, :getter => :usecase_base
    many_to_one :User, :getter => :user_champion
    many_to_one :UseCase, :getter => :usecase_general
    one_to_many :Invocation, :opp_getter => :usecase_provider, :getter => :invocation_provider
    one_to_many :Scenario
    one_to_many :UseCase, :opp_getter => :usecase_base, :getter => :usecase_extensions
    one_to_many :Sign_Off
    one_to_many :UseCase, :opp_getter => :usecase_general, :getter => :usecase_specializations
  end
  # ============= End class UseCase =============

  # ============= Begin class Sign_Off =============
  class Sign_Off < Sequel::Model
    set_dataset :sign_offs
    plugin :boolean_readers
    plugin :specific_associations
    #plugin :change_tracker
    many_to_one :User
    many_to_one :UseCase
  end
  # ============= End class Sign_Off =============
end