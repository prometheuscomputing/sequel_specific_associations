module College
  class Dormitory < Sequel::Model
    set_dataset :dormitories
    plugin :specific_associations
    attribute :address, String
    one_to_many :Student, :opp_getter => :dorm, :getter => :occupants
  end

  # Association classes Contact and Suitemate
  class Contact < Sequel::Model
    set_dataset :contacts
    plugin :specific_associations
    associates :Student => :contact; associates :Student => :contact_of
  end
  class Suitemate < Sequel::Model
    set_dataset :suitemates
    plugin :specific_associations
    associates :Student => :suitemate; associates :Student => :suitemate_of
  end

  class Student < Sequel::Model
    set_dataset :students
    plugin :specific_associations
    attribute :name, String
    many_to_many :Student, :through => :Contact, :opp_getter => :contact_of, :getter => :contacts
    many_to_many :Student, :through => :Contact, :opp_getter => :contacts, :getter => :contact_of
    many_to_one :Dormitory, :opp_getter => :occupants, :getter => :dorm
    # Suitemates must first be added to both 'contacts' and 'dorm.occupants' associations
    # Note that suitemate_of is a potentially ambiguous role name -- this is a many-to-many so it might have been clearer if it would have been suitemates_of or suitemate_ofs or something (even though they both sound stupid)
    many_to_many :Student, :through => :Suitemate, :opp_getter => :suitemate_of, :getter => :suitemates, :subset_of => 'dorm.occupants'
    many_to_many :Student, :through => :Suitemate, :opp_getter => :suitemates, :getter => :suitemate_of, :subset_of => 'dorm.occupants'
    many_to_many :TeachersAssistant, :through => :TeachesStudent, :opp_getter => :students, :getter => :taught_by
    one_to_many :TeachersAssistant, :opp_getter => :favorite_student, :getter => :favorite_of
  end

  # Association class TeachesStudent
  class TeachesStudent < Sequel::Model
    set_dataset :teaches_student
    plugin :specific_associations
    associates :Student => :student; associates :TeachersAssistant => :taught_by
  end

  class TeachersAssistant < Student
    set_dataset :teachers_assistants
    plugin :specific_associations
    attribute :stipend, SpecificAssociations::INT_TYPE
    # Taught students must first be added to 'contacts' association
    many_to_many :Student, :through => :TeachesStudent, :opp_getter => :taught_by, :getter => :students, :subset_of => 'contacts'
    many_to_one :Student, :opp_getter => :favorite_of, :getter => :favorite_student, :subset_of => 'students'
  end

  Dormitory.create_schema
  Contact.create_schema
  Suitemate.create_schema
  Student.create_schema
  TeachesStudent.create_schema
  TeachersAssistant.create_schema
end