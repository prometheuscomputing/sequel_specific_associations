require 'spec_helper'

describe SpecificAssociations, " when returning a hash of associations which includes join entries when possible" do
  context "for a many-to-many" do
    before :each do
      Goal.delete; Actor.delete; System.delete; UseCase.delete;
      @actor = Actor.create
      @system = System.create
      @usecase = UseCase.create
      @actor.add_constrained_goal(@usecase)
      @system.add_constrained_goal(@usecase)
    end
    
    it "should return a hash of associated objects with :through and :to keys" do
      @actor.constrained_goals_associations.length.should == @actor.constrained_goals.length
      @actor.constrained_goals_associations.length.should == 1
      actor_usecase_association = @actor.constrained_goals_associations.first
      actor_usecase_association.keys.should =~ [:through, :to]
      actor_usecase_association[:to].should == @usecase
      actor_usecase_goal = actor_usecase_association[:through]
      actor_usecase_goal.should be_a(Goal)
      actor_usecase_goal.actor_goal.should == @actor
      actor_usecase_goal.constrained_goal.should == @usecase
      @usecase.actor_goals_associations.length.should == @usecase.actor_goals.length
      @usecase.actor_goals_associations.length.should == 2
      @usecase.actor_goals_associations.collect { |a| a[:to]}.should =~ [@actor, @system]
      usecase_actor_goal = @usecase.actor_goals_associations.select { |a| a[:to] == @actor }.first[:through]
      usecase_actor_goal.actor_goal.should == @actor
      usecase_actor_goal.constrained_goal.should == @usecase
      usecase_system_goal = @usecase.actor_goals_associations.select { |a| a[:to] == @system }.first[:through]
      usecase_system_goal.actor_goal.should == @system
      usecase_system_goal.constrained_goal.should == @usecase
    end
    
    it "should respond to limit and offset controls" do
      @usecase.actor_goals_associations({}, 1, 0).length.should == 1
      @usecase.actor_goals_associations({}, 1, 1).length.should == 1
      @usecase.actor_goals_associations({}, 1, 0).should_not == @usecase.actor_goals_associations({}, 1, 1)
    end
  end
end