require 'spec_helper'

describe "domain object when filtering associations" do
  context "when filtering associations with ChangeTracker" do
    before(:each) do
      reset_db
      ChangeTracker.start
      # Setting up many-to-many polymorphic
      @person1 = People::Person.create(:name => "Chris")
      @person2 = People::Person.create(:name => "John")
      @person3 = Automotive::Mechanic.create(:name => "Kyle")
      @car1 = Automotive::Car.create(:vehicle_model => 'Thunderbird', :cost => 5000)
      @car1.make = 'Ford'
      @car2 = Automotive::Motorcycle.create(:vehicle_model => 'Rebel', :cost => 4000)
      @car2.make = 'Honda'
      @car3 = Automotive::Car.create(:vehicle_model => 'Taurus', :cost => 9000)
      @car3.make = 'Ford'
      @person1.add_drife(@car1)
      @person1.add_drife(@car2)
      @person1.add_drife(@car3)

      @person2.add_drife(@car2)
      @person2.add_drife(@car3)

      # Polymorphic to polymorphic relation 
      @person3.add_drife(@car2)
      @repair_shop = Automotive::RepairShop.create(:name => 'RepairShop')
      @car1.being_repaired_by = @repair_shop
      @car2.being_repaired_by = @repair_shop
      @car3.being_repaired_by = @repair_shop
      ChangeTracker.commit
    end

    it "should be able to count filtered objects in a polymorphic many-to-many" do
      @person1.drives_count({'type' => Automotive::Motorcycle}).should == 1
      @person1.drives_count({'type' => Automotive::Car}).should == 2
      @person1.drives_count({:cost => [5000, 4000]}).should == 2
      @person1.drives_count.should == 3
    end

    it "should be able to count filtered objects in a one-to-many polymorphic" do
      @repair_shop.currently_working_on_count.should == 3
      @repair_shop.currently_working_on_count([{:cost => [5000, 4000]}]).should == 2
      @repair_shop.currently_working_on_count([{:cost => [5000, 9000]}, {:vehicle_model => 'Taurus'}]).should == 1
      @repair_shop.currently_working_on_count([{'type' => Automotive::Motorcycle}]).should == 1
    end

    it "should return objects in the correct order with filter when a limit and/or offset is set" do
      # one-to-many
      @repair_shop.currently_working_on({:vehicle_model => "Taurus"}, 1, 0).should == [@car3]
      # one-to-many with type filter
      @repair_shop.currently_working_on({'type' => Automotive::Motorcycle}, 1, 0).should == [@car2]
      # many-to-many
      @person1.drives({:vehicle_model => 'Taurus'}, 1, 0).should == [@car3]
      # many-to-many with type filter
      @person1.drives({:cost => 4000, 'type' => Automotive::Motorcycle}, 1, 0).should == [@car2]
      # many-to-many with range filter and offset
      # The unlimited, non-offset query should result in [@car2, @car3], so an offset of 1 with a limit of 1 should be [@car3]
      @person1.drives({:cost => [4000,9000]}, 1, 1).should == [@car3]
    end

    # it "should be able to filter on a non-existent type" do
    #   @person1.drives('type' => 'NilClass').should == []
    # end

    # it "should be able to count on a non-existent type" do
    #   @repair_shop.currently_working_on_count('type' => 'NilClass').should == 0
    # end
  end

  context "when filtering associations without ChangeTracker" do
    before(:each) do
      reset_db
      # many_to_many type ( Library => PatronStatusAtLibrary <= Patron)
      @library1 = Application::Library.create(:name => "WCU public library")
      @library2 = Application::Library.create(:name => "Sylva public library")

      @patron1 = Application::Patron.create(:first_name => "Bob")
      @patron2 = Application::Patron.create(:first_name => "Sue")
      @patron3 = Application::Patron.create(:first_name => "Bill")
    
      @library1.add_patron(@patron1)
      @library1.add_patron(@patron2)
      @library1.add_patron(@patron3)

      @library2.add_patron(@patron1)
      @library2.add_patron(@patron2)
    
      # one_to_many type ( Library => EBook)
      @book1 = Application::EBook.create(:title => "War and Peace")
      @book2 = Application::EBook.create(:title => "Crime and Punishment")
      @book3 = Application::EBook.create(:title => "The Brothers Karamazov")
      @book4 = Application::Dictionary.create(:title => "Websters")
      @book5 = Application::Dictionary.create(:title => "Oxford")
      @book6 = Application::Dictionary.create(:title => "Russian Dictionary")
    
      @library1.add_book(@book1)
      @library1.add_book(@book2)
      @library1.add_book(@book3)
      @library1.add_book(@book4)
      @library1.add_book(@book5)
      @library1.add_book(@book6)
    end

    it "should be able to count filtered objects in a one-to-many polymorphic" do
      @library1.books_count([{'type' => Application::Dictionary}]).should == 3
      @library1.books_count({:title => 'Russian Dictionary'}).should == 1
    end

    it "should be able to count filtered objects in a many-to-many" do
      # Not polymorphic, but check anyway
      @library1.patrons_count({'type' => Application::Patron}).should == 3
      @library1.patrons_count({:first_name => 'Bob'}).should == 1
      @library1.patrons_count({:first_name => 'No-one'}).should == 0

      @library2.patrons_count({:first_name => 'Bob'}).should == 1

      @patron1.libraries_count({:name => "Sylva public library"}).should == 1
      # Not polymorphic, but check anyway
      @patron1.libraries_count({'type' => Application::Library}).should == 2
    end

    it "should be able to count objects with empty filter" do
      # one-to-many
      @library1.books_count([{}]).should == 6
      # many-to-many
      @library2.patrons_count([]).should == 2
      # Reverse many-to-many
      @patron1.libraries_count({}).should == 2
    end

    it "should return objects in the correct order with filter when a limit and/or offset is set" do
      # one-to-many
      @library1.books({:title => "Websters"}, 1, 0).should == [@book4]
      # one-to-many with type filter
      @library1.books({'type' => Application::Dictionary}, 1, 0).should == [@book4]
      # many-to-many
      @library1.patrons({:first_name => 'Bill'}, 1, 0).should == [@patron3]
      # many-to-many with type filter
      @library1.patrons({:first_name => 'Bill', 'type' => Application::Patron}, 1, 0).should == [@patron3]
      # many-to-many with type filter and offset
      @library1.patrons({'type' => Application::Patron}, 1, 1).should == [@patron2]
    end

    it "should be able to filter on object type" do
      dictionaries = @library1.books({'type' => Application::Dictionary})
      dictionaries.count.should == 3
      dictionaries.should include(@book4)
      dictionaries.should include(@book5)
      dictionaries.should include(@book6)
    
      # Currently setting 'type' EBook excludes subclass 'Dictionary'. This is expected behavior
      regular_books = @library1.books({'type' => Application::EBook})
      regular_books.count.should == 3
      regular_books.should include(@book1)
      regular_books.should include(@book2)
      regular_books.should include(@book3)
    
      all_books = @library1.books({'type' => [Application::Dictionary, Application::EBook]})
      all_books.count.should == 6
    
      # Check no filter
      all_books_without_filter = @library1.books
      all_books_without_filter.count.should == 6
      
      # Check unassoc
      # Simple check to make sure _unassociated is working (no unassociated books for @library1)
      unassociated_dictionaries = @library1.books_unassociated
      unassociated_dictionaries.should be_empty
      
      # Will return a list of all 6 present books
      unassociated_dictionaries = @library2.books_unassociated
      unassociated_dictionaries.count.should == 6
      
      unassociated_dictionaries = @library2.books_unassociated({'type' => Application::Dictionary})
      unassociated_dictionaries.count.should == 3
      unassociated_dictionaries.should include(@book4)
      unassociated_dictionaries.should include(@book5)
      unassociated_dictionaries.should include(@book6)
      
      unassociated_regular_books = @library2.books_unassociated({'type' => Application::EBook})
      unassociated_regular_books.count.should == 3
      unassociated_regular_books.should include(@book1)
      unassociated_regular_books.should include(@book2)
      unassociated_regular_books.should include(@book3)
    end
  
    it "should be able to filter on other columns at the same time in a variety of formats" do
      dictionaries = @library1.books(:title => 'Oxford', 'type' => Application::Dictionary)
      dictionaries.should == [@book5]

      dictionaries = @library1.books({:title => 'Oxford', 'type' => Application::Dictionary})
      dictionaries.should == [@book5]
      
      dictionaries = @library1.books([Sequel.ilike(:title, '%ford'), 'type' => Application::Dictionary])
      dictionaries.should == [@book5]
      
      dictionaries = @library1.books([Sequel.ilike(:title, '%ford'), {'type' => Application::Dictionary, :title => 'Oxford'}, {:title => 'Oxford'}])
      dictionaries.should == [@book5]
    end
    
    it "should be able to filter on a range of values for a column including nil" do
      @book7 = Application::Dictionary.create(:title => nil)
      @library1.add_book(@book7)
      
      dictionaries = @library1.books(:title => ['Russian Dictionary', nil])
      dictionaries.count.should == 2
      dictionaries.should include(@book6)
      dictionaries.should include(@book7)
    end

    # it "should be able to filter on a non-existent type" do
    #   @library1.books('type' => 'NilClass').should == []
    # end

    # it "should be able to count on a non-existent type" do
    #   @library1.patrons_count('type' => 'NilClass').should == 0
    # end
  end

  context "when filtering unassociations with ChangeTracker" do
    before(:each) do
      reset_db
      ChangeTracker.start
      # Setting up many-to-many polymorphic
      @person1 = People::Person.create(:name => "Chris")
      @person2 = People::Person.create(:name => "John")
      @person3 = Automotive::Mechanic.create(:name => "Kyle")
      @car1 = Automotive::Car.create(:vehicle_model => 'Thunderbird', :cost => 5000)
      @car1.make = 'Ford'
      @car2 = Automotive::Motorcycle.create(:vehicle_model => 'Rebel', :cost => 4000)
      @car2.make = 'Honda'
      @car3 = Automotive::Car.create(:vehicle_model => 'Taurus', :cost => 9000)
      @car3.make = 'Ford'
      @person1.add_drife(@car1)
      @person1.add_drife(@car2)
      @person1.add_drife(@car3)

      @person2.add_drife(@car2)
      @person2.add_drife(@car3)

      @unassoc_car1 = Automotive::Car.create(:vehicle_model => 'f150', :cost => 5000)
      @unassoc_car1.make = 'Ford'
      @unassoc_car2 = Automotive::Motorcycle.create(:vehicle_model => 'Trouble', :cost => 4000)
      @unassoc_car2.make = 'Honda'
      @unassoc_car3 = Automotive::Car.create(:vehicle_model => 'f100', :cost => 9000)
      @unassoc_car3.make = 'Ford'
      @unassoc_person1 = People::Person.create(:name => "Jack")
      @unassoc_person2 = People::Person.create(:name => "Phil")
      @unassoc_person3 = Automotive::Mechanic.create(:name => "Uju")

      # Polymorphic to polymorphic relation 
      @person3.add_drife(@car2)
      @repair_shop = Automotive::RepairShop.create(:name => 'RepairShop')
      @car1.being_repaired_by = @repair_shop
      @car2.being_repaired_by = @repair_shop
      @car3.being_repaired_by = @repair_shop
      ChangeTracker.commit
    end

    it "should be able to count filtered objects in a polymorphic many-to-many" do
      @person1.drives_unassociated_count({'type' => @unassoc_car2.class}).should == 1
      @person1.drives_unassociated_count({'type' => Automotive::Car}).should == 2
      @person1.drives_unassociated_count({:vehicle_model => 'f100'}).should == 1
      @person1.drives_unassociated_count.should == 3
    end

    it "should be able to count filtered objects in a one-to-many polymorphic" do
      @repair_shop.currently_working_on_unassociated_count.should == 3
      @repair_shop.currently_working_on_unassociated_count([{:cost => [5000, 9000]}]).should == 2
      @repair_shop.currently_working_on_unassociated_count([{:cost => [5000, 9000]}, {:vehicle_model => 'f150'}]).should == 1
      @repair_shop.currently_working_on_unassociated_count([{'type' => @unassoc_car2.class}]).should == 1
    end

    it "should return objects in the correct order with filter when a limit and/or offset is set" do
      # return empty array when searching for associated vehicle
      @repair_shop.currently_working_on_unassociated({:vehicle_model => "Taurus"}, 1, 0).should == []
      # one-to-many
      @repair_shop.currently_working_on_unassociated({:vehicle_model => "f100"}, 1, 0).should == [@unassoc_car3]
      # one-to-many with type filter
      @repair_shop.currently_working_on_unassociated({'type' => Automotive::Motorcycle}, 1, 0).should == [@unassoc_car2]
      # many-to-many
      @person1.drives_unassociated({:vehicle_model => 'f100'}, 1, 0).should == [@unassoc_car3]
      # many-to-many with type filter
      @person1.drives_unassociated({:cost => 4000, 'type' => Automotive::Motorcycle}, 1, 0).should == [@unassoc_car2]
      # many-to-many with type filter and offset
      @person1.drives_unassociated({:cost => [5000, 9000]}, 1, 1).should == [@unassoc_car3]
    end

    it "should be able to filter on a non-existent type" do
      @person1.drives_unassociated('type' => 'NilClass').should == []
    end

    it "should be able to count on a non-existent type" do
      @repair_shop.currently_working_on_unassociated_count('type' => 'NilClass').should == 0
    end
  end
  
  context "when filtering unassociations without ChangeTracker" do
    before(:each) do
      reset_db
      # many_to_many type ( Library => PatronStatusAtLibrary <= Patron)
      @library1 = Application::Library.create(:name => "WCU public library")
      @library2 = Application::Library.create(:name => "Sylva public library")

      @unassoc_library1 = Application::Library.create(:name => "Waynesville public library")
      @unassoc_library2 = Application::Library.create(:name => "Clyde public library")

      @patron1 = Application::Patron.create(:first_name => "Bob")
      @patron2 = Application::Patron.create(:first_name => "Sue")
      @patron3 = Application::Patron.create(:first_name => "Bill")
    
      @unassoc_patron1 = Application::Patron.create(:first_name => "Jake")
      @unassoc_patron2 = Application::Patron.create(:first_name => "Kyle")
      @unassoc_patron3 = Application::Patron.create(:first_name => "Jade")

      @library1.add_patron(@patron1)
      @library1.add_patron(@patron2)
      @library1.add_patron(@patron3)

      @library2.add_patron(@patron1)
      @library2.add_patron(@patron2)
      @library2.add_patron(@patron3)
    
      # one_to_many type ( Library => EBook)
      @book1 = Application::EBook.create(:title => "War and Peace")
      @book2 = Application::EBook.create(:title => "Crime and Punishment")
      @book3 = Application::EBook.create(:title => "The Brothers Karamazov")
      @book4 = Application::Dictionary.create(:title => "Websters")
      @book5 = Application::Dictionary.create(:title => "Oxford")
      @book6 = Application::Dictionary.create(:title => "Russian Dictionary")

      @unassoc_book1 = Application::EBook.create(:title => "Guiness Book of Work Records")
      @unassoc_book2 = Application::EBook.create(:title => "Treasure Island")
      @unassoc_book3 = Application::EBook.create(:title => "Catch me if you can")
      @unassoc_book4 = Application::Dictionary.create(:title => "Spanish Dictionary")
      @unassoc_book5 = Application::Dictionary.create(:title => "Ukrainian Dictionary")
      @unassoc_book6 = Application::Dictionary.create(:title => "Slovakian Dictionary")
    
      @library1.add_book(@book1)
      @library1.add_book(@book2)
      @library1.add_book(@book3)
      @library1.add_book(@book4)
      @library1.add_book(@book5)
      @library1.add_book(@book6)
    end

    it "should be able to count filtered objects in a one-to-many polymorphic" do
      @library1.books_unassociated_count([{'type' => Application::Dictionary}]).should == 3
      @library1.books_unassociated_count({:title => 'Ukrainian Dictionary'}).should == 1
    end

    it "should be able to count filtered objects in a many-to-many" do
      # Not polymorphic, but check anyway
      @library1.patrons_unassociated_count({'type' => Application::Patron}).should == 3
      @library1.patrons_unassociated_count({:first_name => 'Jake'}).should == 1
      @library1.patrons_unassociated_count({:first_name => 'No-one'}).should == 0

      @library2.patrons_unassociated_count({:first_name => 'Jake'}).should == 1

      @patron1.libraries_unassociated_count({:name => "Waynesville public library"}).should == 1
      # Not polymorphic, but check anyway
      @patron1.libraries_unassociated_count({'type' => Application::Library}).should == 2
    end

    it "should be able to count objects with empty filter" do
      # one-to-many
      @library1.books_unassociated_count([{}]).should == 6
      # many-to-many
      @library2.patrons_unassociated_count([]).should == 3
      # Reverse many-to-many
      @patron1.libraries_unassociated_count({}).should == 2
    end


    it "should return objects in the correct order with filter when a limit and/or offset is set" do
      # one-to-many
      @library1.books_unassociated({:title => "Spanish Dictionary"}, 1, 0).should == [@unassoc_book4]
      # one-to-many with type filter
      @library1.books_unassociated({'type' => Application::Dictionary}, 1, 0).should == [@unassoc_book4]
      # many-to-many
      @library1.patrons_unassociated({:first_name => 'Jade'}, 1, 0).should == [@unassoc_patron3]
      # many-to-many with type filter
      @library1.patrons_unassociated({:first_name => 'Jade', 'type' => Application::Patron}, 1, 0).should == [@unassoc_patron3]
      # many-to-many with type filter and offset
      @library1.patrons_unassociated({'type' => Application::Patron}, 1, 1).should == [@unassoc_patron2]
    end

    it "should be able to filter on object type" do
      dictionaries = @library1.books_unassociated({'type' => Application::Dictionary})
      dictionaries.should =~ [@unassoc_book4, @unassoc_book5, @unassoc_book6]
    
      # Currently setting 'type' EBook excludes subclass 'Dictionary'. This is expected behavior
      regular_books = @library1.books_unassociated({'type' => Application::EBook})
      regular_books.should =~ [@unassoc_book1, @unassoc_book2, @unassoc_book3]
    
      all_books = @library1.books_unassociated({'type' => [Application::Dictionary, Application::EBook]})
      all_books.count.should == 6
    
      # Check no filter
      all_books_without_filter = @library1.books_unassociated
      all_books_without_filter.count.should == 6
      
      # Will return a list of all 6 present books
      unassociated_dictionaries = @library2.books_unassociated
      unassociated_dictionaries.count.should == 12
      
      unassociated_dictionaries = @library2.books_unassociated({'type' => Application::Dictionary})
      unassociated_dictionaries.count.should == 6
      unassociated_dictionaries.should include(@unassoc_book4)
      unassociated_dictionaries.should include(@unassoc_book5)
      unassociated_dictionaries.should include(@unassoc_book6)
      
      unassociated_regular_books = @library2.books_unassociated({'type' => Application::EBook})
      unassociated_regular_books.count.should == 6
      unassociated_regular_books.should include(@unassoc_book1)
      unassociated_regular_books.should include(@unassoc_book2)
      unassociated_regular_books.should include(@unassoc_book3)
    end
  
    it "should be able to filter on other columns at the same time in a variety of formats" do
      dictionaries = @library1.books_unassociated(:title => 'Ukrainian Dictionary', 'type' => Application::Dictionary)
      dictionaries.should == [@unassoc_book5]

      dictionaries = @library1.books_unassociated({:title => 'Ukrainian Dictionary', 'type' => Application::Dictionary})
      dictionaries.should == [@unassoc_book5]
      
      dictionaries = @library1.books_unassociated([Sequel.ilike(:title, '%rainian Dictionary'), 'type' => Application::Dictionary])
      dictionaries.should == [@unassoc_book5]
      
      dictionaries = @library1.books_unassociated([Sequel.ilike(:title, '%rainian Dictionary'), {'type' => Application::Dictionary, :title => 'Ukrainian Dictionary'}, {:title => 'Ukrainian Dictionary'}])
      dictionaries.should == [@unassoc_book5]
    end
    
    it "should be able to filter on a range of values for a column including nil" do
      @unassoc_book7 = Application::Dictionary.create(:title => nil)
      
      dictionaries = @library1.books_unassociated(:title => ['Ukrainian Dictionary', nil])
      dictionaries.count.should == 2
      dictionaries.should include(@unassoc_book5)
      dictionaries.should include(@unassoc_book7)
    end

    it "should be able to filter on a non-existent type" do
      @library1.books_unassociated('type' => 'NilClass').should == []
    end

    it "should be able to count on a non-existent type" do
      @library1.patrons_unassociated_count('type' => 'NilClass').should == 0
    end
  end
  
  context "when filtering on complex attributes" do
    before(:each) do
      reset_db
      @student = ArtClass::Student.create(:name => "Rincewind")
      @pencil  = ArtClass::Pencil.create(:weight => "#2")
      @pencil.thickness = ArtClass::ThicknessRange.new(:min => '400', :max => '500', :units => 'micron')
      @pencil2  = ArtClass::Pencil.create(:weight => "#2")
      @pencil2.thickness = ArtClass::ThicknessRange.new(:min => '400', :max => '500', :units => 'micron')
      @student.add_pencil(@pencil)
      @student.pencils.should == [@pencil]
    end

    it "should be able to filter on the attributes of complex attributes" do
      @student.pencils(:thickness_min => '400').should == [@pencil]
      @student.pencils(Sequel.ilike(:thickness_min, '%0%')).should == [@pencil]
      @student.pencils(:thickness_min => '100').should == []
      @student.pencils(Sequel.ilike(:thickness_min, '%7%')).should == []
      @student.pencils(:thickness => '400').should == []
      @student.pencils(:thickness => true).should == [@pencil]
      
      @student.pencils_unassociated(:thickness_min => '400').should == [@pencil2]
      @student.pencils_unassociated(Sequel.ilike(:thickness_min, '%0%')).should == [@pencil2]
      @student.pencils_unassociated(:thickness_min => '100').should == []
      @student.pencils_unassociated(Sequel.ilike(:thickness_min, '%7%')).should == []
      @student.pencils_unassociated(:thickness => '400').should == []
      @student.pencils_unassociated(:thickness => true).should == [@pencil2]
    end
  end
end