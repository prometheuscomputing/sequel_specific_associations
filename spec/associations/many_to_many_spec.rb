require 'spec_helper'

describe "domain object for a many to many" do
  # ============================================================
  # BEGIN polymorphic-to-polymorphic many-to-many section
  # ============================================================
  context "polymorphic-to-polymorphic relationship -" do
    context "adding an element" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete; Query.delete
        @actor   = Actor.create
        @system  = System.create
        @usecase = UseCase.create
        @domain  = Domain.create
        @query   = Query.create
      end
      #
      # it "should return the model that needs to be saved when adding an association" do
      #   result = (@actor.add_constrained_goal(@usecase))
      #   result.should be_an_instance_of(Goal)
      #   result = (@usecase.add_actor_goal(@actor))
      #   result.should be_an_instance_of(Goal)
      # end
    
      # it "should not commit changes necessary for the association until the model is saved" do
      #   goal = @actor.add_constrained_goal(@usecase)
      #   Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
      #     :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
      #     ].should be_nil
      #   goal.save
      #   Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
      #     :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
      #     ].should_not be_nil
      # end
    
      it "should add the element to the association via the first class" do
        @actor.add_constrained_goal(@usecase)#.save
      
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    
      it "should add the element to the association via the second class" do
        @usecase.add_actor_goal(@actor)#.save
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    
      it "should be able to accept polymorphic relationships on both classes" do
        @system.add_constrained_goal(@usecase)#.save
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    
      it "should reject an element that is not of the polymorphic type" do
        # Query does not inherit from Actor or Constrained
        lambda {@usecase.add_actor_goal @query
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @usecase.actor_goals.should_not include(@query)
        lambda {@actor.add_constrained_goal @query
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @actor.constrained_goals.should_not include(@query)
        lambda {@usecase.add_actor_goal @domain
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @usecase.actor_goals.should_not include(@domain)
        lambda {@actor.add_constrained_goal @system
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @actor.constrained_goals.should_not include(@system)
      end
    end
  
    context "removing an element" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete
        @actor   = Actor.create
        @usecase = UseCase.create
        @system  = System.create
        @domain  = Domain.create
        @actor.add_constrained_goal(@usecase)
        @system.add_constrained_goal(@usecase)
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
=begin      
      it "should return the model that needs to be saved when removing an association" do
        result = @actor.remove_constrained_goal(@usecase)
        result.should be_an_instance_of(Goal)
        result = @usecase.remove_actor_goal(@system)
        result.should be_an_instance_of(Goal)
      end

      it "should not commit changes necessary for the association until the model is saved" do
        goal = @actor.remove_constrained_goal(@usecase)
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
        goal.save
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
      end
=end
      it "should remove the association via the first class" do
        @actor.remove_constrained_goal(@usecase)#.save
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    
      it "should remove the association via the second class" do
        @usecase.remove_actor_goal(@actor)#.save
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    
      it "should reject a non-existant association" do
        lambda {@actor.remove_constrained_goal(@domain)}.should raise_error(/Could not find .* association/i)
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    end
  
    context "removing all elements" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete
        @actor   = Actor.create
        @usecase = UseCase.create
        @system  = System.create
        @domain  = Domain.create
        @actor.add_constrained_goal(@usecase)#.save
        @system.add_constrained_goal(@usecase)#.save
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
  
      it "should remove all elements via the first class" do
        @usecase.remove_all_actor_goals
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
      end
    
      it "should remove all elements via the second class" do
        @actor.remove_all_constrained_goals
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
    end
    
    context "destroying the object" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete
        @actor   = Actor.create
        @usecase = UseCase.create
        @system  = System.create
        @domain  = Domain.create
        @actor.add_constrained_goal(@usecase)#.save
        @system.add_constrained_goal(@usecase)#.save
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
      
      it "should remove all associations to other objects when an object is destroyed via the first object" do
        actor_id = @actor.id
        actor_class = @actor.class.to_s
        @actor.destroy
        # The join table entry should be gone
        Goal[:actor_goal_id => actor_id, :actor_goal_class => actor_class,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should be_nil
        # Unrelated association should not be affected
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ].should_not be_nil
      end
      
      it "should remove all associations to other objects when an object is destroyed via the second object" do
        usecase_id    = @usecase.id
        usecase_class = @usecase.class.to_s
        @usecase.destroy
        # The join table entries should be gone
        Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => usecase_id, :constrained_goal_class => usecase_class
          ].should be_nil
        Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => usecase_id, :constrained_goal_class => usecase_class
          ].should be_nil
      end
    end
  
    context "listing all elements (associated or unassociated)" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete
        @actor   = Actor.create(:name => 'Actor')
        @usecase = UseCase.create(:name => 'UseCase')
        @system  = System.create(:name => 'System')
        @actor.add_constrained_goal(@usecase)
        @system.add_constrained_goal(@usecase)
        # Add unassociated objects
        @unassoc_actor1  = Actor.create(:name => 'Sally')
        @unassoc_actor2  = Actor.create(:name => 'Bob')
        @unassoc_system  = System.create
        @unassoc_usecase = UseCase.create
        @g1 = Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ]
        @g1.should_not be_nil
        @g2 = Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ]
        @g2.should_not be_nil
        # Add a description for @g1
        @g1_desc = UseCaseMetaModelV4::RichText.create(:content => "G1/Actor description", :markup_language => 'text')
        @g1.description = @g1_desc
      end
  
      it "should return an array of all elements associated via the first class" do
        @usecase.actor_goals.should =~ [@actor, @system]
        @usecase.actor_goals_query({}, nil, 0, [], {}, :through).all.should =~ [@g1, @g2]
        @usecase.actor_goals_query({}, nil, 0, [], {}, :both).all.should =~ [{:to => @actor, :through => @g1}, {:to => @system, :through => @g2}]
        @usecase.actor_goals_query(:name => 'Actor').all.should =~ [@g1]
        @usecase.actor_goals_query({:name => 'Actor'}, nil, 0, [], {}, :to).all.should =~ [@actor]
        @usecase.actor_goals_query(:name => 'System').all.should =~ [@g2]
        @usecase.actor_goals_query({:name => 'System'}, nil, 0, [], {}, :to).all.should =~ [@system]
        @usecase.actor_goals_query(:name => 'Invalid').all.should =~ []
        # NOTE: currently there is no way to filter on join table items
        # @usecase.actor_goals_query(:description_id => @g1_desc.id).all.should =~ [@g1]
      end
      
      it "should return an array of all elements associated via the second class" do
        @actor.constrained_goals.should =~ [@usecase]
        @system.constrained_goals.should =~ [@usecase]
      end
      
      it "should return an array of all unassociated elements via the first class" do
        @usecase.actor_goals_unassociated.should =~ [@unassoc_actor1, @unassoc_actor2, @unassoc_system]
      end

      it "should be able to filter the list of returned unassociations via the first class" do
        @usecase.actor_goals_unassociated.should =~ [@unassoc_actor1, @unassoc_actor2, @unassoc_system]
        @usecase.actor_goals_unassociated_count.should == 3

        @usecase.actor_goals_unassociated('type' => @unassoc_system.class).should =~ [@unassoc_system]
        @usecase.actor_goals_unassociated_count('type' => @unassoc_system.class).should == 1

        @usecase.actor_goals_unassociated('type' => @unassoc_actor1.class).should =~ [@unassoc_actor1, @unassoc_actor2, @unassoc_system]
        @usecase.actor_goals_unassociated_count('type' => @unassoc_actor1.class).should == 3

        @usecase.actor_goals_unassociated('type' => @unassoc_actor2.class, :name => @unassoc_actor2.name).should =~ [@unassoc_actor2]
        @usecase.actor_goals_unassociated_count('type' => @unassoc_actor1.class, :name => @unassoc_actor2.name).should == 1
      end
      
      it "should return an array of all unassociated elements via the second class" do
        @actor.constrained_goals_unassociated.should =~ [@unassoc_usecase]
        @system.constrained_goals_unassociated.should =~ [@unassoc_usecase]
      end
      
      it "should return the queries used to get associated and unassociated elements" do
        # Check query count
        @usecase.actor_goals_query.count.should == 2
        # Check query results
        @usecase.actor_goals_query.all.collect{|g| g.actor_goal }.should =~ [@actor, @system]
        @actor.constrained_goals_query.all.collect{|g| g.constrained_goal }.should =~ [@usecase]
        # Check unassociated query results
        @usecase.actor_goals_unassociated_query.all.should =~ [@unassoc_actor1, @unassoc_actor2, @unassoc_system]
        @actor.constrained_goals_unassociated_query.all.should =~ [@unassoc_usecase]
      end
      
      it "should ignore invalid association data (no specified id or class) when retrieving the association" do
        # Mess up the association's data
        @g1[:actor_goal_class] = nil
        @g1.save
        @g2[:actor_goal_id] = nil
        @g2.save
        
        # Check that the associations evaluate to empty
        @usecase.actor_goals.should be_empty
        @actor.constrained_goals.should be_empty
        @system.constrained_goals.should be_empty
        @g1.actor_goal.should be_nil
        @g2.actor_goal.should be_nil
        
        # Check that the 'one-sided' association class instances are still attached to the other side
        @usecase.actor_goals_records.should =~ [@g1, @g2]
      end
      
      it "should ignore invalid association data (missing object) when retrieving the association" do
        # Mess up the association's data
        # @actor.delete
        @g1[:actor_goal_id] = 0
        @g1.save
        # @g2[] = nil
        # @g2.save
        
        # Check association values
        @usecase.actor_goals.should =~ [@system]
        @system.constrained_goals.should =~ [@usecase]
        @g1.actor_goal.should be_nil
        @g2.actor_goal.should == @system
        
        # Check that the 'one-sided' association class instances are still attached to the other side
        @usecase.actor_goals_records.should =~ [@g1, @g2]
        
        # Test that filters do not cause an error with invalid data
        @usecase.actor_goals(:name => 'System').should =~ [@system]
        @usecase.actor_goals(:name => 'Actor').should =~ []
        
        @usecase.actor_goals_records(:name => 'System').should =~ [@g2]
        @usecase.actor_goals_records(:name => 'Actor').should =~ []
        @usecase.actor_goals_records({}).should =~ [@g1, @g2]
      end
      
      it "should be able to filter the list of returned associations via the first class" do
        @usecase.actor_goals(:id => @system.id, 'type' => @system.class).should =~ [@system]
        @usecase.actor_goals_count(:id => @system.id, 'type' => @system.class).should == 1
      end
      
      it "shouldshould be able to filter the list of returned associations via the second class" do
        @actor.constrained_goals(:id => @usecase.id, 'type' => @usecase.class).should =~ [@usecase]
      end
    end
    
    context "manipulating a join table entry" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete
        @actor    = Actor.create
        @actor2   = Actor.create
        @usecase  = UseCase.create
        @usecase2 = UseCase.create
        @system   = System.create
        @system2  = System.create
        @actor.add_constrained_goal(@usecase)#.save
        @system.add_constrained_goal(@usecase)#.save
        @goal1 = Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ]
        @goal1.should_not be_nil
        @goal2 = Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ]
        @goal2.should_not be_nil
      end

      it "should be able to return the current associations" do
        @goal1.actor_goal.should == @actor
        @goal1.constrained_goal.should == @usecase
        
        @goal2.actor_goal.should == @system
        @goal2.constrained_goal.should == @usecase
      end
      
      it "should be able to replace the associated objects - Test 1" do
        @goal1.actor_goal = @system2
        @goal1.actor_goal.should == @system2
        @goal1.constrained_goal = @usecase2
        @goal1.constrained_goal.should == @usecase2
      end
      
      it "should be able to replace the associated objects - Test 2" do
        @goal2.actor_goal = @actor2
        @goal2.actor_goal.should == @actor2
        @goal2.constrained_goal = @usecase2
        @goal2.constrained_goal.should == @usecase2
      end
    end # End manipulating join table entries
    
    context "Getting join table entries" do
      before :each do
        Goal.delete; Actor.delete; System.delete; UseCase.delete; Domain.delete
        @actor    = Actor.create
        @actor2   = Actor.create
        @usecase  = UseCase.create
        @usecase2 = UseCase.create
        @system   = System.create
        @system2  = System.create
        @actor.add_constrained_goal(@usecase)#.save
        @system.add_constrained_goal(@usecase)#.save
        @goal1 = Goal[:actor_goal_id => @actor.id, :actor_goal_class => @actor.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ]
        @goal1.should_not be_nil
        @goal2 = Goal[:actor_goal_id => @system.id, :actor_goal_class => @system.class.to_s,
          :constrained_goal_id => @usecase.id, :constrained_goal_class => @usecase.class.to_s
          ]
        @goal2.should_not be_nil
      end
      it "should be able to return the join table entry for a many-to-many association" do
        usecase = @actor.constrained_goals.first
        usecase.should == @usecase
        # Using #associations method
        goal = @actor.constrained_goals_associations.first[:through]
        usecase = @actor.constrained_goals_associations.first[:to]
        usecase.should == @usecase
        goal.should == @goal1
        # Using #records method
        goal = @actor.constrained_goals_records.first
        goal.should == @goal1
      end
    end
  end
  # ============================================================
  # END polymorphic-to-polymorphic many-to-many section
  # ============================================================
  
  
  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic many-to-many section
  # ============================================================
  context "polymorphic-to-nonpolymorphic relationship -" do
    context "adding an element" do
      before :each do
        Delta.delete; Step.delete; StepGroup.delete; Scenario.delete; Delta_Step_Replacements.delete
        @delta    = Delta.create
        @step     = Step.create
        @step_group    = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = (@step.add_delta_replacement(@delta))
      #   result.should be_an_instance_of(Delta_Step_Replacements)
      #   result = (@delta.add_step_replacement(@step))
      #   result.should be_an_instance_of(Delta_Step_Replacements)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end
    
      it "should add the element to the association via the first class" do
        @step.add_delta_replacement(@delta)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should_not be_nil
      end
    
      it "should add the element to the association via the second class" do
        @delta.add_step_replacement(@step)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should_not be_nil
      end
    
      it "should be able to accept a polymorphic relationship on one class" do
        @delta.add_step_replacement(@step_group)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should_not be_nil
      end
    
      it "should reject an element that is not of the polymorphic type" do
        # Scenario does not inherit from Step
        lambda {@delta.add_step_replacement(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @delta.step_replacements.should_not include(@scenario)
        lambda {@step.add_delta_replacement(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.delta_replacements.should_not include(@scenario)
        lambda {@delta.add_step_replacement(@delta)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @delta.step_replacements.should_not include(@delta)
        lambda {@step.add_delta_replacement(@step_group)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.delta_replacements.should_not include(@step_group)
      end
    end
  
    context "removing an element" do
      before :each do
        Delta.delete; Step.delete; StepGroup.delete; Scenario.delete; Delta_Step_Replacements.delete
        @delta    = Delta.create
        @step     = Step.create
        @step_group    = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      
        @delta.add_step_replacement(@step_group)#.save
        @delta.add_step_replacement(@step)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should_not be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should_not be_nil
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   pending "write test"
      # end
    
      it "should remove the association via the first class" do
        @delta.remove_step_replacement(@step_group)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should_not be_nil
      end
    
      it "should remove the association via the second class" do
        @step_group.remove_delta_replacement(@delta)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should_not be_nil
      end
    
      it "should reject a non-existant association" do
        lambda {@delta.remove_step_replacement(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        lambda {@step.remove_delta_replacement(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
      end
    end
  
    context "removing all elements" do
      before :each do
        Delta.delete; Step.delete; StepGroup.delete; Delta_Step_Replacements.delete
        @delta = Delta.create
        @step  = Step.create
        @step_group = StepGroup.create
      
        @delta.add_step_replacement(@step_group)#.save
        @delta.add_step_replacement(@step)#.save
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should_not be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should_not be_nil
      end
  
      it "should remove all elements via the first class" do
        @delta.remove_all_step_replacements
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should be_nil
      end
    
      it "should remove all elements via the second class" do
        @step.remove_all_delta_replacements
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should_not be_nil
      end
    end
  
    context "listing all elements" do
      before :each do
        Delta.delete; Step.delete; StepGroup.delete; Scenario.delete; Delta_Step_Replacements.delete
        @delta    = Delta.create
        @step     = Step.create
        @step_group    = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      
        @delta.add_step_replacement(@step_group)#.save
        @delta.add_step_replacement(@step)#.save
        @join1 = Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ]
        @join1.should_not be_nil
        @join2 = Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ]
        @join2.should_not be_nil
      end
  
      it "should return an array of all elements associated via the first class" do
        @delta.step_replacements.should =~ [@step, @step_group]
      end
    
      it "should return an array of all elements associated via the second class" do
        @step.delta_replacements.should =~ [@delta]
        @step_group.delta_replacements.should =~ [@delta]
      end
      
      it "should ignore invalid association data when retrieving the association" do
        # Mess up the association's data
        @join1[:step_replacement_class] = nil
        @join1.save
        @join2[:step_replacement_id] = nil
        @join2.save
        
        # Check that the associations evaluate to empty
        @delta.step_replacements.should be_empty
        @step.delta_replacements.should be_empty
        @step_group.delta_replacements.should be_empty
        
        # Check association class instance methods
        @join1.step_replacement.should be_nil
        @join1.delta_replacement.should == @delta
        @join2.step_replacement.should be_nil
        @join2.delta_replacement.should == @delta
        
        # Check that the 'one-sided' association class instances are still attached to the other side
        @delta.step_replacements_records.should =~ [@join1, @join2]
      end
    end
    
    context "Getting join table entries" do
      before :each do
        Delta.delete; Step.delete; StepGroup.delete; Scenario.delete; Delta_Step_Replacements.delete
        @delta    = Delta.create
        @step     = Step.create
        @step_group    = StepGroup.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      
        @delta.add_step_replacement(@step_group)#.save
        @delta.add_step_replacement(@step)#.save
        @replacement1 = Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ]
        @replacement1.should_not be_nil
        @replacement2 = Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ]
        @replacement2.should_not be_nil
      end
      it "should be able to return the join table entry for a many-to-many association" do
        steps = @delta.step_replacements('type' => StepGroup).first
        steps.should == @step_group
        # Using #associations method
        steps_hash = @delta.step_replacements_associations('type' => StepGroup).first
        replacement = steps_hash[:through]
        steps = steps_hash[:to]
        steps.should == @step_group
        replacement.should == @replacement1
        # Using #records method
        replacement = @delta.step_replacements_records('type' => StepGroup).first
        replacement.should == @replacement1
      end
    end

    context "Destroying the object" do
      before :each do
        Delta.delete; Step.delete; StepGroup.delete; Delta_Step_Replacements.delete
        @delta = Delta.create
        @step  = Step.create
        @step_group = StepGroup.create
      
        @delta.add_step_replacement(@step_group)
        @delta.add_step_replacement(@step)
      end
      it "should remove all associations to other objects when an object is destroyed via the second object" do
        delta_id = @delta.id
        @delta.destroy
        # The join table entries should be gone
        Delta_Step_Replacements[
          :delta_replacement_id => delta_id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should be_nil
        Delta_Step_Replacements[
          :delta_replacement_id => delta_id,
          :step_replacement_id => @step.id,
          :step_replacement_class => @step.class.to_s
          ].should be_nil
      end
      it "should remove all associations to other objects when an object is destroyed via the first object" do
        step_id = @step.id
        @step.destroy
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => @step_group.id,
          :step_replacement_class => @step_group.class.to_s
          ].should_not be_nil
        # The join table entries should be gone
        Delta_Step_Replacements[
          :delta_replacement_id => @delta.id,
          :step_replacement_id => step_id,
          :step_replacement_class => @step.class.to_s
          ].should be_nil
      end
    end
  end
  
  # ============================================================
  # END polymorphic-to-nonpolymorphic many-to-many section
  # ============================================================
  
  # ============================================================
  # BEGIN nonpolymorphic-to-nonpolymorphic many-to-many section
  # ============================================================
  
  context "nonpolymorphic-to-nonpolymorphic relationship -" do
    context "adding an element" do
      before :each do
        Domain.delete; System.delete; Domain_System_Association.delete; Scenario.delete;
        @domain   = Domain.create
        @system   = System.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = (@domain.add_system(@system))
      #   result.should be_an_instance_of(Domain_System_Association)
      #   result = (@system.add_domain(@domain))
      #   result.should be_an_instance_of(Domain_System_Association)
      # end
      #
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end
    
      it "should add the element to the association via the first class" do
        @domain.add_system(@system)#.save
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should_not be_nil
      end
    
      it "should add the element to the association via the second class" do
        @system.add_domain(@domain)#.save
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should_not be_nil
      end
    
      it "should reject an element that is not of the required type" do
        # Scenario is not of type System or Domain
        lambda {@system.add_domain(@scenario)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @system.domains.should_not include(@scenario)
        lambda {@domain.add_system(@scenario)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @domain.systems.should_not include(@scenario)
        lambda {@system.add_domain(@system)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @system.domains.should_not include(@system)
        lambda {@domain.add_system(@domain)#.save
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @domain.systems.should_not include(@domain)
      end
    end
  
    context "removing an element" do
      before :each do
        Domain.delete; System.delete; Domain_System_Association.delete; Scenario.delete;
        @domain   = Domain.create
        @system   = System.create
        @system2  = System.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      
        @domain.add_system(@system)#.save
        @domain.add_system(@system2)#.save
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should_not be_nil
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ].should_not be_nil
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #
      # end
      
      it "should remove the association via the first class" do
        @domain.remove_system(@system)#.save
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should be_nil
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ].should_not be_nil
      end
    
      it "should remove the association via the second class" do
        @system.remove_domain(@domain)#.save
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should be_nil
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ].should_not be_nil
      end
    
      it "should reject a non-existant association" do
        lambda {@domain.remove_system(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @domain.systems.should_not include(@scenario)
        lambda {@system.remove_domain(@scenario)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @system.domains.should_not include(@scenario)
      end
    end
  
    context "removing all elements" do
      before :each do
        Domain.delete; System.delete; Domain_System_Association.delete; Scenario.delete;
        @domain   = Domain.create
        @system   = System.create
        @system2  = System.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      
        @domain.add_system(@system)#.save
        @domain.add_system(@system2)#.save
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should_not be_nil
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ].should_not be_nil
      end
  
      it "should remove all elements via the first class" do
        @system.remove_all_domains
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should be_nil
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ].should_not be_nil
      end
    
      it "should remove all elements via the second class" do
        @domain.remove_all_systems
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ].should be_nil
        Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ].should be_nil
      end
    end
  
    context "listing all elements" do
      before :each do
        Domain.delete; System.delete; Domain_System_Association.delete; Scenario.delete;
        @domain   = Domain.create
        @system   = System.create
        @system2  = System.create
        @scenario = Scenario.create # Unrelated class for exclusion testing
      
        @domain.add_system(@system)#.save
        @domain.add_system(@system2)#.save
        @join1 = Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system.id,
          ]
        @join1.should_not be_nil
        @join2 = Domain_System_Association[
          :domain_id => @domain.id,
          :system_id => @system2.id,
          ]
        @join2.should_not be_nil
      end
      
      it "should return an array of all elements associated via the first class" do
        @domain.systems.should =~ [@system, @system2]
      end
    
      it "should return an array of all elements associated via the second class" do
        @system.domains.should =~ [@domain]
        @system2.domains.should =~ [@domain]
      end
      
      it "should also return a hash of all elements and join entries" do
        domain_assocs = @domain.systems_associations
        # Should return an array of hashes
        domain_assocs.should be_an(Array)
        domain_assocs.first.should be_a(Hash)
        # Check hash values
        domain_assocs.length.should == 2
        domain_assocs.should include({:through => @join1, :to => @system})
        domain_assocs.should include({:through => @join2, :to => @system2})
        
        # Check from other side of association
        system_assocs = @system.domains_associations
        system_assocs.length.should == 1
        system_assocs.should include({:through => @join1, :to => @domain})
        
        system2_assocs = @system2.domains_associations
        system2_assocs.length.should == 1
        system2_assocs.should include({:through => @join2, :to => @domain})
      end
      
      it "should ignore invalid association data when retrieving the association" do
        # Mess up the association's data
        @join1[:domain_id] = nil
        @join1.save
        @join2[:system_id] = nil
        @join2.save
        
        # Check that the associations evaluate to empty
        @domain.systems.should be_empty
        @system.domains.should be_empty
        @system2.domains.should be_empty
        @join1.domain.should be_nil
        @join1.system.should == @system
        @join2.system.should be_nil
        @join2.domain.should == @domain
        
        # Check that the 'one-sided' association class instances are still attached to the other side
        @domain.systems_records.should =~ [@join2]
        @system.domains_records.should =~ [@join1]
      end
    end
  end
  
  # ============================================================
  # END nonpolymorphic-to-nonpolymorphic many-to-many section
  # ============================================================
end
