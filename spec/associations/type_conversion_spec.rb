require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Type Conversion" do
  
  before :each do
    ChangeTracker.cancel
    reset_db
  end
  

  # Example: Application::Person (customers) *----* RepairShop (repair_shops)
  context "converting to a similar type" do
    # before :each do
      # @time_before_creation = Time.now
      # @time_after_creation = Time.now
    # end
    it "should convert a person to a mechanic with the same attributes" do
      ChangeTracker.start
      smith = People::Person.new(:date_of_birth => Date.new(1972,2,11), :last_updated => Time.new(2000,1,1,12), :wakes_at => Time.new(2000,1,1,6,30), :family_size => 4, :weight => 211, :dependent => false)
      smith.save
      ChangeTracker.commit
      grease_monkey = SpecificAssociations.convert_object_to_type(smith, "Automotive::Mechanic")
      grease_monkey.date_of_birth.should == Time.new(1972,2,11)
      grease_monkey.last_updated.should  == Time.new(2000,1,1,12)
      grease_monkey.wakes_at.should      == Time.new(2000,1,1,6,30)
      grease_monkey.family_size.should   == 4
      grease_monkey.weight.should        == 211
      grease_monkey.dependent.should     == false
      People::Person.where(:weight => 211).first.should == nil
    end
    
    it "should convert a person to a mechanic with the same attributes even if there is a uniqueness warning" do
      ChangeTracker.start
      neo = People::Person.new(:name => 'Neo')
      neo.save
      ChangeTracker.commit
      grease_monkey = SpecificAssociations.convert_object_to_type(neo, "Automotive::Mechanic")
      grease_monkey.name.should == 'Neo'
      People::Person.where(:name => 'Neo').first.should == nil
    end
    
    it "should convert a person to a mechanic with the same attributes even if there is a uniqueness constraint" do
      ChangeTracker.start
      c1 = Automotive::VIN.new(:vin => 1234)
      c1.save
      ChangeTracker.commit
      c1_id = c1.id
      # The only is_unique constraint we have in the model right now is on something that has no subclasses so we have to convert it to another instance of the same thing.  Ideally we would FIXME and change the model so that we could do a better test of this
      c2 = SpecificAssociations.convert_object_to_type(c1, Automotive::VIN)
      c2.vin.should == 1234
      Automotive::VIN[c1_id].should == nil
    end
    
    it "should convert a person to a mechanic with the same associations" do
      ChangeTracker.start
      hal     = People::Person.new(:weight => 2001)
      car1    = Automotive::Car.new(:vehicle_model => 'Discovery One')
      car2    = Automotive::Car.new(:vehicle_model => 'Discovery Two')
      car3    = Automotive::Car.new(:vehicle_model => 'Galaxy')
      address = Geography::Address.new(:street_name => 'Europa')
      hal.drives_add   car1
      hal.vehicles_add car1
      hal.vehicles_add car2
      hal.vehicles_add car3
      hal.occupying  = car1
      hal.addresses_add address 
      hal.save
      ChangeTracker.commit
      arthur_clarke = SpecificAssociations.convert_object_to_type(hal, "Automotive::Mechanic")
      arthur_clarke.drives.should == [car1]
      arthur_clarke.vehicles.count.should == 3 
      arthur_clarke.vehicles.first.should == car1
      arthur_clarke.vehicles.last.should == car3
      arthur_clarke.occupying.should == car1
      arthur_clarke.addresses.first.street_name.should == 'Europa'
      car1.owners.should include(arthur_clarke)
      People::Person.where(:weight => 2001).first.should == nil
    end
    
    it "should convert a person to a mechanic with the same associations and maintain ordering of the converted object in any ordered collection in which the now mechanic is a member" do
      ChangeTracker.start
      chandra = People::Clown::Clown.new(:name => 'Chandra')
      hal     = People::Person.new(:weight => 2001)
      david   = People::Person.new(:name => 'David Bowman')
      car1    = Automotive::Car.new(:vehicle_model => 'Discovery One')
      chandra.drives_add(car1)
      hal.drives_add(car1)
      david.drives_add(car1)
      chandra.save
      david.save
      hal.save
      ChangeTracker.commit
      arthur_clarke = SpecificAssociations.convert_object_to_type(hal, "Automotive::Mechanic")
      car1.drivers.should include(arthur_clarke)
      # Check ordering
      car1.drivers.should == [chandra, arthur_clarke, david]
      People::Person.where(:weight => 2001).first.should == nil
    end
  end
end