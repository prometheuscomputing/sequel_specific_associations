require 'spec_helper'

describe "recursive association functionality" do
  before(:all) do
    reset_db
  end
  
  it "Test ability to get property recursively" do
    @bob = Recursive::Person.create(:name => 'Bob')
    @spot = Recursive::Dog.create(:name => 'Spot')
    @phil = Recursive::User.create(:name => 'Phil', :username => 'pjfry')
    @sue = Recursive::Person.create(:name => 'Sue')
    @fido = Recursive::Dog.create(:name => 'Fido')
    
    @bob.friends.should be_empty
    @bob.add_friend(@spot)
    @bob.friends.should =~ [@spot]
    @bob.add_friend(@phil)
    @bob.friends.should =~ [@spot, @phil]
    @phil.friends.should be_empty
    @phil.add_friend(@sue)
    @phil.friends.should =~ [@sue]
    @sue.friends.should be_empty
    @sue.add_friend(@fido)
    @sue.friends.should =~ [@fido]
    
    # Get bob's friends recursively
    @bob._get_property_recursively(:friends).should =~ [@spot, @phil, @sue, @fido]
  end
end