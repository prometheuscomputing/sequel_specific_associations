require 'spec_helper'

describe "domain object when creating a schema based on a the object class" do
  context "a schema does not yet exist" do
    before(:each) do
      DB.drop_table Application::Test.table_name if DB.table_exists? Application::Test.table_name
    end
    
    it "should create a schema when using create_schema" do
      Application::Test.create_schema.should == "Application::Test.db.create_table :tests do\n  String :name\n  #{SpecificAssociations::INT_TYPE} :number\n  primary_key :id\nend"
      Application::Test.columns.length.should == 3
      Application::Test.columns.should include(:name)
      Application::Test.columns.should include(:number)
      Application::Test.columns.should include(:id)
    end
    
    it "should create a schema when using create_schema?" do
      Application::Test.create_schema?.should == "Application::Test.db.create_table :tests do\n  String :name\n  #{SpecificAssociations::INT_TYPE} :number\n  primary_key :id\nend"
      Application::Test.columns.length.should == 3
      Application::Test.columns.should include(:name)
      Application::Test.columns.should include(:number)
      Application::Test.columns.should include(:id)
    end
    
    it "should create a schema when using create_schema!" do
      Application::Test.create_schema!.should == "Application::Test.db.create_table :tests do\n  String :name\n  #{SpecificAssociations::INT_TYPE} :number\n  primary_key :id\nend"
      Application::Test.columns.length.should == 3
      Application::Test.columns.should include(:name)
      Application::Test.columns.should include(:number)
      Application::Test.columns.should include(:id)
    end
  end
  
  context "a schema already exists" do
    before(:each) do
      DB.drop_table Application::Test.table_name if DB.table_exists? Application::Test.table_name
      DB.create_table Application::Test.table_name do
        primary_key :old_id
        String :old_name
      end
      # Refresh the model's dataset (Resets columns and other info from changed dataset)
      Application::Test.set_dataset(Application::Test.dataset)
    end
    
    it "should raise an error when using create_schema? and trying to update the schema with another primary key column" do
      lambda {Application::Test.create_schema?}.should raise_error(SpecificAssociations::SchemaConflict, /shouldn't be adding a primary key/)
      Application::Test.columns.should == [:old_id, :old_name]
    end
    
    it "should update the schema when a new attribute column is detected" do
      Application::Thing.create_schema?
      Application::Thing.columns.should =~ [:thing_name, :id, :test_id]
      class Application::Thing
        attribute :color, String
      end
      Application::Thing.create_schema?      
      Application::Thing.columns.should =~ [:thing_name, :id, :color, :test_id]
    end

    it "should update the schema when a new association column is detected" do
      Application::Dude.create_schema?
      Application::Dude.columns.should =~ [:name, :id]
      class Application::Dude
        many_to_one :Problem
      end
      Application::Dude.create_schema?
      Application::Dude.columns.should =~ [:name, :id, :problem_id]
    end
    
    it "should create a new schema when using create_schema!" do
      Application::Test.create_schema!.should == "Application::Test.db.create_table :tests do\n  String :name\n  #{SpecificAssociations::INT_TYPE} :number\n  primary_key :id\nend"
      Application::Test.columns.should == [:id, :name, :number]
    end
  end
end
