require 'spec_helper'

describe "functionality common to properties (attributes and associations" do
  before(:each) do
    reset_db
  end
  
  it "should allow access to any specified additional_data" do
    People::Person.properties[:name][:additional_data].should be_a(Hash)
    People::Person.properties[:name][:additional_data][:modeled_as].should == :attribute
    People::Person.additional_data_for(:name).should == People::Person.properties[:name][:additional_data]
  end
end