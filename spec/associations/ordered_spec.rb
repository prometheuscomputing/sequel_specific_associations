require 'spec_helper'

describe "domain object for a one to many ordered association" do
  before(:each) do
    reset_db
    @book1 = Application::EBook.create(:title => "War and Peace")
    @book2 = Application::EBook.create(:title => "Crime and Punishment")
    @book3 = Application::EBook.create(:title => "The Brothers Karamazov")
    @book4 = Application::Dictionary.create(:title => "Webster's")
    @library = Application::Library.create(:name => "WCU public library")
  end

  it "should add new associations to the end of the associations list" do
    @library.add_book(@book1)
    @library.books.count.should == 1
    @library.books.first.should be_an_instance_of(Application::EBook)
    @library.books.first.title.should == "War and Peace"
    # Check position
    @book1.library_position.should == 0
    
    @library.add_book(@book2)
    @library.books.count.should == 2
    @library.books.first.title.should == @book1.title
    @library.books.last.title.should == @book2.title
    # Check positions
    @book1.library_position.should == 0
    @book2.library_position.should == 1
    
    @library.add_book(@book3)
    @library.books.count.should == 3
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book2.title
    @library.books[2].title.should == @book3.title
    # Check positions
    @book1.library_position.should == 0
    @book2.library_position.should == 1
    @book3.library_position.should == 2
    
    # Remove book2
    @library.remove_book(@book2)
    @library.books.count.should == 2
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    # Check positions
    @book1.library_position.should == 0
    # Must refresh objects that are indirectly modified
    @book3.refresh.library_position.should == 1
    
    # Re-add book2 (should go to end of list)
    @library.add_book(@book2)
    @library.books.count.should == 3
    
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    @library.books[2].title.should == @book2.title
    # Check positions
    @book1.library_position.should == 0
    @book3.library_position.should == 1
    @book2.library_position.should == 2
  end
  
  it "should reorder remaining associations when an association is removed" do
    @library.add_book(@book1)
    @library.add_book(@book2)
    @library.add_book(@book4) # A dictionary
    @library.add_book(@book3)
    
    @library.books[0].id.should == @book1.id
    @library.books[1].id.should == @book2.id
    @library.books[2].id.should == @book4.id
    @library.books[3].id.should == @book3.id
    
    # Remove book2
    @library.remove_book(@book2)
    @book1.refresh; @book4.refresh; @book3.refresh
    # Check number of elements
    @library.books.count.should == 3
    # Check array positions
    @library.books[0].id.should == @book1.id
    @library.books[1].id.should == @book4.id
    @library.books[2].id.should == @book3.id
    # Check position columns
    @library.books[0].library_position.should == 0
    @library.books[1].library_position.should == 1
    @library.books[2].library_position.should == 2
    
    # Remove book4
    @library.remove_book(@book4)
    @book1.refresh; @book3.refresh
    # Check number of elements
    @library.books.count.should == 2
    # Check array positions
    @library.books[0].id.should == @book1.id
    @library.books[1].id.should == @book3.id
    # Check position columns
    @library.books[0].library_position.should == 0
    @library.books[1].library_position.should == 1
  end
  
  
  it "should allow the associations to be reordered to specific positions" do
    # This test assumes that when moving books, the books after the new position of the moved book are
    # moved back one position (stopping if/when the old position of the moved book is reached)
    @library.add_book(@book1)
    @library.add_book(@book2)
    @library.books.count.should == 2
    @library.books.last.title.should == @book2.title
    
    @library.move_book(@book2, 0)
    @library.books.count.should == 2
    # Application::EBooks should now be reversed
    @library.books.first.title.should == @book2.title
    @library.books.last.title.should == @book1.title
    
    @library.move_book(@book1, 0)
    @library.books.count.should == 2
    # Application::EBooks should be reversed to original positions
    @library.books.first.title.should == @book1.title
    @library.books.last.title.should == @book2.title
    
    # Add a third book to the library
    @library.add_book(@book3)
    @library.books.count.should == 3
    @library.books.last.title.should == @book3.title
    
    # Move the third book to the middle position
    @library.move_book(@book3, 1)
    @library.books.count.should == 3
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book3.title
    @library.books[2].title.should == @book2.title
    
    # Move the second book to the first position
    @library.move_book(@book2, 0)
    @library.books.count.should == 3
    @library.books[0].title.should == @book2.title
    @library.books[1].title.should == @book1.title
    @library.books[2].title.should == @book3.title
    
    # Move the first book to the first position
    @library.move_book(@book1, 0)
    @library.books.count.should == 3
    @library.books[0].title.should == @book1.title
    @library.books[1].title.should == @book2.title
    @library.books[2].title.should == @book3.title
  end
  
  it "should raise an ArgumentError on invalid input to the move command" do
    @library.add_book(@book1)
    @library.add_book(@book2)
    
    # Invalid input: @book3 is not part of the collection
    lambda { @library.move_book(@book3, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: nil is not a valid item
    lambda { @library.move_book(nil, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: true is not a valid item
    lambda { @library.move_book(true, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: nil is not a valid position
    lambda { @library.move_book(@book2, nil) }.should raise_error(ArgumentError)
  end
  
  # it "should handle bad ordering information in the database" do
  #   pending
  # end
  
  it "should return objects in the correct order when ordering multiple object types" do
    @library.add_book(@book1)
    @library.add_book(@book2)
    @library.add_book(@book4) # A dictionary
    @library.add_book(@book3)
    
    @library.books.should == [@book1, @book2, @book4, @book3]
  end
  
  it "should return objects in the correct order when a limit and/or offset is set" do
    @library.add_book(@book1)
    @library.add_book(@book2)
    @library.add_book(@book4)
    @library.add_book(@book3)
    
    @library.books({},4,0).should == [@book1, @book2, @book4, @book3]
    @library.books({},3,1).should == [@book2, @book4, @book3]
    @library.books({},2,2).should == [@book4, @book3]
    @library.books({},1,2).should == [@book4]
    # Test where limit + offset is out of bounds
    @library.books({},10,2).should == [@book4, @book3]
    
    # Remove @book2 and retest
    @library.remove_book(@book2)
    @book1.refresh; @book4.refresh; @book3.refresh
    
    @library.books({},3,0).should == [@book1, @book4, @book3]
    @library.books({},2,1).should == [@book4, @book3]
    @library.books({},1,1).should == [@book4]
    # Test where limit + offset is out of bounds
    @library.books({},10,1).should == [@book4, @book3]
  end
  
  it "should return objects in the correct order when filter is set" do
    @library.add_book(@book1)
    @library.add_book(@book2)
    @library.add_book(@book4)
    @library.add_book(@book3)
    
    @library.books(Sequel.|({:title => @book3.title},{:title => @book2.title})).should == [@book2, @book3]
    @library.books(Sequel.|({:title => @book4.title},{:title => @book2.title})).should == [@book2, @book4]
  end
  
  # it "should allow swapping associations' positions" do
  #   @library.add_book(@book1)
  #   @library.add_book(@book2)
  #   @library.books.last.title.should == @book2.title
  #
  #   # Swap the books
  #   pending "Implementation of the swap_ method" do
  #     @library.swap_books(@book1, @book2)
  #   end
  #   @library.books.first.title.should == @book2.title
  #   @library.books.last.title.should == @book1.title
  #
  #   # Add a third book
  #   @library.add_book(@book3)
  #
  #   # Check ordering
  #   @library.books[0].title.should == @book2.title
  #   @library.books[1].title.should == @book1.title
  #   @library.books[2].title.should == @book3.title
  #
  #   # Swap book3 with book2
  #   @library.swap_books(@book3, @book2)
  #   @library.books[0].title.should == @book3.title
  #   @library.books[1].title.should == @book1.title
  #   @library.books[2].title.should == @book2.title
  #
  #   # Swap book3 with book1
  #   @library.swap_books(@book3, @book1)
  #   @library.books[0].title.should == @book1.title
  #   @library.books[1].title.should == @book3.title
  #   @library.books[2].title.should == @book2.title
  # end
end
    
  
describe "domain object for a many to many ordered association" do
  before(:each) do
    reset_db
    @library1 = Application::Library.create(:name => "WCU public library")
    @library2 = Application::Library.create(:name => "Sylva public library")
    @library3 = Application::Library.create(:name => "Franklin public library")
    @patron1 = Application::Patron.create(:first_name => "Bob")
    @patron2 = Application::Patron.create(:first_name => "Sue")
    @patron3 = Application::Patron.create(:first_name => "Bill")
  end

  it "should add new associations to the end of the associations list" do
    @library1.add_patron(@patron1)
    @library1.patrons.count.should == 1
    @library1.patrons.first.should be_an_instance_of(Application::Patron)
    @library1.patrons.first.first_name.should == "Bob"
    
    @library1.add_patron(@patron2)
    @library1.patrons.count.should == 2
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    
    @library1.add_patron(@patron3)
    @library1.patrons.count.should == 3
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    @library1.patrons[2].id.should == @patron3.id
  end
  
  it "should reorder remaining associations when an association is removed" do
    @library1.add_patron(@patron1)
    @library1.add_patron(@patron2)
    @library1.add_patron(@patron3)
    
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    @library1.patrons[2].id.should == @patron3.id
      
    # Remove patron2
    @library1.remove_patron(@patron2)
    # Check number of elements
    @library1.patrons.count.should == 2
    # Check array positions
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron3.id
    # Check position columns
    @library1.patrons_records[0].library_position.should == 0
    @library1.patrons_records[1].library_position.should == 1
    
    # Remove patron3
    @library1.remove_patron(@patron3)
    # Check number of elements
    @library1.patrons.count.should == 1
    # Check array positions
    @library1.patrons[0].id.should == @patron1.id
    # Check position columns
    @library1.patrons_records[0].library_position.should == 0
  end
  
  it "should allow the associations to be reordered to specific positions" do
    # This test assumes that when moving items, the items after the new position of the moved item are
    # moved back one position (stopping if/when the old position of the moved item is reached)
    @library1.add_patron(@patron1)
    @library1.add_patron(@patron2)
    @library1.patrons.last.id.should == @patron2.id
    
    @library1.move_patron(@patron2, 0)
    # Should now be reversed
    @library1.patrons[0].id.should == @patron2.id
    @library1.patrons[1].id.should == @patron1.id
    
    @library1.move_patron(@patron1, 0)
    # Should be reversed to original positions
    @library1.patrons.first.id.should == @patron1.id
    @library1.patrons.last.id.should == @patron2.id
    
    # Add a third patron to the library
    @library1.add_patron(@patron3)
    @library1.patrons.last.id.should == @patron3.id
    
    # Move the third patron to the middle position
    @library1.move_patron(@patron3, 1)
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron3.id
    @library1.patrons[2].id.should == @patron2.id
    
    # Move the second patron to the first position
    @library1.move_patron(@patron2, 0)
    @library1.patrons[0].id.should == @patron2.id
    @library1.patrons[1].id.should == @patron1.id
    @library1.patrons[2].id.should == @patron3.id
    
    # Move the first patron to the first position
    @library1.move_patron(@patron1, 0)
    @library1.patrons[0].id.should == @patron1.id
    @library1.patrons[1].id.should == @patron2.id
    @library1.patrons[2].id.should == @patron3.id
  end
  
  it "should raise an ArgumentError on invalid input to the move command" do
    @library1.add_patron(@patron1)
    @library1.add_patron(@patron2)
    
    # Invalid input: @patron3 is not part of the collection
    lambda { @library1.move_patron(@patron3, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: nil is not a valid item
    lambda { @library1.move_patron(nil, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: true is not a valid item
    lambda { @library1.move_patron(true, 0) }.should raise_error(ArgumentError)
    
    # Invalid input: nil is not a valid position
    lambda { @library1.move_patron(@patron1, nil) }.should raise_error(ArgumentError)
  end
  
  # it "should handle bad ordering information in the database" do
  #   pending "write this test"
  #   fail
  # end
  
  # it "should allow swapping associations' positions" do
  #   @library1.add_patron(@patron1)
  #   @library1.add_patron(@patron2)
  #   @library1.patrons.last.id.should == @patron2.id
  #
  #   # Swap the patrons
  #   pending "Implementation of the swap_ method" do
  #     @library1.swap_patrons(@patron1, @patron2)
  #   end
  #   @library1.patrons.first.id.should == @patron2.id
  #   @library1.patrons.last.id.should == @patron1.id
  #
  #   # Add a third patron
  #   @library1.add_patron(@patron3)
  #
  #   # Check ordering
  #   @library1.patrons[0].id.should == @patron2.id
  #   @library1.patrons[1].id.should == @patron1.id
  #   @library1.patrons[2].id.should == @patron3.id
  #
  #   # Swap patron3 with patron2
  #   @library1.swap_patrons(@patron3, @patron2)
  #   @library1.patrons[0].id.should == @patron3.id
  #   @library1.patrons[1].id.should == @patron1.id
  #   @library1.patrons[2].id.should == @patron2.id
  #
  #   # Swap patron3 with patron1
  #   @library1.swap_patrons(@patron3, @patron1)
  #   @library1.patrons[0].id.should == @patron1.id
  #   @library1.patrons[1].id.should == @patron3.id
  #   @library1.patrons[2].id.should == @patron2.id
  # end
end


describe "domain object for a many to many ordered association where both directions are ordered" do
  before(:each) do
    reset_db
    @book1 = Application::EBook.create(:title => "War and Peace")
    @book2 = Application::EBook.create(:title => "Crime and Punishment")
    @book3 = Application::EBook.create(:title => "The Brothers Karamazov")
    
    @book4 = Application::Dictionary.create(:title => "Websters")
    @book5 = Application::Dictionary.create(:title => "Oxford")
    @book6 = Application::Dictionary.create(:title => "Russian Application::Dictionary")
    
    @series1 = Application::ESeries.create(:label => "Russian Lit")
    @series2 = Application::ESeries.create(:label => "Dictionaries")
    @series3 = Application::ESeries.create(:label => "Favorites")
  end
  
  # Default Order guide::
  
  # Russian Lit1      Dictionaries2       Favorites3
  # book1             book4               book4
  # book2             book5               book1
  # book3             book6               book6
  # book6
  
  # Application::EBook1             Application::EBook2           Application::EBook3           Application::EBook4           Application::EBook5           Application::EBook6
  # Russian Lit1      Russian Lit1    Russian Lit1    Dictionaries2   Dictionaries2   Favorites3
  # Favorites3                                        Favorites3                      Dictionaries2
  #                                                                                   Russian Lit1
  
  it "should set positions for both sides of the associations when adding an element" do
    @series1.add_book(@book1)
    @series1.books.count.should == 1
    @book1.series.count.should == 1
    @series1.books[0].id.should == @book1.id
    @book1.series[0].id.should == @series1.id
    @series1.books_records[0].book.id.should == @book1.id
    @book1.series_records[0].series.id.should == @series1.id
    @series1.books_records[0].series_position.should == 0
    @book1.series_records[0].book_position.should == 0
    
    @series1.add_book(@book2)
    @series1.books.count.should == 2
    @book2.series.count.should == 1
    @series1.books[0].id.should == @book1.id
    @series1.books[1].id.should == @book2.id
    @book2.series[0].id.should == @series1.id
    @series1.books_records[0].book.id.should == @book1.id
    @series1.books_records[1].book.id.should == @book2.id
    @book2.series_records[0].series.id.should == @series1.id
    @series1.books_records[0].series_position.should == 0
    @series1.books_records[1].series_position.should == 1
    @book2.series_records[0].book_position.should == 0
  end
  
  it "should add new associations to the end of the associations list" do
    @series1.add_book(@book1)
    @series1.add_book(@book2)
    @series1.add_book(@book3)

    @series1.books.count.should == 3
    @series1.books[0].id.should == @book1.id
    @series1.books[1].id.should == @book2.id
    @series1.books[2].id.should == @book3.id

    @series2.add_book(@book4)
    @series2.add_book(@book5)
    
    @series2.books.count.should == 2
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    
    @series3.books.count.should == 3
    @series3.books[0].id.should == @book4.id
    @series3.books[1].id.should == @book1.id
    @series3.books[2].id.should == @book6.id
    
    @series2.add_book(@book6)
    @series2.books.count.should == 3
    @series2.books[0].id.should == @book4.id
    @series2.books[1].id.should == @book5.id
    @series2.books[2].id.should == @book6.id
    
    @series1.add_book(@book6)
    @series1.books.count.should == 4
    @series1.books[0].id.should == @book1.id
    @series1.books[1].id.should == @book2.id
    @series1.books[2].id.should == @book3.id
    @series1.books[3].id.should == @book6.id
    
    # Test from opposite end
    @book1.series.count.should == 2
    @book1.series[0].id.should == @series1.id
    @book1.series[1].id.should == @series3.id
    
    @book2.series.count.should == 1
    @book2.series[0].id.should == @series1.id
    
    @book3.series.count.should == 1
    @book3.series[0].id.should == @series1.id
    
    @book4.series.count.should == 2
    @book4.series[0].id.should == @series2.id
    @book4.series[1].id.should == @series3.id
    
    @book5.series.count.should == 1
    @book5.series[0].id.should == @series2.id
    
    @book6.series.count.should == 3
    @book6.series[0].id.should == @series3.id
    @book6.series[1].id.should == @series2.id
    @book6.series[2].id.should == @series1.id
    
  end
  
  it "should reorder remaining associations when an association is removed" do
    @series1.add_book(@book1)
    @series1.add_book(@book2)
    @series1.add_book(@book3)
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    @series2.add_book(@book6)
    @series1.add_book(@book6)
    
    @series3.remove_book(@book4)

    # Check series ordering
    @series3.books.count.should == 2
    @series3.books.should =~ [@book1, @book6]
    @series3.books[0].id.should == @book1.id
    @series3.books[1].id.should == @book6.id
    # Check position column
    @series3.books_records[0].book.id.should == @book1.id
    @series3.books_records[0].series_position.should == 0
    @series3.books_records[1].book.id.should == @book6.id
    @series3.books_records[1].series_position.should == 1
    
    # Check book ordering
    @book4.series.count.should == 1
    @book4.series[0].id.should == @series2.id
    # Check position column
    @book4.series_records[0].series.id.should == @series2.id
    @book4.series_records[0].book_position.should == 0
    
    
    @book1.remove_series(@series1)
    
    # Check book ordering
    @book1.series.count.should == 1
    @book1.series[0].id.should == @series3.id
    # Check position column
    @book1.series_records[0].series.id.should == @series3.id
    @book1.series_records[0].book_position.should == 0
    
    # Check series ordering
    @series1.books.count.should == 3
    @series1.books[0].id.should == @book2.id
    @series1.books[1].id.should == @book3.id
    @series1.books[2].id.should == @book6.id
    # Check position column
    @series1.books_records[0].book.id.should == @book2.id
    @series1.books_records[0].series_position.should == 0
    @series1.books_records[1].book.id.should == @book3.id
    @series1.books_records[1].series_position.should == 1
    @series1.books_records[2].book.id.should == @book6.id
    @series1.books_records[2].series_position.should == 2
  end
  
  it "should be able to set the association from an association class" do
    @series1.add_book(@book1)
    @series1.add_book(@book2)
    @series1.add_book(@book3)
    @series2.add_book(@book4)
    @series2.add_book(@book5)
    @series3.add_book(@book4)
    @series3.add_book(@book1)
    @series3.add_book(@book6)
    @series2.add_book(@book6)
    @series1.add_book(@book6)
    
    @book7 = Application::EBook.create(:title => "Tom Sawyer")
    @book8 = Application::EBook.create(:title => "Huckleberry Finn")
    
    # Get the join entry for series3 => book4
    join = @series3.books_records[0]
    join.series.id.should == @series3.id
    join.book.id.should == @book4.id
    join.series_position.should == 0
    join.book_position.should == 1
    @series3.books.count.should == 3
    @book4.series.count.should == 2
    # Set the book to book7
    join.book = @book7
    # Check associations
    join.series.id.should == @series3.id
    join.book.id.should == @book7.id
    # Position in books should not have changed (took the place of another book)
    join.series_position.should == 0
    # Application::ESeries position is now 0 (Only a member of 1 series)
    join.book_position.should == 0
    
    
    # Get the join entry for series2 => book5
    join = @series2.books_records[1]
    join.series_position.should == 1
    join.book_position.should == 0
    # Set the book to book8
    join.book = @book8
    # Check positions
    join.series_position.should == 1
    join.book_position.should == 0
    
    # Check number of associations
    @series3.books.count.should == 3
    @book4.series.count.should == 1
    @book7.series.count.should == 1
    
    # Set association by creating new instance of association class
    # Using series2 => book7
    @series2.books.count.should == 3
    @book7.series.count.should == 1
    join = Application::EBooksInESeries.create
    join.book = @book7
    join.book_position.should == 1
    join.series = @series2
    join.series_position.should == 3
    
    @series2.books.count.should == 4
    @series2.books[3].id.should == @book7.id
    @series2.books_records[3].series_position.should == 3
    @book7.series.count.should == 2
    @book7.series[1].id.should == @series2.id
    @book7.series_records[1].book_position.should == 1
  end
  
  it "should correctly represent the sorting of the association in the association's information hash" do
    assoc_info = Application::Library.associations[:books]
    assoc_info[:opp_is_ordered].should be_nil
    assoc_info[:ordered].should == true
  end
end

# TODO: time-browsing should work with ordered associations

# TODO: association class methods should work with ordered associations (important)