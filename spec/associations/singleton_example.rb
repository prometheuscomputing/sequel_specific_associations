module SingletonExample
  class MySingleton < Sequel::Model
    plugin :specific_associations
    singleton!
    attribute :name, String
  end
  
  class InheritedSingleton < MySingleton
    attribute :local_property, String
  end
  
  MySingleton.create_schema
  InheritedSingleton.create_schema
end