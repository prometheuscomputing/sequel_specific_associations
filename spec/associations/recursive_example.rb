module Recursive
  class Friendable < Sequel::Model
    set_dataset :r_friendable
    plugin :specific_associations
    interface!
    # NOTE: friendship isn't really acyclic, but is here for this example.
    many_to_many :Friendable, :through => :Friendship, :getter => :friends, :opp_getter => :friend_of, :acyclic => true
    many_to_many :Friendable, :through => :Friendship, :getter => :friend_of, :opp_getter => :friends, :acyclic => true
  end
  
  class Friendship < Sequel::Model
    set_dataset :r_friendship
    plugin :specific_associations
    association_class!
    associates :Friendable => :friend; associates :Friendable => :friend_of
    # Use custom primary key (not :id)
    attribute :friendship_identifier, SpecificAssociations::INT_TYPE, :primary_key => true
    attribute :description, String
  end
  
  class Person < Sequel::Model
    set_dataset :r_person
    plugin :specific_associations
    implements Friendable
    attribute :name, String
  end
  
  class User < Person
    set_dataset :r_user
    attribute :username, String
  end
  
  class Dog < Sequel::Model
    set_dataset :r_dogs
    plugin :specific_associations
    implements Friendable
    attribute :name, String
  end
  
  Friendship.create_schema
  Person.create_schema
  User.create_schema
  Dog.create_schema
end