module ClassRoom

  class CountingNumber < SpecificAssociations::INT_TYPE
    # this is a primitive type
    # extend ModelMetadata
  end

  class Grade < ClassRoom::CountingNumber
      # this is a primitive type
      # extend ModelMetadata
  end

  class Name < String
      # this is a primitive type
      # extend ModelMetadata
  end

  class Student < Sequel::Model
      set_dataset :students_table # :students already exists in another example
      plugin :specific_associations;# plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
      one_to_many :'ClassRoom::Student_LuckyNumber', :wraps => :'ClassRoom::CountingNumber', :getter => :lucky_numbers , :opp_getter => :student_for_lucky_numbers , :primitive => :lucky_number
      one_to_many :'ClassRoom::Student_MiddleName', :wraps => :'ClassRoom::Name', :getter => :middle_names , :opp_getter => :student_for_middle_names , :composition => true, :primitive => :middle_name
      one_to_many :'ClassRoom::Student_Nickname', :wraps => :'ClassRoom::Name', :getter => :nicknames , :opp_getter => :student_for_nicknames , :primitive => :nickname
      one_to_many :'ClassRoom::Student_StageName', :wraps => :'String', :getter => :stage_names , :opp_getter => :student_for_stage_names , :primitive => :stage_name
      one_to_many :'ClassRoom::Student_TestGrade', :wraps => :'ClassRoom::Grade', :getter => :test_grades , :opp_getter => :student_for_test_grades , :primitive => :test_grade
      attribute :favorite_number, SpecificAssociations::INT_TYPE
      attribute :first_name, String
      attribute :last_name, ClassRoom::Name
      attribute :student_number, ClassRoom::CountingNumber
  end
    
  class Student_LuckyNumber < Sequel::Model
      set_dataset :student_lucky_numbers
      plugin :specific_associations;# plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
      many_to_one :'Student', :getter => :student_for_lucky_numbers 
      attribute :lucky_number, ClassRoom::CountingNumber
  end

  class Student_MiddleName < Sequel::Model
      set_dataset :student_middle_names
      plugin :specific_associations;# plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
      many_to_one :'Student', :getter => :student_for_middle_names 
      attribute :middle_name, ClassRoom::Name
  end

  class Student_Nickname < Sequel::Model
      set_dataset :student_nicknames
      plugin :specific_associations;# plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
      many_to_one :'Student', :getter => :student_for_nicknames 
      attribute :nickname, ClassRoom::Name
  end

  class Student_StageName < Sequel::Model
      set_dataset :student_stage_names
      plugin :specific_associations;# plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
      many_to_one :'Student', :getter => :student_for_stage_names 
      attribute :stage_name, String
  end

  class Student_TestGrade < Sequel::Model
      set_dataset :student_test_grades
      plugin :specific_associations;# plugin :timestamps; plugin :boolean_readers; plugin :change_tracker
      many_to_one :'Student', :getter => :student_for_test_grades 
      attribute :test_grade, ClassRoom::Grade
  end

  Student.create_schema
  Student_LuckyNumber.create_schema
  Student_MiddleName.create_schema
  Student_Nickname.create_schema
  Student_StageName.create_schema
  Student_TestGrade.create_schema
end