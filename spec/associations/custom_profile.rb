# TODO: this should not be needed. I specified 'binary_data' as the role for the 'to_one' side of a many_to_one. I'm not sure why this was later singularized to binary_datum
String.inflections.irregular 'binary_data', 'binary_data'

module Custom_Profile # NCT => Not Changed Tracked
# ============= Begin class Code =============
class Code < Sequel::Model
    set_dataset :codes
    plugin :boolean_readers
    plugin :specific_associations
    complex_attribute!
    attribute :language, String
    attribute :content, String
end
# ============= End class Code =============
# ============= Begin class BinaryData =============
class BinaryData < Sequel::Model
    set_dataset :binary_datas
    plugin :boolean_readers
    plugin :specific_associations
    one_to_many :'Custom_Profile::File', :one_to_one => true, :opp_getter => :binary_data
    attribute :data, ::File
end
# ============= End class BinaryData =============
# ============= Begin class RichText =============
class RichText < Sequel::Model
    set_dataset :rich_texts
    plugin :boolean_readers
    plugin :specific_associations
    complex_attribute!
    one_to_many :'Custom_Profile::RichTextImage', :getter => :images , :composition => true
    attribute :content, String
    attribute :markup_language, String
end
# ============= End class RichText =============
# ============= Begin class File =============
class File < Sequel::Model
    set_dataset :files
    plugin :boolean_readers
    plugin :specific_associations
    complex_attribute!
    many_to_one :'Custom_Profile::BinaryData', :one_to_one => true, :getter => :binary_data, :composition => true
    attribute :filename, String
    attribute :mime_type, String
end
# ============= End class File =============
# ============= Begin class RichTextImage =============
class RichTextImage < Sequel::Model
    set_dataset :rich_text_images
    plugin :boolean_readers
    plugin :specific_associations
    many_to_one :'Custom_Profile::RichText', :getter => :rich_text , :opp_getter => :images
    #attribute :richtext, Custom_Profile::RichText
    attribute :image, Custom_Profile::File
end
# ============= End class RichTextImage =============

File.create_schema!
BinaryData.create_schema!
RichText.create_schema!
Code.create_schema!
RichTextImage.create_schema!
end
