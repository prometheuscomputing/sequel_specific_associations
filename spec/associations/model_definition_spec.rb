require 'spec_helper'

describe "domain objectwhen defining the same module multiple times" do
  before(:all) do
    load_model_definition_example
    load_model_definition_example
    load_model_definition_example
  end
  it "should correctly list associations and attributes according to the last definition" do
    DefinitionExampleModule::Person.associations.reject { |g,i| i[:hidden]}.count.should == 2
    DefinitionExampleModule::Person.attributes.count.should == 2
    DefinitionExampleModule::Person.one_to_manys.reject { |i| i[:hidden]}.count.should == 1
    DefinitionExampleModule::Person.many_to_manys.reject { |i| i[:hidden]}.count.should == 1

    DefinitionExampleModule::Child.associations.reject { |g,i| i[:hidden]}.count.should == 3
    DefinitionExampleModule::Child.attributes.count.should == 3
    
    DefinitionExampleModule::Adult.associations.reject { |g,i| i[:hidden]}.count.should == 3
    
    DefinitionExampleModule::Pet.many_to_ones.reject { |i| i[:hidden]}.count.should == 1
    DefinitionExampleModule::Pet.associations.reject { |g,i| i[:hidden]}.count.should == 1
  end
  
  it "should raise an error if you create an attribute with the name 'id'" do
    lambda {DefinitionExampleModule::BadDefinition.attribute :id, SpecificAssociations::INT_TYPE, :column => :identifier}.should raise_error(SpecificAssociations::PrimaryKeyConflict, /named 'id'/)
  end
  
  it "should raise an error if you create an attribute with the column ':id'" do
    lambda {DefinitionExampleModule::BadDefinition.attribute :identifier, SpecificAssociations::INT_TYPE, :column => :id}.should raise_error(SpecificAssociations::PrimaryKeyConflict, /with column :id/)
  end
  
  it "should raise an error if you create an attribute that conflicts with an association's column" do
    lambda {DefinitionExampleModule::BadDefinition.attribute :toy_id, SpecificAssociations::INT_TYPE}.should raise_error(SpecificAssociations::SchemaConflict, /conflicts with association named/)
  end
  
  it "should be able to handle complex attributes properly" do
    # Based off current redefinitions
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should_not be_nil
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should be_a(Hash)
    # Redefine again
    load_model_definition_example
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should_not be_nil
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should be_a(Hash)
    # Undefine and define
    undefine_definition_example_module
    # Define. This is different from other cases since the tables will already exist during this definition.
    load_model_definition_example
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should_not be_nil
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should be_a(Hash)
    load_model_definition_example
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should_not be_nil
    DefinitionExampleModule::BadDefinition.attributes[:my_file].should be_a(Hash)
  end
  
  it "should handle interface attributes properly" do
    DefinitionExampleModule::Adult.create(:name => 'bob', :identifier => '12345')
    # Based off current redefinitions
    lambda {DefinitionExampleModule::Adult.create(:name => 'phil', :identifier => '12345')}.should raise_error(SpecificAssociations::NonUniqueError)
    # Redefine again
    load_model_definition_example
    lambda {DefinitionExampleModule::Adult.create(:name => 'phil', :identifier => '12345')}.should raise_error(SpecificAssociations::NonUniqueError)
    # Undefine and define
    undefine_definition_example_module
    # Define. This is different from other cases since the tables will already exist during this definition.
    load_model_definition_example
    lambda {DefinitionExampleModule::Adult.create(:name => 'phil', :identifier => '12345')}.should raise_error(SpecificAssociations::NonUniqueError)
    load_model_definition_example
    lambda {DefinitionExampleModule::Adult.create(:name => 'phil', :identifier => '12345')}.should raise_error(SpecificAssociations::NonUniqueError)
  end
  
  it "should be able to handle the definition of another model (with the same definitions) pointed at the same table" do
    define_bad_definition2
    DefinitionExampleModule::BadDefinition2.attributes.keys.should == [:name, :my_file]
  end
end