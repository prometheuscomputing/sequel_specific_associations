module CompositionTest
  # TODO: allow modules for interfaces
  class Car < Sequel::Model
    set_dataset :compositions_cars
    plugin :specific_associations
    one_to_many :Part, :composition => true
    many_to_many :Driver, :through => :CarDriver # Non-composition association
    one_to_many :Price
    attribute :name, String
  end
  
  class Price < Sequel::Model
    set_dataset :compositions_prices
    plugin :specific_associations
    many_to_one :Car
    attribute :name, String
  end
  
  class CarDriver < Sequel::Model
    set_dataset :compositions_car_drivers
    plugin :specific_associations
    associates :Car; associates :Driver
  end
  
  class Driver < Sequel::Model
    set_dataset :compositions_drivers
    plugin :specific_associations
    many_to_many :Car, :through => :CarDriver
    attribute :name, String
  end
  
  class Part < Sequel::Model
    set_dataset :compositions_parts
    plugin :specific_associations
    many_to_one :Car
    one_to_many :Component, :composition => true
    one_to_one :Identifier, :composition => true, :holds_key => true
    attribute :name, String
  end
  
  class Component < Sequel::Model
    set_dataset :compositions_components
    plugin :specific_associations
    many_to_one :Part
    many_to_many :Color, :through => :ComponentColor
    attribute :name, String
  end
  
  class Identifier < Sequel::Model
    set_dataset :compositions_identifiers
    plugin :specific_associations
    one_to_one :Part
    attribute :name, String
    is_unique :name
  end
  
  class ComponentColor < Sequel::Model
    set_dataset :compositions_component_colors
    plugin :specific_associations
    associates :Component; associates :Color
  end
  
  class Color < Sequel::Model
    set_dataset :compositions_colors
    plugin :specific_associations
    many_to_many :Component, :through => :ComponentColor
    attribute :name, String
    should_be_unique :name
  end
end

CompositionTest::Car.create_schema
CompositionTest::Price.create_schema
CompositionTest::Part.create_schema
CompositionTest::CarDriver.create_schema
CompositionTest::Driver.create_schema
CompositionTest::Component.create_schema
CompositionTest::Identifier.create_schema
CompositionTest::ComponentColor.create_schema
CompositionTest::Color.create_schema



# Associations diagram
#
#                 Car -------------* Price
#                // \*
#               /    \
#            */       \*
#           Part      Driver
#          // \\
#         /    \
#      */       \
#  Component    Identifier
#     *|
#      |
#     *|
#   Color