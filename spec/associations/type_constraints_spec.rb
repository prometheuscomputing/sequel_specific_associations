require 'spec_helper'

describe "type constraints on associations" do
  before :each do
    reset_db
    ChangeTracker.start
    @shop       = Automotive::RepairShop.create(:name => 'Repair')
    @person     = People::Person.create(:name => 'Person')
    @mechanic   = Automotive::Mechanic.create(:name => 'Mechanic')
    @car        = Automotive::Car.create(:vehicle_model => 'Car')
    @minivan    = Automotive::Minivan.create(:vehicle_model => 'Minivan')
    @electric   = Automotive::ElectricVehicle.create(:vehicle_model => "Electric")
    @hybrid     = Automotive::HybridVehicle.create(:vehicle_model => "Hybrid")
    @motorcycle = Automotive::Motorcycle.create(:vehicle_model => 'Motorcycle')
    ChangeTracker.commit
    ChangeTracker.start
  end
  
  it 'should specify the valid types for an association' do
    Automotive::RepairShop.add_association_options :currently_working_on, :types => [:ElectricVehicle, :Motorcycle]
    Automotive::RepairShop.associations[:currently_working_on][:types].should =~ ['Automotive::ElectricVehicle', 'Automotive::Motorcycle']
    lambda{ @shop.add_currently_working_on(@car) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @shop.add_currently_working_on(@minivan) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @car.being_repaired_by = @shop }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @minivan.being_repaired_by = @shop }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    # types must explicitly include subclasses, so Hybrid is invalid
    lambda{ @shop.add_currently_working_on(@hybrid) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @hybrid.being_repaired_by = @shop }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    @shop.add_currently_working_on(@electric)
    @shop.add_currently_working_on(@motorcycle)
    ChangeTracker.commit
    @shop.currently_working_on.should =~ [@electric, @motorcycle]
    
    # Remove the modification
    Automotive::RepairShop.add_association_options :currently_working_on, :types => nil
  end
  
  it 'should propagate the constraint to subclasses' do
    # ElectricVehicles may only be owned by non-mechanics
    
    Automotive::ElectricVehicle.add_association_options :owners, :types => [:'People::Person']
    # Test property values
    Automotive::Vehicle.associations[:owners][:types].should be_nil
    Automotive::ElectricVehicle.associations[:owners][:types].should =~ ['People::Person']
    Automotive::HybridVehicle.associations[:owners][:types].should =~ ['People::Person']
    # Test constraint when adding
    lambda{ @electric.add_owner(@mechanic) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @hybrid.add_owner(@mechanic) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @mechanic.add_vehicle(@electric) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    lambda{ @mechanic.add_vehicle(@hybrid) }.should raise_error(SpecificAssociations::InvalidAssociationTypeError)
    @electric.add_owner(@person)
    @hybrid.add_owner(@person)
    # Non-electric vehicles aren't affected by constraint
    @car.add_owner(@mechanic)
    @minivan.add_owner(@mechanic)
    @motorcycle.add_owner(@mechanic)
    ChangeTracker.commit
    @person.vehicles.should =~ [@electric, @hybrid]
    @mechanic.vehicles.should =~ [@car, @minivan, @motorcycle]
    
    # Remove the modification
    Automotive::ElectricVehicle.remove_immediate_property(:owners)
  end
end