require 'spec_helper'

describe "domain object when defining custom accessors for associations" do
  before(:each) do
    reset_db
    
    @bob    = DefinitionExampleModule::Adult.create(:name => 'bob')
    @sally  = DefinitionExampleModule::Adult.create(:name => 'sally')
    
    @timmy  = DefinitionExampleModule::Child.create(:name => 'timmy')
    @martha = DefinitionExampleModule::Child.create(:name => 'martha')
    
    @toy1   = DefinitionExampleModule::Toy.create(:name => 'top')
    @toy2   = DefinitionExampleModule::Toy.create(:name => 'bike')
    
    @fido   = DefinitionExampleModule::Pet.create(:name => 'fido')
    @spot   = DefinitionExampleModule::Pet.create(:name => 'spot')
    #load_model_definition_example
    @timmy.add_toy @toy1
    @timmy.add_toy @toy2
    
    # These need to be in here because more than one test depends on them.
    DefinitionExampleModule::Person.add_association_adder(:pets) do |pet|
      pet.name += " is my pet (#{name}'s)"
      pet
    end
    DefinitionExampleModule::Person.add_association_getter_for_each(:pets) do |pet|
      pet.name += '!'
      pet
    end
  end
  
  it "should run any predefined custom getters when retrieving the association" do
    @toy1.name.should == 'top'
    @toy2.name.should == 'bike'
    
    toy_list = @timmy.toys
    toy_list.count.should == 2
    toy_list[0].name.should == "my top (timmy's)"
    toy_list[1].name.should == "my bike (timmy's)"
  end
  
  context "when defining or redefining custom hooks on associations" do
    
    it "should allow the addition of a custom_adder" do
      pet = @bob.add_pet(@fido)
      pet.name.should == "fido is my pet (bob's)"
      # Test Child subclass
      pet2 = @timmy.add_pet(@spot)
      pet2.name.should == "spot is my pet (timmy's)"
    end
    
    it "should allow the addition of a custom_remover" do
      DefinitionExampleModule::Person.add_association_remover(:pets) do |pet|
        val = (pet.name =~ /^(.*) is my pet \(bob\'s\)$/)
        actual_name = $1
        pet.name = actual_name
        pet
      end
      pet = @bob.add_pet(@fido)
      pet.name.should == "fido is my pet (bob's)"
      pet = @bob.remove_pet(pet)
      pet.name.should == 'fido'
    end

    it "should allow the addition of a custom_getter_for_each" do
      pet = @bob.add_pet(@fido)
      pet.name.should == "fido is my pet (bob's)"
      @bob.pets.first.name.should == "fido is my pet (bob's)!"
    end
    
    it "should allow the addition of a custom_getter" do
      DefinitionExampleModule::Person.add_association_getter(:pets) do |pets|
        pets.collect { |p| p.name += '?'; p }
      end
      pet = @bob.add_pet(@fido)
      pet.name.should == "fido is my pet (bob's)"
      @bob.pets.first.name.should == "fido is my pet (bob's)!?"
    end
    
    it "should allow the addition of a custom_setter_for_each" do
      DefinitionExampleModule::Adult.add_association_setter_for_each(:dependents) do |d|
        d.name = name + '\'s dependent: ' + d.name
        d
      end
      @bob.dependents = [@timmy, @martha]
      @timmy.name.should == "bob's dependent: timmy"
      @martha.name.should == "bob's dependent: martha"
    end
    
    it "should allow the addition of a custom_adder to a subclass of a defined association" do
      DefinitionExampleModule::Person.add_association_adder(:pets) do |pet|
        pet
      end
      DefinitionExampleModule::Child.add_association_adder(:pets) do |pet|
        pet.name += " is the pet of the child named #{name}"
        pet
      end
      # Test that parent's custom adder is unchanged
      pet = @bob.add_pet(@fido)
      pet.name.should == "fido"
      # Test Child subclass
      pet2 = @timmy.add_pet(@spot)
      pet2.name.should == "spot is the pet of the child named timmy"
      
      # Remove child customizations on pets association (so as not to interfere with other tests)
      DefinitionExampleModule::Child.remove_immediate_property(:pets)
      # Test removal
      pet3 = @timmy.class[@timmy.id].add_pet(DefinitionExampleModule::Pet.create(:name => 'rover'))
      pet3.name.should == "rover"
    end
    
    it "should allow different custom adders to be defined for different classes " do
      DefinitionExampleModule::Person.add_association_adder(:pets) do |pet|
        pet.name += " is my pet (#{name}'s)"
        pet
      end
      DefinitionExampleModule::Child.add_association_adder(:pets) do |pet|
        pet.name += " is the pet of the child named #{name}"
        pet
      end
      # Test that parent's custom adder is unchanged by Child custom adder
      pet = @bob.add_pet(@fido)
      pet.name.should == "fido is my pet (bob's)"
      # Test Child subclass
      pet2 = @timmy.add_pet(@spot)
      pet2.name.should == "spot is the pet of the child named timmy"
      
      # Remove child customizations on pets association (so as not to interfere with other tests)
      DefinitionExampleModule::Child.remove_immediate_property(:pets)
      # Test removal
      pet3 = @timmy.class[@timmy.id].add_pet(DefinitionExampleModule::Pet.create(:name => 'rover'))
      pet3.name.should == "rover is my pet (timmy's)"
    end
  end
end