require 'spec_helper'

describe "domain object when inspecting associations" do

  it "should return correct information about all of the Model's associations and their types" do
    
    sys_associations = System.associations
    
    # Test many to ones
    description_association_info = sys_associations[:actor_super]
    description_association_info[:getter].to_s.should == "actor_super"
    description_association_info[:opp_getter].to_s.should == "actor_subs"
    description_association_info[:label].should be_nil
    description_association_info[:class].should == "UseCaseMetaModelV4::Actor"
    description_association_info[:type].should == :many_to_one
    
    opp_klass = eval(description_association_info[:class])
    opp_info = opp_klass.associations[description_association_info[:opp_getter].to_sym]
    opp_info[:getter].should == :actor_subs
    opp_info[:opp_getter].should == :actor_super
    opp_info[:label].should be_nil
    opp_info[:class].should == "UseCaseMetaModelV4::Actor"
    opp_info[:type].should == :one_to_many
    
    # Test one to manys
    usecases_association_info = sys_associations[:use_cases]
    usecases_association_info[:getter].should == :use_cases
    usecases_association_info[:opp_getter].should == :system
    usecases_association_info[:label].should be_nil
    usecases_association_info[:class].should == "UseCaseMetaModelV4::UseCase"
    usecases_association_info[:type].should == :one_to_many
    
    opp_klass = eval(usecases_association_info[:class])
    opp_info = opp_klass.associations[usecases_association_info[:opp_getter].to_sym]
    opp_info[:getter].should == :system
    opp_info[:opp_getter].should == :use_cases
    opp_info[:label].should be_nil
    opp_info[:class].should == "UseCaseMetaModelV4::System"
    opp_info[:type].should == :many_to_one

    # Test many to manys
    domains_association_info = sys_associations[:"domains"]
    domains_association_info[:getter].should == :domains
    domains_association_info[:opp_getter].should == :systems
    domains_association_info[:label].should be_nil
    domains_association_info[:through].should == "UseCaseMetaModelV4::Domain_System_Association"
    domains_association_info[:class].should == "UseCaseMetaModelV4::Domain"
    domains_association_info[:type].should == :many_to_many
  
    opp_klass = eval(domains_association_info[:class])
    opp_info = opp_klass.associations[domains_association_info[:opp_getter].to_sym]
    opp_info[:getter].should == :systems
    opp_info[:opp_getter].should == :domains
    opp_info[:label].should be_nil
    opp_info[:through].should == "UseCaseMetaModelV4::Domain_System_Association"
    opp_info[:class].should == "UseCaseMetaModelV4::System"
    opp_info[:type].should == :many_to_many

    step_associations = Step.associations
    
    # Test one to ones
    invocation_association_info = step_associations[:invocation]
    invocation_association_info[:getter].should == :invocation
    invocation_association_info[:opp_getter].should == :step
    invocation_association_info[:label].should be_nil
    invocation_association_info[:class].should == "UseCaseMetaModelV4::Invocation"
    invocation_association_info[:type].should == :one_to_one
    expect(invocation_association_info[:holds_key]).to be_truthy
    
    opp_klass = eval(invocation_association_info[:class])
    opp_info = opp_klass.associations[invocation_association_info[:opp_getter].to_sym]
    opp_info[:getter].should == :step
    opp_info[:opp_getter].should == :invocation
    opp_info[:label].should be_nil
    opp_info[:class].should == "UseCaseMetaModelV4::Step"
    opp_info[:type].should == :one_to_one
    
  end
  
  it "should return information about an association via the info and opp_info methods" do
    v_info = Automotive::Mechanic.new.vehicles_info
    v_info[:as].should == 'People::Person'
    v_info[:class].should == 'Automotive::Vehicle'
    v_info[:through].should == 'People::Ownership'
    v_info[:getter].should == :vehicles
    v_info[:singular_getter].should == :vehicle
    v_info[:opp_getter].should == :owners
    v_info[:singular_opp_getter].should == :owner
    v_info[:type].should == :many_to_many
    
    # Check opp_info method
    Automotive::Car.new.owners_opp_info.should == v_info
  end
  
  it "should place additional unknown association options into the informational hash" do
    sys_actor_super = System.associations[:actor_super]
    sys_actor_super[:foo].should == :bar
  end
end
