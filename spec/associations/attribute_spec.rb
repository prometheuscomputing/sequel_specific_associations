require 'spec_helper'
# Using uniqueness example

describe "domain object when defining attributes" do
  before(:each) do
    Application::TestPerson.delete
    Application::TestIdentifier.delete
    Application::TestEmployee.delete
    Application::TestEmployer.delete
    Application::TestDog.delete
    Application::TestBark.delete
    Application::TestCoord.delete
  end

  it "create_schema should create a schema based off the defined attributes" do
    # first_name and last_name were defined via #attribute in the Application::TestPerson model, and then create_schema was called
    p = Application::TestPerson.create(:first_name => 'bob', :last_name => 'cat')
    p.first_name.should == 'bob'
    p.last_name.should == 'cat'
  end
  
  it "should allow inheritance of attributes for models" do
    # first_name and last_name attributes are inherited from their definitions in Application::TestPerson and only need to be defined once
    e = Application::TestEmployee.create(:first_name => 'snow', :last_name => 'leopard', :employee_number => 1)
    e.first_name.should == 'snow'
    e.last_name.should == 'leopard'
    e.employee_number.should == 1
    # Test method assignment for column
    e.nickname = 'Snowy'
    e.save
    e.nickname.should == 'Snowy'
  end
  
  it "should maintain a consistent definition for an attribute by not copying the definition to a subclass, but instead referencing the parent class' definition" do
    # Application::TestEmployee has 4 unique attributes plus 3 inherited from Application::TestPerson

    # Application::TestEmployee.attributes will return the combined number of attributes (local and inherited)
    attrs = Application::TestEmployee.attributes
    attrs.count.should == 9
    # Get the Application::TestEmployee-specific attributes
    attrs = Application::TestEmployee.attributes(false)
    # NOTE: can't check number of attributes listed, since that is modified by another test
    attrs.keys.should include(:employee_number)
    attrs.keys.should include(:last_name)
    attrs.keys.should include(:nickname)
    attrs.keys.should include(:id_string)
    attrs.keys.should include(:long_id_string)
    # Get the Application::TestPerson attributes
    attrs = Application::TestPerson.attributes(false)
    attrs.count.should == 4
  end
  
  it "should allow redefinition of an attribute with different options by a subclass" do
    Application::TestPerson.attributes[:last_name][:foobar].should be_nil
    Application::TestEmployee.attributes[:last_name][:foobar].should == true
  end
  
  it "should allow the addition of options to a subclass that do not affect the superclass" do
    Application::TestEmployee.add_attribute_options(:username, :foobar => true)
    Application::TestPerson.attributes[:username][:foobar].should be_nil
    Application::TestEmployee.attributes[:username][:foobar].should == true
  end
end

describe "domain object when defining attributes with getter/setter blocks" do
  before(:each) do
    Application::TestPerson.delete
    Application::TestIdentifier.delete
    Application::TestEmployee.delete
    Application::TestDog.delete
    Application::TestBark.delete
    Application::TestCoord.delete
  end
  
  it "should run the given getter block when retrieving the attribute value" do
    p = Application::TestPerson.create(:first_name => 'bob', :last_name => 'cat')
    p.name.should == 'bob cat'
  end
  
  it "should run the given setter block when setting the attribute value" do
    p = Application::TestPerson.create(:first_name => 'bob', :last_name => 'cat')
    p.name = 'New Name'
    p.first_name.should == 'New'
    p.last_name.should == 'Name'
  end
  
  it "should allow the derived getter/setter code to be inherited by child models" do
    e = Application::TestEmployee.create(:first_name => 'test', :last_name => 'employee', :employee_number => 1)
    e.name.should == 'test employee'
    e.name = 'New Name'
    e.first_name.should == 'New'
    e.last_name.should == 'Name'
  end
  
  it "should allow attributes to specify an alternate column for storage" do
    e = Application::TestEmployee.create(:first_name => 'test', :last_name => 'employee', :employee_number => 1)
    e.id_string.should == '1_employee'
    e.columns.should_not include(:id_string)
    e.columns.should include(:derived_id_string)
    e[:derived_id_string].should == '1_employee'
  end
  
  it "should allow the option not to store the value of a derived attribute" do
    e = Application::TestEmployee.create(:first_name => 'test', :last_name => 'employee', :employee_number => 1, :nickname => 'nick')
    e.long_id_string.should == '1testemployeenick'
    e.columns.should_not include(:long_id_string)
  end
  
  it "should allow the later definition or redefinition of getter/setter methods on attributes" do
    Application::TestEmployee.send(:eval, "add_attribute_getter(:employee_number) { |v| v == 42 ? v + 100 : v}")
    # class Application::TestEmployee
    #   add_attribute_getter(:employee_number) do |value|
    #     value == 42 ? : value + 100 : value
    #   end
    # end
    # Test new getter
    e = Application::TestEmployee.create(:first_name => 'test', :last_name => 'employee', :employee_number => 42)
    e.employee_number.should == 142
    e[:employee_number].should == 42 # Did not save_on_get
    e2 = Application::TestEmployee.create(:first_name => 'another', :last_name => 'employee', :employee_number => 43)
    e2.employee_number.should == 43
    e2[:employee_number].should == 43
    
    #lambda {e.employee_number = 'foo1'}.should raise_error
    e.employee_number = 2
    e.employee_number.should == 2
    Application::TestEmployee.send(:eval, "add_attribute_setter(:employee_number) { |v| v.to_i + 1 }")
    e.employee_number = 2
    e.employee_number.should == 3
    e.save
    
    # Test redefinition of :employee_number getter/setter
    Application::TestEmployee.send(:eval, "add_attribute_setter(:employee_number) { |v| v}") # Back to default
    Application::TestEmployee.send(:eval, "add_attribute_getter(:employee_number) { |v| v == 314 ? v + 100 : v}")
    e = Application::TestEmployee.create(:first_name => 'new', :last_name => 'guy', :employee_number => 42)
    e.employee_number.should == 42
    e = Application::TestEmployee.create(:first_name => 'another', :last_name => 'guy', :employee_number => 314)
    e.employee_number.should == 414
  end
  
  it "should allow attribute options to be changed later" do
    # Remove the custom getter/setter so that they won't affect other tests
    Application::TestEmployee.send(:eval, "add_attribute_options(:employee_number, :custom_setter => nil)")
    e = Application::TestEmployee.create(:first_name => 'test', :last_name => 'employee', :employee_number => 42)
    e.employee_number.should == 42
    e.employee_number = 5
    e.employee_number.should == 5
  end
  
  it "should correctly interpret the scope of the getter/setter block" do
    Application::TestEmployee.send(:eval, "add_attribute_getter(:employee_number) { |v| 
      is_a?(Application::TestEmployee).should == true
      v}")
    lambda {Application::TestEmployee.create(:employee_number => 1).employee_number}.should_not raise_error
    
    # Now test namespacing with modules... currently not working
    Work::WorkEmployee.send(:eval, "
      add_attribute_setter(:employer_name) do |name|
        # Should be in Work::WorkEmployee scope here, so don't need 'Work::' namespacing
        self.should be_a(WorkEmployee)
        self.should be_a(Home::HomePerson) # Should inherit from Home::HomePerson
        WorkEmployer.filter(:name => name).all.should_not be_empty
      end
    ")
    Work::WorkEmployer.create(:first_name => 'Ebenezer', :last_name => 'Scrooge')
    bob = Work::WorkEmployee.create(:first_name => 'Bob', :last_name => 'Cratchit')
    
    pending "finding a way to correctly interpret constants from an external block definition"
    # Could use the 'sourcify' gem and call passed block like so: instance_eval(getter_block.to_source).call but this could cause instability or other complications.
    lambda {bob.employer_name = 'Ebenezer Scrooge'}.should_not raise_error
  end
  
  it "should return an informational hash on a attribute" do
    first_name_attr = Application::TestPerson.attributes[:first_name]
    first_name_attr.should be_a(Hash)
    first_name_attr[:getter].should == :first_name
    first_name_attr[:column].should == :first_name
  end
  
  it "should not include accidentally specified association columns in the attributes hash" do
    DefinitionExampleModule::BadDefinition.attributes.keys.should_not include(:toy_id)
  end
  
  it "should not include the default primary key in the attributes has" do
    DefinitionExampleModule::BadDefinition.attributes.keys.should_not include(:id)
  end
  
  it "should place unknown additional options from an attribute's definition into the informational hash" do
    first_name_attr = Application::TestPerson.attributes[:first_name]
    first_name_attr[:test].should == 'wombat'
    first_name_attr[:foo].should == :bar
  end
  
  it "should return the type of attributes when #<attribute_name>_type is called" do
    p = Application::TestPerson.create
    p.first_name_type.should == [String]
    e = Application::TestEmployee.create
    e.first_name_type.should == [String]
  end
end