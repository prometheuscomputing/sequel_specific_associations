require 'spec_helper'

describe "acyclic association functionality" do
  before(:all) do
    reset_db
  end
  
  it "setting :acyclic => true should disallow cyclic associations" do
    # Setup initial state
    ChangeTracker.commit { 
      @p1 = Automotive::Part.create(:name => 'Engine')
      @p2 = Automotive::Part.create(:name => 'Battery')
      @p3 = Automotive::Part.create(:name => 'Terminal')
      @p4 = Automotive::Part.create(:name => 'Wombat')
      @p1.add_sub_part(@p2)
      @p2.add_sub_part(@p3)
    }
    @p1.sub_parts.should =~ [@p2]
    @p2.sub_parts.should =~ [@p3]
    
    # Test acyclic behavior
    # Any of these additions should be considered cyclic and disallowed
    ChangeTracker.start
    lambda{ @p3.add_sub_part(@p1) }.should raise_error(Sequel::Error, /cyclic/)
    lambda{ @p3.add_sub_part(@p2) }.should raise_error(Sequel::Error, /cyclic/)
    lambda{ @p3.add_sub_part(@p3) }.should raise_error(Sequel::Error, /cyclic/)
    lambda{ @p1.part_of = @p3 }.should raise_error(Sequel::Error, /cyclic/)
    lambda{ @p1.part_of = @p2 }.should raise_error(Sequel::Error, /cyclic/)
    lambda{ @p1.part_of = @p1 }.should raise_error(Sequel::Error, /cyclic/)
    ChangeTracker.commit.should == false # No changes should have occurred
    
    # Test unassociated getters
    pending 'acyclic check for unassociated getters'
    @p1.sub_parts_unassociated.should =~ [@p4]
    @p2.sub_parts_unassociated.should =~ [@p4]
    @p3.sub_parts_unassociated.should =~ [@p4]
    
    # TODO: test without SCT -SD
  end
  
  # TODO: set up and test many-to-many acyclic association
end