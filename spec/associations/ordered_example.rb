module Application
  class Library < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    attribute :created_at_string, String
    one_to_many :Readable, :order => true, :getter => :books
    many_to_many :Patron, :order => true, :through => :PatronStatusAtLibrary
  end

  class OnlineLibrary < Library
    attribute :url, String
  end

  class PatronStatusAtLibrary < Sequel::Model
    plugin :specific_associations
    attribute :status, String
    attribute :created_at_string, String
    associates :Library, :is_ordered => true; associates :Patron, :is_ordered => true
  end

  class Readable < Sequel::Model
    plugin :specific_associations
    attribute :title, String
    # We forgot to specify opp_getter initially, and we have made another change that requires it to be correct.
    # We are leaving it in place to ensure backwards compatibilty.
    many_to_one :Library, :is_ordered => true#, :opp_getter => :books

    many_to_many :ESeries, :order => true, :is_ordered => true, :through => :EBooksInESeries, :opp_getter => :books, :getter => :series
  end

  class EBook < Readable
    plugin :specific_associations
  end

  class Dictionary < Readable
    set_dataset :dictionaries
  end

  class Patron < Sequel::Model
    plugin :specific_associations
    attribute :first_name, String
    attribute :created_at_string, String
    many_to_many :Library, :is_ordered => true, :through => :PatronStatusAtLibrary
  end

  class EBooksInESeries < Sequel::Model
    plugin :specific_associations
    association_class!
    associates :Readable => :book, :is_ordered => true; associates :ESeries => :series, :is_ordered => true
  end

  class ESeries < Sequel::Model
    plugin :specific_associations
    attribute :label, String
    many_to_many :Readable, :order => true, :is_ordered => true, :through => :EBooksInESeries, :opp_getter => :series, :getter => :books
  end
end

Application::Library.create_schema
Application::OnlineLibrary.create_schema
Application::PatronStatusAtLibrary.create_schema
Application::Readable.create_schema
Application::EBook.create_schema
Application::Dictionary.create_schema
Application::Patron.create_schema
Application::EBooksInESeries.create_schema
Application::ESeries.create_schema