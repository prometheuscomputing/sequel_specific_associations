require 'spec_helper'

describe "attribue aliases" do
  context "when filtering associations where the types in the association have not attributes in common" do
    before(:each) do
      reset_db
      # Setting up many-to-many polymorphic
      @student  = ArtClass::Student.create(:name => "Rincewind")
      @brush    = ArtClass::Brush.create(:style  => "Fine", :softness => 'soft')
      @canvas   = ArtClass::Substrate.create(:name => "Canvas", :hardness => 'hard')
      @paint    = ArtClass::Paint.create(:color => "Octorene", :consistency => 'thin')
      @brush2   = ArtClass::Brush.create(:style  => "Fine", :softness => 'nice')
      @canvas2  = ArtClass::Substrate.create(:name => "Fine", :hardness => 'nice')
      @paint2   = ArtClass::Paint.create(:color => "Canvas", :consistency => 'nice')
      @student.add_material(@brush)
      @student.add_material(@canvas)
      @student.add_material(@paint)
      @student.materials.should =~ [@brush, @canvas, @paint]

      @paint.viscosity = ArtClass::ThicknessRange.new(:min => '5', :max => '50', :units => 'thou')
      @brush.bristle_thickness = ArtClass::ThicknessRange.new(:min => '10', :max => '20', :units => 'micron')
      @canvas.thickness = ArtClass::ThicknessRange.new(:min => '100', :max => '1000', :units => 'thou')

      @paint2.viscosity = ArtClass::ThicknessRange.new(:min => '5', :max => '50', :units => 'thou')
      @brush2.bristle_thickness = ArtClass::ThicknessRange.new(:min => '10', :max => '20', :units => 'micron')
      @canvas2.thickness = ArtClass::ThicknessRange.new(:min => '100', :max => '1000', :units => 'thou')
      
      @pencil = ArtClass::Pencil.create(:weight => "#2")
      @pencil.thickness = ArtClass::ThicknessRange.new(:min => '400', :max => '500', :units => 'micron')
      @pencil.usable_length = ArtClass::NumericalRange.new(:low => '77', :high => '999')
      
      @student.add_pencil(@pencil)
      @student.pencils.should == [@pencil]
    end

    it "should leave the aliased property's methods intact" do
      @brush.style.should == 'Fine'
      # @brush.style_count.should == 1 # Dang, you gotta implement that too...
      @brush.style = 'Coarse'
      @brush.save
      @brush.style.should == 'Coarse'
    end

    it "should be able to get and set attributes by calling the alias" do
      @brush.name.should == 'Fine'
      # @brush.name_count.should == 1 # Dang, you gotta implement that too...
      @brush.name = 'Coarse'
      @brush.save
      @brush.name.should == 'Coarse'
      @brush.style.should == 'Coarse'
    end

    it "should not interfere with unfiltered associations" do
      @student.materials.should =~ [@brush, @canvas, @paint]
      @student.materials_unassociated.should =~ [@brush2, @canvas2, @paint2, @pencil]
    end

    it "should be able to filter on aliased getters" do
      @student.materials(:name => 'Octorene').should == [@paint]
      @student.materials(:name => 'Canvas').should == [@canvas]
      @student.materials(:name => 'Fine').should == [@brush]
      @student.materials(:name => 'Nameless').should == []
      @student.materials(:name => nil).should == []
      @student.materials_unassociated(:name => 'Octorene').should == []
      @student.materials_unassociated(:name => nil).should == []
      @student.materials_unassociated(:name => 'Fine').should =~ [@brush2, @canvas2]
    end

    it "should be able to filter on aliased getters for complex attributes" do
      @student.materials(:t_range_min => '10').should =~ [@brush]
      @student.materials(Sequel.ilike(:t_range_min, '10%')).should =~ [@canvas, @brush]
      @student.materials_unassociated(:t_range_min => '10').should =~ [@brush2]
      @student.materials_unassociated(Sequel.ilike(:t_range_min, '10%')).should =~ [@brush2, @canvas2]
    end
    
    it "should be able to filter on aliased columns for complex attributes" do
      @student.pencils(:usable_length_low => 77).should =~ [@pencil]
      @student.pencils(:at_least => 77).should =~ [@pencil]
      @student.materials_unassociated(:at_least => 77).should =~ [@pencil]
    end
    
    it "should be able to invoke custom getters on aliased getters" do
      @student.name.should == 'Rincewind'
      @student.eman.should == 'Rincewind'
      @student.class.class_eval do
        add_attribute_getter(:eman) { |n| n.reverse }
      end
      @student.name.should == 'Rincewind'
      @student.eman.should == 'Rincewind'.reverse
    end
    
    it "should be able to invoke custom getters on aliased columns" do
      @pencil.usable_length_low.should == 77
      @pencil.at_least.should == 77
      @pencil.class.class_eval do
        add_attribute_getter(:at_least) { |val| val = val * (-1) }
      end
      @pencil.usable_length_low.should == 77
      @pencil.at_least.should == -77
    end
    
  end
end
