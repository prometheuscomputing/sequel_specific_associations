require 'spec_helper'

describe "domain object for a one to one" do
  # ============================================================
  # BEGIN polymorphic-to-polymorphic one-to-one section
  # ============================================================
  context "polymorphic-to-polymorphic relationship -" do
    context "setting the element" do
      before :each do
        Application::Person.delete; Application::SmartDog.delete; Application::Frisbee.delete; Application::Disc.delete; Actor.delete
        @person   = Application::Person.create
        @smartdog = Application::SmartDog.create
        @frisbee  = Application::Frisbee.create
        @disc     = Application::Disc.create
        @actor    = Actor.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = @person.method(:throwable=).call(@frisbee)
      #   #result = (@person.throwable = @frisbee)
      #   result.should be_an_instance_of(Frisbee)
      #   result = @frisbee.method(:thrower=).call(@person)
      #   #result = (@frisbee.thrower = @person)
      #   result.should be_an_instance_of(Frisbee)
      # end
      # 
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end
    
      it "should add the element to the association via the first class" do
        @person.throwable = @frisbee
        # @person.save; @frisbee.save
        (@person.throwable == @frisbee).should == true
        (@frisbee.thrower == @person).should == true
        @person.throwable = nil
        # @person.save; @frisbee.save
        @smartdog.throwable = @disc
        # @smartdog.save; @disc.save
        (@smartdog.throwable == @disc).should == true
        (@disc.thrower == @smartdog).should == true
      end
      
      it "should add the element to the association via the second class" do
        @frisbee.thrower = @person
        # @frisbee.save; @person.save
        (@person.throwable == @frisbee).should == true
        (@frisbee.thrower == @person).should == true
        @frisbee.thrower = nil
        # @frisbee.save; @person.save
        @disc.thrower = @smartdog
        # @disc.save; @smartdog.save
        (@smartdog.throwable == @disc).should == true
        (@disc.thrower == @smartdog).should == true
      end
    
      it "should reject an element that is not of the polymorphic type" do
        # Actor is not of type Throwable or of type Thrower
        lambda {@person.throwable = @actor
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @person.throwable.should be_nil
        lambda {@frisbee.thrower = @actor
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @frisbee.thrower.should be_nil
        lambda {@person.throwable = @smartdog
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @person.throwable.should be_nil
        lambda {@frisbee.thrower = @disc
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @frisbee.thrower.should be_nil
      end
    end
  
    context "removing the association" do
      before :each do
        Application::Person.delete; Application::SmartDog.delete; Application::Frisbee.delete; Application::Disc.delete; Actor.delete
        @person = Application::Person.create
        @smartdog = Application::SmartDog.create
        @frisbee = Application::Frisbee.create
        @disc = Application::Disc.create
        @actor = Actor.create # Unrelated class for exclusion testing
      
        @person.throwable = @frisbee
        # @person.save; @frisbee.save
        @smartdog.throwable = @disc
        # @smartdog.save; @disc.save
        (@person.throwable == @frisbee).should == true
        (@smartdog.throwable == @disc).should == true
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = @person.remove_throwable(@frisbee)
      #   result.should be_an_instance_of(Frisbee)
      #   result = @disc.remove_thrower(@smartdog)
      #   result.should be_an_instance_of(Disc)
      # end
    
      it "should remove the association via the first class" do
        @person.throwable = nil
        @person.throwable.should be_nil
        @frisbee.refresh
        @frisbee.thrower.should be_nil
        @smartdog.throwable = nil
        @disc.refresh
        @smartdog.throwable.should be_nil
        @disc.thrower.should be_nil
      end
    
      it "should remove the association via the second class" do
        @frisbee.thrower = nil
        @person.throwable.should be_nil
        @frisbee.thrower.should be_nil
        @disc.thrower = nil
        @smartdog.throwable.should be_nil
        @disc.thrower.should be_nil
      end
    
      it "should allow reassociation of classes after removing their association" do
        @person.throwable = nil
        @person.throwable = @frisbee
        @person.refresh; @frisbee.refresh
        (@person.throwable == @frisbee).should == true
        (@frisbee.thrower == @person).should == true
        @person.throwable = nil
        @person.throwable = @disc
        @person.refresh; @disc.refresh; @frisbee.refresh
        (@person.throwable == @disc).should == true
        (@disc.thrower == @person).should == true
        @frisbee.thrower.should be_nil
        @disc.thrower = nil
        @disc.thrower = @smartdog
        @disc.refresh; @smartdog.refresh; @person.refresh
        (@smartdog.throwable == @disc).should == true
        (@disc.thrower == @smartdog).should == true
        @person.throwable.should be_nil      
      end
    end
  
    context "retrieving the element" do
      before :each do
        Application::Person.delete; Application::SmartDog.delete; Application::Frisbee.delete; Application::Disc.delete; Actor.delete
        @person = Application::Person.create
        @smartdog = Application::SmartDog.create
        @unused_person = Application::Person.create
        
        @frisbee = Application::Frisbee.create
        @disc = Application::Disc.create
        @unused_disc = Application::Disc.create
        
        @actor = Actor.create # Unrelated class for exclusion testing
      
        @person.throwable = @frisbee
        @smartdog.throwable = @disc
        # @person.save; @frisbee.save; @smartdog.save; @disc.save
      end
  
      it "should return the associated element via the first class" do
        (@person.throwable == @frisbee).should == true
        (@smartdog.throwable == @disc).should == true
      end
    
      it "should return the associated element via the second class" do
        (@frisbee.thrower == @person).should == true
        (@disc.thrower == @smartdog).should == true
      end
      
      it "should ignore invalid association data when retrieving the association" do
        # Check that columns are as we expect
        @frisbee[:thrower_id].should == @person.id
        @frisbee[:thrower_class].should == "Application::Person"
        @disc[:thrower_id].should == @smartdog.id
        @disc[:thrower_class].should == "Application::SmartDog"
        
        # Mess up the association's data
        @frisbee[:thrower_id] = nil
        @frisbee.save
        @disc[:thrower_class] = nil
        @disc.save
        
        # Check that the associations evaluate to nil
        @disc.thrower.should be_nil
        @person.throwable.should be_nil
        @frisbee.thrower.should be_nil
        @smartdog.throwable.should be_nil
      end
      
      it "should be able to count the currently associated and unassociated objects" do
        # Test first to-one side
        @person.throwable_count.should == 1
        @person.throwable_unassociated_count.should == 2
        @smartdog.throwable_count.should == 1
        @smartdog.throwable_unassociated_count.should == 2
        @unused_person.throwable_count.should == 0
        @unused_person.throwable_unassociated_count.should == 3
        
        # Test second to-one side
        @frisbee.thrower_count.should == 1
        @frisbee.thrower_unassociated_count.should == 2
        @disc.thrower_count.should == 1
        @disc.thrower_unassociated_count.should == 2
        @unused_disc.thrower_count.should == 0
        @unused_disc.thrower_unassociated_count.should == 3
      end
    end
  end
  # ============================================================
  # END polymorphic-to-polymorphic one-to-one section
  # ============================================================
  
  
  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic one-to-one section
  # ============================================================
  context "polymorphic-to-nonpolymorphic relationship -" do
    context "adding an element" do
      before :each do
        UseCaseMetaModelV4::Invocation.delete; UseCaseMetaModelV4::Step.delete; UseCaseMetaModelV4::StepGroup.delete; UseCaseMetaModelV4::Scenario.delete
        @invocation = UseCaseMetaModelV4::Invocation.create
        @invocation2 = UseCaseMetaModelV4::Invocation.create
        @step = UseCaseMetaModelV4::Step.create
        @step_group = UseCaseMetaModelV4::StepGroup.create
        @scenario = UseCaseMetaModelV4::Scenario.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = @step.method(:invocation=).call(@invocation)
      #   result.should be_an_instance_of(UseCaseMetaModelV4::Step)
      #   result = @invocation.method(:step=).call(@step)
      #   result.should be_an_instance_of(UseCaseMetaModelV4::Step)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end
    
      it "should set the element for the association via the first class" do
        @step.invocation = @invocation
        # @step.save; @invocation.save
        (@step.invocation == @invocation).should == true
        (@invocation.step == @step).should == true
        @step_group.invocation = @invocation2
        # @step_group.save; @invocation2.save
        (@step_group.invocation == @invocation2).should == true
        (@invocation2.step == @step_group).should == true
      end

      it "should set the element for the association via the second class" do
        @invocation.step = @step
        # @invocation.save; @step.save
        (@step.invocation == @invocation).should == true
        (@invocation.step == @step).should == true
        @invocation2.step = @step_group
        # @invocation2.save; @step_group.save
        (@step_group.invocation == @invocation2).should == true
        (@invocation2.step == @step_group).should == true
      end

      it "should reject an element that is not of the polymorphic type" do
        # UseCaseMetaModelV4::Scenario is not of type UseCaseMetaModelV4::Step or UseCaseMetaModelV4::Invocation
        lambda {@invocation.step = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @invocation.step.should be_nil
        lambda {@step.invocation = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.invocation.should be_nil
        lambda {@invocation.step = @invocation
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @invocation.step.should be_nil
        lambda {@step.invocation = @step_group
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @step.invocation.should be_nil
      end
    end

    context "removing the association" do
      before :each do
        UseCaseMetaModelV4::Invocation.delete; UseCaseMetaModelV4::Step.delete; UseCaseMetaModelV4::StepGroup.delete; UseCaseMetaModelV4::Scenario.delete
        @invocation = UseCaseMetaModelV4::Invocation.create
        @invocation2 = UseCaseMetaModelV4::Invocation.create
        @step = UseCaseMetaModelV4::Step.create
        @step_group = UseCaseMetaModelV4::StepGroup.create
        @scenario = UseCaseMetaModelV4::Scenario.create # Unrelated class for exclusion testing

        @invocation.step = @step
        # @invocation.save; @step.save
        @invocation.step.should == @step
        @step.invocation.should == @invocation
        @invocation2.step = @step_group
        # @step_group.save; @invocation2.save
        @invocation2.step.should == @step_group
        @step_group.invocation.should == @invocation2        
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = @invocation.step = nil
      #   result.should be_an_instance_of(UseCaseMetaModelV4::Step)
      #   result = @step_group.remove_invocation(@invocation2)
      #   result.should be_an_instance_of(UseCaseMetaModelV4::StepGroup)
      # end

      it "should remove the association via the first class" do
        @invocation.step = nil
        @invocation.refresh; @step.refresh
        @invocation.step.should be_nil
        @step.invocation.should be_nil
      end

      it "should remove the association via the second class" do
        @step.invocation = nil
        @step.refresh; @invocation.refresh
        @step.invocation.should be_nil
        @invocation.step.should be_nil
      end
    
      it "should allow reassociation if the association is removed" do
        @invocation.step = nil
        @invocation.refresh; @step.refresh
        @step.invocation.should be_nil
        @invocation.step.should be_nil
        # Set the new association
        @invocation.step = @step_group
        @invocation.refresh; @step_group.refresh
        (@invocation.step == @step_group).should == true
        (@step_group.invocation == @invocation).should == true
      end
    end

    context "retrieving the element" do
      before :each do
        UseCaseMetaModelV4::Invocation.delete; UseCaseMetaModelV4::Step.delete; UseCaseMetaModelV4::StepGroup.delete; UseCaseMetaModelV4::Scenario.delete
        @invocation = UseCaseMetaModelV4::Invocation.create
        @step = UseCaseMetaModelV4::Step.create
        @step_group = UseCaseMetaModelV4::StepGroup.create
        @scenario = UseCaseMetaModelV4::Scenario.create # Unrelated class for exclusion testing

        @invocation.step = @step
        # @invocation.save; @step.save
        (@invocation.step == @step).should == true
      end

      it "should return the associated element via the first class" do
        (@invocation.step == @step).should == true
      end

      it "should return the associated element via the second class" do
        (@step.invocation == @invocation).should == true
      end
      
      it "should ignore invalid association data when retrieving the association" do
        # Check that the column is as we expect
        @step[:invocation_id].should == @invocation.id
        
        # Mess up the association's data
        @step[:invocation_id] = nil
        @step.save
        
        # Check that the associations evaluate to nil
        @step.invocation.should be_nil
        @invocation.step.should be_nil
      end
    end
    
    context "multilevel operations" do
      before :each do
        UseCaseMetaModelV4::Codeable.delete; UseCaseMetaModelV4::Code.delete; UseCaseMetaModelV4::RichText.delete
      end
      it "should allow single-level operations" do
        
        c = UseCaseMetaModelV4::Codeable.create
        code_obj = UseCaseMetaModelV4::Code.create
        c.code = code_obj
        r = UseCaseMetaModelV4::RichText.create(:content => '# DO STUFF', :markup_language => 'Ruby')
        #code_obj.code = r
        r_val = code_obj.send(:code=, r)
        #puts r_val.inspect
        #puts r_val.class.to_s
        c.save; code_obj.save; r.save
        
        
        
        c = UseCaseMetaModelV4::Codeable.all.first
        c.should be_an_instance_of(UseCaseMetaModelV4::Codeable)
        code_obj = c.code
        code_obj.should be_an_instance_of(UseCaseMetaModelV4::Code)
        r = UseCaseMetaModelV4::RichText.all.first
        #puts r.inspect
        r_text = code_obj.code
        r_text.should be_an_instance_of(UseCaseMetaModelV4::RichText)
        r_text.markup_language == 'Ruby'
      end
      
      it "should allow multilevel operations" do
        c = UseCaseMetaModelV4::Codeable.create
        c.code = UseCaseMetaModelV4::Code.create
        c.code.code = UseCaseMetaModelV4::RichText.create(:content => '# DO STUFF', :markup_language => 'Ruby')
        c.code.code.should_not be_nil
        c.code.code.should be_an_instance_of(UseCaseMetaModelV4::RichText)
        c.code.code.content.should == '# DO STUFF'
        c.code.code.markup_language.should == 'Ruby'
      end
      
      
    end
  end
  # ============================================================
  # END polymorphic-to-nonpolymorphic one-to-one section
  # ============================================================
  
  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic one-to-one section -- association attribute
  # ============================================================
  context "polymorphic-to-nonpolymorphic relationship (association attribute)-" do
    context "adding an element" do
      before :each do
        reset_db
        @actor = Actor.create
        @system = System.create
        @richtext1 = UseCaseMetaModelV4::RichText.create
        @richtext2 = UseCaseMetaModelV4::RichText.create
        @category = Category.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = @actor.method(:description=).call(@richtext1)
      #   result.should be_an_instance_of(Actor)
      #   result = @system.method(:description=).call(@richtext2)
      #   result.should be_an_instance_of(System)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   result = @actor.method(:description=).call(@richtext1)
      #   @actor.description_id.should be_nil
      #   @actor.description.should be_nil
      #   result.save # save the actor
      #   @actor.description_id.should == @richtext1.id
      #   @actor.description.should == @richtext1
      # end
    
      it "should set the element for the association via the first class" do
        @actor.method(:description=).call(@richtext1)#.save
        @actor.description.should == @richtext1
        @system.method(:description=).call(@richtext2)#.save
        @system.description.should == @richtext2
      end

      it "cannot set the element for the association via the second class" do
        @richtext1.should_not respond_to(:uc_component)
      end

      it "should reject an element that is not of the polymorphic type" do
        # Category is not of type UC_Component
        lambda {@actor.method(:description=).call(@category)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @actor.description.should be_nil
        lambda {@system.method(:description=).call(@category)
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @system.description.should be_nil
      end
    end

    context "removing the association" do
      before :each do
        reset_db
        @actor = Actor.create
        @system = System.create
        @richtext1 = UseCaseMetaModelV4::RichText.create
        @richtext2 = UseCaseMetaModelV4::RichText.create
        @category = Category.create # Unrelated class for exclusion testing

        @actor.method(:description=).call(@richtext1)#.save
        @actor.description.should == @richtext1
        @system.method(:description=).call(@richtext2)#.save
        @system.description.should == @richtext2 
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   #result = @actor.remove_description(@richtext1)
      #   result = @actor.description = nil
      #   result.should be_an_instance_of(Actor)
      #   #result = @system.remove_description(@richtext2)
      #   result = @system.description = nil
      #   result.should be_an_instance_of(System)
      # end

      it "should remove the association via the first class" do
        #@actor.remove_description(@richtext1).save
        @actor.description = nil
        @actor.description.should be_nil
        #@system.remove_description(@richtext2).save
        @system.description = nil
        @system.description.should be_nil
      end

      it "cannot remove the association via the second class" do
        @richtext1.should_not respond_to(:remove_uc_component)
      end
    
      it "should allow reassociation if the association is removed" do
        #@actor.remove_description(@richtext1).save
        @actor.description = nil
        @actor.description.should be_nil
        #@system.remove_description(@richtext2).save
        @system.description = nil
        @system.description.should be_nil
        # Set the new association
        @actor.method(:description=).call(@richtext2)#.save
        @actor.description.should == @richtext2
      end
    end

    context "retrieving the element" do
      before :each do
        reset_db
        @actor = Actor.create
        @system = System.create
        @richtext1 = UseCaseMetaModelV4::RichText.create
        @richtext2 = UseCaseMetaModelV4::RichText.create
        @category = Category.create # Unrelated class for exclusion testing

        @actor.method(:description=).call(@richtext1)#.save
        @actor.description.should == @richtext1
        @system.method(:description=).call(@richtext2)#.save
        @system.description.should == @richtext2
      end

      it "should return the associated element via the first class" do
        @actor.description.should == @richtext1
        @system.description.should == @richtext2
      end

      it "cannot return the associated element via the second class" do
        @richtext.should_not respond_to(:uc_component)
      end
    end
  end
  # ============================================================
  # END polymorphic-to-nonpolymorphic one-to-one section -- association attribute
  # ============================================================
  
  
  # ============================================================
  # BEGIN nonpolymorphic-to-polymorphic one-to-one section
  # ============================================================  
  # ============================================================
  # END nonpolymorphic-to-polymorphic one-to-one section
  # ============================================================
  
    
  # ============================================================
  # BEGIN nonpolymorphic-to-nonpolymorphic one-to-one section
  # ============================================================
  context "nonpolymorphic-to-nonpolymorphic relationship -" do
    context "adding an element" do
      before :each do
        Param.delete; Query.delete; UseCaseMetaModelV4::Scenario.delete
        @param = Param.create
        @query = Query.create
        @scenario = UseCaseMetaModelV4::Scenario.create # Unrelated class for exclusion testing
      end
    
      # it "should return the model that needs to be saved when adding an association" do
      #   result = @param.method(:query_type=).call(@query)
      #   result.should be_an_instance_of(Param)
      #   result = @query.method(:param_type=).call(@param)
      #   result.should be_an_instance_of(Param)
      # end
      
      # it "should not commit changes necessary for the association until the model is saved" do
      #   pending "write test"
      # end
    
      it "should set the element for the association via the first class" do
        @param.query_type = @query
        # @param.save; @query.save
        (@param.query_type == @query).should == true
        (@query.param_type == @param).should == true
      end

      it "should set the element for the association via the second class" do
        @query.param_type = @param
        # @query.save; @param.save
        (@query.param_type == @param).should == true
        (@param.query_type == @query).should == true
      end

      it "should reject an element that is not of the class type" do
        # UseCaseMetaModelV4::Scenario is not of type Param or Query
        lambda {@query.param_type = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @query.param_type.should be_nil
        lambda {@param.query_type = @scenario
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @param.query_type.should be_nil
        lambda {@query.param_type = @query
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @query.param_type.should be_nil
        lambda {@param.query_type = @param
          }.should raise_error(Sequel::Error, /Invalid type for association/i)
        @param.query_type.should be_nil
      end
    end
  
    context "removing an element" do
      before :each do
        Param.delete; Query.delete; UseCaseMetaModelV4::Scenario.delete
        @param = Param.create
        @param2 = Param.create
        @query = Query.create
        @scenario = UseCaseMetaModelV4::Scenario.create # Unrelated class for exclusion testing

        @param.query_type = @query
        # @param.save; @query.save
        (@param.query_type == @query).should == true
        (@query.param_type == @param).should == true
      end
      
      # it "should return the model that needs to be saved when removing an association" do
      #   result = @param.remove_query_type(@query).save
      #   result.should be_an_instance_of(Param)
      #   
      #   # Re-add association
      #   @param.query_type = @query
      #   @param.save; @query.save
      #   
      #   result = @query.remove_param_type(@param)
      #   result.should be_an_instance_of(Param)
      # end
      
      it "should remove the association via the first class" do
        @param.query_type = nil
        # @param.save; @query.save
        @param.query_type.should be_nil
        @query.param_type.should be_nil
      end
    
      it "should remove the association via the second class" do
        @query.param_type = nil
        @query.refresh; @param.refresh
        @query.param_type.should be_nil
        @param.query_type.should be_nil
      end
    
      # it "should reject a non-existant association" do
      #   @param.query_type = nil
      #   @param.refresh; @query.refresh
      #   @param.query_type.should be_nil
      #   @query.param_type.should be_nil
      #   lambda {@param.query_type = nil
      #     }.should raise_error(Sequel::Error, /Could not find .* association/i)
      #   lambda {@query.param_type = nil
      #     }.should raise_error(Sequel::Error, /Could not find .* association/i)
      #   lambda {@param.query_type = nil
      #     }.should raise_error(Sequel::Error, /Invalid type for association/i)
      #   lambda {@query.param_type = nil
      #     }.should raise_error(Sequel::Error, /Invalid type for association/i)
      # end
    end
    
    context "retrieving all elements" do
      before :each do
        reset_db
        @actor = Actor.create
        @system = System.create
      end
      
      it "should return all children of a class when returning 'all'" do

        Named.children.should include(Actor)
        Named.children.should include(System)
        Actor.all.length.should == 2
        System.all.length.should == 1
        Named.all.length.should == 2
      
        new_system = System.create
        Actor.all.length.should == 3
        System.all.length.should == 2
        Named.all.length.should == 3
      end
      
      
    end
  
    context "retrieving the element" do
      before :each do
        Param.delete; Query.delete; UseCaseMetaModelV4::Scenario.delete
        @param = Param.create
        @param2 = Param.create
        @query = Query.create
        @scenario = UseCaseMetaModelV4::Scenario.create # Unrelated class for exclusion testing

        @param.query_type = @query
        # @param.save; @query.save
        (@param.query_type == @query).should == true
        (@query.param_type == @param).should == true
      end

      it "should return the associated element via the first class" do
        (@param.query_type == @query).should == true
      end

      it "should return the associated element via the second class" do
        (@query.param_type == @param).should == true
      end
    end
  end
  # ============================================================
  # END nonpolymorphic-to-nonpolymorphic one-to-one section
  # ============================================================
  
  # ============================================================
  # BEGIN one-to-one, holds-key-to-non-holds-key section
  # ============================================================
  context "non-composition relationship -" do
    # Check for specific bug causing this scenario to break
    context "retrieving unassociated elements" do
      before :each do
        reset_db
        ChangeTracker.start
        @rs1 = Automotive::RepairShop.create
        @m1 = Automotive::Mechanic.create
        @m2 = Automotive::Mechanic.create
        ChangeTracker.commit
      end
      
      it "should retrieve unassociated elements when there is no current association" do
        @rs1.chief_mechanic_unassociated.should =~ [@m1, @m2]
      end
      
      it "should retrieve unassociated elements when there is a current association" do
        ChangeTracker.start
        @rs1.chief_mechanic = @m1
        ChangeTracker.commit
        @rs1.chief_mechanic_unassociated.should =~ [@m2]
      end
      
      it "should retrieve unassociated elements before being saved" do
        rs2 = Automotive::RepairShop.new
        ChangeTracker.start
        rs2.chief_mechanic = @m1
        rs2.chief_mechanic_unassociated.should =~ [@m2]
        # Note that the query method doesn't use an ignore_objs parameter to filter out added_objs, so 
        #  it returns both until changes are saved. For now I'm just recording this as expected behavior -SD
        rs2.chief_mechanic_unassociated_query.all.should =~ [@m1, @m2]
        ChangeTracker.commit
        rs2.chief_mechanic_unassociated.should =~ [@m2]
        rs2.chief_mechanic_unassociated_query.all.should =~ [@m2]
      end
    end
  end
  # ============================================================
  # END one-to-one, holds-key-to-non-holds-key section
  # ============================================================
  
  # ============================================================
  # BEGIN one-to-one, non-holds-key-to-holds-key section
  # ============================================================
  context "non-composition relationship -" do
    # Check for specific bug causing this scenario to break
    context "retrieving unassociated elements" do
      before :each do
        reset_db
        ChangeTracker.start
        @rs1 = Automotive::RepairShop.create
        @rs2 = Automotive::RepairShop.create
        @m1 = Automotive::Mechanic.create
        ChangeTracker.commit
        @m2 = Automotive::Mechanic.new
      end
      
      it "should retrieve unassociated elements when there is no current association" do
        @m1.chief_for_unassociated.should =~ [@rs1, @rs2]
        @m2.chief_for_unassociated.should =~ [@rs1, @rs2]
      end
      
      it "should retrieve unassociated elements when there is a current association" do
        ChangeTracker.start
        @m1.chief_for = @rs1
        ChangeTracker.commit
        @m1.chief_for_unassociated.should =~ [@rs2]
      end
    end
  end
  # ============================================================
  # END one-to-one, non-holds-key-to-holds-key section
  # ============================================================
end
