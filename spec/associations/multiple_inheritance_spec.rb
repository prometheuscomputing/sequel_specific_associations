require 'spec_helper'

describe "multiple inheritance functionality" do
  before(:each) do
    Application::MI_Person.delete
    Application::MI_Salesman.delete
    Application::MI_Developer.delete
    Application::MI_Clown.delete
    Application::MI_SoftwareSalesman.delete
    Application::MI_Identifier.delete
    Application::MI_CodeMonkey.delete
  end
  
  it "create_schema should create a schema based off the defined and inherited attributes and associations" do
    private_key_file_cols = [:private_key, :private_key_binary_data_id, :private_key_filename, :private_key_mime_type]
    Application::MI_Person.columns.should =~ [:id, :name, :identifier_id]
    Application::MI_Salesman.columns.should =~ [:id, :name, :identifier_id, :sales_id]
    Application::MI_Developer.columns.should =~ [:id, :name, :identifier_id, :dev_id] + private_key_file_cols
    Application::MI_SoftwareSalesman.columns.should =~ [:id, :name, :identifier_id, :dev_id, :sales_id, :software_sold] + private_key_file_cols
    Application::MI_Clown.columns.should =~ [:id, :name, :identifier_id, :clown_skill]
    Application::MI_Identifier.columns.should =~ [:id, :value]
    Application::MI_CodeMonkey.columns.should =~ [:id, :name, :identifier_id, :dev_id, :clown_skill, :worst_mistake, :most_chars_on_one_line] + private_key_file_cols
  end
  
  it "should list multiply-inherited attributes and associations correctly" do
    Application::MI_CodeMonkey.attributes.keys.should =~ [:name, :dev_id, :clown_skill, :worst_mistake, :most_chars_on_one_line, :private_key]
    Application::MI_CodeMonkey.associations.reject { |k,i| i[:hidden]}.keys.should =~ [:private_key_binary_data, :identifier]
  end
  
  it "should allow inheritance of attributes and associations for models" do
    cm = Application::MI_CodeMonkey.create(:name => 'bob', :dev_id => 1, :clown_skill => 'coding', :worst_mistake => 'foobar', :most_chars_on_one_line => 451)
    cm.identifier = Application::MI_Identifier.create(:value => 'AGX11')
    cm.identifier.should_not be_nil
    cm.identifier.value.should == 'AGX11'
    cm.name.should == 'bob'
    cm.dev_id.should == 1
    cm.clown_skill.should == 'coding'
    cm.worst_mistake.should == 'foobar'
    cm.most_chars_on_one_line.should == 451
  end
  
  it "should allow subclasses to overwrite attributes and associations defined in parent classes" do
    # TODO: write this.
  end
  
  it "should allow inheritance of complex attributes" do
    # # Set up richtext
    # r = Custom_Profile::RichText.new
    # r.markup_language = "LaTeX"
    # r.content = "Hello World! \\alpha"
    # # Check that the object does not have an id
    # r.id.should be_nil
    # 
    # # Set up code
    # c = Custom_Profile::Code.new
    # c.language = "Ruby"
    # c.content = 'puts "Hello World!"'
    # c.id.should be_nil
    # 
    # Set up file
    # Custom_Profile::File is _NOT_ persisted (changes are saved when File is assigned to an object with a file attribute )
    f = Custom_Profile::File.new(:filename => 'private_key', :mime_type => 'keyfile')
    # Custom_Profile::BinaryData _IS_ persisted (and will be associated to the object with a file attribute upon assignment of the File)
    binary_data = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    f.binary_data = binary_data
    f.save
    
    cm = Application::MI_CodeMonkey.create(:name => 'bob', :dev_id => 1, :clown_skill => 'coding', :worst_mistake => 'foobar', :most_chars_on_one_line => 451)
    cm.private_key = f
    cm.save
    
    cm_from_db = Application::MI_CodeMonkey.first
    cm_from_db.private_key.should be_a(Custom_Profile::File)
    cm_from_db.private_key.filename.should == 'private_key'
    cm_from_db.private_key.mime_type.should == 'keyfile'
    cm_from_db.private_key.data.should =~ /Hello World!/
  end
end