require 'spec_helper'

describe "clone functionality" do
  before(:each) do
    CompositionTest::Car.delete
    CompositionTest::Price.delete
    CompositionTest::CarDriver.delete
    CompositionTest::Driver.delete
    CompositionTest::Part.delete
    CompositionTest::Component.delete
    CompositionTest::Identifier.delete
    CompositionTest::ComponentColor.delete
    CompositionTest::Color.delete
    
    # Setup instances and associations
    @car = CompositionTest::Car.create(:name => 'beetle')
    @price1 = CompositionTest::Price.create(:name => 'base price')
    @price2 = CompositionTest::Price.create(:name => 'bells and whistles price')
    @car.add_price(@price1)
    @car.add_price(@price2)
    @seat = CompositionTest::Part.create(:name => 'seat')
    @car.add_part(@seat)
    @door = CompositionTest::Part.create(:name => 'door')
    @car.add_part(@door)
    @component1 = @seat.add_component(CompositionTest::Component.create(:name => 'seat lever'))
    @component2 = @seat.add_component(CompositionTest::Component.create(:name => 'height adjuster'))
    @identifier1 = @seat.identifier = CompositionTest::Identifier.create(:name => 'beetle seat 001')
    @component3 = @door.add_component(CompositionTest::Component.create(:name => 'lock switch'))
    @component4 = @door.add_component(CompositionTest::Component.create(:name => 'window'))
    @identifier2 = @door.identifier = CompositionTest::Identifier.create(:name => 'beetle door 005')
    @driver1 = @car.add_driver(CompositionTest::Driver.create(:name => 'bob'))
    @driver2 = @car.add_driver(CompositionTest::Driver.create(:name => 'phil'))
    
    @black = CompositionTest::Color.create(:name => 'black')
    @white = CompositionTest::Color.create(:name => 'white')
    @red = CompositionTest::Color.create(:name => 'red')
    @component1.add_color(@black)
    @component1.add_color(@red)
    @component2.add_color(@black)
    @component2.add_color(@white)
    
    # Ensure the correct total number of objects exist
    @car_count = CompositionTest::Car.count
    @price_count = CompositionTest::Price.count
    @car_driver_count = CompositionTest::CarDriver.count
    @driver_count = CompositionTest::Driver.count
    @part_count = CompositionTest::Part.count
    @component_count = CompositionTest::Component.count
    @identifier_count = CompositionTest::Identifier.count
    @component_color_count = CompositionTest::ComponentColor.count
    @color_count = CompositionTest::Color.count
    
    @car_count.should == 1
    @price_count.should == 2
    @car_driver_count.should == 2
    @driver_count.should == 2
    @part_count.should == 2
    @component_count.should == 4
    @identifier_count.should == 2
    @component_color_count.should == 4
    @color_count.should == 3
  end
  
  it "should be able to perform a shallow clone of an object (ignoring unlinkable objects -- default behavior)" do
    clone_car = @car.shallow_clone
    # Cars are not the same
    @car.id.should_not == clone_car.id
    
    # Attributes are the same
    @car.name.should == clone_car.name
    
    # Associations link to existing objects or are empty
    clone_car.drivers.length.should == 2
    clone_car.prices.should be_empty # one_to_many makes prices unlinkable
    clone_car.drivers.should =~ @car.drivers
    clone_car.parts.should be_empty # one_to_many makes parts unlinkable
    
    # Ensure the correct total number of objects exist
    CompositionTest::Car.count.should == @car_count * 2
    CompositionTest::Price.count.should == @price_count
    CompositionTest::CarDriver.count.should == @car_driver_count * 2 # additional association to existing drivers
    CompositionTest::Driver.count.should == @driver_count
    CompositionTest::Part.count.should == @part_count
    CompositionTest::Component.count.should == @component_count
    CompositionTest::Identifier.count.should == @identifier_count
    CompositionTest::ComponentColor.count.should == @component_color_count
    CompositionTest::Color.count.should == @color_count
  end
  
  it "should be able to perform a shallow clone of an object (duplicating unlinkable objects)" do
    clone_car = @car.shallow_clone(:duplicate_unlinkable_objects => true)
    # Cars are not the same
    @car.id.should_not == clone_car.id
    
    # Attributes are the same
    @car.name.should == clone_car.name
    
    # Associations link to existing objects when possible
    # many_to_many association cars <-> drivers. clone_car should have the same drivers
    clone_car.drivers.length.should == 2
    clone_car.drivers.should =~ @car.drivers
    
    # prices association is one_to_many. Clone car cannot link to existing since they would be stolen from original car
    # Since option :duplicate_unlinkable_objects _was_ specified, this association should have 2 new Prices
    clone_car.prices.length.should == 2
    # Verify @car prices are intact
    @car.prices.length.should == 2
    # Verify prices are duplicated
    clone_car.prices[0].name.should == @car.prices[0].name
    clone_car.prices[0].id.should_not == @car.prices[0].id
    clone_car.prices[1].name.should == @car.prices[1].name
    clone_car.prices[1].id.should_not == @car.prices[1].id
    
    # one_to_many association makes linking impossible. clone_car should have new parts
    clone_car.parts.length.should == 2
    # verify parts were not 'stolen' from @car
    @car.parts.length.should == 2
    clone_car.parts[0].name.should == @car.parts[0].name
    clone_car.parts[0].id.should_not == @car.parts[0].id
    clone_car.parts[1].name.should == @car.parts[1].name
    clone_car.parts[1].id.should_not == @car.parts[1].id
    
    # Check newly created parts' components (one_to_many... should be new as well)
    clone_car.parts[0].components[0].name.should == @car.parts[0].components[0].name
    clone_car.parts[0].components[0].id.should_not == @car.parts[0].components[0].id
    clone_car.parts[0].components[1].name.should == @car.parts[0].components[1].name
    clone_car.parts[0].components[1].id.should_not == @car.parts[0].components[1].id
    clone_car.parts[1].components[0].name.should == @car.parts[1].components[0].name
    clone_car.parts[1].components[0].id.should_not == @car.parts[1].components[0].id
    clone_car.parts[1].components[1].name.should == @car.parts[1].components[1].name
    clone_car.parts[1].components[1].id.should_not == @car.parts[1].components[1].id
    
    
    # check newly created components' colors (many_to_many... should be linked to existing)
    clone_car.parts[0].components[0].colors.length.should == 2
    clone_car.parts[0].components[0].colors.should =~ @car.parts[0].components[0].colors
    clone_car.parts[0].components[1].colors.length.should == 2
    clone_car.parts[0].components[1].colors.should =~ @car.parts[0].components[1].colors
    clone_car.parts[1].components[0].colors.length.should == 0
    clone_car.parts[1].components[0].colors.should =~ @car.parts[1].components[0].colors
    clone_car.parts[1].components[1].colors.length.should == 0
    clone_car.parts[1].components[1].colors.should =~ @car.parts[1].components[1].colors
    
    
    # check newly created parts' identifiers (one_to_one... should be new items)
    clone_car.parts[0].identifier.name.should be_nil # Name is not duplicated because it is a uniqueness-constrained column
    clone_car.parts[0].identifier.id.should_not == @car.parts[0].identifier.id
    clone_car.parts[1].identifier.name.should be_nil # Name is not duplicated because it is a uniqueness-constrained column
    clone_car.parts[1].identifier.id.should_not == @car.parts[1].identifier.id
    
    # Ensure the correct total number of objects exist
    CompositionTest::Car.count.should == @car_count * 2
    CompositionTest::Price.count.should == @price_count * 2
    CompositionTest::CarDriver.count.should == @car_driver_count * 2
    CompositionTest::Driver.count.should == @driver_count
    CompositionTest::Part.count.should == @part_count * 2
    CompositionTest::Component.count.should == @component_count * 2
    CompositionTest::Identifier.count.should == @identifier_count * 2
    CompositionTest::ComponentColor.count.should == @component_color_count * 2
    CompositionTest::Color.count.should == @color_count
  end
  
  it "should be able to create a deep clone of an object (duplicate all composition objects -- default behavior)" do
    clone_car = @car.deep_clone
    # Cars are not the same
    @car.id.should_not == clone_car.id
    
    # Attributes are the same
    @car.name.should == clone_car.name
    
    # Associations link to existing objects when possible
    # many_to_many association cars <-> drivers. clone_car should have the same drivers
    clone_car.drivers.length.should == 2
    clone_car.drivers.should =~ @car.drivers
    
    # prices association is not a composition, but is one_to_many. Clone car cannot link to existing since they would be stolen from original car
    # Since option :duplicate_unlinkable_objects was not specified, this association should be empty on clone_car
    clone_car.prices.should be_empty
    # Verify @car prices are intact
    @car.prices.length.should == 2
    
    # parts association is a composition. clone_car should have new parts
    clone_car.parts.length.should == 2
    # verify parts were not 'stolen' from @car
    @car.parts.length.should == 2
    clone_car.parts[0].name.should == @car.parts[0].name
    clone_car.parts[0].id.should_not == @car.parts[0].id
    clone_car.parts[1].name.should == @car.parts[1].name
    clone_car.parts[1].id.should_not == @car.parts[1].id
    
    # Check newly created parts' components (composition... should be new as well)
    clone_car.parts[0].components[0].name.should == @car.parts[0].components[0].name
    clone_car.parts[0].components[0].id.should_not == @car.parts[0].components[0].id
    clone_car.parts[0].components[1].name.should == @car.parts[0].components[1].name
    clone_car.parts[0].components[1].id.should_not == @car.parts[0].components[1].id
    clone_car.parts[1].components[0].name.should == @car.parts[1].components[0].name
    clone_car.parts[1].components[0].id.should_not == @car.parts[1].components[0].id
    clone_car.parts[1].components[1].name.should == @car.parts[1].components[1].name
    clone_car.parts[1].components[1].id.should_not == @car.parts[1].components[1].id
    
    # check newly created components' colors (non-composition... should be linked to existing)
    clone_car.parts[0].components[0].colors.length.should == 2
    clone_car.parts[0].components[0].colors.should =~ @car.parts[0].components[0].colors
    clone_car.parts[0].components[1].colors.length.should == 2
    clone_car.parts[0].components[1].colors.should =~ @car.parts[0].components[1].colors
    clone_car.parts[1].components[0].colors.length.should == 0
    clone_car.parts[1].components[0].colors.should =~ @car.parts[1].components[0].colors
    clone_car.parts[1].components[1].colors.length.should == 0
    clone_car.parts[1].components[1].colors.should =~ @car.parts[1].components[1].colors
    
    # check newly created parts' identifiers (composition... should be new items)
    clone_car.parts[0].identifier.name.should be_nil # Name is not duplicated because it is a uniqueness-constrained column
    clone_car.parts[0].identifier.id.should_not == @car.parts[0].identifier.id
    clone_car.parts[1].identifier.name.should be_nil # Name is not duplicated because it is a uniqueness-constrained column
    clone_car.parts[1].identifier.id.should_not == @car.parts[1].identifier.id
    
    # Ensure the correct total number of objects exist
    CompositionTest::Car.count.should == @car_count * 2
    CompositionTest::Price.count.should == @price_count
    CompositionTest::CarDriver.count.should == @car_driver_count * 2
    CompositionTest::Driver.count.should == @driver_count
    CompositionTest::Part.count.should == @part_count * 2
    CompositionTest::Component.count.should == @component_count * 2
    CompositionTest::Identifier.count.should == @identifier_count * 2
    CompositionTest::ComponentColor.count.should == @component_color_count * 2
    CompositionTest::Color.count.should == @color_count
  end
  
  it "should be able to create a deep clone of an object (duplicate all composition objects and unlinkable objects)" do
    clone_car = @car.deep_clone(:duplicate_unlinkable_objects => true)
    # Cars are not the same
    @car.id.should_not == clone_car.id
    
    # Attributes are the same
    @car.name.should == clone_car.name
    
    # Associations link to existing objects when possible
    # many_to_many association cars <-> drivers. clone_car should have the same drivers
    clone_car.drivers.length.should == 2
    clone_car.drivers.should =~ @car.drivers
    
    # prices association is not a composition, but is one_to_many. Clone car cannot link to existing since they would be stolen from original car
    # Since option :duplicate_unlinkable_objects _was_ specified, this association should have 2 new Prices
    clone_car.prices.length.should == 2
    # Verify @car prices are intact
    @car.prices.length.should == 2
    # Verify prices are duplicated
    clone_car.prices[0].name.should == @car.prices[0].name
    clone_car.prices[0].id.should_not == @car.prices[0].id
    clone_car.prices[1].name.should == @car.prices[1].name
    clone_car.prices[1].id.should_not == @car.prices[1].id
    
    # parts association is a composition. clone_car should have new parts
    clone_car.parts.length.should == 2
    # verify parts were not 'stolen' from @car
    @car.parts.length.should == 2
    clone_car.parts[0].name.should == @car.parts[0].name
    clone_car.parts[0].id.should_not == @car.parts[0].id
    clone_car.parts[1].name.should == @car.parts[1].name
    clone_car.parts[1].id.should_not == @car.parts[1].id
    
    # Check newly created parts' components (composition... should be new as well)
    clone_car.parts[0].components[0].name.should == @car.parts[0].components[0].name
    clone_car.parts[0].components[0].id.should_not == @car.parts[0].components[0].id
    clone_car.parts[0].components[1].name.should == @car.parts[0].components[1].name
    clone_car.parts[0].components[1].id.should_not == @car.parts[0].components[1].id
    clone_car.parts[1].components[0].name.should == @car.parts[1].components[0].name
    clone_car.parts[1].components[0].id.should_not == @car.parts[1].components[0].id
    clone_car.parts[1].components[1].name.should == @car.parts[1].components[1].name
    clone_car.parts[1].components[1].id.should_not == @car.parts[1].components[1].id
    
    # check newly created components' colors (non-composition... should be linked to existing)
    clone_car.parts[0].components[0].colors.length.should == 2
    clone_car.parts[0].components[0].colors.should =~ @car.parts[0].components[0].colors
    clone_car.parts[0].components[1].colors.length.should == 2
    clone_car.parts[0].components[1].colors.should =~ @car.parts[0].components[1].colors
    clone_car.parts[1].components[0].colors.length.should == 0
    clone_car.parts[1].components[0].colors.should =~ @car.parts[1].components[0].colors
    clone_car.parts[1].components[1].colors.length.should == 0
    clone_car.parts[1].components[1].colors.should =~ @car.parts[1].components[1].colors
    
    # check newly created parts' identifiers (composition... should be new items)
    clone_car.parts[0].identifier.name.should be_nil # Name is not duplicated because it is a uniqueness-constrained column
    clone_car.parts[0].identifier.id.should_not == @car.parts[0].identifier.id
    clone_car.parts[1].identifier.name.should be_nil # Name is not duplicated because it is a uniqueness-constrained column
    clone_car.parts[1].identifier.id.should_not == @car.parts[1].identifier.id
    
    # Ensure the correct total number of objects exist
    CompositionTest::Car.count.should == @car_count * 2
    CompositionTest::Price.count.should == @price_count * 2
    CompositionTest::CarDriver.count.should == @car_driver_count * 2
    CompositionTest::Driver.count.should == @driver_count
    CompositionTest::Part.count.should == @part_count * 2
    CompositionTest::Component.count.should == @component_count * 2
    CompositionTest::Identifier.count.should == @identifier_count * 2
    CompositionTest::ComponentColor.count.should == @component_color_count * 2
    CompositionTest::Color.count.should == @color_count
  end
  
  it "should be able to optionally ignore composer associations when deep cloning" do
    clone_seat = @seat.deep_clone(:ignore_composer => true)
    clone_seat.car.should be_nil
    clone_seat.components.length.should == 2
    # Identifier is cloned, but name isn't set due to uniqueness constraints
    clone_seat.identifier.name.should be_nil
    clone_seat.identifier.name.should_not == @seat.identifier.name
    clone_seat.identifier.id.should_not == @seat.identifier.id
  end
  
  it "should be able to optionally ignore composition associations when shallow_cloning" do
    clone_seat = @seat.shallow_clone(:ignore_compositions => true)
    clone_seat.name.should == @seat.name
    clone_seat.car.should == @car
    clone_seat.identifier.should be_nil
    clone_seat.components.should be_empty
  end
  
  it "should be able to deep clone an object with an empty to-one composition association" do
    part_without_identifier = CompositionTest::Part.create
    clone_part = part_without_identifier.deep_clone
    clone_part.identifier.should be_nil
  end
end