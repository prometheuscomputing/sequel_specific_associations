require 'spec_helper'

describe "abstract classes" do
  # before(:each) do
  #   reset_db
  #   @codeable = Student.create(:name => 'Bob')
  #   @student2 = Student.create(:name => 'Phil')
  #   @student3 = Student.create(:name => 'Ted')
  #   @ta1 = TeachersAssistant.create(:name => 'Alex')
  #   @ta2 = TeachersAssistant.create(:name => 'Vicky')
  # end
  
  it "should be able to mark a class as abstract" do
    new_klass = Class.new(UseCaseMetaModelV4::UC_Component)
    new_klass.abstract?.should == false
    new_klass.abstract!
    new_klass.abstract?.should == true
  end
  
  it "should be able to distinguish abstract classes" do
    UseCaseMetaModelV4::UC_Component.abstract?.should == true
    UseCaseMetaModelV4::Named.abstract?.should == true
    UseCaseMetaModelV4::Codeable.abstract?.should == false
  end
  
  it "should be able to return a concrete implementation of an abstract class (if available)" do
    klass = UseCaseMetaModelV4::UC_Component.concrete_class
    klass.parents.should include(UseCaseMetaModelV4::UC_Component)
    klass.abstract?.should == false
    
    # Create new_klass that has no concrete instance
    new_abstract_klass = Class.new(UseCaseMetaModelV4::UC_Component)
    new_abstract_klass.abstract!
    lambda {new_abstract_klass.concrete_class}.should raise_error(SpecificAssociations::NoConcreteClassFound)
  end
  
  it "should not create tables for abstract classes (or interfaces)" do
    abstract_table = UseCaseMetaModelV4::Named.table_name
    interface_table = Automotive::HasSerial.table_name
    DB.tables.should_not include(abstract_table)
    DB.tables.should_not include(interface_table)
  end
end