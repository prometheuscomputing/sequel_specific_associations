require 'spec_helper'

describe 'SSA association hooks' do
  describe 'general hooks (Apply to all associations on a class)' do
    before(:each) do
      reset_db
      ChangeTracker.commit{
        # NOTE: Country has before/after association add/remove hooks defined on it
        # * after_association_addition and after_association_removal simply add the added/removed object
        # to an appropriate list of objects (recent_additions/recent_removals)
        # * before_association_add will prevent the addition of an object with the name "BadState"
        # * before_association_remove will prevent the removal of an object with the name "GoodState"
        @usa = Geography::Country.create(:name => "United States")
        @my_state = Geography::State.create(:name => "MyState")
        # This object is not allowed to be added
        @bad_state = Geography::State.create(:name => "BadState")
        # This object is not allowed to be removed
        @good_state = Geography::State.create(:name => "GoodState")
      }
      ChangeTracker.start
    end
  
    it 'should run association addition hooks (one-to-many)' do
      # Test after addition hook
      @usa.add_state(@my_state)
      @usa.recent_additions.should include(@my_state)
      # Test before addition hook failure
      lambda{ @usa.add_state(@bad_state) }.should raise_error(Sequel::HookFailed, /before_association_add/)
      @usa.recent_additions.should include(@my_state)
      # Test after removal hook
      @usa.remove_state(@my_state)
      @usa.recent_removals.should include(@my_state)
      # Test before removal hook failure
      @usa.add_state(@good_state)
      lambda{ @usa.remove_state(@good_state) }.should raise_error(Sequel::HookFailed, /before_association_remove/)
    end
  
    it 'should run association addition hooks (many-to-one)' do
      # Test after addition hook
      @my_state.country = @usa
      @usa.recent_additions.should include(@my_state)
      # Test before addition hook failure
      lambda{ @bad_state.country = @usa }.should raise_error(Sequel::HookFailed, /before_association_add/)
      @usa.recent_additions.should include(@my_state)
      # Test after removal hook
      @my_state.country = nil
      @usa.recent_removals.should include(@my_state)
      # Test before removal hook failure
      @good_state.country = @usa
      lambda{ @good_state.country = nil }.should raise_error(Sequel::HookFailed, /before_association_remove/)
    end
  end
  
  describe 'specific hooks (Apply to a single association on a class)' do
    before(:each) do
      reset_db
      ChangeTracker.commit{
        # NOTE: Country has before/after association add/remove hooks defined on it
        # * after_association_addition and after_association_removal simply add the added/removed object
        # to an appropriate list of objects (recent_additions/recent_removals)
        # * before_association_add will prevent the addition of an object with the name "BadState"
        # * before_association_remove will prevent the removal of an object with the name "GoodState"
        @car = Automotive::Car.create
        @address = Geography::Address.create(:street_number => 123, :street_name => 'Mountain View')
        # The registered_vehicles association is constrained to be a subset of the address's person.vehicles, so set up constraint
        @person = People::Person.create
        @person.add_vehicle(@car)
        @person.add_address(@address)
        @clown = People::Clown::Clown.create(:name => 'Toodles')
        @unicycle = People::Clown::Unicycle.create
      }
      ChangeTracker.start
    end
  
    it 'should run association addition hooks (one-to-many)' do
      # Test addition hooks
      @address.add_registered_vehicle(@car)
      @car.instance_variable_get(:@new_address).should == @address
      @car.instance_variable_get(:@new_address_line).should == "#{@address.street_number} #{@address.street_name}"
      # Test removal hooks
      @address.remove_registered_vehicle(@car)
      @car.instance_variable_get(:@old_address).should == @address
      @car.instance_variable_get(:@old_address_line).should == "#{@address.street_number} #{@address.street_name}"
      @car.instance_variable_get(:@old_address_copy).should == @address
    end
  
    it 'should run association addition hooks (many-to-one)' do
      # Test addition hooks
      @car.registered_at = @address
      @car.instance_variable_get(:@new_address).should == @address
      @car.instance_variable_get(:@new_address_line).should == "#{@address.street_number} #{@address.street_name}"
      # Test removal hooks
      @car.registered_at = nil
      @car.instance_variable_get(:@old_address).should == @address
      @car.instance_variable_get(:@old_address_line).should == "#{@address.street_number} #{@address.street_name}"
      @car.instance_variable_get(:@old_address_copy).should == @address
    end
    
    # Test before_add procs and before_add_association hook
    it 'should successfully run before_add hooks and complete the association if none returns false' do
      # Test adding an association that triggers no 'false' return values from before_add hooks
      @person.occupying = @car
      @person.occupying.should == @car
    end
    it 'should not complete the association if a before_add proc returns false' do
      # Test trigger of failing :before_add proc
      @car.vehicle_model = "DeathTrap"
      lambda{ @person.occupying = @car }.should raise_error(Sequel::HookFailed)
    end
    it 'should not complete the association if an override of before_add_association returns false' do
      # Test trigger of failing before_add_association override
      @person.name = "Persona Non Grata"
      lambda{ @person.occupying = @car }.should raise_error(Sequel::HookFailed)
    end
    it 'should run the subclass\'s before_add hooks' do
      @clown.occupying = @unicycle
      @clown.name.should == 'Toodles on a unicycle!'
      @clown.occupying = @car
      @clown.name.should == 'Toodles on a unicycle!'
    end
    # This test is not currently working
    it 'should not complete the association if a superclass\'s before_add proc returns false' do
      pending "rework add_association_options to additively merge proc hooks"
      @car.vehicle_model = "DeathTrap"
      lambda{ @clown.occupying = @car }.should raise_error(Sequel::HookFailed)        
    end
    it 'should not complete the association if an override of a superclass\'s before_add_association returns false' do
      @clown.name = 'Persona Non Grata'
      lambda{ @clown.occupying = @car }.should raise_error(Sequel::HookFailed)
    end
    # TODO: write tests similar to those above for after_add, before_remove, after_remove
  end
end