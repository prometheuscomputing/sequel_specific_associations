require 'spec_helper'

describe "domain object when using uniqueness constraints" do
  before(:each) do
    Application::TestPerson.delete
    Application::TestIdentifier.delete
    Application::TestEmployee.delete
    Application::TestEmployer.delete
    Application::TestDog.delete
    Application::TestBark.delete
    Application::TestCoord.delete
    Application::TestPerson.create(:first_name => 'bob', :last_name => 'cat', :username => 'bob1')
    Application::TestIdentifier.create(:value => 'f00', :numeric_value => 5, :guid => 'x')
    Application::TestEmployee.create(:first_name => 'snow', :last_name => 'leopard', :employee_number => 1, :serial => 1234, :username => 'snow1')
    dog      = Application::TestDog.create(:name => 'thomas', :nickname => 'tom')
    dog.bark = Application::TestBark.create(:sound => 'woof')
    dog.save
    Application::TestCoord.create(:x => 0, :y => 0, :z => 0)
  end
  
  it "should raise a warning when attempting to add a duplicate set of should-be-unique columns" do
    p = Application::TestPerson.create
    p.first_name = 'bob'
    lambda {p.last_name = 'cat'}.should raise_error(SpecificAssociations::NonUniqueWarning)
  end
  
  it "should raise a warning when an instance is created with the same column as a parent class" do
    p = Application::TestEmployee.create
    p.first_name = 'bob'
    lambda {p.last_name = 'cat'}.should raise_error(SpecificAssociations::NonUniqueWarning)
  end
  
  it "should raise a warning when an instance is created with the same column as a sibling class" do
    p = Application::TestEmployer.create
    p.first_name = 'snow'
    lambda {p.last_name = 'leopard'}.should raise_error(SpecificAssociations::NonUniqueWarning, /This is the same as Application::TestEmployee/)
  end
  
  it "should raise a warning when an instance is created with the same column as a child class" do
    p = Application::TestPerson.create
    p.first_name = 'snow'
    lambda {p.last_name = 'leopard'}.should raise_error(SpecificAssociations::NonUniqueWarning)
  end
  
  it "should raise an error when attempting to add a duplicate is-unique column" do
    lambda { Application::TestIdentifier.create(:guid => 'x')}.should raise_error(SpecificAssociations::NonUniqueError)
  end
  
  it "should raise an error when attempting to add a duplicate is-unique column (on an interface-defined attribute)" do
    lambda {Application::TestEmployee.create(:serial => 1234)}.should raise_error(SpecificAssociations::NonUniqueError)
  end
  
  it "should raise an error when attempting to add a duplicate is-unique column (on an inherited attribute)" do
    lambda {Application::TestEmployee.create(:username => 'snow1')}.should raise_error(SpecificAssociations::NonUniqueError)
  end
  
  it "should not raise an error when attempting to add a duplicate column that is only constrained by a subclass" do
    lambda {Application::TestPerson.create(:username => 'snow1')}.should_not raise_error
    lambda {Application::TestPerson.create(:username => 'bob1')}.should_not raise_error
  end
  
  it "should raise a warning/error describing multiple failures if they occur" do
    i =  Application::TestIdentifier.create
    i.numeric_value = 5
    lambda {i.value = 'f00'}.should raise_error(SpecificAssociations::NonUniqueError)
    begin
      i.value = 'f00'
    rescue SpecificAssociations::NonUniqueError => e
      #puts e.failure_message
      e.failures.length.should == 2
    end
  end
  
  it "should allow constraints to be inherited" do
    # Application::TestEmployee should inherit the first_name+last_name uniqueness constraint
    lambda {Application::TestEmployee.create(:first_name => 'snow', :last_name => 'leopard', :employee_number => 2)}.should raise_error(SpecificAssociations::NonUniqueWarning)
    # Application::TestEmployee should also have its own constraint on employee_number
    lambda {Application::TestEmployee.create(:first_name => 'snow', :last_name => 'lion', :employee_number => 1)}.should raise_error(SpecificAssociations::NonUniqueError)
  end
  
  it "should not constrain empty or nil attributes by default" do
    Application::TestEmployee.create()
    # Should not raise error on creation of second test employee with no defined employee number
    Application::TestEmployee.create()
    
    Application::TestEmployee.create(:employee_number => nil)
    # Should not raise error on creation of second test employee with a nil employee number
    Application::TestEmployee.create(:employee_number => nil)
    
    
     Application::TestIdentifier.create(:guid => '')
    # Should not raise error on creation of second test identifier with an empty guid
     Application::TestIdentifier.create(:guid => '')
  end
  
  it "should constrain empty string attributes if the check_empty option is set" do 
    Application::TestEmployee.create(:nickname => '')
    # Should not raise error on creation of second test identifier with an empty guid
    lambda {Application::TestEmployee.create(:nickname => '')}.should raise_error(SpecificAssociations::NonUniqueError)
  end
  
  it "should allow uniqueness warnings to be disabled via a setting" do
    p = Application::TestPerson.create
    p.first_name = 'bob'
    lambda {p.last_name = 'cat'}.should raise_error(SpecificAssociations::NonUniqueWarning)
    SpecificAssociations.disable_uniqueness_warnings
    p.last_name = 'cat'
    SpecificAssociations.enable_uniqueness_warnings
    p.save
    Application::TestPerson.filter(:first_name => 'bob', :last_name => 'cat').count.should == 2
  end
  
  it "should allow uniqueness warnings to be disabled for the duration of a block" do
    p = Application::TestPerson.create
    p.first_name = 'bob'
    
    lambda {p.last_name = 'cat'}.should raise_error(SpecificAssociations::NonUniqueWarning)
    SpecificAssociations.disable_uniqueness_warnings_while {
      p.last_name = 'cat'
    }
    p.save
    Application::TestPerson.filter(:first_name => 'bob', :last_name => 'cat').count.should == 2
    
    # Should be set back to original value after being disabled for block
    SpecificAssociations.uniqueness_warnings_enabled?.should == true
    
    p2 = Application::TestPerson.create
    p2.first_name = 'bob'
    lambda {p2.last_name = 'cat'}.should raise_error(SpecificAssociations::NonUniqueWarning)
    SpecificAssociations.disable_uniqueness_warnings
    SpecificAssociations.disable_uniqueness_warnings_while {
      p2.last_name = 'cat'
    }
    p2.save
    Application::TestPerson.filter(:first_name => 'bob', :last_name => 'cat').count.should == 3
    # Should be set back to original value after being disabled for block
    SpecificAssociations.uniqueness_warnings_enabled?.should == false
    SpecificAssociations.enable_uniqueness_warnings
  end

=begin  
  it "should print a description of any explicitly defined identifiers or the constrained columns in warnings/errors" do
    lambda {Application::TestCoord.create(:x => 0, :y => 0, :z => 0)}.should raise_error(/X, Y, and Z.*0 0 0/)
    lambda {Application::TestEmployee.create(:first_name => 'test', :last_name => 'employee', :employee_number => 1)}.should raise_error(/Employee Number.*Test Employees: 1$/)
    dog = Application::TestDog.create(:name => 'Fido', :nickname => 'Fluffy')
    used_bark = Application::TestBark[:sound => 'woof']
    # Should not show the bark_id since show_value_in_message is set to false
    pending "uniqueness support for associations" do
      lambda {dog.bark = used_bark}.should raise_error(/[^0-9]*Bark.*Test Dogs: thomas/)
    end
  end
=end
  
  it "should allow customization of warnings and errors via options on the constraint" do
    # The label on the constraint calls the column combination "Names"
    lambda {Application::TestDog.create(:name => 'thomas', :nickname => 'tom')}.should raise_error(/name => 'thomas' and nickname => 'tom', as the Names of this Test Dog.*Application::TestDog: name => 'thomas' nickname => 'tom'$/)
  end
      
  it "uniqueness warnings should only be disabled on the current thread" do
    $t2_is_done = false
    t1 = Thread.new {
      SpecificAssociations.disable_uniqueness_warnings_while {
        Application::TestPerson.create(:first_name => 'bob', :last_name => 'cat')
        until $t2_is_done do
          sleep(0.01)
        end
      }
    }
    t2 = Thread.new {
      lambda {Application::TestPerson.create(:first_name => 'bob', :last_name => 'cat')}.should raise_error(SpecificAssociations::NonUniqueWarning)
      $t2_is_done = true
    }
    
    # Wait for threads to complete
    t2.join; t1.join
  end
end