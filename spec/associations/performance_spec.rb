require 'spec_helper'
require 'car_example/populate_data'

# Returns the average time taken to execute the given block in ms
def time(num_samples = 1)
  t = Time.now
  num_samples.times do
    yield
  end
  (Time.now - t)/num_samples * 1000
end

describe "Performance" do
  before(:all) do
    # NOTE: I set expected performance at 5x what it takes on my desktop. Please run the
    #         tests before making changes and adjust the max_factors as needed.
    #       I've also set tests to catch if performance is greater than expected. The performance times
    #         should be adjusted in this case. -SD
    @max_factor = 8
    # @max_factor = 1.6
    @medium_precision_max_factor = 6.5
    # @medium_precision_max_factor = 1.3
    @high_precision_max_factor = 5.5
    # @high_precision_max_factor = 1.1
    @min_factor = 0.7
    @medium_precision_min_factor = 0.85
    @high_precision_min_factor = 0.95
  end

  context "With SCT disabled -" do
    before(:each) do
      ChangeTracker.cancel
      ChangeTracker.disabled = true
      reset_db
      CarExample.populate_data
      @bob = People::Person[:name => 'Bob'] # Starts with vehicles: 'S80', occupying: 'S80'
      @bill = Automotive::Mechanic[:name => 'Bill']
      @rick = Automotive::Mechanic[:name => 'Rick']
      @s80 = Automotive::Car[:vehicle_model => 'S80'] # Starts with occupants: 'Bob', 'Sally', 'Phil'
      @prius = Automotive::HybridVehicle[:vehicle_model => 'Prius']
      @r1 = Automotive::RepairShop[:name => 'Tires and More']
      @r2 = Automotive::RepairShop[:name => 'West-side Tire Service']
      1000.times do |i|
        ChangeTracker.start
        car = Automotive::Car.create(:vehicle_model => i.to_s)
        mechanic = Automotive::Mechanic.create(:name => i.to_s)
        repair_shop = Automotive::RepairShop.create(:name => i.to_s)
        @prius.add_occupant(mechanic)
        @rick.add_vehicle(car)
        ChangeTracker.commit
      end
      ChangeTracker.start
    end
    
    after(:all) do
      ChangeTracker.disabled = false
    end
    
    it "should be able to add a new element to an association quickly" do
      # Many to many
      print "Warning expected here: "
      time_taken = time(2){ @bob.add_vehicle(@prius) }
      # Time taken on sam's computer
      sams_time = 2.7 # 4.4
      puts "Adding to many_to_many took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # Many to one
      time_taken = time(1){ @bill.occupying = @s80 }
      # Time taken on sam's computer
      sams_time = 3.0 # 1.6
      puts "Adding to many_to_one took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # One to many
      print "Warning expected here: "
      time_taken = time(2){ @s80.add_occupant(@rick) }
      # Time taken on sam's computer
      sams_time = 2.4 # 1.2
      puts "Adding to one_to_many took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # One to one (holds_key => truthy)
      time_taken = time(1){ @r1.chief_mechanic = @bill }
      # Time taken on sam's computer
      sams_time = 0.42 # 0.3
      puts "Adding to one_to_one (holds_key) took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # One to one (holds_key => nil)
      time_taken = time(1){ @rick.chief_for = @r2 }
      # Time taken on sam's computer
      sams_time = 1.2 # 1.3
      puts "Adding to one_to_one took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
    end
    
    it "should be able to retrieve associated and unassociated elements quickly" do
      count = nil
      # Many to many - associated
      time_taken = time(2){ count = @rick.vehicles.count }
      # Time taken on sam's computer
      sams_time = 301 # 642
      puts "Returning #{count} associated items for a many_to_many took #{time_taken}"
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
      
      # Many to many - unassociated
      time_taken = time(2){ count = @bob.vehicles_unassociated.count }
      # Time taken on sam's computer
      sams_time = 209 # 330
      puts "Returning #{count} unassociated items for a many_to_many took #{time_taken}"
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
      
      
      # Many to one - associated
      time_taken = time(2){ @bill.occupying }
      # Time taken on sam's computer in ms
      # NOTE: there is a problem with this test that causes it to take 10x longer unless DB is reset after "should be able to add" test above
      # TODO: Fix the issue causing this problem, then switch before(:each) back to before(:all) -SD
      sams_time = 0.03 # 0.025
      puts "Returning an associated item for a many_to_one took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # Many to one - unassociated
      time_taken = time(2){ count = @bill.occupying_unassociated.count }
      # Time taken on sam's computer in ms
      sams_time = 213 # 334
      puts "Returning #{count} unassociated items for a many_to_one took #{time_taken}"
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
      

      # One to many - associated
      time_taken = time(2){ count = @prius.occupants.count }
      # Time taken on sam's computer in ms
      sams_time = 218 # 349
      puts "Returning #{count} associated items for a one_to_many took #{time_taken}"
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
      
      # One to many - unassociated
      time_taken = time(2){ count = @s80.occupants_unassociated.count }
      # Time taken on sam's computer in ms
      sams_time = 221 # 353
      puts "Returning #{count} unassociated items for a one_to_many took #{time_taken}"        
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
      
      
      # One to one (holds_key => truthy) - associated
      time_taken = time(2){ @r1.chief_mechanic }
      # Time taken on sam's computer
      sams_time = 0.03 # 0.026
      puts "Returning an associated item for a one_to_one (holds_key) took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # One to one (holds_key => truthy) - unassociated
      time_taken = time(2){ count = @r1.chief_mechanic_unassociated.count }
      # Time taken on sam's computer
      sams_time = 213 # 375
      puts "Returning #{count} unassociated items for a one_to_one (holds_key) took #{time_taken}"
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
      
      
      # One to one (holds_key => nil) - associated
      time_taken = time(2){ @rick.chief_for }
      # Time taken on sam's computer
      sams_time = 0.53 # 0.48
      puts "Returning an associated item for a one_to_one took #{time_taken}"
      expect(time_taken).to be < sams_time * @max_factor
      expect(time_taken).to be > sams_time * @min_factor
      
      # One to one (holds_key => nil) - unassociated
      time_taken = time(2){ count = @rick.chief_for_unassociated.count }
      # Time taken on sam's computer
      sams_time = 167 # 230
      puts "Returning #{count} unassociated items for a one_to_one took #{time_taken}"
      expect(time_taken).to be < sams_time * @medium_precision_max_factor
      expect(time_taken).to be > sams_time * @medium_precision_min_factor
    end
  end
  
  context "With SCT enabled -" do
    before(:all) do
      ChangeTracker.cancel
      reset_db
      CarExample.populate_data
      @bob = People::Person[:name => 'Bob'] # Starts with vehicles: 'S80', occupying: 'S80'
      @bill = Automotive::Mechanic[:name => 'Bill']
      @rick = Automotive::Mechanic[:name => 'Rick']
      @s80 = Automotive::Car[:vehicle_model => 'S80'] # Starts with occupants: 'Bob', 'Sally', 'Phil'
      @prius = Automotive::HybridVehicle[:vehicle_model => 'Prius']
      @r1 = Automotive::RepairShop[:name => 'Tires and More']
      @r2 = Automotive::RepairShop[:name => 'West-side Tire Service']
      ChangeTracker.start
    end
    
    it "should be able to add a new element to an association quickly" do
      # Many to many
      print "Warning expected here: "
      time_taken = time(2){ @bob.add_vehicle(@prius) }
      # Time taken on sam's computer
      sams_time = 3.2
      puts "Adding to many_to_many took #{time_taken}"
      # expect(time_taken).to be < sams_time * @max_factor
      # expect(time_taken).to be > sams_time * @min_factor
      
      # Many to one
      time_taken = time(1){ @bill.occupying = @s80 }
      # Time taken on sam's computer
      sams_time = 1.1 # 0.6
      puts "Adding to many_to_one took #{time_taken}"
      # expect(time_taken).to be < sams_time * @max_factor
      # expect(time_taken).to be > sams_time * @min_factor
      
      # One to many
      print "Warning expected here: "
      time_taken = time(2){ @s80.add_occupant(@rick) }
      # Time taken on sam's computer
      sams_time = 0.91
      puts "Adding to one_to_many took #{time_taken}"
      # expect(time_taken).to be < sams_time * @max_factor
      # expect(time_taken).to be > sams_time * @min_factor
      
      # One to one (holds_key => truthy)
      time_taken = time(1){ @r1.chief_mechanic = @bill }
      # Time taken on sam's computer
      sams_time = 0.31
      puts "Adding to one_to_one (holds_key) took #{time_taken}"
      # expect(time_taken).to be < sams_time * @max_factor
      # expect(time_taken).to be > sams_time * @min_factor
      
      # One to one (holds_key => nil)
      time_taken = time(1){ @rick.chief_for = @r2 }
      # Time taken on sam's computer
      sams_time = 1
      puts "Adding to one_to_one took #{time_taken}"
      # expect(time_taken).to be < sams_time * @max_factor
      # expect(time_taken).to be > sams_time * @min_factor
    end
  end
end
