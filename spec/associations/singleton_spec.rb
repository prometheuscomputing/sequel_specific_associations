require 'spec_helper'

describe "domain object when using one-to-many primitive associations" do
  before(:each) do
    reset_db
  end
  
  it "should allow the creation of a single row in the singleton table" do
    s = SingletonExample::MySingleton.instance
    s.name = 'Foo'
    s.save
    SingletonExample::MySingleton.instance.should == s
    SingletonExample::MySingleton.count.should == 1
  end
  
  it "should have private create/new/instantiate methods" do
    lambda { SingletonExample::MySingleton.new }.should raise_error(NoMethodError, /private method/)
    lambda { SingletonExample::MySingleton.create }.should raise_error(NoMethodError, /private method/)
    lambda { SingletonExample::MySingleton.instantiate }.should raise_error(NoMethodError, /private method/)
  end
  
  it "should allow child classes of the singleton to also have an independent instance" do
    s = SingletonExample::MySingleton.instance
    i = SingletonExample::InheritedSingleton.instance
    s.should be_a(SingletonExample::MySingleton)
    i.should be_a(SingletonExample::InheritedSingleton)
    SingletonExample::MySingleton.instance.should be_a(SingletonExample::MySingleton)
    SingletonExample::InheritedSingleton.instance.should be_a(SingletonExample::InheritedSingleton)
    # Getting all instances of MySingleton will return both the instance of MySingleton and the instance of InheritedSingleton
    # Note that old behavior was: #all method doesn't return child singleton classes
    SingletonExample::MySingleton.all.length.should == 2
    SingletonExample::InheritedSingleton.all.length.should == 1
    s.name = 'Foo'
    s.save
    i.name = 'Bar'
    i.local_property = 'test'
    i.save
    SingletonExample::MySingleton.instance.name.should_not == SingletonExample::InheritedSingleton.instance.name
  end
  
  it "should recover to a new instance if the database row is deleted" do
    s = SingletonExample::MySingleton.instance
    s.name = 'Bob'
    s.save
    reset_db
    x = SingletonExample::MySingleton.instance
    x.should be_a(SingletonExample::MySingleton)
    x.name.should be_nil
  end
end