module DefinitionExampleModule
  class HasIdentifier < Sequel::Model
    set_dataset :dd_has_identifier
    plugin :specific_associations
    interface!
    attribute :identifier, String
  end
  class Person < Sequel::Model
    set_dataset :dd_people
    plugin :specific_associations
    abstract!
    implements HasIdentifier
    is_unique :identifier
    attribute :name, String
    one_to_many :Pet
    # A Person can depend on one or more Adults
    many_to_many :Adult, :through => :DependentType, :getter => :depends_on, :opp_getter => :dependents
  end
  class Child < Person
    set_dataset :dd_children
    attribute :allowance, SpecificAssociations::INT_TYPE
    one_to_many :Toy, :custom_getter => proc { |toys| toys.collect { |t| t.name = "my #{t.name} (#{name}'s)"; t } }
  end
  class Adult < Person
    set_dataset :dd_adult
    many_to_many :Person, :through => :DependentType, :getter => :dependents, :opp_getter => :depends_on
  end
  class DependentType < Sequel::Model
    plugin :specific_associations
    association_class!
    attribute :type, String
    associates :Person => :dependent; associates :Adult => :depends_on
  end
  class Pet < Sequel::Model
    set_dataset :dd_pets
    plugin :specific_associations
    attribute :name, String
    many_to_one :Person
  end
  class BadDefinition < Sequel::Model
    set_dataset :dd_bad_definitions
    plugin :specific_associations
    many_to_one :Toy
    attribute :name, String
    attribute :my_file, Custom_Profile::File
  end
  class Toy < Sequel::Model
    set_dataset :dd_toys
    plugin :specific_associations
    implements HasIdentifier
    attribute :name, String
    many_to_one :Child
  end
end

def undefine_definition_example_module
  Object.send(:remove_const, :DefinitionExampleModule)
end

def define_bad_definition2
  eval "
    class DefinitionExampleModule::BadDefinition2 < Sequel::Model
      set_dataset :dd_bad_definitions
      plugin :specific_associations
      many_to_one :Toy
      attribute :name, String
      attribute :my_file, Custom_Profile::File
    end
  "
end

DefinitionExampleModule::HasIdentifier.create_schema?
DefinitionExampleModule::Person.create_schema?
DefinitionExampleModule::Child.create_schema?
DefinitionExampleModule::Adult.create_schema?
DefinitionExampleModule::DependentType.create_schema?
DefinitionExampleModule::Pet.create_schema?
DefinitionExampleModule::Toy.create_schema?
DefinitionExampleModule::BadDefinition.create_schema?