module Home
  class HomePerson < Sequel::Model
    plugin :specific_associations
    attribute :first_name, String
    attribute :last_name, String
    attribute :name, String, :custom_getter => proc { |v| first_name + ' ' + last_name} , :custom_setter => proc { |v| first_name = v.split(' ').first; last_name = v.split(' ').last}, :store_on_get => true
    should_be_unique :first_name, :last_name
  end
end

module Work
  class WorkPerson < Home::HomePerson
    attribute :work_login, :String
  end
  
  class WorkEmployer < WorkPerson
    one_to_many :WorkEmployee
  end
  
  class WorkEmployee < WorkPerson
    many_to_one :WorkEmployer
    attribute :employer_name, String
  end
end

Home::HomePerson.create_schema
Work::WorkPerson.create_schema
Work::WorkEmployer.create_schema
Work::WorkEmployee.create_schema

module Qux
  VAL = "string test"
  module Foo
    VAL = 1
    class Foo
      VAL = {:test => "hash"}
    end
    class Bar
      VAL = ["test", "array"]
    end
  end
  module Baz
    VAL  = Regexp.new("Regexp test")
    # Neither of these will work with generated models!!  VAL2 will fail because it evaluates to a class and so won't get declared by the generator code and VAL3 will fail because inspect will returns something like '#<Qux::Foo::Bar:0x007fdd3334ad88>' which a) comments out stuff and b) is nothing even close to what you wanted anyway.
    # VAL2 = Qux::Foo::Bar
    # VAL3 = VAL2.new
  end
end
