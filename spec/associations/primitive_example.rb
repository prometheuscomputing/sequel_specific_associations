module PrimitiveExample
  class Library < Sequel::Model
    set_dataset :primitive_libraries
    plugin :specific_associations
    attribute :name, String
    one_to_many :Book, :primitive => :title
    many_to_many :Patron, :ordered => true, :through => :PatronStatusAtLibrary, :primitive => :first_name
    many_to_one :BookDistributor, :primitive => :name
    one_to_many :LibraryAcronym, :one_to_one => true, :primitive => :acronym
    many_to_one :LibraryID, :one_to_one => true, :primitive => :library_id
  end
  
  class LibraryAcronym < Sequel::Model
    set_dataset :primitive_library_acronyms
    plugin :specific_associations
    attribute :acronym, String
    many_to_one :Library, :one_to_one => true
  end
  
  class LibraryID < Sequel::Model
    set_dataset :primitive_library_ids
    plugin :specific_associations
    attribute :library_id, SpecificAssociations::INT_TYPE
    one_to_many :Library, :one_to_one => true
  end
  
  class BookDistributor < Sequel::Model
    set_dataset :primitive_book_distributors
    plugin :specific_associations
    attribute :name, String
    one_to_many :Library
  end

  class PatronStatusAtLibrary < Sequel::Model
    set_dataset :primitive_patron_statuses_at_library
    plugin :specific_associations
    attribute :status, String
    associates :Library, :opp_is_ordered => true; associates :Patron, :opp_is_ordered => true
  end

  class Book < Sequel::Model
    set_dataset :primitive_books
    plugin :specific_associations
    attribute :title, String
    many_to_one :Library, :opp_is_ordered => true
  end

  class Dictionary < Book
    set_dataset :primitive_dictionaries
  end

  class Patron < Sequel::Model
    set_dataset :primitive_patrons
    plugin :specific_associations
    attribute :first_name, String
    many_to_many :Library, :opp_is_ordered => true, :through => :PatronStatusAtLibrary
  end

  Library.create_schema
  LibraryAcronym.create_schema
  LibraryID.create_schema
  BookDistributor.create_schema
  PatronStatusAtLibrary.create_schema
  Book.create_schema
  Dictionary.create_schema
  Patron.create_schema
end