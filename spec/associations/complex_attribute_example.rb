# Sequel doesn't understand nexus => nexuses or nexus => nexus 
# and incorrectly singularizes 'nexus' -> 'nexu' and 'nexuses' -> 'nexuse'
String.inflections.irregular 'nexus', 'nexuses'
# Could also define more complicated pluralization via regex as follows:
#  String.inflections.plural /^nexus$/, 'nexuses'
#  String.inflections.singular /^nexuses$/, 'nexus'
#  Or more generally
#  String.inflections.plural /^(.+)us$/, '\1uses'
#  String.inflections.singular /^(.+)uses$/, '\1us'
# That last rule is not very good however since it will incorrectly singularize 'abuses' to 'abus'

module Application
  class Document < Sequel::Model
    plugin :specific_associations
    attribute :title, String
    # Define RichText complex attribute
    attribute :summary, Custom_Profile::RichText
    attribute :body, Custom_Profile::RichText
    attribute :script, Custom_Profile::Code
    attribute :pdf, Custom_Profile::File
  end

  class WhitePaper < Document

  end
  
  class Editor < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    attribute :description, Custom_Profile::RichText
  end
end
Application::Document.create_schema!
Application::WhitePaper.create_schema!
Application::Editor.create_schema!