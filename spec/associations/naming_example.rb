# Sequel doesn't understand nexus => nexuses or nexus => nexus 
# and incorrectly singularizes 'nexus' -> 'nexu' and 'nexuses' -> 'nexuse'
String.inflections.irregular 'nexus', 'nexuses'
# Could also define more complicated pluralization via regex as follows:
#  String.inflections.plural /^nexus$/, 'nexuses'
#  String.inflections.singular /^nexuses$/, 'nexus'
#  Or more generally
#  String.inflections.plural /^(.+)us$/, '\1uses'
#  String.inflections.singular /^(.+)uses$/, '\1us'
# That last rule is not very good however since it will incorrectly singularize 'abuses' to 'abus'

module Application
  class Nexus < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    one_to_many :Status, :opp_getter => :nexus, :getter => :statuses
    one_to_many :Cactus, :one_to_one => true, :opp_getter => :nexus, :getter => :cactus
    many_to_one :Cactus, :opp_getter => :nexus_in_tests, :getter => :cactus_in_tests
    many_to_many :Mass, :through => :NexusMass, :opp_getter => :nexuses, :getter => :masses
  end


  class Status < Sequel::Model
    plugin :specific_associations
    attribute :status, String
    many_to_one :Nexus, :opp_getter => :statuses, :getter => :nexus
  end


  class Cactus < Sequel::Model
    plugin :specific_associations
    many_to_one :Nexus, :opp_getter => :cactus, :getter => :nexus
    one_to_many :Nexus, :one_to_one => true, :opp_getter => :cactus_in_tests, :getter => :nexus_in_tests
  end


  class Mass < Sequel::Model
    plugin :specific_associations
    many_to_many :Nexus, :through => :NexusMass, :opp_getter => :masses, :getter => :nexuses
  end


  class NexusMass < Sequel::Model
    plugin :specific_associations
    associates :Nexus => :nexus; associates :Mass => :mass
  end
end
Application::Nexus.create_schema
Application::Status.create_schema
Application::Cactus.create_schema
Application::Mass.create_schema
Application::NexusMass.create_schema
