require 'spec_helper'

describe "composition functionality" do
  before(:each) do
    CompositionTest::Car.delete
    CompositionTest::Price.delete
    CompositionTest::CarDriver.delete
    CompositionTest::Driver.delete
    CompositionTest::Part.delete
    CompositionTest::Component.delete
    CompositionTest::Identifier.delete
    CompositionTest::ComponentColor.delete
    CompositionTest::Color.delete
    
    # Setup instances and associations
    @car = CompositionTest::Car.create(:name => 'beetle')
    @seat = CompositionTest::Part.create(:name => 'seat')
    @car.add_part(@seat)
    @door = CompositionTest::Part.create(:name => 'door')
    @car.add_part(@door)
    @component1 = @seat.add_component(CompositionTest::Component.create(:name => 'seat lever'))
    @component2 = @seat.add_component(CompositionTest::Component.create(:name => 'height adjuster'))
    @identifier1 = @seat.identifier = CompositionTest::Identifier.create(:name => 'beetle seat 001')
    @component3 = @door.add_component(CompositionTest::Component.create(:name => 'lock switch'))
    @component4 = @door.add_component(CompositionTest::Component.create(:name => 'window'))
    @identifier2 = @door.identifier = CompositionTest::Identifier.create(:name => 'beetle door 005')
    @driver1 = @car.add_driver(CompositionTest::Driver.create(:name => 'bob'))
    @driver2 = @car.add_driver(CompositionTest::Driver.create(:name => 'phil'))
  end
  
  it "should include whether or not an association is a composition in the association information" do
    CompositionTest::Car.associations.reject { |g,i| i[:hidden]}.keys.should =~ [:parts, :drivers, :prices]
    CompositionTest::Car.associations[:parts][:composition].should == true
    CompositionTest::Car.associations[:drivers][:composition].should be_nil
    CompositionTest::Car.associations[:prices][:composition].should be_nil
    
    CompositionTest::Part.associations.reject { |g,i| i[:hidden]}.keys.should =~ [:car, :components, :identifier]
    CompositionTest::Part.associations[:car][:composition].should be_nil
    CompositionTest::Part.associations[:components][:composition].should == true
    CompositionTest::Part.associations[:identifier][:composition].should == true
  end
  
  it "should be able to return an array of composition associations" do
    CompositionTest::Car.compositions.length.should == 1
    CompositionTest::Car.compositions.first[:getter].should == :parts
    
    CompositionTest::Part.compositions.length.should == 2
    CompositionTest::Part.compositions.any?{ |c| c[:getter] == :components}.should == true
    CompositionTest::Part.compositions.any?{ |c| c[:getter] == :identifier}.should == true
    
    CompositionTest::Component.compositions.length.should == 0
    CompositionTest::Identifier.compositions.length.should == 0
    CompositionTest::Driver.compositions.length.should == 0
  end
  
  it "should be able to return an array of composer associations" do
    CompositionTest::Car.composers.length.should == 0
    
    CompositionTest::Part.composers.length.should == 1
    CompositionTest::Part.composers.first[:getter].should == :car
    
    CompositionTest::Component.composers.length.should == 1
    CompositionTest::Component.composers.first[:getter].should == :part
    CompositionTest::Identifier.composers.length.should == 1
    CompositionTest::Identifier.composers.first[:getter].should == :part
    CompositionTest::Driver.composers.length.should == 0
  end
  
  it "should be able to return the composing objects for an object instance" do
    @car.compositions.should =~ [@seat, @door]
    # Passing false recurses through children
    @car.compositions(false).should =~ [@seat, @door, @component1, @component2, @component3, @component4, @identifier1, @identifier2]
    
    @seat.compositions.should =~ [@component1, @component2, @identifier1]
    @seat.compositions(false).should =~ [@component1, @component2, @identifier1]
    
    @door.compositions.should =~ [@component3, @component4, @identifier2]
    @door.compositions(false).should =~ [@component3, @component4, @identifier2]
    
    @component1.compositions.should be_empty
    @identifier1.compositions.should be_empty
    @driver1.compositions.should be_empty
    
    # Also test empty composition types (to_one, to_many)
    new_part = CompositionTest::Part.create(:name => 'test part')
    new_part.compositions(false).should =~ []
    new_part.compositions.should =~ []
  end
  
  it "should be able to return the composer object for an object instance" do
    @car.composer.should be_nil
    @seat.composer.should == @car
    @door.composer.should == @car
    
    @component1.composer.should == @seat
    @component2.composer.should == @seat
    @identifier1.composer.should == @seat
    
    @component3.composer.should == @door
    @component4.composer.should == @door
    @identifier2.composer.should == @door
    
    @driver1.composer.should be_nil
    @driver2.composer.should be_nil
  end
  
  it "should delete all composing objects when the composer is destroyed" do
    # Check for issues destroying with empty to_one composition association
    @identifier1.destroy
    CompositionTest::Identifier[:id => @identifier1.id].should be_nil
    # get Driving records
    driving1, driving2 = @car.drivers_records
    @car.destroy
    CompositionTest::Part[:id => @seat.id].should be_nil
    CompositionTest::Part[:id => @door.id].should be_nil
    CompositionTest::Component[:id => @component1.id].should be_nil
    CompositionTest::Component[:id => @component2.id].should be_nil
    CompositionTest::Identifier[:id => @identifier1.id].should be_nil
    CompositionTest::Component[:id => @component3.id].should be_nil
    CompositionTest::Component[:id => @component4.id].should be_nil
    CompositionTest::Identifier[:id => @identifier2.id].should be_nil
    # Drivers should still exist
    CompositionTest::Driver[:id => @driver1.id].should_not be_nil
    CompositionTest::Driver[:id => @driver2.id].should_not be_nil
    # But Driving records should be destroyed
    CompositionTest::CarDriver[:id => driving1.id].should be_nil
    CompositionTest::CarDriver[:id => driving2.id].should be_nil
  end
end