require 'spec_helper'

describe "domain objects for a relationship through an association class" do
  context "retrieving a list of objects for the association" do
    before(:each) do
      reset_db
      @actor = Actor.create
      @system = System.create
      @scenario = Scenario.create
      @extension_scenario = Extension_Scenario.create
      
      # Setup initial associations
      @actor.add_constrained_goal(@scenario)
      @actor.add_constrained_goal(@extension_scenario)
      @system.add_constrained_goal(@scenario)
      @system.add_constrained_goal(@extension_scenario)
      
    end
    it "should return an array of objects for the association" do
      # Verify associations
      @actor.constrained_goals.should be_an_instance_of(Array)
      @actor.constrained_goals.length.should == 2
      @actor.constrained_goals.should include(@scenario)
      @actor.constrained_goals.should include(@extension_scenario)
      @system.constrained_goals.length.should == 2
    end
    
    it "should ignore join table records that do not have an associated object when returning objects" do
      actor_goal_records = @actor.constrained_goals_records # same as calling get_many_constrained_goals
      system_goal_records = @system.constrained_goals_records # same as calling get_many_constrained_goals
      # Assign Goals to variables
      actor_scenario_join = actor_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Scenario'}.first
      actor_extension_scenario_join = actor_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Extension_Scenario'}.first
      system_scenario_join = system_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Scenario'}.first
      system_extension_scenario_join = system_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Extension_Scenario'}.first
      
      actor_scenario_join.constrained_goal = nil
      actor_scenario_join.save
      
      @actor.constrained_goals_records.count.should == 2
      @actor.constrained_goals_records.should =~ [actor_scenario_join, actor_extension_scenario_join]
      @actor.constrained_goals_count.should == 1
      @actor.constrained_goals.should =~ [@extension_scenario]
    end
    
    it "should be able to get the join table records of the association" do
      actor_goal_records = @actor.constrained_goals_records
      actor_goal_records.count.should == 2
      actor_goal_records[0].should be_an_instance_of(Goal)
      actor_goal_records[1].should be_an_instance_of(Goal)
      
      system_goal_records = @system.constrained_goals_records
      system_goal_records.count.should == 2
      system_goal_records[0].should be_an_instance_of(Goal)
      system_goal_records[1].should be_an_instance_of(Goal)
    end
    
    it "should be able to retrieve the association ends of an association class" do
      actor_goal_records = @actor.constrained_goals_records
      system_goal_records = @system.constrained_goals_records
      
      actor_scenario_join = actor_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Scenario'}.first
      actor_scenario_join.constrained_goal.should == @scenario
      actor_scenario_join.actor_goal.should == @actor
      
      actor_extension_scenario_join = actor_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Extension_Scenario'}.first
      actor_extension_scenario_join.constrained_goal.should == @extension_scenario
      actor_extension_scenario_join.actor_goal.should == @actor
      
      system_scenario_join = system_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Scenario'}.first
      system_scenario_join.constrained_goal.should == @scenario
      system_scenario_join.actor_goal.should == @system
      
      system_extension_scenario_join = system_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Extension_Scenario'}.first
      system_extension_scenario_join.constrained_goal.should == @extension_scenario
      system_extension_scenario_join.actor_goal.should == @system
    end
    
    it "should be able to set the association ends of an association class" do
      actor_goal_records = @actor.constrained_goals_records
      system_goal_records = @system.constrained_goals_records
      actor_scenario_join = actor_goal_records.select { |r| r.constrained_goal_class == 'UseCaseMetaModelV4::Scenario'}.first
      
      new_actor = Actor.create

      # Set the association from @actor to new_actor
      actor_scenario_join.actor_goal = new_actor
      # Check the association class getters
      actor_scenario_join.actor_goal.should == new_actor
      actor_scenario_join.constrained_goal.should == @scenario
      # Check the new_actor's constrained goals
      new_actor.constrained_goals.count.should == 1
      new_actor.constrained_goals.should include(@scenario)
      # Check the scenario's actor goals
      @scenario.actor_goals.count.should == 2
      @scenario.actor_goals.should include(new_actor)
      # Check the @actor's constrained goals (should not have @scenario association)
      @actor.constrained_goals.count.should == 1
      @actor.constrained_goals.should_not include(@scenario)
    end
  end
  context "when using methods defined on the association class" do
    before(:each) do
      reset_db
      @actor = Actor.create
      @system = System.create
      @scenario = Scenario.create
      @extension_scenario = Extension_Scenario.create
      
      # Setup initial association
      @actor.add_constrained_goal(@scenario)

      actor_goal_records = @actor.constrained_goals_records
      @actor_scenario_goal = actor_goal_records[0]
      @actor_scenario_goal.should be_an_instance_of(Goal)
    end
    
    it "should be able to retrieve both sides of the association" do
      @actor_scenario_goal.constrained_goal.should == @scenario
      @actor_scenario_goal.actor_goal.should == @actor
    end
    
    it "should be able to set both sides of the association" do
      goal = @actor_scenario_goal
      goal.constrained_goal = @extension_scenario
      
      @scenario.actor_goals.should be_empty
      @extension_scenario.actor_goals.count.should == 1
      @actor.constrained_goals.count.should == 1
      
      goal.actor_goal = @system
      
      @scenario.actor_goals.should be_empty
      @extension_scenario.actor_goals.count.should == 1
      @actor.constrained_goals.should be_empty
      @system.constrained_goals.count.should == 1
    end
    
    it "should be able to return the unassociated objects for either end of the association" do
      @actor_scenario_goal.actor_goal_unassociated.count.should == 1
      @actor_scenario_goal.actor_goal_unassociated.should include(@system)
      
      @actor_scenario_goal.constrained_goal_unassociated.count.should == 1
      @actor_scenario_goal.constrained_goal_unassociated.should include(@extension_scenario)
    end
  end
  context("when testing a one to one association with an association class") do
    before(:each) do
      reset_db
      # Car Example setup so the examples actually make sense
      ChangeTracker.disabled = true
      @bob = People::Person.create(:name => 'Bob')
      @sylva = Geography::City.create(:name => 'Sylva')
      @founding = People::Founding.create
      
      #extra stuff for unassociated queries
      @cullowhee = Geography::City.create(:name => 'Cullowhee')
      @new_york = Geography::City.create(:name => 'New York')
      @sally = People::Person.create(:name => 'Sally')
    end
    
    after(:each) do
      ChangeTracker.disabled = false
    end
    
    it "should be able to set a one to one association with an association class" do
      @bob.founded = @sylva
      
      @bob.founded_through.first.class.should == People::Founding
      @bob.founded_through.count.should == 1
      @sylva.founder_through.first.class.should == People::Founding
      @sylva.founder_through.count.should == 1
      @bob.founded.should == @sylva
      @sylva.founder.should == @bob
      
    end
    
    it "should be able to set both ends of the association from the association class" do
      @founding.founded.should == nil
      @founding.founder.should == nil
      
      @founding.founder = @bob
      
      @founding.founder.should == @bob
      @founding.founded.should == nil
      
      @founding.founded = @sylva
      
      @founding.founder.should == @bob
      @founding.founded.should == @sylva
    end
    
    it "should be able to set an attribute on the association class" do
      @founding.date.should == nil  
      @founding.date = Time.parse('1900-01-01')
      @founding.save
      @founding.date.should == Time.parse('1900-01-01')
      
    end
    
    it "should be able to return the unassociated objects for either end of the association" do
      @founding.founded_unassociated.count.should == 3
      @founding.founded_unassociated.should include(@sylva)
      @founding.founder_unassociated.count.should == 2
      @founding.founder_unassociated.should include(@bob)
      
      @founding.founded = @sylva
      @founding.founder = @bob
      
      @founding.founded_unassociated.count.should == 2
      @founding.founded_unassociated.should_not include(@sylva)
      @founding.founder_unassociated.count.should == 1
      @founding.founder_unassociated.should_not include(@bob)
      
    end
    
  end
  context("when testing a one to many association with an association class") do
    before(:each) do
      reset_db
      #setup clowns and unicyles
      ChangeTracker.disabled = true
      @clown1 = People::Clown::Clown.create
      @unicycle1 = People::Clown::Unicycle.create
      @riding = People::Clown::Riding.create
      
      #extra stuff for unassociated queries
      @clown2 = People::Clown::Clown.create
      @clown3 = People::Clown::Clown.create
      @clown4 = People::Clown::Clown.create
      @unicycle2 = People::Clown::Unicycle.create
      @unicycle3 = People::Clown::Unicycle.create
      
    end
    after(:each) do
      ChangeTracker.disabled = false
    end
    
    it "should be able to set a one to many association with an association class from the to many side" do
      @clown1.unicycles_add(@unicycle1)
      @clown1.unicycles_through.first.class.should == People::Clown::Riding
      @clown1.unicycles_through.count.should == 1
      @clown1.unicycles.count.should == 1
      @clown1.unicycles.should include(@unicycle1)
      @unicycle1.clown.should == @clown1
      
      @clown1.unicycles_add(@unicycle2)
      @clown1.unicycles_through.count.should == 2
      @clown1.unicycles.count.should == 2
      @clown1.unicycles.should include(@unicycle2)
      @unicycle2.clown.should == @clown1
      
    end
    
    it "should be able to set a one to many association with an association class from the to one side" do
      @unicycle1.clown = @clown1
      
      @unicycle1.clown_through.first.class.should == People::Clown::Riding
      @unicycle1.clown_through.count.should == 1
      @unicycle1.clown.should == @clown1
      @clown1.unicycles.should include(@unicycle1)
      
      @unicycle1.clown = @clown2
      @unicycle1.clown_through.first.class.should == People::Clown::Riding
      @unicycle1.clown_through.count.should == 1
      @unicycle1.clown.should == @clown2
      @clown1.unicycles.count.should == 0
      @clown1.unicycles_through.count.should == 0
      
    end
    
    it "should be able to set both ends of the association from the association class" do
      @riding.clown.should == nil
      @riding.unicycle.should == nil
      
      @riding.clown = @clown1
      @riding.unicycle.should == nil
      @riding.clown.should == @clown1
      @clown1.unicycles.count.should == 0
      @riding.unicycle = @unicycle1
      @riding.unicycle.should == @unicycle1
      @unicycle1.clown.should == @clown1
      @clown1.unicycles.count.should == 1
      @unicycle1.clown_through.first.should == @riding
      
    end
    
    it "should be able to set attributes on the association class" do
      @riding.is_favorite = true
      @riding.nickname = 'Betsy'
      @riding.description_content = 'This is my favorite unicycle'
      @riding.save
      
      @riding.is_favorite.should == true
      @riding.nickname.should == 'Betsy'
      @riding.description_content.should == 'This is my favorite unicycle'
      
    end
    
    it "should be able to return the unassociated objects for either end of the association" do
      @riding.clown_unassociated.count.should == 4
      @riding.unicycle_unassociated.count.should == 3
      
      @riding.clown = @clown1
      
      @riding.clown_unassociated.count.should == 3
      @riding.clown_unassociated.should_not include(@clown1)
      
      @riding.unicycle = @unicycle1
      @riding.unicycle_unassociated.count.should == 2
      @riding.unicycle_unassociated.should_not include(@unicycle1)
      
    end
  end
end
