require 'spec_helper'

describe "domain object when using alias associations" do
  before :each do
    reset_db
    ChangeTracker.start
    @bob = People::Person.create(:name => 'Bob')
    
    # Create 'cheap' vehicles
    @v1 = Automotive::Car.create(:cost => '10000', :vehicle_model => 'S80') # Bob's
    @v1.make = 'Volvo'; @v1.save 
    @v2 = Automotive::Motorcycle.create(:cost => '11000', :vehicle_model => 'ProtoX') # Bob's
    @v2.make = 'Other'; @v2.save 
    @v3 = Automotive::Car.create(:cost => '12000', :vehicle_model => 'Super Prototype')
    @v3.make = 'Other'; @v3.save 
    @v4 = Automotive::Motorcycle.create(:cost => '13000', :vehicle_model => 'ProtoX2')
    @v4.make = 'Other'; @v4.save 
    
    # Create 'expensive' vehicles
    @v5 = Automotive::Motorcycle.create(:cost => '30000', :vehicle_model => 'X2') # Bob's
    @v5.make = 'Other'; @v5.save 
    @v6 = Automotive::Car.create(:cost => '40000', :vehicle_model => 'S40') # Bob's
    @v6.make = 'Volvo'; @v6.save 
    @v7 = Automotive::Motorcycle.create(:cost => '50000', :vehicle_model => 'ProtoZ')
    @v7.make = 'Other'; @v7.save 
    @v8 = Automotive::Car.create(:cost => '60000', :vehicle_model => 'ProtoS')
    @v8.make = 'Volvo'; @v8.save 
    @v9 = Automotive::Car.create(:cost => '70000', :vehicle_model => 'Z')
    @v9.make = 'Other'; @v9.save 
    
    # Add several vehicles to Bob
    @bob.vehicles = [@v1, @v2, @v5, @v6]
    ChangeTracker.commit
    ChangeTracker.start
  end
  
  it "should return correct information about the Model's alias associations" do
    associations = People::Person.associations
    # Test derived association
    associations.keys.should include(:motorcycles)
    motorcycles_association_info = associations[:motorcycles]
    motorcycles_association_info[:derived].should == true
    motorcycles_association_info[:alias].should == true
    motorcycles_association_info[:getter].to_s.should == "motorcycles"
    motorcycles_association_info[:class].should == "Automotive::Motorcycle"
    motorcycles_association_info[:type].should == :many_to_many
    
    # TODO: Test that info method returns the same information
    
    associations.keys.should include(:expensive_vehicles)
    expensive_vehicles_association_info = associations[:expensive_vehicles]
    expensive_vehicles_association_info[:derived].should == true
    expensive_vehicles_association_info[:alias].should == true
    expensive_vehicles_association_info[:getter].to_s.should == "expensive_vehicles"
    expensive_vehicles_association_info[:class].should == "Automotive::Vehicle"
    expensive_vehicles_association_info[:type].should == :many_to_many
    
    # TODO: Test that info method returns the same information
  end
  
  it "should be able to retrieve the alias associations (and un-associations)" do
    @bob.motorcycles.should =~ [@v2, @v5]
    @bob.motorcycles_unassociated.should =~ [@v4, @v7]
    
    @bob.expensive_vehicles.should =~ [@v5, @v6]
    @bob.expensive_vehicles_unassociated.should =~ [@v7, @v8, @v9]
    
    @bob.prototypes.should =~ [@v2]
    @bob.prototypes_unassociated.should =~ [@v3, @v4, @v7, @v8]
    
    pending 'implement Sequel::SQL::BooleanExpression matching in #matches? method'
    lambda {@bob.add_prototype(@v9)}.should raise_error(Sequel::Error, /does not match filter.*vehicle_model/i)
  end
  
  it "should be able to add alias associations" do
    @bob.add_motorcycle @v4
    @bob.add_expensive_vehicle @v8
    
    # Commit and re-check
    ChangeTracker.commit
    @bob.motorcycles.should =~ [@v2, @v5, @v4]
    @bob.motorcycles_unassociated.should =~ [@v7]
    @bob.expensive_vehicles.should =~ [@v5, @v6, @v8]
    @bob.expensive_vehicles_unassociated.should =~ [@v7, @v9]
    
    # Check original association
    @bob.vehicles.should =~ [@v1, @v2, @v4, @v5, @v6, @v8]
    @bob.vehicles_unassociated.should =~ [@v3, @v7, @v9]
    
    # Test bad associations
    ChangeTracker.start
    lambda {@bob.add_motorcycle(@v3)}.should raise_error(Sequel::Error, /Invalid type/i)
    lambda {@bob.add_expensive_vehicle(@v3)}.should raise_error(Sequel::Error, /does not match filter.*cost/i)
    # TODO this doesn't raise an error because the filter it is checking against is a Sequel.ilike and we don't know how to validate against that!!
    pending 'implement Sequel.ilike matching in #matches? method'
    lambda {@bob.add_prototype(@v9)}.should raise_error(Sequel::Error, /does not match filter.*make/i)
  end
  
  it "should be able to add an alias association that does not initially meet the filter criteria" do
    @new_car = Automotive::Car.create(:vehicle_model => 'Test One')
    @bob.add_expensive_vehicle @new_car
    @new_car.cost.should == 25000
    # Commit and re-check
    ChangeTracker.commit
    ChangeTracker.start
    @new_car.cost.should == 25000
    @bob.expensive_vehicles.should =~ [@v5, @v6, @new_car]
    @bob.expensive_vehicles_unassociated.should =~ [@v7, @v8, @v9]
    
    # Check motorcycles
    @bob.motorcycles.should =~ [@v2, @v5]
    # Check original association
    @bob.vehicles.should =~ [@v1, @v2, @v5, @v6, @new_car]
    
    # Add to prototypes
    @new_volvo = Automotive::Car.create(:vehicle_model => 'ProtoZ2')
    @bob.add_prototype @new_volvo
    @new_volvo.vehicle_model == 'ProtoZ2'
    @new_volvo.cost.should == 100
    # Commit and re-check
    ChangeTracker.commit
    @new_volvo.vehicle_model == 'ProtoZ2'
    @new_volvo.cost.should == 100
    @bob.prototypes.should =~ [@v2, @new_volvo]
    @bob.prototypes_unassociated.should =~ [@v3, @v4, @v7, @v8]
  end
  
  it "should be able to retrieve pending alias association additions" do
    @bob.add_motorcycle @v4
    @bob.motorcycles.should =~ [@v2, @v5, @v4]
    @bob.motorcycles_unassociated.should =~ [@v7]
    
    @bob.add_expensive_vehicle @v8
    @bob.expensive_vehicles.should =~ [@v5, @v6, @v8]
    @bob.expensive_vehicles_unassociated.should =~ [@v7, @v9]
  end
  
  it "should be able to remove alias associations" do
    @bob.remove_motorcycle @v2
    @bob.remove_expensive_vehicle @v6
    
    # Commit and re-check
    ChangeTracker.commit
    @bob.motorcycles.should =~ [@v5]
    @bob.motorcycles_unassociated.should =~ [@v2, @v4, @v7]
    @bob.expensive_vehicles.should =~ [@v5]
    @bob.expensive_vehicles_unassociated.should =~ [@v6, @v7, @v8, @v9]
    
    # Check original association
    @bob.vehicles.should =~ [@v1, @v5]
    @bob.vehicles_unassociated.should =~ [@v2, @v3, @v4, @v6, @v7, @v8, @v9]
  end
  
  it "should be able to retrieve alias associations minus pending removals" do
    @bob.remove_motorcycle @v2
    @bob.motorcycles.should =~ [@v5]
    @bob.motorcycles_unassociated.should =~ [@v2, @v4, @v7]
    
    @bob.remove_expensive_vehicle @v6
    @bob.expensive_vehicles.should =~ [@v5]
    @bob.expensive_vehicles_unassociated.should =~ [@v6, @v7, @v8, @v9]
  end
  
  it "should be able to remove all alias associations" do
    @bob.remove_all_motorcycles
    @bob.remove_all_expensive_vehicles
    
    # Commit and re-check
    ChangeTracker.commit

    @bob.motorcycles.should be_empty
    @bob.motorcycles_unassociated.should =~ [@v2, @v4, @v5, @v7]
    
    @bob.expensive_vehicles.should be_empty
    @bob.expensive_vehicles_unassociated.should =~ [@v5, @v6, @v7, @v8, @v9]
    
    # Check original association
    @bob.vehicles.should =~ [@v1]
    @bob.vehicles_unassociated.should =~ [@v2, @v3, @v4, @v5, @v6, @v7, @v8, @v9]
  end
  
  it "should be able to set the alias associations" do
  end
  
  it "should be able to count alias associations" do
    @bob.motorcycles_count.should == 2
    @bob.motorcycles_unassociated_count.should == 2
    
    @bob.expensive_vehicles_count.should == 2
    @bob.expensive_vehicles_unassociated_count.should == 3
  end
  
  it "should be able to return subtypes of specified alias association type" do
    @ev = Automotive::ElectricVehicle.instantiate(:cost => '90000', :vehicle_model => 'EX', :make => 'Other')
    @hv = Automotive::HybridVehicle.instantiate(:cost => '75000', :vehicle_model => 'MyHybrid', :make => 'Other')
    ChangeTracker.commit
    ChangeTracker.start
    @bob.electric_vehicles_count.should == 0
    @bob.electric_vehicles.should == []
    @bob.electric_vehicles_unassociated_count.should == 2
    @bob.electric_vehicles_unassociated.should =~ [@ev, @hv]
    # Add HybridVehicle
    @bob.add_electric_vehicle @hv
    ChangeTracker.commit
    ChangeTracker.start
    @bob.electric_vehicles_count.should == 1
    @bob.electric_vehicles.should =~ [@hv]
    @bob.electric_vehicles_unassociated_count.should == 1
    @bob.electric_vehicles_unassociated.should =~ [@ev]
    # Add ElectricVehicle
    @bob.add_electric_vehicle @ev
    ChangeTracker.commit
    ChangeTracker.start
    @bob.electric_vehicles_count.should == 2
    @bob.electric_vehicles.should =~ [@hv, @ev]
    @bob.electric_vehicles_unassociated_count.should == 0
    @bob.electric_vehicles_unassociated.should == []
  end
  
  it "should be able to filter alias associations" do
    @bob.motorcycles(:vehicle_model => 'ProtoX').should =~ [@v2]
    @bob.motorcycles(:cost => 0..20000).should =~ [@v2]
    @bob.motorcycles([{:cost => 0..20000},{:vehicle_model => 'ProtoX'}]).should =~ [@v2]
    @bob.motorcycles([Sequel.ilike(:vehicle_model, 'Proto%')]).should =~ [@v2]
    @bob.motorcycles([Sequel.ilike(:vehicle_model, 'Proto%'), {:cost => 0..20000}]).should =~ [@v2]
    @bob.motorcycles([Sequel.ilike(:vehicle_model, 'Proto%'), {:cost => 20000..100000}]).should =~ []
    @bob.motorcycles([Sequel.ilike(:vehicle_model, '%2'), {:cost => 20000..100000}]).should =~ [@v5]
  end
  
  it "should automatically reflect any changes to the base association" do
    $processed_vehicles = []
    People::Person.add_association_options :vehicles, :custom_adder => proc{|new_vehicle| $processed_vehicles << new_vehicle; new_vehicle }
    @bob.add_motorcycle @v7
    $processed_vehicles.should =~ [@v7]
    # Unset custom adder
    People::Person.add_association_options :vehicles, :custom_adder => nil
  end
  
  it "should correcly disable association hooks for alias associations" do
    lambda{ People::Person.add_association_options :motorcycles, :custom_remover => proc{|x| x }}.should raise_error(/Cannot define hooks.* on an alias association/)
  end
  
  it "should return alias associations hash via the _associations method" do
    @bob.motorcycles_associations.collect{|assoc| assoc[:to]}.should =~ [@v2, @v5]
  end
end
