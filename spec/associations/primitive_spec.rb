require 'spec_helper'

describe "domain object when using one-to-many primitive associations" do
  before(:each) do
    reset_db
    @library = PrimitiveExample::Library.create(:name => "WCU public library")
    @library.add_book "War and Peace"
    @library.add_book "Crime and Punishment"
    @library.add_book "The Brothers Karamazov"
  end

  it "should be able to retrieve the primitive association" do
    @library.books.should == ["War and Peace", "Crime and Punishment", "The Brothers Karamazov"]
  end
  
  it "should be able to remove a primitive from the association" do
    @library.remove_book "War and Peace"
    @library.books.should == ["Crime and Punishment", "The Brothers Karamazov"]
  end
  
  it "should not raise an error when attempting to remove a non-existant primitive" do
    @library.remove_book "FOOBAR"
    @library.books.should == ["War and Peace", "Crime and Punishment", "The Brothers Karamazov"]
  end
  
  it "should completely remove the primitive from the database when dissociated" do
    @library.remove_book "War and Peace"
    @library.books.should == ["Crime and Punishment", "The Brothers Karamazov"]
    primitive_type = @library.books_type.first
    primitive_type[:title => "War and Peace"].should be_nil
  end
end

# NOTE: many-to-many primitives do not make sense. Don't do this.
# Testing here merely to make sure we do not error out.
describe "domain object when using many-to-many primitive associations" do
  before(:each) do
    reset_db
    @library1 = PrimitiveExample::Library.create(:name => "WCU public library")
    @library2 = PrimitiveExample::Library.create(:name => "Sylva public library")
    @library3 = PrimitiveExample::Library.create(:name => "Franklin public library")
    
    @library1.add_patron "Bob"
    @library1.add_patron "Sue"
    @library2.add_patron "Bob"
    @library2.add_patron "Sally"
    @library3.add_patron "Jim"
    @library3.add_patron "Bob"
  end

  it "should be able to retrieve the primitive association" do
    @library1.patrons.should == ["Bob", "Sue"]
    @library2.patrons.should == ["Bob", "Sally"]
    @library3.patrons.should == ["Jim", "Bob"]
  end
  
  it "should be able to remove a primitive from the association" do
    @library1.remove_patron "Bob"
    @library1.patrons.should == ["Sue"]
  end
  
  it "should not raise an error when attempting to remove a non-existant primitive" do
    @library1.remove_patron "FOOBAR"
    @library1.patrons.should == ["Bob", "Sue"]
  end
end

describe "domain object when using many-to-one primitive associations" do
  before(:each) do
    reset_db
    @library1 = PrimitiveExample::Library.create(:name => "WCU public library")
    @library2 = PrimitiveExample::Library.create(:name => "Sylva public library")
    @library3 = PrimitiveExample::Library.create(:name => "Franklin public library")
  
    @library1.book_distributor = "Books Inc."
    @library2.book_distributor = "Books Inc."
    @library3.book_distributor = "Books Inc."
  end

  it "should be able to retrieve the primitive association" do
    @library1.book_distributor.should == "Books Inc."
    @library2.book_distributor.should == "Books Inc."
    @library3.book_distributor.should == "Books Inc."
  end
  
  it "should be able to remove a primitive from the association" do
    @library1.book_distributor = nil
    @library1.book_distributor.should be_nil
    @library2.book_distributor.should == "Books Inc."
    @library3.book_distributor.should == "Books Inc."
  end
end

# The generators do not create associations from one-to-one associations from primitives but instead cast them as attributes.  That being the case, these last two sets of tests are somewhat irrelevant to any models generated currently.
describe "domain object when using one-to-one primitive associations (one-to-many side)" do
  before(:each) do
    reset_db
    @library = PrimitiveExample::Library.create(:name => "WCU public library")
    @library.library_acronym = "WCUPL"
  end

  it "should be able to retrieve the primitive association" do
    @library.library_acronym.should == "WCUPL"
  end
  
  it "should be able to remove a primitive from the association" do
    @library.library_acronym = nil
    @library.library_acronym.should be_nil
  end
end

describe "domain object when using one-to-one primitive associations (many-to-one side)" do
  before(:each) do
    reset_db
    @library = PrimitiveExample::Library.create(:name => "WCU public library")
    @library.library_id = 123456
  end

  it "should be able to retrieve the primitive association" do
    @library.library_id.should == 123456
  end
  
  it "should be able to remove a primitive from the association" do
    @library.library_id = nil
    @library.library_id.should be_nil
  end
end
