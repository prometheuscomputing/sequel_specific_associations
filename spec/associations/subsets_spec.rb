require 'spec_helper'

describe "associations with subset constraints" do
  before(:each) do
    reset_db
    @dorm = College::Dormitory.create(:address => "n/a")
    @student1 = College::Student.create(:name => 'Bob')
    @student2 = College::Student.create(:name => 'Phil')
    @student3 = College::Student.create(:name => 'Ted')
    @ta1 = College::TeachersAssistant.create(:name => 'Alex')
    @ta2 = College::TeachersAssistant.create(:name => 'Vicky')
  end
  
  it "should not allow adding an element that is not in the defined superset" do
    @dorm.add_occupant @student1
    # Fails
    lambda {@student1.add_suitemate(@student2)}.should raise_error(SpecificAssociations::FailedSubsetConstraint)
    # Satisfy first constraint
    @dorm.add_occupant @student2
    # Succeeds
    @student1.add_suitemate(@student2)
    @student1.suitemates.should =~ [@student2]
    
    # Second example
    # 1st student
    lambda {@ta1.add_student(@student1)}.should raise_error(SpecificAssociations::FailedSubsetConstraint)
    @ta1.add_contact(@student1)
    @ta1.add_student(@student1)
    @ta1.students.should =~ [@student1]
    # 2nd student
    lambda {@ta1.add_student(@student2)}.should raise_error(SpecificAssociations::FailedSubsetConstraint)
    @ta1.add_contact(@student2)
    @ta1.add_student(@student2)
    @ta1.students.should =~ [@student1, @student2]
    
    # Third example
    lambda {@ta2.favorite_student = @student1}.should raise_error(SpecificAssociations::FailedSubsetConstraint)
    @ta2.add_contact(@student1)
    @ta2.add_student(@student1)
    @ta2.favorite_student = @student1
    @ta2.favorite_student.should == @student1
  end
  
  it "should only return unassociated objects that are within the defined superset" do
    # Test that student can report unassociated suitemates even if dorm is not defined (should be empty)
    @student1.suitemates_unassociated.should =~ []
    
    @dorm.add_occupant @student1
    @dorm.add_occupant @student2
    @student1.suitemates_unassociated.should =~ [@student1, @student2]
    
    @student1.add_suitemate @student2
    @student1.suitemates_unassociated.should =~ [@student1]
    
    @student1.add_suitemate @student1
    @student1.suitemates_unassociated.should =~ []
    
    # Third example
    @ta1.favorite_student_unassociated.should =~ []
    @ta1.add_contact(@student1)
    @ta1.add_contact(@student2)
    @ta1.add_student(@student1)
    @ta1.add_student(@student2)
    @ta1.favorite_student_unassociated.should =~ [@student1, @student2]
    @ta1.favorite_student = @student1
    @ta1.favorite_student_unassociated.should =~ [@student2]
  end
  
  it "should (currently) leave subset associations alone when the superset is altered -- This behavior may change" do
    @dorm.add_occupant @student1
    @dorm.add_occupant @student2
    @student1.add_suitemate(@student2)
    @dorm.remove_occupant @student2
    @student1.suitemates.should == [@student2]
  end
end