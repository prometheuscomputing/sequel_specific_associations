require 'spec_helper'

describe 'intantiate funcationality' do
  before :each do
    reset_db
    ChangeTracker.start
  end
  
  it 'should create a new instance of an object with the specified associations and attributes' do
    @minivan = Automotive::Minivan.create
    @bob = People::Person.instantiate(:name => 'Bob', :vehicles => [Automotive::Car.create, Automotive::Motorcycle.instantiate(:make => 'Honda'), @minivan], :occupying => @minivan, :handedness => 'Right Handed')
    @bob.name.should == 'Bob'
    @bob.handedness.value.should == 'Right Handed'
    @bob.vehicles.count.should == 3
    @bob.vehicles.should include(@minivan)
    @bob.occupying.should == @minivan
    @bob.motorcycles.first.make.value.should == 'Honda'
  end
end