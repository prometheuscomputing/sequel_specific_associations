module InterfaceTest
  # TODO: allow modules for interfaces
  class Named < Sequel::Model
    set_dataset :interfaces_named
    plugin :specific_associations
    interface!
    attribute :name, String
  end
  
  class Operator < Sequel::Model
    set_dataset :interfaces_operators
    plugin :specific_associations
    interface!
    one_to_many :System
  end
  
  class SpecialOperator < Operator
    set_dataset :interfaces_special_operators
    attribute :callsign, String
  end
  
  class Commando < Sequel::Model
    set_dataset :interfaces_commandos
    plugin :specific_associations
    implements Named
    implements SpecialOperator
  end
  
  class Person < Sequel::Model
    set_dataset :interfaces_people
    plugin :specific_associations
    implements Named
    implements Operator
    attribute :phone, String
    many_to_one :Identifier, :one_to_one => true, :opp_getter => :person, :getter => :identifier
  end
  
  class Seller < Sequel::Model
    plugin :specific_associations
    interface!
    attribute :product, String
    many_to_one :Identifier, :one_to_one => true, :opp_getter => :seller, :getter => :sales_identifier
  end
  
  class Salesman < Person
    set_dataset :interfaces_salesmen
    plugin :specific_associations
    implements Seller
    attribute :sales_id, SpecificAssociations::INT_TYPE
  end
  
  class Developer < Person
    set_dataset :interfaces_developers
    plugin :specific_associations
    attribute :dev_id, SpecificAssociations::INT_TYPE
    attribute :private_key, Custom_Profile::File
  end
  
  class SoftwareSalesman < Salesman
    set_dataset :interfaces_software_salesmen
    plugin :specific_associations
    child_of Developer
    attribute :software_sold, String
  end
  
  class Identifier < Sequel::Model
    set_dataset :interfaces_identifiers
    plugin :specific_associations
    attribute :value, String
    one_to_many :Person, :one_to_one => true, :opp_getter => :identifier, :getter => :person
    one_to_many :Seller, :one_to_one => true, :opp_getter => :sales_identifier, :getter => :seller
  end
  
  class Computer < Sequel::Model
    set_dataset :interfaces_computers
    plugin :specific_associations
    implements Operator
    attribute :name, String
  end
  
  class System < Sequel::Model
    set_dataset :interfaces_systems
    plugin :specific_associations
    attribute :name, String
    many_to_one :Operator
  end
  
  class SubSystem < System
    set_dataset :interfaces_sub_systems
    plugin :specific_associations
  end
end

# Computer and Person are Operators
# A System has one operators
# An Operator has many Systems

InterfaceTest::Commando.create_schema
InterfaceTest::Person.create_schema
InterfaceTest::Salesman.create_schema
InterfaceTest::Developer.create_schema
InterfaceTest::SoftwareSalesman.create_schema
InterfaceTest::Identifier.create_schema
InterfaceTest::Computer.create_schema
InterfaceTest::System.create_schema
InterfaceTest::SubSystem.create_schema