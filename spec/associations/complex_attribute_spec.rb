require 'spec_helper'

describe "complex attributes" do
  before :each do
    Application::Document.delete
    Application::WhitePaper.delete
    Application::Editor.delete
    @document = Application::Document.create(:title => "My Application::Document")
    @editor = Application::Editor.create(:name => "My Editor")
  end
  
  it "should correctly name columns when generated from association info" do
    Application::Document.columns.should include(:summary_content)
    Application::Document.columns.should include(:summary_markup_language)
    Application::Document.columns.should include(:summary)
    Application::Document.columns.should include(:script_content)
    Application::Document.columns.should include(:script_language)
    Application::Document.columns.should include(:script)
    Application::Document.columns.should include(:pdf_binary_data_id)
    Application::Document.columns.should include(:pdf_filename)
    Application::Document.columns.should include(:pdf_mime_type)
    Application::Document.columns.should include(:pdf)
    Application::Document.columns.should include(:body_content)
    Application::Document.columns.should include(:body_markup_language)
    Application::Document.columns.should include(:body)
    # Check complex attributes. They are not associations and should not have an id.
    Application::Document.columns.should_not include(:summary_id)
    Application::Document.columns.should_not include(:script_id)
    Application::Document.columns.should_not include(:pdf_id)
    Application::Document.columns.should_not include(:body_id)
  end
  
  it "should be able to set a complex attribute" do
    # Set up richtexts
    # Set up r1
    r1 = Custom_Profile::RichText.new
    r1.markup_language = "LaTeX"
    r1.content = "Hello World! \\alpha"
    r1.content.should == "Hello World! \\alpha"
    # Check that the object does not have an id
    r1.id.should be_nil
    # Add RichTextImage
    image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    rti = Custom_Profile::RichTextImage.create
    rti.image = image_file
    r1.add_image rti
    # Set up r2
    r2 = Custom_Profile::RichText.new
    r2.markup_language = "plaintext"
    r2.content = "plaintext string"
    
    
    
    # Set up code
    c = Custom_Profile::Code.new
    c.language = "Ruby"
    c.content = 'puts "Hello World!"'
    c.id.should be_nil
    
    # Set up file
    # Custom_Profile::File is _NOT_ persisted (changes are saved when File is assigned to an object with a file attribute )
    f = Custom_Profile::File.new
    # Custom_Profile::BinaryData _IS_ persisted (and will be associated to the object with a file attribute upon assignment of the File)
    binary_data = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    f.binary_data = binary_data
    
    # Go ahead and check that assignment produced a valid id for 'data_id'
    f.binary_data.id.should_not be_nil
    f.filename = 'example_file.txt'
    f.mime_type = 'Text' # Not sure on this
    f.id.should be_nil
    

    # Set & check richtext info
    @document.summary = r1
    @document.body = r2
    # Check r1
    @document.summary.should == r1
    @document[:summary_content].should == r1.content
    @document[:summary_markup_language].should == r1.markup_language
    # Check r1 associations
    @document.summary_content.should == r1.content
    @document.summary_markup_language.should == r1.markup_language
    @document.summary_images.should =~ [rti]
    # Check r2
    @document.body.should == r2
    @document[:body_content].should == r2.content
    @document[:body_markup_language].should == r2.markup_language
    # Check r2 associations
    @document.body_content.should == r2.content
    @document.body_markup_language.should == r2.markup_language
    @document.body_images.should =~ []

    
    # Set & check code info
    @document.script = c
    @document.script.should == c
    @document[:script_content].should == c.content
    @document[:script_language].should == c.language
    
    # Set & check file info
    @document.pdf = f
    # Check columns
    @document[:pdf_binary_data_id].should == binary_data.id
    @document[:pdf_filename].should == f.filename
    @document[:pdf_mime_type].should == f.mime_type
    # Check object access
    @document.pdf.filename.should == f.filename
    @document.pdf.mime_type.should == f.mime_type
    @document.pdf.binary_data.should == binary_data
    @document.pdf.binary_data.data.should == binary_data.data
  end
  
  it "should not create id's on the complex attributes if they are saved" do
    r = Custom_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
    c = Custom_Profile::Code.new(:language => "Ruby", :content => 'puts "Hello World!"')
    f = Custom_Profile::File.new(:filename => 'example_file.txt', :mime_type => 'Text')
    f.binary_data = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    
    
    r.id.should be_nil
    c.id.should be_nil
    f.id.should be_nil
    
    # Set complex attributes
    @document.summary = r
    @document.script = c
    @document.pdf = f

    # Check that id's are nil
    r.id.should be_nil
    c.id.should be_nil
    f.id.should be_nil
    
    # Check that id's are not set if the objects are accidentally saved
    r.save
    r.id.should be_nil
    c.save
    c.id.should be_nil
    f.save
    f.id.should be_nil
    new_r = @document.summary
    new_r.save
    new_r.id.should be_nil
    new_c = @document.script
    new_c.save
    new_c.id.should be_nil
    new_f = @document.pdf
    new_f.save
    new_f.id.should be_nil
  end
  
  it "should remove all attributes and composition type associations of a complex attribute when it is reset or removed (File example)" do
    # Set attribute
    f1 = Custom_Profile::File.new(:filename => 'example_file1.txt', :mime_type => 'Text')
    f1.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    @document.pdf = f1
    
    # Reset attribute
    f2 = Custom_Profile::File.new(:filename => 'example_file2.txt')
    f2.binary_data = bd2 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    @document.pdf = f2
    
    # Check that original File attributes and associated BinaryData were destroyed
    Custom_Profile::BinaryData[bd1.id].should be_nil
    @document[:pdf].should == true
    @document[:pdf_filename].should == 'example_file2.txt'
    @document[:pdf_mime_type].should be_nil
    @document[:pdf_binary_data_id].should == bd2.id
    
    # Delete attribute
    @document.pdf = nil
    
    # Check that new File attributes and associated BinaryData were destroyed
    Custom_Profile::BinaryData[bd2.id].should be_nil
    @document[:pdf].should == false
    @document[:pdf_filename].should be_nil
    @document[:pdf_mime_type].should be_nil
    @document[:pdf_binary_data_id].should be_nil
  end
  
  it "should delete composition associations of complex attributes when they are destroyed" do
    image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    image_file.destroy
    Custom_Profile::BinaryData[bd1.id].should be_nil
  end
  
  it "should delete composition associations of complex attributes when they are destroyed (multiple levels of composition)" do
    image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i1 = Custom_Profile::RichTextImage.create
    i1.image = image_file
    i1.destroy
    Custom_Profile::BinaryData[bd1.id].should be_nil
  end
    
  it "should remove all attributes and composition type associations of a complex attribute when it is reset or removed (RichText example)" do
    # Set attributes
    r1 = Custom_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
    image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i1 = Custom_Profile::RichTextImage.create
    i1.image = image_file
    r1.add_image i1
    r1.images.should =~ [i1]
    @document.summary = r1
    # Check state
    r1.images.should =~ [i1]
    @document.summary_images.should =~ [i1]
    @document.body_images.should =~ []
    
    # Add another attribute
    r3 = Custom_Profile::RichText.new(:content => 'Rich Text 3!', :markup_language => 'n/a')
    image_file_3 = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file_3.binary_data = bd3 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i3 = Custom_Profile::RichTextImage.create
    i3.image = image_file_3
    r3.add_image i3
    @document.body = r3
    # Check state
    r1.images.should =~ [i1]
    r3.images.should =~ [i3]
    @document.summary_images.should =~ [i1]
    @document.body_images.should =~ [i3]
    
    # Reset attribute
    r2 = Custom_Profile::RichText.new(:content => 'Rich Text 2!', :markup_language => 'Plain')
    image_file_2 = Custom_Profile::File.new(:filename => 'photo_2.jpeg', :mime_type => 'image/jpeg')
    image_file_2.binary_data = bd2 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i2 = Custom_Profile::RichTextImage.create
    i2.image = image_file_2
    r2.add_image i2
    r2.images.should =~ [i2]
    @document.summary = r2
    
    # Check that original File attributes and associated BinaryData were destroyed
    Custom_Profile::BinaryData[bd1.id].should_not be_nil
    @document[:summary].should == true
    @document[:summary_content].should == 'Rich Text 2!'
    @document[:summary_markup_language].should == 'Plain'
    #Note this was changed to not delete the old images and err on the side of caution for data loss prevention.  Association data on complex attributes will have to be explicitly deleted now
    @document.summary_images.should =~ [i2, i1]
    
    # Check that accessing the complex attribute works
    rt = @document.summary
    #Note this was changed to not delete the old images and err on the side of caution for data loss prevention.  Association data on complex attributes will have to be explicitly deleted now
    # rt.images.should =~ [i2]
    rt.images.should =~ [i2, i1]
    rt.images.first.image.data.should =~ /Hello World!/
    
    # Delete attribute
    @document.summary = nil
    
    # Check that new File attributes and associated BinaryData were destroyed
    Custom_Profile::BinaryData[bd2.id].should_not be_nil
    @document[:summary].should == false
    @document[:summary_content].should be_nil
    @document[:summary_markup_language].should be_nil
    #Note this was changed to not delete the old images and err on the side of caution for data loss prevention.  Association data on complex attributes will have to be explicitly deleted now
    @document.summary_images.should =~ [i2, i1]
    
    # Manually remove an complex attribute association element
    @document.remove_summary_image(i1)
    @document.summary_images.should =~ [i2]
    @document.body_images.should =~ [i3]
  end
  
  # NOTE: this test may cause issues elsewhere due to manipulation of the database schema.
  it "should remain backwards compatible with datasets that do not have a complex getter column" do
    Custom_Profile::RichTextImage.db.drop_column(Custom_Profile::RichTextImage.table_name, :rich_text_complex_getter)
    Custom_Profile::RichTextImage.set_dataset(Custom_Profile::RichTextImage.table_name)
    
    # Set summary rich text attribute with two images
    r1 = Custom_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
    image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    # add image_file
    i1 = Custom_Profile::RichTextImage.create
    i1.image = image_file
    r1.add_image i1
    # add image_file_2
    image_file_2 = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file_2.binary_data = bd2 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i2 = Custom_Profile::RichTextImage.create
    i2.image = image_file_2
    r1.add_image i2
    @document.summary = r1
    # Check state -- NOTE: without a complex attribute getter, the fallback is to return all images associated with the base object
    @document.summary_images.should =~ [i1, i2]
    @document.body_images.should =~ [i1, i2] # Incorrect, but best effort in fallback mode. Should be []
  
    # Add another rich text attribute with an image
    r3 = Custom_Profile::RichText.new(:content => 'Rich Text 3!', :markup_language => 'n/a')
    image_file_3 = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file_3.binary_data = bd3 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i3 = Custom_Profile::RichTextImage.create
    i3.image = image_file_3
    r3.add_image i3
    @document.body = r3
    # Check state
    @document.summary_images.should =~ [i1, i2, i3] # Incorrect, but best effort in fallback mode. Should be [i1, i2]
    @document.body_images.should =~ [i1, i2, i3] # Incorrect, but best effort in fallback mode. Should be [i3]
    
    # Manually remove an complex attribute association element
    @document.remove_summary_image(i1)
    @document.summary_images.should =~ [i2, i3] # Incorrect, but best effort in fallback mode. Should be [i2]
    @document.body_images.should =~ [i2, i3] # Incorrect, but best effort in fallback mode. Should be [i3]
    
    # Remove schema modifications
    Custom_Profile::RichTextImage.create_schema!
  end
  
  it "should remain backwards compatible with datasets that have a complex getter column that is not yet populated" do
    # Set summary rich text attribute with two images
    r1 = Custom_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
    image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    # add image_file
    i1 = Custom_Profile::RichTextImage.create
    i1.image = image_file
    r1.add_image i1
    # add image_file_2
    image_file_2 = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file_2.binary_data = bd2 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i2 = Custom_Profile::RichTextImage.create
    i2.image = image_file_2
    r1.add_image i2
    @document.summary = r1
    # Simulate unpoplulated complex getters by removing them prior to state check
    Custom_Profile::RichTextImage.each do |rti|
      rti[:rich_text_complex_getter] = nil
      rti.save
    end
    # Check state -- NOTE: without a complex attribute getter, the fallback is to return all images associated with the base object
    @document.summary_images.should =~ [i1, i2]
    @document.body_images.should =~ [i1, i2] # Incorrect, but best effort in fallback mode. Should be []
  
    # Add another rich text attribute with an image
    r3 = Custom_Profile::RichText.new(:content => 'Rich Text 3!', :markup_language => 'n/a')
    image_file_3 = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
    image_file_3.binary_data = bd3 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    i3 = Custom_Profile::RichTextImage.create
    i3.image = image_file_3
    r3.add_image i3
    @document.body = r3
    # Simulate unpoplulated complex getters by removing them prior to state check
    Custom_Profile::RichTextImage.each do |rti|
      rti[:rich_text_complex_getter] = nil
      rti.save
    end
    # Check state
    @document.summary_images.should =~ [i1, i2, i3] # Incorrect, but best effort in fallback mode. Should be [i1, i2]
    @document.body_images.should =~ [i1, i2, i3] # Incorrect, but best effort in fallback mode. Should be [i3]
    
    # Manually remove an complex attribute association element
    @document.remove_summary_image(i1)
    # Simulate unpoplulated complex getters by removing them prior to state check
    Custom_Profile::RichTextImage.each do |rti|
      rti[:rich_text_complex_getter] = nil
      rti.save
    end
    @document.summary_images.should =~ [i2, i3] # Incorrect, but best effort in fallback mode. Should be [i2]
    @document.body_images.should =~ [i2, i3] # Incorrect, but best effort in fallback mode. Should be [i3]
    
    # Test that resetting associations populates the empty column
    @document.summary_images = [i2]
    @document.body_images = [i3]
    @document.summary_images.should =~ [i2]
    @document.body_images.should =~ [i3]
  end
  
  context "Manipulating complex attributes' associations from the complex attribute owner" do
    it "should remove all associations" do
      # Set attribute
      r1 = Custom_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
      image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
      image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
      i1 = Custom_Profile::RichTextImage.create
      i1.image = image_file
      r1.add_image i1
      @document.summary = r1
    
      # Check current state
      @document.summary_images.should =~ [i1]
      @document.body_images.should =~ []
      @editor.description_images.should =~ []
      # Remove images from summary
      @document.remove_all_summary_images
      # Check state again
      @document.summary_images.should be_empty
      @document.body_images.should be_empty
      @editor.description_images.should be_empty
    end
    
    it "should set the association" do
      r1 = Custom_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
      image_file = Custom_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
      image_file.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
      i1 = Custom_Profile::RichTextImage.create
      i1.image = image_file
      r1.add_image i1
      @document.summary = r1
      
      i2 = Custom_Profile::RichTextImage.create
      i3 = Custom_Profile::RichTextImage.create
      
      @editor.description_images.should =~ []
      @document.summary_images.should =~ [i1]
      @document.summary_images = [i1, i2, i3]
      @editor.description_images.should =~ []
      @document.summary_images.should =~ [i1, i2, i3]
    end
  end
  
  # This is functioning as intended, so I'm taking this test out. -SD
  # The File -> BinaryData association is intended to be directional, so this is expected behavior.
  # it "should correctly dissociated complex attribute associations" do
  #   # Set attribute
  #   f1 = Custom_Profile::File.new(:filename => 'example_file1.txt', :mime_type => 'Text')
  #   f1.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
  #   @document.pdf = f1
  #
  #   # Dissociate the binary data
  #   pending "fixing complex association manipulation"
  #   fail
  #   bd1.file = nil
  #   # Check that the column indicates that the binary_data association is empty
  #   @document[:pdf_binary_data].should == false
  #   @document.pdf_binary_data.should be_nil
  # end
  
  it "should correctly destroy complex attribute associations" do
    # Set attribute
    f1 = Custom_Profile::File.new(:filename => 'example_file1.txt', :mime_type => 'Text')
    f1.binary_data = bd1 = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    @document.pdf = f1
    
    # Destroy the binary data
    bd1.destroy
    @document.pdf_binary_data.should be_nil
  end
  
  it "should correctly list the types of classes available to associate with for the complex attribute association" do
    # expect([Application::Document]).to match_array([Application::Document])
    Custom_Profile::RichTextImage.new.rich_text_type.should =~ [Application::Document, Application::WhitePaper, Application::Editor]
  end
  
  # This is no longer possible with the addition of the complex_attribute! method on RichText and other complex attributes (can never save or create them)
  # it "should still allow complex attributes to be used in associations" do
  #   r = Custom_Profile::RichText.create(:markup_language => "Markdown", :content => "Hi")
  #   r.id.should_not be_nil
  #   # Set & check richtext info
  #   @document.body = r
  #   @document.body.should == r
  # end
  
  it "should return nil if a complex attribute has not been set" do
    @document.summary.should be_nil
    @document.script.should be_nil
    @document.pdf.should be_nil
  end
  
  it "should allow a complex attribute to be unset" do
    r = Custom_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
    c = Custom_Profile::Code.new(:language => "Ruby", :content => 'puts "Hello World!"')
    f = Custom_Profile::File.new(:filename => 'example_file.txt', :mime_type => 'Text')
    f.binary_data = Custom_Profile::BinaryData.create(:data => File.binread(File.join(File.dirname(__FILE__), 'example_file.txt')))
    
    # Set complex attributes
    @document.summary = r
    @document.script = c
    @document.pdf = f
    
    # Unset complex attributes
    @document.summary = nil
    @document.script = nil
    @document.pdf = nil
    
    # Check results
    @document.summary.should be_nil
    @document.script.should be_nil
    @document.pdf.should be_nil
  end
  
  it "should correctly handle helper methods defined on complex attribute: File" do
    f = Custom_Profile::File.new(:filename => 'example_file.txt', :mime_type => 'Text')
    
    # Test custom setter method (seamlessly creates or reuses a BinaryData object)
    data_string = File.binread(File.join(File.dirname(__FILE__), 'example_file.txt'))
    f.data = data_string
    
    # Set file to document
    @document.pdf = f
    # Test existence of data
    @document.pdf.binary_data.data.should_not be_nil
    @document.pdf.binary_data.data.should == data_string
    
    # Test custom getter method (seamlessly accesses BinaryData object or returns nil if non-existent)
    @document.pdf.data.should_not be_nil
    @document.pdf.data.should == data_string
    
    # Test custom getter when BinaryData is nil
    f.binary_data = nil
    @document.pdf = f
    @document.pdf.binary_data.should be_nil
    @document.pdf.data.should be_nil
  end
  
  it "should allow the setting and unsetting of custom getters and setters" do
    Application::Document.add_attribute_getter(:summary) { |rt| rt[:content] = rt[:content].upcase; rt }
    Application::Document.add_attribute_setter(:summary) { |rt| rt[:content] = rt[:content].downcase; rt }
    r = Custom_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
    @document.summary = r
    
    # Check that custom setter was applied
    @document[:summary_content].should == "hello world! \\alpha"
    
    # Check that custom getter is applied
    @document.summary.content.should == "HELLO WORLD! \\ALPHA"
    
    # Unset custom getter/setter
    Application::Document.add_attribute_options(:summary, :custom_getter => nil, :custom_setter => nil)
    # Check that unset is working
    r2 = Custom_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
    @document.summary = r2
    @document[:summary_content].should == "Hello World! \\alpha" # No change in case
    @document.summary.content.should == "Hello World! \\alpha" # No change in case
  end
  
  it "should allow hooking custom code defined on the complex attribute for further customization" do
    Custom_Profile::RichText.send(:define_method, :on_retrieve) { |retrieved_from| self[:content] = self[:content].upcase }
    Custom_Profile::RichText.send(:define_method, :on_assign) { |assigned_to| self[:content] = self[:content].downcase }
    r = Custom_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
    @document.summary = r
    
    # Check that on_retrieve was called
    @document[:summary_content].should == "hello world! \\alpha"
    
    # Check that on_assign was called
    @document.summary.content.should == "HELLO WORLD! \\ALPHA"
    
    # Override methods to do nothing (so as not to mess up any additional tests)
    Custom_Profile::RichText.send(:define_method, :on_retrieve) { |retrieved_from| }
    Custom_Profile::RichText.send(:define_method, :on_assign) { |assigned_to| }
    # Check that overrides are working
    r2 = Custom_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
    @document.summary = r2
    @document[:summary_content].should == "Hello World! \\alpha" # No change in case
    @document.summary.content.should == "Hello World! \\alpha" # No change in case
  end
  
  it "should return informational hashes about the complex attributes" do
    attr_info = Application::Document.attributes
    attr_info[:summary].should be_a(Hash)
    attr_info[:summary][:complex].should == true
    
    attr_info[:script].should be_a(Hash)
    attr_info[:script][:complex].should == true
    
    attr_info[:pdf].should be_a(Hash)
    attr_info[:pdf][:complex].should == true
  end
end