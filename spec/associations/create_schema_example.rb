module Application
  class Test < Sequel::Model
    plugin :specific_associations
    attribute :name, String, :label => 'myName'#, :primary_key => true
    attribute :number, SpecificAssociations::INT_TYPE
    one_to_many :Thing
  #  puts create_schema

  #  puts columns
  end

  class Thing < Sequel::Model
    plugin :specific_associations
    attribute :thing_name, String
    many_to_one :Test
  end
  # TODO: Add tests for above association
  
  class Dude < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    # many_to_one :Problem
  end
  
  class Problem < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    # one_to_many :Dude
  end
end
