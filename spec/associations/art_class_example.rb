module ArtClass
  class ThicknessRange < Sequel::Model
      # set_dataset :thickness_ranges
      plugin :specific_associations
      complex_attribute!
      many_to_one :'ArtClass::Descriptor', :getter => :units , :opp_getter => :thickness_ranges_with_units , :enumeration => true
      attribute :min, String
      attribute :max, String
  end
  
  class NumericalRange < Sequel::Model
      # set_dataset :thickness_ranges
      plugin :specific_associations
      complex_attribute!
      attribute :low, Integer
      attribute :high, Integer
  end
  
  class Material < Sequel::Model
    # set_dataset :materials
    plugin :specific_associations
    interface!
    many_to_many :'ArtClass::Student', :getter => :students, :opp_getter => :materials, :through => :MaterialStudent
  end
  
  class Pencil < Sequel::Model
    set_dataset :art_class_pencils
    plugin :specific_associations
    implements ArtClass::Material
    attribute :weight, String
    attribute :thickness, :'ArtClass::ThicknessRange'
    attribute :usable_length, :'ArtClass::NumericalRange'
    many_to_one :'ArtClass::Student', :getter => :user, :opp_getter => :pencils
  end
  
  class Paint < Sequel::Model
    set_dataset :art_class_paints
    plugin :specific_associations
    implements ArtClass::Material
    attribute :color, String
    attribute :viscosity, :'ArtClass::ThicknessRange'
    many_to_one :'ArtClass::Descriptor', :getter => :consistency , :opp_getter => :paints_with_consistency , :enumeration => true
  end
  
  class Brush < Sequel::Model
    set_dataset :art_class_brushes
    plugin :specific_associations
    implements ArtClass::Material
    attribute :style, String
    attribute :bristle_thickness, :'ArtClass::ThicknessRange'
    many_to_one :'ArtClass::Descriptor', :getter => :softness , :opp_getter => :brushes_with_softness , :enumeration => true
  end

  class Substrate < Sequel::Model
    set_dataset :art_class_substrates
    plugin :specific_associations
    implements ArtClass::Material
    attribute :name, String
    attribute :thickness, :'ArtClass::ThicknessRange'
    many_to_one :'ArtClass::Descriptor', :getter => :hardness , :opp_getter => :substrates_with_hardness , :enumeration => true
  end
  
  class Student < Sequel::Model
    set_dataset :art_class_students
    plugin :specific_associations
    attribute :name, String
    many_to_many :'ArtClass::Material', :getter => :materials, :opp_getter => :students, :through => :MaterialStudent
    one_to_many :'ArtClass::Pencil', :getter => :pencils, :opp_getter => :user
  end
  
  class MaterialStudent < Sequel::Model
    set_dataset :art_class_materials_students
    plugin :specific_associations
    associates :Material
    associates :Student
  end
  

  
  # An improbably enumeration that we just dump all different kinds of descriptors into for the sake of testing
  class Descriptor < Sequel::Model
      set_dataset :descriptors
      plugin :specific_associations
      enumeration!
      one_to_many :'ArtClass::ThicknessRange', :getter => :thickness_ranges_with_units , :opp_getter => :units
      one_to_many :'ArtClass::Paint', :getter => :paints_with_consistency , :opp_getter => :consistency
      one_to_many :'ArtClass::Brush', :getter => :brushes_with_softness , :opp_getter => :softness
      one_to_many :'ArtClass::Substrate', :getter => :substrates_with_hardness , :opp_getter => :hardness
      attribute :value, String
  end
end

ArtClass::Paint.create_schema?
ArtClass::Brush.create_schema?
ArtClass::Substrate.create_schema?
ArtClass::Pencil.create_schema?
ArtClass::Student.create_schema?
ArtClass::MaterialStudent.create_schema?
# ArtClass::ThicknessRange.create_schema? # unnecessary, right?
ArtClass::Descriptor.create_schema?

# create some enum literals.  probably more than we need...
ArtClass::Descriptor.where(:value => 'nice').first   || ArtClass::Descriptor.create(:value => 'nice')
ArtClass::Descriptor.where(:value => 'thin').first   || ArtClass::Descriptor.create(:value => 'thin')
ArtClass::Descriptor.where(:value => 'thick').first  || ArtClass::Descriptor.create(:value => 'thick')
ArtClass::Descriptor.where(:value => 'hard').first   || ArtClass::Descriptor.create(:value => 'hard')
ArtClass::Descriptor.where(:value => 'medium').first || ArtClass::Descriptor.create(:value => 'medium')
ArtClass::Descriptor.where(:value => 'soft').first   || ArtClass::Descriptor.create(:value => 'soft')
ArtClass::Descriptor.where(:value => 'thou').first   || ArtClass::Descriptor.create(:value => 'thou')
ArtClass::Descriptor.where(:value => 'micron').first || ArtClass::Descriptor.create(:value => 'micron')

#  Model Extensions
class ArtClass::Brush
  alias_attribute(:name, :style)
  alias_attribute(:t_range, :bristle_thickness)
end

class ArtClass::Paint
  alias_attribute(:name, :color)
  alias_attribute(:t_range, :viscosity)
end

class ArtClass::Substrate
  alias_attribute(:t_range, :thickness)
end

class ArtClass::Pencil
  alias_column(:at_least, :usable_length_low, Integer)
end

class ArtClass::Student
  alias_attribute(:eman, :name)
end
