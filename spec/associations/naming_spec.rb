require 'spec_helper'

describe SpecificAssociations, " when deriving method names from properties and classes" do
  before :each do
    Application::Nexus.delete; Application::Status.delete; Application::Cactus.delete; Application::Mass.delete; Application::NexusMass.delete
    @nexus = Application::Nexus.create(:name => 'myNexus')
    @nexus2 = Application::Nexus.create(:name => 'Nexus2')
    @status = Application::Status.create(:status => 'myStatus')
    @cactus = Application::Cactus.create
    @cactus2 = Application::Cactus.create
    @mass = Application::Mass.create
  end
  
  context "column naming for associations" do
    it "should correctly name columns when generated from association info" do
      Application::Nexus.columns.should include(:cactus_in_tests_id)
      Application::Status.columns.should include(:nexus_id)
      Application::Cactus.columns.should include(:nexus_id)
      Application::NexusMass.columns.should include(:nexus_id)
      Application::NexusMass.columns.should include(:mass_id)
    end
  end
  
  context "getter naming for one_to_one/one_to_many/many_to_one" do
    it "should not get confused by classes or property names ending in 's' from the singular side" do
      @nexus.add_status @status
      @nexus.statuses.should include(@status)
      @nexus.cactus = @cactus
      @nexus.cactus.should == @cactus
      @nexus.cactus_in_tests = @cactus2
      @nexus.cactus_in_tests.should == @cactus2
      @nexus.save
    end
    
    it "should not get confused by classes or properties ending in 's' from the plural side" do
      @status.nexus = @nexus
      @status.nexus.should == @nexus
      @status.save
      @cactus.nexus = @nexus
      @cactus.nexus.should == @nexus
      @cactus.nexus_in_tests = @nexus2
      @cactus.nexus_in_tests.should == @nexus2
      @cactus.save
    end
  end
  
  context "getter naming for many_to_many" do
    it "should not get confused by classes or properties ending in 's' from either plural side" do
      @nexus.add_mass @mass
      @nexus.masses.should include(@mass)
      @nexus.save
      @mass.nexuses.should include(@nexus)
      @mass.remove_nexus(@nexus)
      @mass.nexuses.should be_empty
      @mass.add_nexus @nexus
      @mass.nexuses.should include(@nexus)
    end
  end
  
  # context "naming mismatches" do
  #   it "should guess when naming mismatches have occurred and use a best-guess approach for backwards compatibility" do
  #     pending "Write this test"
  #     fail
  #   end
  #
  #   it "should not equate 'associates' methods with normal associations" do
  #     pending "Write this test"
  #     fail
  #   end
  # end
end