require 'spec_helper'
# While it may be true that the database orders by id by default but we should not be testing for that because it isn't a behavior of SSA.  On that basis, the tests for default ordering by id are commented out.
describe "domain object when manually ordering associations" do
  context "when ordering associations with ChangeTracker" do
    before(:each) do
      reset_db
      ChangeTracker.start
      # Setting up many-to-many polymorphic
      @person1 = People::Person.create(:name => "Chris")
      @person2 = People::Person.create(:name => "John")
      @person3 = Automotive::Mechanic.create(:name => "Kyle")
      @car1 = Automotive::Car.create(:vehicle_model => 'Thunderbird', :cost => 5000)
      @car1.make = 'Ford'; @car1.save 
      @car2 = Automotive::Motorcycle.create(:vehicle_model => 'Rebel', :cost => 4000)
      @car2.make = 'Honda'; @car2.save 
      @car3 = Automotive::Car.create(:vehicle_model => 'Taurus', :cost => 9000)
      @car3.make = 'Ford'; @car3.save 
      @person1.add_drife(@car1)
      @person1.add_drife(@car2)
      @person1.add_drife(@car3)

      @person2.add_drife(@car2)
      @person2.add_drife(@car3)

      # Polymorphic to polymorphic relation
      @person3.add_drife(@car2)
      @repair_shop = Automotive::RepairShop.create(:name => 'RepairShop')
      @car1.being_repaired_by = @repair_shop
      @car2.being_repaired_by = @repair_shop
      @car3.being_repaired_by = @repair_shop
      ChangeTracker.commit
    end
  end

  context "when ordering associations without ChangeTracker" do
    before(:each) do
      reset_db
      # many_to_many type ( Library => PatronStatusAtLibrary <= Patron)
      @library1 = Application::Library.create(:name => "WCU public library",   :created_at_string => 'A')
      @library2 = Application::Library.create(:name => "Sylva public library", :created_at_string => 'B')
      @library3 = Application::OnlineLibrary.create(:name => "New York online library")

      @patron1 = Application::Patron.create(:first_name => "Bob")
      @patron2 = Application::Patron.create(:first_name => "Sue")
      @patron3 = Application::Patron.create(:first_name => "Bill")
    
      @library1.add_patron(@patron1)
      @library1.add_patron(@patron2)
      @library1.add_patron(@patron3)

      @library2.add_patron(@patron1)
      @library2.add_patron(@patron2)

      @library3.add_patron(@patron1)

      @patron1_and_library1_join = Application::PatronStatusAtLibrary.where(:patron_id => @patron1.id, :library_id => @library1.id, :library_class => @library1.class.to_s).first
      @patron1_and_library1_join.status = 'Completed'
      @patron1_and_library1_join.created_at_string = 'B'
      @patron1_and_library1_join.save
      @patron1_and_library2_join = Application::PatronStatusAtLibrary.where(:patron_id => @patron1.id, :library_id => @library2.id, :library_class => @library2.class.to_s).first
      @patron1_and_library2_join.status = 'Pending'
      @patron1_and_library2_join.created_at_string = 'A'
      @patron1_and_library2_join.save
      @patron1_and_library3_join = Application::PatronStatusAtLibrary.where(:patron_id => @patron1.id, :library_id => @library3.id, :library_class => @library3.class.to_s).first
      @patron1_and_library3_join.status = nil
      @patron1_and_library3_join.created_at_string = 'C'
      @patron1_and_library3_join.save
    
      # one_to_many type ( Library => EBook)
      @book1 = Application::EBook.create(:title => "War and Peace")
      @book2 = Application::EBook.create(:title => "Crime and Punishment")
      @book3 = Application::EBook.create(:title => "The Brothers Karamazov")
      @book4 = Application::Dictionary.create(:title => "Websters")
      @book5 = Application::Dictionary.create(:title => "Oxford")
      @book6 = Application::Dictionary.create(:title => "Russian Dictionary")
    
      @library1.add_book(@book1)
      @library1.add_book(@book2)
      @library1.add_book(@book3)
      @library1.add_book(@book4)
      @library1.add_book(@book5)
      @library1.add_book(@book6)
    end
    context "in a one to many" do
      # it "should be able to order by the default id ordering with type filter" do
      #   @library1.books([{'type' => Application::Dictionary}]).should == [@book4, @book5, @book6]
      # end
      #
      # it "should be able to order by the default id ordering" do
      #   # Orderd by Type, then ordered by ID
      #   # Not currently supporting a default ordering, but this test passes consistently
      #   @library1.books([{}]).should == [@book1, @book2, @book3, @book4, @book5, @book6]
      # end

      it "should be able to order by type ascending" do
        @book7 = Application::EBook.create(:title => "The Lord of the Rings")
        @library1.add_book(@book7)
        @library1.books([{}], nil, nil, false, {:column => 'type'}).should == [@book4, @book5, @book6, @book1, @book2, @book3, @book7]
      end

      it "should be able to order by type descending" do
        @book7 = Application::EBook.create(:title => "The Lord of the Rings")
        @library1.add_book(@book7)
        @library1.books([{}], nil, nil, false, {:column => 'type', :descending_order => true}).should == [@book1, @book2, @book3, @book7, @book4, @book5, @book6]
      end

      it "should be able to order by ascending string column with type filter" do
        @library1.books([{'type' => Application::Dictionary}], nil, nil, false, {:column => :title}).should == [@book5, @book6, @book4]
        # Check that default value matches passed in value for descending order
        @library1.books([{'type' => Application::Dictionary}], nil, nil, false, {:column => :title, :descending_order => false}).should == [@book5, @book6, @book4]
      end
      
      it "should be able to order by descending string column with type filter" do
        @library1.books([{'type' => Application::Dictionary}], nil, nil, false, {:column => :title, :descending_order => true}).should == [@book4, @book6, @book5]
      end

      it "should be able to order by ascending string column" do
        @library1.books([{}], nil, nil, false, {:column => :title}).should == [@book2, @book5, @book6, @book3, @book1, @book4]
      end

      it "should be able to order by descending string column" do
        @library1.books([{}], nil, nil, false, {:column => :title, :descending_order => true}).should == [@book4, @book1, @book3, @book6, @book5, @book2]
      end

      it "should be able to order by ascending position column" do
        @library1.books([{}], nil, nil, false, {:column => :library_position}).should == [@book1, @book2, @book3, @book4, @book5, @book6]
      end

      it "should be able to order by descending position column" do
        @library1.books([{}], nil, nil, false, {:column => :library_position, :descending_order => true}).should == [@book6, @book5, @book4, @book3, @book2, @book1]
      end
    end
    context "in a many to many" do
      # it "should be able to order by default id column" do
      #   # NOT right... it's ordering by groups, then by ids, not ids by themselves.
      #   @patron1.libraries.should == [@library1, @library2, @library3]
      # end
      it "should be able to order by ascending id column" do
        @patron1.libraries([{}], nil, nil, false, {:column => :id}).should == [@library1, @library2, @library3]
      end
      it "should be able to order by descending id column" do
        @patron1.libraries([{}], nil, nil, false, {:column => :id, :descending_order => true}).should == [@library3, @library2, @library1]
      end
      it "should be able to order by ascending string column" do
        @patron1.libraries([{}], nil, nil, false, {:column => :name}).should == [@library3, @library2, @library1]
      end
      it "should be able to order by descending string column" do
        @patron1.libraries([{}], nil, nil, false, {:column => :name, :descending_order => true}).should == [@library1, @library2, @library3]
      end
      it "should be able to order by ascending type column" do
        @patron1.libraries([{}], nil, nil, false, {:column => 'type'}).should == [@library1, @library2, @library3]
        @patron1.libraries([{}], nil, nil, false, {:column => 'type', :descending_order => false}).should == [@library1, @library2, @library3]
      end
      it "should be able to order by descending type column" do
        @patron1.libraries([{}], nil, nil, false, {:column => 'type', :descending_order => true}).should == [@library3, @library1, @library2]
      end
      # Makes no sense to try to 'order' the types of the association class, but need to make sure it doesn't break anything.
      # The code only has login in place to sort by association type, not the association class type (will order by association types).
      it "should be able to order by ascending type column of the association class?" do
        @patron1.libraries([{}], nil, nil, false, {:column => 'type', :on_association_class => true}).should == [@library1, @library2, @library3]
      end
      # Makes no sense to try to 'order' the types of the association class, but need to make sure it doesn't break anything
      # The code only has login in place to sort by association type, not the association class type (will order by association types).
      it "should be able to order by descending type column of the association class?" do
        @patron1.libraries([{}], nil, nil, false, {:column => 'type', :descending_order => true, :on_association_class => true}).should == [@library3, @library1, @library2]
      end
      # Order by column when one of it's values is nil?
      it "should be able to order by ascending string column with one column containing a nil value" do
        @patron1.libraries([{}], nil, nil, false, {:column => :name}).should == [@library3, @library2, @library1]
      end
      it "should be able to order by descending string column with one column containing a nil value" do
        @patron1.libraries([{}], nil, nil, false, {:column => :name, :descending_order => true}).should == [@library1, @library2, @library3]
      end
      it "should be able to order by ascending association class attribute column" do
        # Automatically determining that the attribute is on the association class.
        @patron1.libraries([{}], nil, nil, false, {:column => :status}).should == [@library1, @library2, @library3]
        # Manually specifying the attribute location on the association class.
        @patron1.libraries([{}], nil, nil, false, {:column => :status, :on_association_class => true}).should == [@library1, @library2, @library3]
      end
      it "should be able to order by descending association class attribute column" do
        # Automatically determining that the attribute is on the association class.
        @patron1.libraries([{}], nil, nil, false, {:column => :status, :descending_order => true}).should == [@library3, @library2, @library1]
        # Manually specifying the attribute location on the association class.
        @patron1.libraries([{}], nil, nil, false, {:column => :status, :descending_order => true, :on_association_class => true}).should == [@library3, @library2, @library1]
      end
      it "should be able to order by ascending association class attribute column that has the same name as the association" do
        # Automatically determining that the attribute is on the association class, as per default.
        @patron1.libraries([{}], nil, nil, false, {:column => :created_at_string}).should == [@library2, @library1, @library3]
        # Manually specifying the attribute location on the association class.
        @patron1.libraries([{}], nil, nil, false, {:column => :created_at_string, :on_association_class => true}).should == [@library2, @library1, @library3]
        # Manually specifying the attribute location on the association.
        @patron1.libraries([{}], nil, nil, false, {:column => :created_at_string, :on_association_class => false}).should == [@library1, @library2, @library3]
      end
      it "should be able to order by descending association class attribute column that has the same name as the association" do
        # Automatically determining that the attribute is on the association class, as per default.
        @patron1.libraries([{}], nil, nil, false, {:column => :created_at_string, :descending_order => true}).should == [@library3, @library1, @library2]
        # Manually specifying the attribute location on the association class.
        @patron1.libraries([{}], nil, nil, false, {:column => :created_at_string, :descending_order => true, :on_association_class => true}).should == [@library3, @library1, @library2]
        # Manually specifying the attribute location on the association.
        @patron1.libraries([{}], nil, nil, false, {:column => :created_at_string, :descending_order => true, :on_association_class => false}).should == [@library3, @library2, @library1]
      end
    end
  end

  context "when ordering unassociations with ChangeTracker" do
    before(:each) do
      reset_db
      ChangeTracker.start
      # Setting up many-to-many polymorphic
      @person1 = People::Person.create(:name => "Chris")
      @person2 = People::Person.create(:name => "John")
      @person3 = Automotive::Mechanic.create(:name => "Kyle")
      @car1 = Automotive::Car.create(:vehicle_model => 'Thunderbird', :cost => 5000)
      @car1.make = 'Ford'; @car1.save 
      @car2 = Automotive::Motorcycle.create(:vehicle_model => 'Rebel', :cost => 4000)
      @car2.make = 'Honda'; @car2.save 
      @car3 = Automotive::Car.create(:vehicle_model => 'Taurus', :cost => 9000)
      @car3.make = 'Ford'; @car3.save 
      @person1.add_drife(@car1)
      @person1.add_drife(@car2)
      @person1.add_drife(@car3)

      @person2.add_drife(@car2)
      @person2.add_drife(@car3)

      @unassoc_car1 = Automotive::Car.create(:make => 'Ford', :vehicle_model => 'f150', :cost => 5000)
      @unassoc_car2 = Automotive::Motorcycle.create(:make => 'Honda', :vehicle_model => 'Trouble', :cost => 4000)
      @unassoc_car3 = Automotive::Car.create(:make => 'Ford', :vehicle_model => 'f100', :cost => 9000)
      @unassoc_person1 = People::Person.create(:name => "Jack")
      @unassoc_person2 = People::Person.create(:name => "Phil")
      @unassoc_person3 = Automotive::Mechanic.create(:name => "Uju")

      # Polymorphic to polymorphic relation
      @person3.add_drife(@car2)
      @repair_shop = Automotive::RepairShop.create(:name => 'RepairShop')
      @car1.being_repaired_by = @repair_shop
      @car2.being_repaired_by = @repair_shop
      @car3.being_repaired_by = @repair_shop
      ChangeTracker.commit
    end
  end
  
  context "when ordering unassociations without ChangeTracker" do
    before(:each) do
      reset_db
      # many_to_many type ( Library => PatronStatusAtLibrary <= Patron)
      @library1 = Application::Library.create(:name => "WCU public library")
      @library2 = Application::Library.create(:name => "Sylva public library")
      @online_library1 = Application::OnlineLibrary.create(:name => "New York online library")

      @unassoc_library1 = Application::Library.create(:name => "Waynesville public library")
      @unassoc_library2 = Application::Library.create(:name => "Clyde public library")

      @patron1 = Application::Patron.create(:first_name => "Bob")
      @patron2 = Application::Patron.create(:first_name => "Sue")
      @patron3 = Application::Patron.create(:first_name => "Bill")
    
      @unassoc_patron1 = Application::Patron.create(:first_name => "Jake")
      @unassoc_library1.add_patron(@unassoc_patron1)
      @unassoc_library2.add_patron(@unassoc_patron1)
      @unassoc_patron2 = Application::Patron.create(:first_name => "Kyle")
      @unassoc_patron3 = Application::Patron.create(:first_name => "Jade")

      @library1.add_patron(@patron1)
      @library1.add_patron(@patron2)
      @library1.add_patron(@patron3)

      @library2.add_patron(@patron1)
      @library2.add_patron(@patron2)
      @library2.add_patron(@patron3)
    
      # one_to_many type ( Library => EBook)
      @book1 = Application::EBook.create(:title => "War and Peace")
      @book2 = Application::Dictionary.create(:title => "Websters")

      @unassoc_book1 = Application::EBook.create(:title => "War and Peace")
      @unassoc_book2 = Application::EBook.create(:title => "Crime and Punishment")
      @unassoc_book3 = Application::EBook.create(:title => "The Brothers Karamazov")
      @unassoc_book4 = Application::Dictionary.create(:title => "Websters")
      @unassoc_book5 = Application::Dictionary.create(:title => "Oxford")
      @unassoc_book6 = Application::Dictionary.create(:title => "Russian Dictionary")
    
      @library1.add_book(@book1)
      @library1.add_book(@book2)
    end
    context "in a one to many" do
      it "should be able to order by the default id ordering with filter" do
        @library1.books_unassociated([{'type' => Application::Dictionary}]).should == [@unassoc_book4, @unassoc_book5, @unassoc_book6]
      end

      it "should be able to order by the default id ordering" do
        pending # No default ordering yet. When run as a single test, gets different results then when run in a suite
        # (when run as a single test)
        #<Dictionary @values={:id=>81, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Websters"}>,
        #<EBook @values={:id=>83, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"War and Peace"}>,
        #<Dictionary @values={:id=>82, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Oxford"}>,
        #<EBook @values={:id=>84, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Crime and Punishment"}>,
        #<Dictionary @values={:id=>83, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Russian Dictionary"}>,
        #<EBook @values={:id=>85, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"The Brothers Karamazov"}>]
        # (when run in a suite)
        #<Dictionary @values={:id=>81, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Websters"}>,
        #<Dictionary @values={:id=>82, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Oxford"}>,
        #<Dictionary @values={:id=>83, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Russian Dictionary"}>,
        #<EBook @values={:id=>83, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"War and Peace"}>,
        #<EBook @values={:id=>84, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"Crime and Punishment"}>,
        #<EBook @values={:id=>85, :library_id=>nil, :library_class=>nil, :library_position=>nil, :title=>"The Brothers Karamazov"}>]
        @library1.books_unassociated([{}]).should == [@unassoc_book4, @unassoc_book1, @unassoc_book5, @unassoc_book2, @unassoc_book6, @unassoc_book3]
      end

      it "should be able to order by type ascending" do
        @unassoc_book7 = Application::EBook.create(:title => "The Lord of the Rings")
        @library1.books_unassociated([{}], nil, nil, {:column => 'type'}).should == [@unassoc_book4, @unassoc_book5, @unassoc_book6, @unassoc_book1, @unassoc_book2, @unassoc_book3, @unassoc_book7]
      end

      it "should be able to order by type descending" do
        @unassoc_book7 = Application::EBook.create(:title => "The Lord of the Rings")
        @library1.books_unassociated([{}], nil, nil, {:column => 'type', :descending_order => true}).should == [@unassoc_book1, @unassoc_book2, @unassoc_book3, @unassoc_book7, @unassoc_book4, @unassoc_book5, @unassoc_book6]
      end

      it "should be able to order by ascending string column with type filter" do
        @library1.books_unassociated([{'type' => Application::Dictionary}], nil, nil, {:column => :title}).should == [@unassoc_book5, @unassoc_book6, @unassoc_book4]
        # Check that default value matches passed in value for descending order
        @library1.books_unassociated([{'type' => Application::Dictionary}], nil, nil, {:column => :title, :descending_order => false}).should == [@unassoc_book5, @unassoc_book6, @unassoc_book4]
      end
      
      it "should be able to order by descending string column with type filter" do
        @library1.books_unassociated([{'type' => Application::Dictionary}], nil, nil, {:column => :title, :descending_order => true}).should == [@unassoc_book4, @unassoc_book6, @unassoc_book5]
      end

      it "should be able to order by ascending string column" do
        @library1.books_unassociated([{}], nil, nil, {:column => :title}).should == [@unassoc_book2, @unassoc_book5, @unassoc_book6, @unassoc_book3, @unassoc_book1, @unassoc_book4]
      end

      it "should be able to order by descending string column" do
        @library1.books_unassociated([{}], nil, nil, {:column => :title, :descending_order => true}).should == [@unassoc_book4, @unassoc_book1, @unassoc_book3, @unassoc_book6, @unassoc_book5, @unassoc_book2]
      end
    end
    context "in a many to many" do
      it "should be able to order by default id column" do
        pending # No default ordering yet. When run as a single test, gets different results then when run in a suite
        # (when run as a single test)
        #<OnlineLibrary @values={:id=>34, :name=>"New York online library", :created_at_string=>nil, :url=>nil}>,
        #<Library @values={:id=>84, :name=>"Sylva public library", :created_at_string=>nil}>]
        # (when run in a suite)
        #<OnlineLibrary @values={:id=>34, :name=>"New York online library", :created_at_string=>nil, :url=>nil}>,
        #<Library @values={:id=>83, :name=>"WCU public library", :created_at_string=>nil}>,
        #<Library @values={:id=>84, :name=>"Sylva public library", :created_at_string=>nil}>]
        @unassoc_patron1.libraries_unassociated.should == [@library1, @online_library1, @library2]
      end
      it "should be able to order by ascending id column" do
        pending # No default ordering yet. When run as a single test, gets different results then when run in a suite
        # (when run as a single test)
        #<Library @values={:id=>5, :name=>"WCU public library", :created_at_string=>nil}>,
        #<OnlineLibrary @values={:id=>2, :name=>"New York online library", :created_at_string=>nil, :url=>nil}>,
        #<Library @values={:id=>6, :name=>"Sylva public library", :created_at_string=>nil}>]
        # (when run in a suite)
        #<OnlineLibrary @values={:id=>2, :name=>"New York online library", :created_at_string=>nil, :url=>nil}>,
        #<Library @values={:id=>5, :name=>"WCU public library", :created_at_string=>nil}>,
        #<Library @values={:id=>6, :name=>"Sylva public library", :created_at_string=>nil}>
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => :id}).should == [@library1, @online_library1, @library2]
      end
      it "should be able to order by descending id column" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => :id, :descending_order => true}).should == [@library2, @library1, @online_library1]
      end
      it "should be able to order by ascending string column" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => :name}).should == [@online_library1, @library2, @library1]
      end
      it "should be able to order by descending string column" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => :name, :descending_order => true}).should == [@library1, @library2, @online_library1]
      end
      it "should be able to order by ascending type column" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => 'type'}).should == [@library1, @library2, @online_library1]
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => 'type', :descending_order => false}).should == [@library1, @library2, @online_library1]
      end
      it "should be able to order by descending type column" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => 'type', :descending_order => true}).should == [@online_library1, @library1, @library2]
      end
      # Order by column when one of it's values is nil?
      it "should be able to order by ascending string column with one column containing a nil value" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => :name}).should == [@online_library1, @library2, @library1]
      end
      it "should be able to order by descending string column with one column containing a nil value" do
        @unassoc_patron1.libraries_unassociated([{}], nil, nil, {:column => :name, :descending_order => true}).should == [@library1, @library2, @online_library1]
      end
    end
  end
end
