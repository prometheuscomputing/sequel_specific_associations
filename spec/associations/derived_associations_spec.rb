require 'spec_helper'

describe "domain object when inspecting derived associations" do
  it "should return correct information about all of the Model's associations and their types" do
    associations = People::Person.associations
    # Test derived association
    associations.keys.should include(:co_owners)
    co_owner_association_info = associations[:co_owners]
    co_owner_association_info[:derived].should == true
    co_owner_association_info[:getter].to_s.should == "co_owners"
    co_owner_association_info[:class].should == "People::Person"
    co_owner_association_info[:type].should == :one_to_many
  end
end
