module Custom_Profile
  class File
    def data=(data_string)
      existing_binary_data = @binary_data
      if existing_binary_data
        return if existing_binary_data.data == data_string
        existing_binary_data.data = data_string
        existing_binary_data.save
      else # No existing binary_data (Custom_Profile::BinaryData)
        new_binary_data = Custom_Profile::BinaryData.create(:data => data_string)
        @binary_data = new_binary_data
      end
    end
    
    def data
      existing_binary_data = @binary_data
      if existing_binary_data
        existing_binary_data.data
      else
        nil
      end
    end
    
    # Can't delete normally since File is not persisted
    def delete
      @binary_data.destroy if @binary_data
      self
    end
    
    # Create a File from a local path
    def self.from_path(path)
      expanded_path    = ::File.expand_path(path)
      filename         = ::File.basename(expanded_path)
      mime_type        = MIME::Types.type_for(expanded_path).first
      mime_type_string = mime_type.content_type if mime_type
      data_string      = ::File.binread(expanded_path)
      file             = Custom_Profile::File.new(:filename => filename, :mime_type => mime_type_string)
      file.data        = data_string
      file
    end
  end
end