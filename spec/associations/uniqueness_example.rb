module Application
  class TestHasSerial < Sequel::Model
    plugin :specific_associations
    interface!
    attribute :serial, SpecificAssociations::INT_TYPE
  end

  class TestSerialed < Sequel::Model
    plugin :specific_associations
    child_of TestHasSerial
    interface!
  end

  class TestPerson < Sequel::Model
    plugin :specific_associations
    attribute :first_name, String, :test => 'wombat', :foo => :bar
    attribute :last_name, String
    attribute :name, String, :custom_getter => proc { |v| first_name + ' ' + last_name} , :custom_setter => proc { |v| self.first_name = v.split(' ').first; self.last_name = v.split(' ').last}, :store_on_get => true
    attribute :username, String
    should_be_unique :first_name, :last_name
  end

  class TestIdentifier < Sequel::Model
    plugin :specific_associations
    attribute :value, String
    attribute :numeric_value, SpecificAssociations::INT_TYPE
    attribute :guid, String
    should_be_unique :value
    is_unique :value, :numeric_value
    is_unique :guid
  end

  class TestEmployee < TestPerson
    implements TestSerialed
    attribute :employee_number, SpecificAssociations::INT_TYPE
    attribute :first_name, String, :test => 'wombat', :foo => :bar # Redefinition of attribute with same options is ignored
    attribute :last_name, String, :foobar => true # Redefining already inherited attribute with additional option
    attribute :nickname, String
    attribute :id_string, String, :custom_getter => proc { |v| employee_number.to_s + '_' + last_name}, :column => :derived_id_string, :store_on_get => true
    attribute :long_id_string, String, :custom_getter => proc {employee_number.to_s + first_name + last_name + nickname}, :store_value => false
    is_unique :employee_number, :identifiers => [:employee_number, :nickname]
    is_unique :nickname, :check_empty => true
    is_unique :serial
    is_unique :username
  end

  class TestEmployer < TestPerson
    implements TestSerialed
  end

  class TestDog < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    attribute :nickname, String
    many_to_one :TestBark, :one_to_one => true, :opp_getter => :dog , :getter => :bark
    should_be_unique :name, :nickname, :label => 'Names'
    should_be_unique :bark_id, :show_value_in_message => false, :label => 'Bark', :identifiers => [:name]
  end

  class TestBark < Sequel::Model
    plugin :specific_associations
    attribute :sound, String
    should_be_unique :sound
    one_to_many :TestDog, :one_to_one => true, :opp_getter => :bark, :getter => :dog
  end

  class TestCoord < Sequel::Model
    plugin :specific_associations
    attribute :x, SpecificAssociations::INT_TYPE
    attribute :y, SpecificAssociations::INT_TYPE
    attribute :z, SpecificAssociations::INT_TYPE
    should_be_unique :x, :y, :z
  end
end
Application::TestPerson.create_schema
Application::TestIdentifier.create_schema
Application::TestEmployee.create_schema
Application::TestEmployer.create_schema
Application::TestDog.create_schema
Application::TestBark.create_schema
Application::TestCoord.create_schema
