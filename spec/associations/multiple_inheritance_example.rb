module Application
  # Purely alternate_inheritance defined inheritance hierarchy
  class MI_Person < Sequel::Model
    plugin :specific_associations
    attribute :name, String
    many_to_one :MI_Identifier, :opp_getter => :person, :getter => :identifier
  end

  class MI_Salesman < Sequel::Model
    plugin :specific_associations
    child_of MI_Person
    attribute :sales_id, SpecificAssociations::INT_TYPE
  end

  class MI_Developer < Sequel::Model
    plugin :specific_associations
    child_of MI_Person
    attribute :dev_id, SpecificAssociations::INT_TYPE
    attribute :private_key, Custom_Profile::File
  end

  class MI_SoftwareSalesman < Sequel::Model
    plugin :specific_associations
    child_of MI_Salesman, MI_Developer
    attribute :software_sold, String
  end

  class MI_Clown < Sequel::Model
    plugin :specific_associations
    attribute :clown_skill, String
    child_of MI_Person
  end


  class MI_Identifier < Sequel::Model
    plugin :specific_associations
    attribute :value, String
    one_to_many MI_Person, :opp_getter => :identifier, :getter => :person
  end


  # Test combination of inheritance hierarchies
  class MI_CodeMonkey < MI_Developer
    plugin :specific_associations
    child_of MI_Clown
    attribute :worst_mistake, String
    attribute :most_chars_on_one_line, SpecificAssociations::INT_TYPE
  end
end

Application::MI_Person.create_schema
Application::MI_Salesman.create_schema
Application::MI_Developer.create_schema
Application::MI_Clown.create_schema
Application::MI_SoftwareSalesman.create_schema
Application::MI_Identifier.create_schema
Application::MI_CodeMonkey.create_schema