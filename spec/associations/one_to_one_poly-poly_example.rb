#Each person may have many throwable items
module Application
  class Throwable < Sequel::Model
    plugin :specific_associations
    many_to_one :Thrower, :one_to_one => true
    # equivalent to
    # one_to_one :Thrower, :holds_key => true
  end

  class Frisbee < Throwable
    set_dataset :frisbees
  end
  class Disc < Throwable
    set_dataset :discs
  end

  class Thrower < Sequel::Model
    plugin :specific_associations
    one_to_many :Throwable, :one_to_one => true
    # equivalent to
    # one_to_one :Throwable # :holds_key is false if not explicitly true
    attribute :name, String
  end
  
  class Person < Thrower
    set_dataset :people_example
  end
  class SmartDog < Thrower
    set_dataset :smartdogs
  end
end

Application::Thrower.create_schema
Application::Throwable.create_schema
Application::Person.create_schema
Application::Disc.create_schema
Application::SmartDog.create_schema
Application::Frisbee.create_schema