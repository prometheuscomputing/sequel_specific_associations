require 'spec_helper'

describe "interface functionality" do
  before(:each) do
    InterfaceTest::Person.delete
    InterfaceTest::Salesman.delete
    InterfaceTest::Developer.delete
    InterfaceTest::SoftwareSalesman.delete
    InterfaceTest::Identifier.delete
    InterfaceTest::Computer.delete
    InterfaceTest::System.delete
  end
  
  it "create_schema should create a schema based off the defined and inherited attributes and associations" do
    private_key_file_cols = [:private_key, :private_key_binary_data_id, :private_key_filename, :private_key_mime_type]
    person_cols = [:id, :name, :identifier_id, :phone]
    InterfaceTest::Person.columns.should =~ person_cols
    InterfaceTest::Developer.columns.should =~ person_cols + [:dev_id] + private_key_file_cols
    InterfaceTest::Salesman.columns.should =~ person_cols + [:sales_id, :product, :sales_identifier_id]
    InterfaceTest::SoftwareSalesman.columns.should =~ person_cols + [:dev_id, :sales_id, :product, :sales_identifier_id, :software_sold] + private_key_file_cols
    InterfaceTest::Identifier.columns.should =~ [:id, :value]
    InterfaceTest::System.columns.should =~ [:id, :name, :operator_id, :operator_class]
  end
  
  it "should allow inheritance of attributes and associations for models" do
    ss = InterfaceTest::SoftwareSalesman.create(:name => 'bob', :phone => '123456', :dev_id => 1, :product => 'experimental prototypes', :software_sold => 'ssa')
    ss.identifier = InterfaceTest::Identifier.create(:value => "AGX11")
    ss.sales_identifier = InterfaceTest::Identifier.create(:value => 'abc123')
    ss.identifier.value.should == 'AGX11'
    ss.sales_identifier.value.should == 'abc123'
    ss.name.should == 'bob'
    ss.phone.should == '123456'
    ss.dev_id.should == 1
    ss.product.should == 'experimental prototypes'
    ss.software_sold.should == 'ssa'
  end
  
  it "should allow local modifications of associations inherited from an interface" do
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should be_nil
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should be_nil
    
    # Add type constraint to SoftwareSalesman
    InterfaceTest::SoftwareSalesman.add_association_options :systems, :types => [:System] # SubSystem is excluded
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should be_nil
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should =~ ['InterfaceTest::System']
    # Clear constraint
    InterfaceTest::SoftwareSalesman.remove_immediate_property(:systems)
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should be_nil
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should be_nil
    
    # Add type constraint to Person
    InterfaceTest::Person.add_association_options :systems, :types => [:System] # SubSystem is excluded
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should =~ ['InterfaceTest::System']
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should =~ ['InterfaceTest::System']
    # Clear constraint
    InterfaceTest::Person.remove_immediate_property(:systems)
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should be_nil
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should be_nil
    
    # Add type constraint to Person and SoftwareSalesman
    InterfaceTest::Person.add_association_options :systems, :types => [:SubSystem] # System is excluded
    InterfaceTest::SoftwareSalesman.add_association_options :systems, :types => [:System] # SubSystem is excluded
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should =~ ['InterfaceTest::SubSystem']
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should =~ ['InterfaceTest::System']
    # Clear constraints
    InterfaceTest::Person.remove_immediate_property(:systems)
    InterfaceTest::SoftwareSalesman.remove_immediate_property(:systems)
    InterfaceTest::Operator.associations[:systems][:types].should be_nil
    InterfaceTest::Person.associations[:systems][:types].should be_nil
    InterfaceTest::SoftwareSalesman.associations[:systems][:types].should be_nil
  end
  
  it "should allow associations to interfaces" do
    sales_ident = InterfaceTest::Identifier.create(:value => 'abc123')
    ss = InterfaceTest::SoftwareSalesman.create(:name => 'bob')
    sales_ident.seller = ss
    sales_ident.seller.should == ss
    ss.sales_identifier.should == sales_ident
  end
  
  it "should allow the use of an interface to choose between multiple types to fulfil a role" do
    person = InterfaceTest::Person.create(:name => 'Bob')
    dev = InterfaceTest::Developer.create(:name => 'Steve')
    comp = InterfaceTest::Computer.create(:name => 'Hal')
    system1 = InterfaceTest::System.create(:name => 'System 1')
    system2 = InterfaceTest::System.create(:name => 'System 2')
    system3 = InterfaceTest::System.create(:name => 'System 3')
    
    # Test person
    person.add_system system1
    person.add_system system2
    person.systems.should =~ [system1, system2]
    
    system1.operator.should == person
    system2.operator.should == person
    
    # Test dev
    dev.add_system system1 # Taken from person
    dev.systems.should =~ [system1]
    person.systems.should =~ [system2]
    
    # Test comp
    comp.add_system system3
    comp.systems.should =~ [system3]
    system3.operator.should == comp
  end
  
  it "should be able to mark a class as an interface" do
    new_klass = Class.new(Sequel::Model)
    new_klass.plugin :specific_associations
    new_klass.interface?.should == false
    new_klass.interface!
    new_klass.interface?.should == true
  end
  
  it "should be able to distinguish interfaces" do
    InterfaceTest::Named.interface?.should == true
    InterfaceTest::Operator.interface?.should == true
    InterfaceTest::Person.interface?.should == false
  end
  
  it "should be able to return a concrete implementor of the interface (if available)" do
    klass = InterfaceTest::Seller.concrete_class
    klass.should == InterfaceTest::Salesman
    
    # Create new_klass that has no concrete implementor
    new_interface = Class.new(Sequel::Model)
    new_interface.plugin :specific_associations
    new_interface.interface!
    lambda {new_interface.concrete_class}.should raise_error(SpecificAssociations::NoConcreteClassFound)
  end
  
  # NOTE: this test fails using model_metadata v0.0.0 and is fixed by v0.0.1
  it "should handle interface inheritance (from another interface)" do
    # Commando implements the SpecialOperator interface which inherits from the Operator interface
    InterfaceTest::Operator.interface?.should be true
    InterfaceTest::SpecialOperator.interface?.should be true
    InterfaceTest::Operator.implementors.should include(InterfaceTest::Commando)
    InterfaceTest::SpecialOperator.implementors.should include(InterfaceTest::Commando)
    # NOTE: not doing exact matching of interfaces since Commando will additionally implement Tyrant::Object and Tyrant::PolicyElement if SPM is applied # FIXME test this now because SPM is now defunct
    InterfaceTest::Commando.interfaces.should include(*[InterfaceTest::Named, InterfaceTest::Operator, InterfaceTest::SpecialOperator])
    InterfaceTest::Commando.immediate_interfaces.should include(*[InterfaceTest::Named, InterfaceTest::SpecialOperator])
    InterfaceTest::Commando.attributes.keys.should =~ [:name, :callsign]
    InterfaceTest::Commando.associations.reject { |g,i| i[:hidden]}.keys.should =~ [:systems]
  end
end