require 'spec_helper'

describe 'enumerations and enumeration attributes' do
  before(:each) do
    reset_db
    ChangeTracker.start
    @car = Automotive::Car.create
  end
  
  context 'listing enumeration literals' do
    it 'should be able to list enumeration literals via the literal_values method' do
      Automotive::VehicleMaker.literal_values.should =~ ['Ford', 'Dodge', 'Honda', 'Volvo', 'Unknown', 'Home Made', 'Other']
      Automotive::DomesticVehicleMaker.literal_values.should =~ ['Ford', 'Dodge', 'Unknown', 'Home Made', 'Other']
      Automotive::OtherVehicleMaker.literal_values.should =~ ['Unknown', 'Home Made', 'Other']
    end
    
    it 'should be able to list enumeration literals for an association via the <attribute>_literals method' do
      @car.make_literals.should =~ ['Ford', 'Dodge', 'Honda', 'Volvo', 'Unknown', 'Home Made', 'Other']
    end
  end
  
  context 'setting and retrieving enumeration values via association methods' do
    it 'should set an enumeration either via an enumeration literal instance or via a string' do
      @car.make = 'Ford'
      @car.make.should be_a(Automotive::DomesticVehicleMaker)
      @car.make.value.should == 'Ford'
      @car.make = Automotive::OtherVehicleMaker.where(:value => 'Other').first
      @car.make.should be_a(Automotive::OtherVehicleMaker)
      @car.make.value.should == 'Other'
    end
    
    it 'should be able to add an enumeration value using the _add method' do
      @car._add(:make, 'Ford')
      @car.make.should be_a(Automotive::DomesticVehicleMaker)
      @car.make.value.should == 'Ford'
    end
  end
end
