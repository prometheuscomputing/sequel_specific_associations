require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker many to many associations" do

  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # TODO: Need to check multiple users interacting with the data simultaneously
  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic many-to-many section -- ChangeTracker
  # ============================================================
  # Example: Person (customers) *----* RepairShop (repair_shops)
  context "many-to-many (polymorphic-to-nonpolymorphic) relationship using ChangeTracker -" do
    before :each do
      @time_before_creation = Time.now
      @repair_shop1 = Automotive::RepairShop.create(:name => 'RepairShop1')
      @repair_shop2 = Automotive::RepairShop.create(:name => 'RepairShop2')
      @person = People::Person.create(:name => 'Stewart')
      @mechanic = Automotive::Mechanic.create(:name => 'Tim')
      @car = Automotive::Car.create(:vehicle_model => 'Thunderbird') # Unrelated class for exclusion testing
      ChangeTracker.commit
      @time_after_creation = Time.now
      ChangeTracker.start
    end
    
    
    context "adding an element" do
      it "should add elements to the association via the first class" do
        # Add to an existing object
        @repair_shop1.add_customer @person
        @repair_shop1.customers.should =~ [@person]
        @repair_shop1.add_customer @mechanic
        @repair_shop1.customers.should =~ [@person, @mechanic]
        ChangeTracker.commit
        @repair_shop1.customers.should =~ [@person, @mechanic]
        
        # Add to a new object
        ChangeTracker.start
        @new_repair_shop = Automotive::RepairShop.create
        @new_repair_shop.add_customer(@person)
        @new_repair_shop.customers.should == [@person]
        ChangeTracker.commit
        @new_repair_shop.customers.should == [@person]
      end
      
      it "should add the element to the association via the second class" do
        # Add to an existing object
        @person.add_repair_shop(@repair_shop1)
        @person.repair_shops.should =~ [@repair_shop1]
        @person.add_repair_shop(@repair_shop2)
        @person.repair_shops.should =~ [@repair_shop1, @repair_shop2]
        ChangeTracker.commit
        @person.repair_shops.should =~ [@repair_shop1, @repair_shop2]
        
        # Add to a new object
        ChangeTracker.start
        @new_person = People::Person.create
        @new_person.add_repair_shop(@repair_shop1)
        @new_person.repair_shops.should == [@repair_shop1]
        ChangeTracker.commit
        @new_person.repair_shops.should == [@repair_shop1]
      end
      
      it "should reject an element that is not of the correct type" do
        lambda {@person.add_repair_shop(@car)}.should raise_error(Sequel::Error, /Invalid type for association/i)
        @person.repair_shops.should == []
        lambda {@repair_shop1.add_customer(@car)}.should raise_error(Sequel::Error, /Invalid type for association/i)
        @repair_shop1.customers.should == []
      end
      
      # it "should only update the version of modified objects" do
      #   person_initial_version = @person.ct_version
      #   repair_shop1_initial_version = @repair_shop1.ct_version
      #   uow = ChangeTracker.unit_of_work
        
      #   # Uncomment sleep for debugging. It is _not_ required, but it will make the time difference 
      #   #   show in timestamps without having to inspect milliseconds
      #   # sleep(1)
        
      #   # This operation should not change the version of @repair_shop1 or @person as only the join table entry is changed
      #   @repair_shop1.add_customer(@person)
      #   ChangeTracker.commit
      #   @repair_shop1.ct_version.should == repair_shop1_initial_version
      #   @person.ct_version.should == person_initial_version
      #   # Look at uow in DB
      #   saved_uow = ChangeTracker::UnitOfWork[uow.id]
      #   saved_uow.objects.length.should == 1
      #   modified_object = saved_uow.objects.first
      #   modified_object.should be_an_instance_of(Automotive::PersonRepairShops_RepairShopCustomers)
      #   modified_object.ct_version.should == saved_uow.completion_time
      # end
      
      it "should ignore attempts to add multiple associations between the same objects" do
        # Set up association
        @repair_shop1.add_customer(@person)
        ChangeTracker.commit
        ChangeTracker.start
        
        puts
        puts "Warnings expected here:"
        # Simple test
        @repair_shop1.add_customer(@person)
        ChangeTracker.commit
        @repair_shop1.customers.length.should == 1
        @person.repair_shops.length.should == 1
        
        # Remove existing association to do more thorough test
        ChangeTracker.start
        @repair_shop1.remove_customer(@person)
        ChangeTracker.commit
        @repair_shop1.customers.length.should == 0
        @person.repair_shops.length.should == 0
        
        # More thorough test
        ChangeTracker.start
        @repair_shop1.add_customer(@person)
        @repair_shop1.add_customer(@person)
        @person.add_repair_shop(@repair_shop1)
        @person.add_repair_shop(@repair_shop1)
        ChangeTracker.commit
        ChangeTracker.start
        @repair_shop1.add_customer(@person)
        @repair_shop1.add_customer(@person)
        @person.add_repair_shop(@repair_shop1)
        @person.add_repair_shop(@repair_shop1)
        ChangeTracker.commit
        puts "End expected warnings"
        @repair_shop1.customers.length.should == 1
        @person.repair_shops.length.should == 1
      end
    end
    
    context "setting an association" do
      it "should allow setting an association" do
        ChangeTracker.start
        @repair_shop1.customers = [@person, @mechanic]
        @repair_shop1.customers.should =~ [@person, @mechanic]
        ChangeTracker.commit
        @repair_shop1.customers.should =~ [@person, @mechanic]
      end
      
      it "should allow resetting within a transaction" do
        ChangeTracker.start
        @repair_shop1.customers = [@person, @mechanic]
        @repair_shop1.customers.should =~ [@person, @mechanic]
        @repair_shop1.customers = [@mechanic]
        @repair_shop1.customers.should =~ [@mechanic]
        ChangeTracker.commit
        @repair_shop1.customers.should =~ [@mechanic]
      end
    end
    
    context "destroying an element" do
      before :each do
        @repair_shop1.add_customer(@person)
        @repair_shop2.add_customer(@person)
        ChangeTracker.commit
        @joins_to_person = @person.repair_shops_associations.collect { |assoc| assoc[:through]}
        @joins_to_person.length.should == 2
        @join_to_repair_shop1 = @repair_shop1.customers_associations.collect { |assoc| assoc[:through]}.first
        @join_to_repair_shop1.should_not == nil
        ChangeTracker.start
      end
      
      it "should correctly destroy the object and any associated objects (association classes) via the first class" do
        @person.destroy
        @person.class.deleted.all.should =~ [@person]
        ChangeTracker.commit
        @person.class.deleted.count.should == 1
        
        # Ensure joins are deleted as well
        @joins_to_person.each do |join|
          join.class[join.id].should == nil
        end
        # Ensure person was deleted
        @person.class[@person.id].should == nil
        Automotive::PersonRepairShops_RepairShopCustomers.count.should == 0
      end
      
      it "should correctly destroy the object and any associated objects (association classes) via the second class" do
        @repair_shop1.destroy
        @repair_shop1.class.deleted.all.should =~ [@repair_shop1]
        ChangeTracker.commit
        @repair_shop1.class.deleted.count.should == 1
        # Ensure join is deleted as well
        @join_to_repair_shop1.class[@join_to_repair_shop1.id].should == nil
        # Ensure repair_shop1 was deleted
        @repair_shop1.class[@repair_shop1.id].should == nil
        Automotive::PersonRepairShops_RepairShopCustomers.count.should == 1
      end
    end
    
    context "removing element(s)" do
      before :each do
        @repair_shop1.add_customer(@person)
        @repair_shop1.add_customer(@mechanic)
        @repair_shop2.add_customer(@person)
        ChangeTracker.commit
        ChangeTracker.start
      end
      
      it "should remove the association via the first class" do
        # Remove @person
        @repair_shop1.remove_customer(@person)
        ChangeTracker.commit
        @repair_shop1.customers.should == [@mechanic]
        # Remove @mechanic
        ChangeTracker.start
        @repair_shop1.remove_customer(@mechanic)
        ChangeTracker.commit
        @repair_shop1.customers.should == []
      end
      
      it "should remove the association via the second class" do
        # Remove repair_shop1
        @person.remove_repair_shop(@repair_shop1)
        @person.repair_shops.should == [@repair_shop2]
        # Remove repair_shop2
        @person.remove_repair_shop(@repair_shop2)
        @person.repair_shops.should == []
        
        # Test removal from @mechanic
        @mechanic.repair_shops.should == [@repair_shop1]
        @mechanic.remove_repair_shop(@repair_shop1)
        @mechanic.repair_shops.should == []
      end
      
      it "should be able to handle multiple deletes on the same association" do
        # Remove first time
        ChangeTracker.commit { @repair_shop2.remove_customer(@person)}
        # @repair_shop2.remove_customer(@person)
        @repair_shop2.customers.should == []
        
        # Re-add and remove second time
        ChangeTracker.commit { @repair_shop2.add_customer(@person)}
        # @repair_shop2.add_customer(@person)
        @repair_shop2.customers.should == [@person]
        ChangeTracker.commit { @repair_shop2.remove_customer(@person)}
        # @repair_shop2.remove_customer(@person)
        @repair_shop2.customers.should == []
        
        # Re-add and remove third time
        ChangeTracker.commit { @repair_shop2.add_customer(@person)}
        # @repair_shop2.add_customer(@person)
        @repair_shop2.customers.should == [@person]
        ChangeTracker.commit { @repair_shop2.remove_customer(@person)}
        # @repair_shop2.remove_customer(@person)
        @repair_shop2.customers.should == []
        
        # Ensure that there are 3 deleted join entries
        Automotive::PersonRepairShops_RepairShopCustomers.deleted.all.length.should == 3
      end
      
      it "should allow reassociation in the same transaction" do
        @repair_shop2.remove_customer(@person)
        @repair_shop2.add_customer(@person)
        @repair_shop2.customers.should == [@person]
      end
      
      it "should allow removal in the same transaction" do
        # Set up for test by removing @person from @repair_shop2
        @repair_shop2.remove_customer(@person)
        @repair_shop2.add_customer(@person)
        @repair_shop2.remove_customer(@person)
        @repair_shop2.customers.should == []
      end
      
      it "should reject an invalid type for the association" do
        lambda { @repair_shop2.remove_customer(@car) }.should raise_error(Sequel::Error, /Invalid type for association/i)
        lambda { @person.remove_repair_shop(@car) }.should raise_error(Sequel::Error, /Invalid type for association/i)
      end
      
      it "should remove all elements via the first class" do
        # Test removing all elements (existing associations)
        @repair_shop1.remove_all_customers
        @repair_shop1.customers.should == []
        
        # Test removing all elements (new associations)
        @repair_shop1.add_customer(@person)
        @repair_shop1.add_customer(@mechanic)
        @repair_shop1.remove_all_customers
        @repair_shop1.customers.should == []
      end
      
      it "should remove all elements via the second class" do
        # Test removing all elements (existing associations)
        @person.remove_all_repair_shops
        @person.repair_shops.should == []
        
        # Test removing all elements (new associations)
        @person.add_repair_shop(@repair_shop1)
        @person.add_repair_shop(@repair_shop2)
        @person.remove_all_repair_shops
        @person.repair_shops.should == []
      end
    end
    
    context "multilevel operations" do
      before :each do
        Codeable.delete; Code.delete; RichText.delete
      end
      
      it "should allow single-level operations" do
        c = Codeable.create
        code_obj = Code.create
        c.code = code_obj
        r = RichText.create(:content => '# DO STUFF', :markup_language => 'Ruby')
        code_obj.code = r
        
        c = Codeable.all.first
        c.should be_an_instance_of(Codeable)
        code_obj = c.code
        code_obj.should be_an_instance_of(Code)
        
        r = RichText.all.first
        r_text = code_obj.code
        r_text.should be_an_instance_of(RichText)
        r_text.markup_language == 'Ruby'
      end
      
      it "should allow multilevel operations" do
        c = Codeable.create
        c.code = Code.create
        c.code.code = RichText.create(:content => '# DO STUFF', :markup_language => 'Ruby')
        c.code.code.should_not be_nil
        c.code.code.should be_an_instance_of(RichText)
        c.code.code.content.should == '# DO STUFF'
        c.code.code.markup_language.should == 'Ruby'
      end
    end
    
    context "using the change browser" do
      before :each do
        @person.name = 'Stewie' # Changed from 'Stewart' to 'Stewie'
        @repair_shop1.name = 'newRepairShop' # Changed from 'RepairShop1' to 'newRepairShop'
        @person.save; @repair_shop1.save
        @time_after_update = Time.now
      end
      
      after(:each) do
        # ChangeTracker.browse_time = nil
      end
      
    # -BD I don't think browse time should work if ChangeTracker is disabled
      # it "should only return objects as they existed at the browse time" do
      #   People::Person[@person.id].ct_history.changes_since(@time_before_creation).length.should == 0
      #   ChangeTracker.browse_time = @time_after_creation
      #   a = People::Person[@person.id]
      #   a.name.should == 'Stewart'
      #   s = Automotive::RepairShop[@repair_shop1.id]
      #   s.name.should == 'RepairShop1'
      #   ChangeTracker.browse_time = @time_after_update
      #   a = People::Person[@person.id]
      #   a.name.should == 'Stewie'
      #   s = Automotive::RepairShop[@repair_shop1.id]
      #   s.name.should == 'newRepairShop'
      #   ChangeTracker.browse_time = @time_before_creation
      #   a = People::Person[@person.id]
      #   a.should be_nil
      #   s = Automotive::RepairShop[@repair_shop1.id]
      #   s.should be_nil
      #   # Removing browse time should return to most recent time
      #   ChangeTracker.browse_time = nil
      #   People::Person[@person.id].name.should == 'Stewie'
      #   Automotive::RepairShop[@repair_shop1.id].name.should == 'newRepairShop'
      # end
      
      # it "should return associations as they existed at the browse time" do
      #   @time_before_association = Time.now
      #   ChangeTracker.start
        
      #   @person.add_repair_shop(@repair_shop1)
      #   ChangeTracker.commit
      #   @time_after_association = Time.now

      #   # Make sure the association is working
      #   People::Person[@person.id].repair_shops.should include(@repair_shop1)
      #   Automotive::RepairShop[@repair_shop1.id].customers.should include(@person)
        
      #   # Set the browse time to before the association
      #   ChangeTracker.browse_time = @time_before_association
      #   People::Person[@person.id].repair_shops.should be_empty
      #   Automotive::RepairShop[@repair_shop1.id].customers.should be_empty
        
      #   # Set the browse time after the association
      #   ChangeTracker.browse_time = @time_after_association
      #   People::Person[@person.id].repair_shops.should include(Automotive::RepairShop[@repair_shop1.id])
      #   Automotive::RepairShop[@repair_shop1.id].customers.should include(People::Person[@person.id])
        
      #   # Reset browse_time, and ensure things are restored
      #   ChangeTracker.browse_time = nil
      #   @person.refresh; @repair_shop1.refresh
        
      #   People::Person[@person.id].repair_shops.should include(@repair_shop1)
      #   Automotive::RepairShop[@repair_shop1.id].customers.should include(@person)
        
      #   @time_before_changes = Time.now
        
      #   # Make changes
      #   ChangeTracker.start
      #   person = People::Person[@person.id]
      #   person.name = 'Stew'
      #   person.save
      #   repair_shop1 = Automotive::RepairShop[@repair_shop1.id]
      #   repair_shop1.name = 'Shop1'
      #   repair_shop1.save
      #   # ChangeTracker.commit.should == true
        
      #   @time_before_dissociation = Time.now
      #   # Dissociate & Reassociate
      #   ChangeTracker.start
      #   repair_shop1 = Automotive::RepairShop[@repair_shop1.id]
      #   repair_shop1.name = 'Shoppe1'
      #   repair_shop1.save
      #   # ChangeTracker.commit.should == true
      #   ChangeTracker.start
      #   person = People::Person[@person.id]
      #   repair_shop1 = Automotive::RepairShop[@repair_shop1.id]
      #   person.remove_repair_shop(repair_shop1)
      #   # ChangeTracker.commit.should == true

      #   @time_after_dissociation = Time.now
        
      #   People::Person[@person.id].repair_shops.should be_empty
      #   Automotive::RepairShop[@repair_shop1.id].customers.should be_empty
      #   ChangeTracker.start
      #   person = People::Person[@person.id]
      #   repair_shop1 = Automotive::RepairShop[@repair_shop1.id]
      #   person.add_repair_shop(repair_shop1)
      #   # ChangeTracker.commit.should == true
      #   People::Person[@person.id].repair_shops.should include(Automotive::RepairShop[@repair_shop1.id])
      #   Automotive::RepairShop[@repair_shop1.id].customers.should include(People::Person[@person.id])
        
      #   # Test association objects are from the correct time when browsing
      #   ChangeTracker.browse_time = @time_before_dissociation
      #   repair_shop1 = People::Person[@person.id].repair_shops.first
      #   repair_shop1.should be_an_instance_of(Automotive::RepairShop)

      #   repair_shop1.name.should == 'Shop1'
        
      #   ChangeTracker.browse_time = @time_before_changes
      #   repair_shop1 = People::Person[@person.id].repair_shops.first
      #   repair_shop1.should be_an_instance_of(Automotive::RepairShop)
      #   repair_shop1.name.should == 'newRepairShop'
        
      #   ChangeTracker.browse_time = @time_after_dissociation
      #   People::Person[@person.id].repair_shops.should be_empty
      #   Automotive::RepairShop[@repair_shop1.id].customers.should be_empty
      #   Automotive::RepairShop[@repair_shop1.id].name.should == 'Shoppe1'
      # end
        
      # it "should successfully retrieve items that have been deleted" do
      #   @time_before_delete = Time.now
      #   ChangeTracker.start
      #   @person.destroy
      #   @repair_shop1.destroy
      #   ChangeTracker.commit
      #   @time_after_delete = Time.now
      #   People::Person[@person.id].should be_nil
      #   Automotive::RepairShop[@repair_shop1.id].should be_nil
      #   # Set the browse time
      #   ChangeTracker.browse_time = @time_before_delete
      #   People::Person[@person.id].should_not be_nil
      #   Automotive::RepairShop[@repair_shop1.id].should_not be_nil
      #   People::Person[@person.id].name.should == 'Stewie'
      #   Automotive::RepairShop[@repair_shop1.id].name.should == 'newRepairShop'
      #   # Remove the browse time and test that the objects are still deleted
      #   ChangeTracker.browse_time = nil
      #   People::Person[@person.id].should be_nil
      #   Automotive::RepairShop[@repair_shop1.id].should be_nil
      #   # Set the browse time to after the deletion and test that the objects are deleted
      #   ChangeTracker.browse_time = @time_after_delete
      #   People::Person[@person.id].should be_nil
      #   Automotive::RepairShop[@repair_shop1.id].should be_nil
      #   ChangeTracker.browse_time = nil
      # end

      # it "should return a correct listing of all items of a model for the given time" do
      #   @time_before_delete = Time.now
      #   ChangeTracker.start
      #   @person.destroy
      #   ChangeTracker.commit
      #   @time_after_delete = Time.now
      #   People::Person.all.length.should == 1
      #   Automotive::Mechanic.all.length.should == 1
      #   ChangeTracker.browse_time = @time_before_delete
      #   People::Person.all.length.should == 2
      #   Automotive::Mechanic.all.length.should == 1
      #   ChangeTracker.browse_time = @time_after_delete
      #   People::Person.all.length.should == 1
      #   Automotive::Mechanic.all.length.should == 1
      #   ChangeTracker.browse_time = nil
      #   People::Person.all.length.should == 1
      #   Automotive::Mechanic.all.length.should == 1
      # end
    end
  end
  
  # ============================================================
  # END polymorphic-to-nonpolymorphic many-to-many section -- ChangeTracker
  # ============================================================
  
  # ============================================================
  # BEGIN polymorphic-to-polymorphic many-to-many association class section -- ChangeTracker
  # ============================================================
  # Example: Person (owners) *----* Vehicle (vehicles)
  context "many-to-many (polymorphic-to-polymorphic) relationship using ChangeTracker -" do
    before :each do
      @person = People::Person.create(:name => 'Stewart')
      @mechanic = Automotive::Mechanic.create(:name => 'Tim')
      @car = Automotive::Car.create(:vehicle_model => 'Thunderbird')
      @hybrid = Automotive::HybridVehicle.create(:vehicle_model => 'Avalon')
      ChangeTracker.commit
      ChangeTracker.start
    end
    
    context "adding an element" do
      before :each do
        @new_person = People::Person.create
        @new_car = Automotive::Car.create
        @new_hybrid = Automotive::HybridVehicle.create
        @new_ownership = People::Ownership.create
      end
      
      it "should create associations on objects that have not been saved yet (via the association class)" do
        # Set up new association via the association class
        @new_ownership.owner = @new_person
        @new_ownership.owner.should == @new_person
        @new_ownership.vehicle = @new_car
        @new_ownership.vehicle.should == @new_car
        
        # Check that relationship is saved
        @new_ownership.owner.should == @new_person
        @new_ownership.vehicle.should == @new_car
        @new_person.vehicles.should == [@new_car]
        @new_car.owners.should == [@new_person]
      end
      
      it "should be able to return an association hash including an association that has not been saved yet" do
        @new_person.add_vehicle(@new_car)
        @new_person.add_vehicle(@new_hybrid)
        
        assocs = @new_person.vehicles_associations
        car_assoc = assocs.select { |a| a[:to].class == Automotive::Car }.first
        hybrid_assoc = assocs.select { |a| a[:to].class == Automotive::HybridVehicle }.first
        
        car_assoc.should_not == nil
        car_assoc[:to].should == @new_car
        car_assoc[:through].should be_a(People::Ownership)
        
        hybrid_assoc.should_not be_nil
        hybrid_assoc[:to].should == @new_hybrid
        hybrid_assoc[:through].should be_a(People::Ownership)
        
        # Try combination of saved/unsaved associations
        @new_mechanic = Automotive::Mechanic.create
        @new_car.add_owner(@new_mechanic)
        assocs = @new_car.owners_associations
        mechanic_assoc = assocs.select { |a| a[:to].class == Automotive::Mechanic }.first
        
        mechanic_assoc.should_not == nil
        mechanic_assoc[:to].should == @new_mechanic
        mechanic_assoc[:through].should be_a(People::Ownership)
        
        @new_car.owners.should =~ [@new_person, @new_mechanic]
      end
    end
    
    context "manipulating a join table entry" do
      before :each do
        @person.add_vehicle(@car)
        @person.add_vehicle(@hybrid)
        
        assocs = @person.vehicles_associations
        @person_car_assoc = assocs.select { |a| a[:to].class == Automotive::Car }.first
        @person_hybrid_assoc = assocs.select { |a| a[:to].class == Automotive::HybridVehicle }.first
      end
      
      it "should create the join table entries properly" do 
        @person_car_assoc.should_not == nil
        @person_car_assoc[:to].should == @car
        @person_car_assoc[:through].should be_a(People::Ownership)
        People::Ownership[:vehicle_id => @car.id, :vehicle_class => @car.class.to_s,
                          :owner_id => @person.id, :owner_class => @person.class.to_s
                          ].should == @person_car_assoc[:through]
        
        @person_hybrid_assoc.should_not == nil
        @person_hybrid_assoc[:to].should == @hybrid
        @person_hybrid_assoc[:through].should be_a(People::Ownership)
        People::Ownership[:vehicle_id => @hybrid.id, :vehicle_class => @hybrid.class.to_s,
                          :owner_id => @person.id, :owner_class => @person.class.to_s
                          ].should == @person_hybrid_assoc[:through]
      end
      
      # it "should create a pending association creation/deletion(s) when setting its association(s)" do
      #   @person_car_assoc[:through].owner = @mechanic
      #   changes = ChangeTracker.unit_of_work.changes
      #   # Should be an association removal change and an association addition change
      #   changes.length.should == 2
      #   changes[0].should be_an_instance_of(ChangeTracker::ChangeAssociation)
      #   changes[1].should be_an_instance_of(ChangeTracker::ChangeAssociation)
      #   changes[0].association_operation.should be_an_instance_of(SpecificAssociations::PendingAssociationRemoval)
      #   changes[1].association_operation.should be_an_instance_of(SpecificAssociations::PendingAssociationAddition)
      # end
      
      it "should be able to replace the associated objects" do
        @new_person = People::Person.create
        @new_car = Automotive::Car.create
        person_car_ownership = @person_car_assoc[:through]
        person_car_ownership.owner = @new_person

        person_car_ownership.owner.should == @new_person
        person_car_ownership.vehicle = @new_car
        ChangeTracker.commit
        person_car_ownership.vehicle.should == @new_car
      end
    end # End manipulating join table entries
    
    context "listing all elements (associated or unassociated)" do
      before :each do
        # Add unassociated objects
        @unassoc_person = People::Person.create
        @unassoc_mechanic = Automotive::Mechanic.create
        @unassoc_car = Automotive::Car.create
        
        # Create associations
        @person.add_vehicle(@car)
        @person.add_vehicle(@hybrid)
      end
      
      it "should return an array of all elements associated via the first class" do
        @person.vehicles.should =~ [@car, @hybrid]
      end

      it "should return an array of all elements associated via the second class" do
        @car.owners.should == [@person]
        @hybrid.owners.should == [@person]
      end
      
      it "should return an array of all unassociated elements via the first class" do
        @person.vehicles_unassociated_count.should == 1
        @person.vehicles_unassociated.should =~ [@unassoc_car]
      end
      
      it "should return an array of all unassociated elements via the second class" do
        @car.owners_unassociated.should =~ [@mechanic, @unassoc_person, @unassoc_mechanic]
        @hybrid.owners_unassociated.should =~ [@mechanic, @unassoc_person, @unassoc_mechanic]
      end
    end
    
    context "destroying an element" do
      before :each do
        # Add unassociated objects
        @unassoc_person = People::Person.create
        @unassoc_mechanic = Automotive::Mechanic.create
        @unassoc_car = Automotive::Car.create
        
        # Create associations
        @person.add_vehicle(@car)
        @person.add_vehicle(@hybrid)
        
        
        assocs = @person.vehicles_associations
        @person_car_assoc = assocs.select { |a| a[:to].class == Automotive::Car }.first
        @person_hybrid_assoc = assocs.select { |a| a[:to].class == Automotive::HybridVehicle }.first
        @person_car_ownership = @person_car_assoc[:through]
        @person_hybrid_ownership = @person_hybrid_assoc[:through]
      end
      
      it "should correctly destroy the object and any associated objects (association classes) via the first class" do
        @person.destroy
        @person.class.deleted.all.should =~ [@person]
        ChangeTracker.commit
        @person.class.deleted.count.should == 1
        
        # Ensure person and joins are deleted
        @person.class[@person.id].should == nil
        @person_car_ownership.class[@person_car_ownership.id].should == nil
        @person_hybrid_ownership.class[@person_hybrid_ownership.id].should == nil
        People::Ownership.count.should == 0
      end
      
      it "should correctly destroy the object and any associated objects (association classes) via the second class" do
        @car.destroy
        @car.class.deleted.all.should =~ [@car]
        ChangeTracker.commit
        @car.class.deleted.count.should == 1
        
        # Ensure car and join are deleted
        @car.class[@car.id].should == nil
        @person_car_ownership.class[@person_car_ownership.id].should == nil
        People::Ownership.count.should == 1
      end
    end
  end
  # ============================================================
  # END polymorphic-to-polymorphic many-to-many association class section -- ChangeTracker
  # ============================================================
end