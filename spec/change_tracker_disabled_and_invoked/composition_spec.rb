require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker composition functionality" do
  
  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  # ============================================================
  # BEGIN ChangeTracked Composition section
  # ============================================================

  # Example: Vehicle class
  context "composition functionality in conjunction with SCT" do
    before(:each) do
      # Setup instances and associations
      @car = Automotive::Car.create(:vehicle_model => 'Thunderbird', :cost => 50000)
      @car.make = 'Ford'
      
      # Set up composition associations
      # many-to-one
      @component1 = Automotive::Component.create(:name => 'engine')
      @component2 = Automotive::Component.create(:name => 'wheel')
      @car.components = [@component1, @component2]
      @part1 = Automotive::Part.create(:name => 'spark plug')
      @part2 = Automotive::Part.create(:name => 'air filter')
      @component1.parts = [@part1, @part2]
      # one-to-one
      @vin = Automotive::VIN.create(:vin => 101010)
      @car.vin = @vin
      
      # Set up normal associations
      # many-to-one
      @repair_shop = Automotive::RepairShop.create(:name => 'RepairShop')
      @car.being_repaired_by = @repair_shop
      @warranty = Automotive::Warranty.create(:coverage => 'Full')
      @car.warranty = @warranty
      # one-to-many
      @person = People::Person.create(:name => 'Stewart')
      @mechanic = Automotive::Mechanic.create(:name => 'Tim')
      @car.occupants = [@person, @mechanic]
      # many-to-many
      @car.maintained_by = [@mechanic]
      ChangeTracker.commit
      ChangeTracker.start
    end
    
    it "should be able to return the composing objects for an object instance" do
      @car.compositions.should =~ [@component1, @component2, @vin]
      # Passing false recurses through children
      @car.compositions(false).should =~ [@component1, @component2, @vin, @part1, @part2]
      
      @component1.compositions.should =~ [@part1, @part2]
      @component1.compositions(false).should =~ [@part1, @part2]
      
      @part1.compositions.should == []
      @part1.compositions(false).should == []
      
      # Also test empty composition associations
      ChangeTracker.start
      new_car = Automotive::Car.create
      ChangeTracker.commit
      new_car.compositions.should == []
      new_car.compositions(false).should == []
      
      # Test class without composition associations
      @repair_shop.compositions.should == []
      @repair_shop.compositions(false).should == []
    end
    
    it "should be able to return the composer object for an object instance" do
      @car.composer.should == nil
      @component1.composer.should == @car
      @component2.composer.should == @car
      @vin.composer.should == @car
      
      @part1.composer.should == @component1
      @part2.composer.should == @component1
      
      # Test class without composer associations
      @person.composer.should == nil
    end
    
    it "should delete all composing objects when the composer is destroyed" do
      # get many-to-many joins before destruction
      maintained_by_join = @car.maintained_by_associations.first[:through]
      
      ChangeTracker.commit { @car.destroy}
      # @car.destroy
      
      # Ensure that @car, its compositions, and its associations were deleted
      should_be_deleted = [@car, @component1, @component2, @vin, @part1, @part2, maintained_by_join]
      should_be_deleted.each { |c| c.class[c.id].should == nil }
      
      # Ensure that no other associated classes were deleted
      should_not_be_deleted = [@repair_shop, @warranty, @person, @mechanic]
      should_not_be_deleted.each { |c| c.class[c.id].should_not == nil }
    end
    
    it "should be able to clone an object" do

      clone_car = @car.deep_clone
      ChangeTracker.commit
      # mechanic, warranty, and repair_shop must be refreshed because deep_clone method on car created new associations for them which updated their updated_at timestamp
      @mechanic.refresh
      @repair_shop.refresh
      @warranty.refresh
      (@car.occupants.last).should == @mechanic
      # expect(@car.occupants.last).to eq(@mechanic)
      
      clone_car.should_not == @car
      
      # Check attributes
      clone_car.vehicle_model.should == nil # Can't clone should-be-unique attribute
      clone_car.cost.should == 50000

      # Check enumeration value
      clone_car.make.value.should == 'Ford'
      
      # Check composition associations
      # Component - cloned
      clone_car.components.count.should == 2
      clone_engine = clone_car.components.select { |c| c.name == 'engine'}.first
      clone_engine.should_not == nil
      clone_engine.should_not == @component1
      # Part - cloned
      clone_engine.parts.count.should == 2
      clone_spark_plug = clone_engine.parts.select { |p| p.name == 'spark plug'}.first
      clone_spark_plug.should_not == nil
      clone_spark_plug.should_not == @part1
      # VIN - cloned
      clone_vin = clone_car.vin
      clone_vin.should_not == nil
      clone_vin.should_not == @vin
      clone_vin.vin.should == nil # Can't clone is-unique attribute
      # RepairShop - associated
      @car.being_repaired_by.should == @repair_shop
      clone_car.being_repaired_by.should == @repair_shop # no change to @car
      # Warranty - associated
      @car.warranty.should == @warranty # no change to @car
      clone_car.warranty.should == @warranty
      # Occupants - ignored since it is from-one and not a composition
      # @car.occupants.last.should equal(@mechanic)
      @car.occupants.should =~ [@person, @mechanic] # no change to @car
      clone_car.occupants.should == []
      # Maintained By - associated
      @car.maintained_by.should =~ [@mechanic] # no change to @car
      clone_car.maintained_by.should =~ [@mechanic]
    end
    
    it "should be able to create a deep clone without a composer" do
      clone_engine = @component1.deep_clone(:ignore_composer => true)
      ChangeTracker.commit
      clone_engine.vehicle.should be_nil
      clone_engine.parts.length.should == 2
    end
  end

  context "self composition functionality with a one to many relation" do
    before :each do
      @component = Automotive::Component.create
      @alternator_part = Automotive::Part.create(:name => 'Alternator')
      @component.add_part(@alternator_part)
      @engine_part = Automotive::Part.create(:name => 'Engine')
      @component.add_part(@engine_part)
      ChangeTracker.commit
      ChangeTracker.start
    end

    it "should have no parent part for top level composition" do
      @alternator_part.part_of.should == nil
    end

    it "should be able to add and traverse composition associations" do
      @brush_head = Automotive::Part.create(:name => 'Brush Head')
      @alternator_part.add_sub_part(@brush_head)

      # Check states BEFORE CT commit
      # Check associations of top level part
      @alternator_part.sub_parts.should =~ [@brush_head]
      @alternator_part.part_of.should == nil
      # @alternator_part.send(:pending_association_operations).collect { |h| h.model }.should =~ [@brush_head] # Returns array of SpecificAssociations

      # Check associations of child part
      @brush_head.part_of.should == @alternator_part
      @brush_head.sub_parts.should == []

      # @brush_head.send(:pending_association_operations).collect { |h| h.model }.should =~ [@brush_head] # Returns an array of SpecificAssociations
      # @brush_head.send(:pending_association_operations).collect { |h| h.second_model }.should =~ [@alternator_part] # Returns an array of SpecificAssociations
      
      # Had an issue previously where it wasn't differentiating between getters, part_of and sub_parts
      # @brush_head.send(:pending_association_additions, :part_of).collect { |h| h[:object] }.should =~ [@alternator_part] # Returns an array of hashes
      # @brush_head.send(:pending_association_additions, :sub_parts).should == []


      ChangeTracker.commit
      ChangeTracker.start

      # Check states AFTER CT commit
      # Check associations of top level part
      @alternator_part.sub_parts.should =~ [@brush_head]
      @alternator_part.part_of.should == nil
      @alternator_part.send(:pending_association_operations).should == []

      # Check associations of child part
      @brush_head.part_of.should == @alternator_part
      @brush_head.sub_parts.should == []
      @brush_head.send(:pending_association_operations).should == []

      # Create grandchild part
      @brush_bristles = Automotive::Part.create(:name => 'Brush Bristles')
      @brush_head.add_sub_part(@brush_bristles)

      # Check states BEFORE CT commit
      # Check associations of top level part
      @alternator_part.sub_parts.should =~ [@brush_head]
      @alternator_part.part_of.should == nil
      # @alternator_part.send(:pending_association_operations).collect { |h| h.model }.should =~ [@brush_head] # Returns array of SpecificAssociations

      # Check associations of child part
      @brush_head.part_of.should == @alternator_part
      @brush_head.sub_parts.should == [@brush_bristles]
      # @brush_head.send(:pending_association_operations).collect { |h| h.model }.should =~ [@brush_bristles] # Returns array of SpecificAssociations
      @brush_head.send(:pending_association_operations).should == []
      @brush_bristles.send(:pending_association_operations).should == []

      # Check associations of grandchild part
      @brush_bristles.part_of.should == @brush_head
      @brush_bristles.sub_parts.should == []
      # @brush_bristles.send(:pending_association_operations).collect { |h| h.model }.should =~ [@brush_head] # Returns array of SpecificAssociations
      # Had an issue where it wasn't differentiating between parent parts and sub parts
      # @brush_bristles.send(:pending_association_additions, :part_of).collect { |h| h[:object] }.should =~ [@brush_head]
      # @brush_bristles.send(:pending_association_additions, :sub_parts).should == []
      @brush_head.send(:pending_association_operations).should == []
      @brush_bristles.send(:pending_association_operations).should == []

      ChangeTracker.commit
      ChangeTracker.start

      # Check states AFTER CT commit
      # Check associations of top level part
      @alternator_part.sub_parts.should =~ [@brush_head]
      @alternator_part.part_of.should == nil
      # @alternator_part.send(:pending_association_operations).should == []

      # Check associations of child part
      @brush_head.part_of.should == @alternator_part
      @brush_head.sub_parts.should == [@brush_bristles]
      @brush_head.send(:pending_association_operations).should == []

      # Check associations of grandchild part
      @brush_bristles.part_of.should == @brush_head
      @brush_bristles.sub_parts.should == []
      @brush_bristles.send(:pending_association_operations).should == []

      # Traverse from bottom part to top part
      @brush_bristles.part_of.part_of.should == @alternator_part
    end
  end

  context "self composition functionality with a one to one relation" do
    before :each do
      # @state = Geography::State.create
      # @large_doll = Geography::City.create(:name => 'Cullowhee')
      # @state.add_city(@large_doll)
      @person = People::Clown::Clown.create
      @large_doll = People::Clown::NestingDoll.create(:color => 'Red')
      @person.add_doll(@large_doll)
      ChangeTracker.commit
      ChangeTracker.start
    end

    it "should have no parent city for top level composition" do
      @large_doll.outer_doll.should == nil
    end

    it "should have no default sub cities for top level composition" do
      @large_doll.inner_doll.should == nil
    end

    # large_doll = alternator_part
    # medium_doll = brush_head
    # small_doll = brush_bristles

    it "should be able to add and traverse composition associations" do
      @medium_doll = People::Clown::NestingDoll.create(:color => 'Blue')
      @large_doll.inner_doll = @medium_doll

      # Check states BEFORE CT commit
      # Check associations of top level part
      @large_doll.inner_doll.should == @medium_doll
      @large_doll.outer_doll.should == nil
      # ERROR, getting sub_cullowhee city for parent of large_doll
      # puts "INSPECTING HEREL #{@large_doll.send(:pending_association_additions).inspect}"
      # @large_doll.send(:pending_association_additions, :outer_doll).should == [] # Returns an array of hashes
      # @large_doll.send(:pending_association_additions, :inner_doll).collect { |paa| paa[:object]}.should == [@medium_doll]
      @large_doll.send(:pending_association_operations).should == []



      # STILL WORKING ON THE REST
      puts "EXPERIMENTAL TESTS BELOW"



      # Check associations of child part
      @medium_doll.outer_doll.should == @large_doll
      @medium_doll.inner_doll.should == nil

      # These tests' results were backwards? Should we be testing a function this lowly? We're already testing the pending_associations_additions function that uses this helper function. - BD
      # @medium_doll.send(:pending_association_operations).collect { |h| h.model }.should =~ [@medium_doll] # Returns an array of SpecificAssociations
      # @medium_doll.send(:pending_association_operations).collect { |h| h.second_model }.should =~ [@large_doll] # Returns an array of SpecificAssociations
      
      # Had an issue previously where it wasn't differentiating between getters, in_city and sub_city
      # @medium_doll.send(:pending_association_additions, :outer_doll).collect { |h| h[:object] }.should =~ [@large_doll] # Returns an array of hashes
      # @medium_doll.send(:pending_association_additions, :inner_doll).should == []
      @medium_doll.send(:pending_association_additions).should == []


      ChangeTracker.commit
      ChangeTracker.start

      # Check states AFTER CT commit
      # Check associations of top level part
      @large_doll.inner_doll.should == @medium_doll
      @large_doll.outer_doll.should == nil
      @large_doll.send(:pending_association_operations).should == []

      # Check associations of child part
      @medium_doll.outer_doll.should == @large_doll
      @medium_doll.inner_doll.should == nil
      @medium_doll.send(:pending_association_operations).should == []

      # Create grandchild part
      @small_doll = People::Clown::NestingDoll.create(:color => 'Green')
      @medium_doll.inner_doll = @small_doll

      # Check states BEFORE CT commit
      # Check associations of top level part
      @large_doll.inner_doll.should == @medium_doll
      @large_doll.outer_doll.should == nil
      # @large_doll.send(:pending_association_operations).collect { |h| h.model }.should =~ [@medium_doll] # Returns array of SpecificAssociations

      # Check associations of child part
      @medium_doll.outer_doll.should == @large_doll
      @medium_doll.inner_doll.should == @small_doll
      # @medium_doll.send(:pending_association_operations).collect { |h| h.model }.should =~ [@small_doll] # Returns array of SpecificAssociations

      # Check associations of grandchild part
      @small_doll.outer_doll.should == @medium_doll
      @small_doll.inner_doll.should == nil
      # @small_doll.send(:pending_association_operations).collect { |h| h.model }.should =~ [@medium_doll] # Returns array of SpecificAssociations
      # Had an issue where it wasn't differentiating between parent parts and sub parts
      # @small_doll.send(:pending_association_additions, :outer_doll).collect { |h| h[:object] }.should =~ [@medium_doll]
      # @small_doll.send(:pending_association_additions, :inner_doll).should == []
      @small_doll.send(:pending_association_additions).should == []

      ChangeTracker.commit
      ChangeTracker.start

      # Check states AFTER CT commit
      # Check associations of top level part
      @large_doll.inner_doll.should == @medium_doll
      @large_doll.outer_doll.should == nil
      @large_doll.send(:pending_association_operations).should == []

      # Check associations of child part
      @medium_doll.outer_doll.should == @large_doll
      @medium_doll.inner_doll.should == @small_doll
      @medium_doll.send(:pending_association_operations).should == []

      # Check associations of grandchild part
      @small_doll.outer_doll.should == @medium_doll
      @small_doll.inner_doll.should == nil
      @small_doll.send(:pending_association_operations).should == []

      # Traverse from bottom part to top part
      @small_doll.outer_doll.outer_doll.should == @large_doll
    end
  end
end
