require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker pending associations meta information" do
  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # Example: Person *----* Vehicles (repairshops)
  context "confirm pending association listings polymorphic many-to-many polymorphic for a committed item" do
    before :each do
      @person1 = Automotive::Mechanic.create
      @person2 = People::Person.create
      @person3 = People::Person.create
      @car1 = Automotive::Car.create
      @car2 = Automotive::Car.create
      @car3 = Automotive::Car.create
      @moto1 = Automotive::Motorcycle.create
      ChangeTracker.commit
      ChangeTracker.start
    end

    it "should be able to list the pending association additions" do
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3

      @car1.add_owner(@person1)
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1]
      @car1.owners_unassociated.should =~ [@person2, @person3]
      @car1.owners_count.should == 1
      @car1.owners_unassociated_count.should == 2

      @car1.add_owner(@person2)
      # Try collecting?
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1, @person2]
      @car1.owners_unassociated.should =~ [@person3]
      @car1.owners_count.should == 2
      @car1.owners_unassociated_count.should == 1

      @car1.add_owner(@person3)
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0
    end

    it "should be able to list the pending association removals" do
      @car1.add_owner(@person1)
      @car1.add_owner(@person2)
      @car1.add_owner(@person3)

      ChangeTracker.commit
      ChangeTracker.start

      @car1.send(:pending_association_removals).should =~ []
      @car1.owners.should =~ [@person1, @person2,  @person3]
      @car1.owners_unassociated.should =~ []
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0

      @car1.remove_owner(@person1)
      @car1.send(:pending_association_removals).should =~ []
      @car1.owners.should =~ [@person2, @person3]
      @car1.owners_unassociated.should =~ [@person1]
      @car1.owners_count.should == 2
      @car1.owners_unassociated_count.should == 1

      @car1.remove_owner(@person2)
      @car1.send(:pending_association_removals).should =~ []
      @car1.owners.should =~ [@person3]
      @car1.owners_unassociated.should =~ [@person1, @person2]
      @car1.owners_count.should == 1
      @car1.owners_unassociated_count.should == 2


      @car1.remove_owner(@person3)
      @car1.send(:pending_association_removals).should =~ []
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3
    end
  end

  # Example: Person *----* Vehicles (repairshops)
  context "confirm pending association listings polymorphic many-to-many polymorphic for an uncommitted item" do
    before :each do
      @person1 = Automotive::Mechanic.create
      @person2 = People::Person.create
      @person3 = People::Person.create
      @car2 = Automotive::Car.create
      @car3 = Automotive::Car.create
      ChangeTracker.commit
      ChangeTracker.start
      @car1 = Automotive::Car.create
      @moto1 = Automotive::Motorcycle.create
    end

    it "should be able to list the pending association additions" do
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3

      @car1.add_owner(@person1)
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1]
      @car1.owners_unassociated.should =~ [@person2, @person3]
      @car1.owners_count.should == 1
      @car1.owners_unassociated_count.should == 2

      @car1.add_owner(@person2)
      # Try collecting?
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1, @person2]
      @car1.owners_unassociated.should =~ [@person3]
      @car1.owners_count.should == 2
      @car1.owners_unassociated_count.should == 1

      @car1.add_owner(@person3)
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0
    end

    it "should be able to list the pending association removals" do
      @moto1.add_owner(@person1)
      @moto1.add_owner(@person2)
      @moto1.add_owner(@person3)

      ChangeTracker.commit
      ChangeTracker.start

      @moto1.send(:pending_association_removals).should =~ []
      @moto1.owners.should =~ [@person1, @person2,  @person3]
      @moto1.owners_unassociated.should =~ []
      @moto1.owners_count.should == 3
      @moto1.owners_unassociated_count.should == 0

      @moto1.remove_owner(@person1)
      @moto1.send(:pending_association_removals).should =~ []
      @moto1.owners.should =~ [@person2, @person3]
      @moto1.owners_unassociated.should =~ [@person1]
      @moto1.owners_count.should == 2
      @moto1.owners_unassociated_count.should == 1

      @moto1.remove_owner(@person2)
      @moto1.send(:pending_association_removals).should =~ []
      @moto1.owners.should =~ [@person3]
      @moto1.owners_unassociated.should =~ [@person1, @person2]
      @moto1.owners_count.should == 1
      @moto1.owners_unassociated_count.should == 2


      @moto1.remove_owner(@person3)
      @moto1.send(:pending_association_removals).should =~ []
      @moto1.owners.should =~ []
      @moto1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @moto1.owners_count.should == 0
      @moto1.owners_unassociated_count.should == 3
    end
  end

  # Example: Person (customers) *----* RepairShop (repairshops)
  context "confirm pending association listings polymorphic many-to-many" do
    before :each do
      @customer1 = Automotive::Mechanic.create
      @customer2 = People::Person.create
      @customer3 = People::Person.create
      @repair_shop = Automotive::RepairShop.create
    end
    
    it "should be able to list the pending association additions" do
      @repair_shop.customers_associations.count.should == 0
      @repair_shop.send(:pending_association_additions).should =~ []
      @repair_shop.add_customer(@customer1)
      @repair_shop.send(:pending_association_additions).should =~ []
      @repair_shop.customers_associations.count.should == 1
      @repair_shop.add_customer(@customer2)
      @repair_shop.send(:pending_association_additions).should =~ []
      @repair_shop.customers_associations.count.should == 2
      @repair_shop.add_customer(@customer3)
      @repair_shop.send(:pending_association_additions).should =~ []
      @repair_shop.customers_associations.count.should == 3
    end
    
    it "should be able to list the pending association removals" do
      @repair_shop.add_customer(@customer1)
      @repair_shop.add_customer(@customer2)
      @repair_shop.add_customer(@customer3)
      ChangeTracker.commit
      ChangeTracker.start
      @repair_shop.customers_associations.count.should == 3
      @associations = @repair_shop.customers_associations.collect { |assoc| assoc }
      association1 = @associations.first
      association2 = @associations.last
      association2[:through].customer = nil
      
      @repair_shop.customers_associations.count.should == 3
      
      @repair_shop.remove_customer(@customer2)
      @repair_shop.send(:pending_association_removals).should =~ []
      @repair_shop.customers_associations.count.should == 2
      
      @repair_shop.remove_customer(@customer1)
      @repair_shop.send(:pending_association_removals).should =~ []
      @repair_shop.customers_associations.count.should == 1
      
      @repair_shop.add_customer(@customer3)
      @repair_shop.remove_customer(@customer3)
      @repair_shop.send(:pending_association_removals).should =~ []
      @repair_shop.customers_associations.count.should == 1
    end
  end
end
