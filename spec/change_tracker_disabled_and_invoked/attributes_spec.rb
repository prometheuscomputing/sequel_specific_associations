require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker attributes" do
  
  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # TODO: Need to check multiple users interacting with the data simultaneously
  
  
  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic one-to-one section -- ChangeTracker
  # ============================================================
  # Test that re-adding an existing association does not destroy existing association
  # ============================================================
  # END polymorphic-to-nonpolymorphic one-to-one section -- ChangeTracker
  # ============================================================
  
  # Complex attribute testing with ChangeTracker
  # Example: Person attributes: photo & description
  context "domain object with complex attributes while using ChangeTracker" do
    before :each do
      Application::Document.delete
      ChangeTracker.start
      @person = People::Person.create(:name => 'Stewart')
      ChangeTracker.commit
    end
    
    # Application::Document.plugin :change_tracker has been applied from #setup_change_tracker
    it "should be able to set a complex attribute" do
      # Set up richtext
      r = Gui_Builder_Profile::RichText.new
      r.markup_language = "LaTeX"
      r.content = "Hello World! \\alpha"
      # Check that the object does not have an id
      r.id.should be_nil
      
      # Set up code
      c = Gui_Builder_Profile::Code.new
      c.language = "Ruby"
      c.content = 'puts "Hello World!"'
      c.id.should be_nil
      
      # Set up file
      f = Gui_Builder_Profile::File.new
      ChangeTracker.start
      fd = Gui_Builder_Profile::BinaryData.create(:data => File.binread(relative('../associations/example_file.txt')))
      fd.should be_a(Gui_Builder_Profile::BinaryData)
      ChangeTracker.commit
      ChangeTracker.start # See note below about this
      f.binary_data = fd
      f.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      f.filename = 'example_file.txt'
      f.mime_type = 'Text' # Not sure on this
      f.id.should be_nil
      f.save
      
      # Set & check file info
      # Can't start SCT here without breaking things since the changes to the complex attribute GBP::File would be lost
      # Will be fixed when (if?) complex attributes stop being Sequel::Models
      #ChangeTracker.start
      @person.photo = f
      f.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      ChangeTracker.commit
      # Must start changeTracker again in order to query pdf at the moment.
      # This is because pdf is a File that is 'built' by assigning constituent attributes
      # This causes SCT to choke when it hits the assignment pdf.binary_data = binary_data_from_db
      # The workaround for this is to have ChangeTracker.started
      ChangeTracker.start
      f.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo.filename.should == f.filename
      @person.photo.mime_type.should == f.mime_type
      @person.photo.binary_data.should == fd
      @person[:photo_binary_data_id].should == f.binary_data.id
      @person[:photo_filename].should == f.filename
      @person[:photo_mime_type].should == f.mime_type
      
      # Set & check richtext info
      ChangeTracker.start
      @person.description = r
      ChangeTracker.commit
      @person.description.should == r
      @person[:description_content].should == r.content
      # @person[:description_markup_language].should == r.markup_language
      @person[:description_markup_language_id].should == Gui_Builder_Profile::MarkupType.where(:value => 'LaTeX').first.id
      
      # Set & check code info -- PENDING ADDITION OF CODE ATTRIBUTE FOR PERSON
      ChangeTracker.start
      # @person.script = c
      ChangeTracker.commit
      # @person.script.should == c
      # @person[:script_content].should == c.content
      # @person[:script_language].should == c.language
    end
    
    it "should not create id's on the complex attributes if they are saved" do
      r = Gui_Builder_Profile::RichText.new(:markup_language => "LaTeX", :content => "Hello World! \\alpha")
      
      c = Gui_Builder_Profile::Code.new(:language => "Ruby", :content => 'puts "Hello World!"')
      
      f = Gui_Builder_Profile::File.new(:filename => 'example_file.txt', 
                                        :mime_type => 'Text')
      ChangeTracker.start
      f.binary_data = Gui_Builder_Profile::BinaryData.create(:data => File.binread(relative('../associations/example_file.txt')))
      ChangeTracker.commit
      
      r.id.should be_nil
      c.id.should be_nil
      f.id.should be_nil
      
      ChangeTracker.start
      # Set complex attributes
      @person.description = r
      # @person.script = c
      @person.photo = f
      ChangeTracker.commit
      
      # Check that id's are nil
      r.id.should be_nil
      c.id.should be_nil
      f.id.should be_nil
      
      # Check that id's are not set if the objects are accidentally saved
      ChangeTracker.start
      r.save
      c.save
      f.save
      ChangeTracker.commit
      ChangeTracker.start
      r.id.should be_nil
      c.id.should be_nil
      f.id.should be_nil
      new_r = @person.description
      # new_c = @person.script
      new_f = @person.photo
      ChangeTracker.start
      new_r.save
      # new_c.save
      new_f.save
      ChangeTracker.commit
      new_r.id.should be_nil
      # new_c.id.should be_nil
      new_f.id.should be_nil
    end
    
    it "should allow setting file data within a single commit" do
      ChangeTracker.start
      f = Gui_Builder_Profile::File.new(:filename => 'example_file.txt', :mime_type => 'Text')
      f.binary_data = Gui_Builder_Profile::BinaryData.create(:data => File.binread(relative('../associations/example_file.txt')))
      #f.send(:pending_association_operations).should_not be_empty
      f.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo = f
      ChangeTracker.commit
      ChangeTracker.start
      @person.photo.should be_a(Gui_Builder_Profile::File)
      @person.photo.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo.binary_data.data.should be_a(String)
    end
    
    it "should allow the use of the data shortcut accessor on File's within a single commit" do
      ChangeTracker.start
      f = Gui_Builder_Profile::File.new(:filename => 'example_file.txt', :mime_type => 'Text')
      f.data = File.binread(relative('../associations/example_file.txt'))
      #f.send(:pending_association_operations).should_not be_empty
      f.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo = f
      ChangeTracker.commit
      ChangeTracker.start
      @person.photo.should be_a(Gui_Builder_Profile::File)
      @person.photo.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo.binary_data.data.should be_a(String)
      @person.photo.data.should be_a(String)
    end
    
    it "should allow the use of instantiate along with the data accessor to create a File in a single command" do
      ChangeTracker.start
      data_string = File.binread(relative('../associations/example_file.txt'))
      f = Gui_Builder_Profile::File.instantiate(:filename => 'example_file.txt', :mime_type => 'Text', :data => data_string)
      #f.send(:pending_association_operations).should_not be_empty
      f.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo = f
      ChangeTracker.commit
      ChangeTracker.start
      @person.photo.should be_a(Gui_Builder_Profile::File)
      @person.photo.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo.binary_data.data.should be_a(String)
      @person.photo.data.should be_a(String)
    end
    
    it "should allow the complex attribute to be queried without change tracker started" do
      ChangeTracker.start
      data_string = File.binread(relative('../associations/example_file.txt'))
      f = Gui_Builder_Profile::File.instantiate(:filename => 'example_file.txt', :mime_type => 'Text', :data => data_string)
      #f.send(:pending_association_operations).should_not be_empty
      @person.photo = f
      ChangeTracker.commit
      @person.photo.should be_a(Gui_Builder_Profile::File)
      @person.photo.binary_data.should be_a(Gui_Builder_Profile::BinaryData)
      @person.photo.binary_data.data.should be_a(String)
      @person.photo.data.should be_a(String)
    end
    
    context "Manipulating complex attributes' associations from the complex attribute owner" do
      it "should remove all associations" do
        # Set attribute
        ChangeTracker.start
        r1 = Gui_Builder_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
        image_file = Gui_Builder_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
        image_file.binary_data = bd1 = Gui_Builder_Profile::BinaryData.create(:data => File.binread(relative('../associations/example_file.txt')))
        i1 = Gui_Builder_Profile::RichTextImage.create
        i1.image = image_file
        r1.add_image i1
        @person.description = r1
        ChangeTracker.commit
        ChangeTracker.start
        
        @person.description_images.should =~ [i1]
        @person.remove_all_description_images
        @person.description_images.should =~ []
        ChangeTracker.commit
        @person.description_images.should =~ []
      end
      
      it "should set the association" do
        ChangeTracker.start
        r1 = Gui_Builder_Profile::RichText.new(:content => 'Rich Text!', :markup_language => 'Markdown')
        image_file = Gui_Builder_Profile::File.new(:filename => 'photo.jpeg', :mime_type => 'image/jpeg')
        image_file.binary_data = bd1 = Gui_Builder_Profile::BinaryData.create(:data => File.binread(relative('../associations/example_file.txt')))
        i1 = Gui_Builder_Profile::RichTextImage.create
        i1.image = image_file
        r1.add_image i1
        @person.description = r1
        
        i2 = Gui_Builder_Profile::RichTextImage.create
        i3 = Gui_Builder_Profile::RichTextImage.create
        ChangeTracker.commit
        ChangeTracker.start
        
        @person.description_images.should =~ [i1]
        @person.description_images = [i1, i2, i3]
        @person.description_images.should =~ [i1, i2, i3]
        ChangeTracker.commit
        @person.description_images.should =~ [i1, i2, i3]
      end
    end
  end
end
