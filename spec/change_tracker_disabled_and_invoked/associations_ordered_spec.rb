require 'spec_helper'

describe "Disabled and Invoked ChangeTracker ordered associations" do
  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end

  # Example: Person (occupants) {ordered} *---- Vehicle (occupying)
  context "ChangeTracked one-to-many ordered associations" do
    before :each do
      @car = Automotive::Car.create(:vehicle_model => 'Thunderbird')
      @occupant1 = People::Person.create(:name => 'Alice')
      @occupant2 = People::Person.create(:name => 'Bob')
      @occupant3 = Automotive::Mechanic.create(:name => 'Carol')
      @occupant4 = People::Person.create(:name => 'Dave')
      @occupants = [@occupant1, @occupant2, @occupant3, @occupant4]
      @car.occupants = @occupants
      ChangeTracker.commit
      @occupant1.occupying_position.should == 0
      @occupant2.occupying_position.should == 1
      @occupant3.occupying_position.should == 2
      @occupant4.occupying_position.should == 3
      @car.occupants.collect { |o| o.occupying_position }.should == [0, 1, 2, 3]
    end
    
    it "should return objects in the correct order when a limit and/or offset is set" do
      @car.occupants({}, 4, 0).should == [@occupant1, @occupant2, @occupant3, @occupant4]
      @car.occupants({}, 3, 1).should ==             [@occupant2, @occupant3, @occupant4]
      @car.occupants({}, 2, 2).should ==                         [@occupant3, @occupant4]
      @car.occupants({}, 1, 2).should ==                         [@occupant3]
      # Test where limit + offset is out of bounds
      @car.occupants({}, 10, 2).should ==                        [@occupant3, @occupant4]
      
      # Remove @occupant2 and retest
      ChangeTracker.commit { @car.remove_occupant(@occupant2)}
      # @car.remove_occupant(@occupant2)
      
      @occupants.each { |o| o.refresh } # Refresh model instances to retrieve newly assigned positions
      
      @car.occupants({}, 3, 0).should == [@occupant1, @occupant3, @occupant4]
      @car.occupants({}, 2, 1).should ==             [@occupant3, @occupant4]
      @car.occupants({}, 1, 1).should ==             [@occupant3]
      # Test where limit + offset is out of bounds
      @car.occupants({}, 10, 1).should ==            [@occupant3, @occupant4]
    end
    
    it "should correctly reorder associations if multiple items are removed in the same commit (example 1)" do
      ChangeTracker.start
      @car.remove_occupant(@occupant2)
      @car.remove_occupant(@occupant3)
    ChangeTracker.commit
      @occupants.each { |o| o.refresh } # Refresh model instances to retrieve newly assigned positions
      
      @car.occupants.should == [@occupant1, @occupant4]
      @car.occupants[0].occupying_position.should == 0
      @car.occupants[1].occupying_position.should == 1
    end
    
    it "should correctly reorder associations if multiple items are removed in the same commit (example 2)" do
    ChangeTracker.start
      @car.remove_occupant(@occupant4)
      @car.remove_occupant(@occupant2)
    ChangeTracker.commit
      @occupants.each { |o| o.refresh } # Refresh model instances to retrieve newly assigned positions
      
      @car.occupants.should == [@occupant1, @occupant3]
      @car.occupants[0].occupying_position.should == 0
      @car.occupants[1].occupying_position.should == 1
    end
  end
  
  # Example: Person (drivers) {ordered} *---(Driving)---* {ordered} Vehicle (drives)
  context "ChangeTracked many-to-many ordered associations where both directions are ordered" do
    before :each do
      # Create objects --------------
      @v1 = Automotive::Car.create(:vehicle_model => 'One')
      @v2 = Automotive::Motorcycle.create(:vehicle_model => 'Two')
      @v3 = Automotive::Car.create(:vehicle_model => 'Three')
      @v4 = Automotive::HybridVehicle.create(:vehicle_model => 'Four')
      @vehicles = [@v1, @v2, @v3, @v4]
      
      @driver1 = People::Person.create(:name => 'Alice')
      @driver2 = People::Person.create(:name => 'Bob')
      @driver3 = Automotive::Mechanic.create(:name => 'Carol')
      @driver4 = People::Person.create(:name => 'Dave')
      @drivers = [@driver1, @driver2, @driver3, @driver4]
      
      # Setup associations --------------
      @v1.drivers = @drivers
      @v2.drivers = @drivers.reverse
      # These commands would remove/add the drivers from @v1 and @v2, and ordering would be modified
      # TODO: fix this issue. Setter should do nothing if existing association is passed.
      #@driver1.drives = @vehicles
      #@driver2.drives = @vehicles.reverse
      # Currently @driver1 should have drives => [@v1, @v2]
      # Desired sorting is => [@v1, @v2, @v3, @v4]
      @driver1.add_drife(@v3)
      @driver1.add_drife(@v4)
      # Currently @driver1 should have drives => [@v1, @v2]
      # Desired sorting is => [@v4, @v3, @v2, @v1]
      @driver2.add_drife(@v3)
      @driver2.add_drife(@v4)
    ChangeTracker.commit
      # Move @driver2 drives into correct positions
      # TODO: fix move command so that it doesn't need its own commit
    ChangeTracker.commit { @driver2.move_drife(@v4, 0)}
      # @driver2.move_drife(@v4, 0)
    ChangeTracker.commit { @driver2.move_drife(@v3, 1)}
      # @driver2.move_drife(@v3, 1)
    ChangeTracker.commit { @driver2.move_drife(@v2, 2)}
      # @driver2.move_drife(@v2, 2)
    ChangeTracker.commit { @driver2.move_drife(@v1, 3)}
      # @driver2.move_drife(@v1, 3)
      
      # Check association ordering --------------
      @v1.drivers.should == @drivers
      @v2.drivers.should == @drivers.reverse
      @driver1.drives.should == @vehicles
      @driver2.drives.should == @vehicles.reverse
      
      # Check position numbering --------------
      @v1.drivers_records.collect { |o| o.drife_position }.should == [0, 1, 2, 3]
      @v2.drivers_records.collect { |o| o.drife_position }.should == [0, 1, 2, 3]
      @driver1.drives_records.collect { |o| o.driver_position }.should == [0, 1, 2, 3]
      @driver2.drives_records.collect { |o| o.driver_position }.should == [0, 1, 2, 3]
    end
    
    it "should return objects in the correct order when a limit and/or offset is set" do
      @v1.drivers({}, 4, 0).should == [@driver1, @driver2, @driver3, @driver4]
      @v1.drivers({}, 3, 1).should ==             [@driver2, @driver3, @driver4]
      @v1.drivers({}, 2, 2).should ==                         [@driver3, @driver4]
      @v1.drivers({}, 1, 2).should ==                         [@driver3]
      # Test where limit + offset is out of bounds
      @v1.drivers({}, 10, 2).should ==                        [@driver3, @driver4]
    
      # Remove @driver2 and retest
    ChangeTracker.commit { @v1.remove_driver(@driver2)}
      # @v1.remove_driver(@driver2)
      @drivers.each { |o| o.refresh } # Refresh model instances to retrieve newly assigned positions
    
      @v1.drivers({}, 3, 0).should == [@driver1, @driver3, @driver4]
      @v1.drivers({}, 2, 1).should ==             [@driver3, @driver4]
      @v1.drivers({}, 1, 1).should ==             [@driver3]
      # Test where limit + offset is out of bounds
      @v1.drivers({}, 10, 1).should ==            [@driver3, @driver4]
    end
    
    it "should correctly reorder associations if multiple items are removed in the same commit (example 1)" do
    ChangeTracker.start
      @v1.remove_driver(@driver2)
      @v1.remove_driver(@driver3)
    ChangeTracker.commit
      @drivers.each { |o| o.refresh } # Refresh model instances to retrieve newly assigned positions
    
      @v1.drivers.should == [@driver1, @driver4]
      @v1.drivers_records[0].drife_position.should == 0
      @v1.drivers_records[1].drife_position.should == 1
      
      # Test other associations
      @v2.drivers.should == @drivers.reverse
      @driver1.drives.should == @vehicles
      @driver2.drives.should == [@v4, @v3, @v2]
    end
    
    it "should correctly reorder associations if multiple items are removed in the same commit (example 2)" do
    ChangeTracker.start
      @v2.remove_driver(@driver4)
      @v2.remove_driver(@driver2)
    ChangeTracker.commit
      @drivers.each { |o| o.refresh } # Refresh model instances to retrieve newly assigned positions
    
      @v2.drivers.should == [@driver3, @driver1]
      @v2.drivers_records[0].drife_position.should == 0
      @v2.drivers_records[1].drife_position.should == 1
      
      # Test other associations
      @v1.drivers.should == @drivers
      @driver1.drives.should == @vehicles
      @driver2.drives.should == [@v4, @v3, @v1]
    end
    
    it "should be able to set the association from an association class" do
    ChangeTracker.start
      @v5 = Automotive::Minivan.create(:vehicle_model => 'Five')
      @driver5 = People::Clown::Clown.create(:name => 'Eli')
      
      # Get association hash for v2 -> driver2
      driver2_assoc = @v2.drivers({}, 1, 2, true).first
      driver2_assoc[:to].should == @driver2
      driving2 = driver2_assoc[:through]
      driving2.driver.should == @driver2
      driving2.drife.should == @v2
      
      # Set driver to driver5
      driving2.driver = @driver5
    ChangeTracker.commit
      
      # Check ordering
      @v2.drivers.should == [@driver4, @driver3, @driver5, @driver1]
      @v2.drivers_records.collect { |o| o.drife_position }.should == [0, 1, 2, 3]
      @driver2.drives.should == [@v4, @v3, @v1]
      @driver2.drives_records.collect { |o| o.driver_position }.should == [0, 1, 2]
      @driver5.drives.should == [@v2]
      @driver5.drives_records.collect { |o| o.driver_position }.should == [0]
      
      # Set drife to v5
    ChangeTracker.start
      driving2.drife = @v5
    ChangeTracker.commit
      
      # Check ordering
      @v2.drivers.should == [@driver4, @driver3, @driver1]
      @v2.drivers_records.collect { |o| o.drife_position }.should == [0, 1, 2]
      @v5.drivers.should == [@driver5]
      @v5.drivers_records.collect { |o| o.drife_position }.should == [0]
      @driver2.drives.should == [@v4, @v3, @v1]
      @driver2.drives_records.collect { |o| o.driver_position }.should == [0, 1, 2]
      @driver5.drives.should == [@v5]
      @driver5.drives_records.collect { |o| o.driver_position }.should == [0]
    end
  end
end