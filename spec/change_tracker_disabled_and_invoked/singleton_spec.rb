require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker singleton feature" do
  
  before :each do
    ChangeTracker.cancel
    ChangeTracker.disabled = true 
    reset_db
    ChangeTracker.start
  end
  
  # Example: ProjectOptions singleton
  context "domain object when using singletons in conjunction with SCT" do
    # 
    it "should allow the creation of a single row in the singleton table" do
      opts = Gui_Builder_Profile::ProjectOptions.instance
      opts.user_registration = 'Invitation'
      opts.save
      Gui_Builder_Profile::ProjectOptions.instance.should == opts
      Gui_Builder_Profile::ProjectOptions.count.should == 1
    end
    
    it "should have private create/new/instantiate methods" do
      lambda { Gui_Builder_Profile::ProjectOptions.new }.should raise_error(NoMethodError, /private method/)
      lambda { Gui_Builder_Profile::ProjectOptions.create }.should raise_error(NoMethodError, /private method/)
      lambda { Gui_Builder_Profile::ProjectOptions.instantiate }.should raise_error(NoMethodError, /private method/)
    end
    
    it "should recover to a new instance if the database row is deleted" do
      opts = Gui_Builder_Profile::ProjectOptions.instance
      opts.user_registration = 'Email'
      opts.save
      reset_db
      new_opts = Gui_Builder_Profile::ProjectOptions.instance
      new_opts.should be_a(Gui_Builder_Profile::ProjectOptions)
      new_opts.user_registration = 'Open'
      new_opts.save
      Gui_Builder_Profile::ProjectOptions.instance.should == new_opts
    end
  end
end