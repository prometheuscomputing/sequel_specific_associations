require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker many to one or one to many associations" do
  
  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # TODO: Need to check multiple users interacting with the data simultaneously
  
  # ============================================================
  # BEGIN ChangeTracked many-to-one section
  # ============================================================
  # Example: Vehicle (occupying) -----* Person (occupants)
  context "many-to-one/one-to-many relationship" do
    context "adding an element" do
      before(:each) do
        @person = People::Person.create(:name => 'Stewart')
        @mechanic = Automotive::Mechanic.create(:name => 'Tim')
        @car = Automotive::Car.create(:vehicle_model => 'Thunderbird')
        @hybrid = Automotive::HybridVehicle.create(:vehicle_model => 'Avalon')
      end
      
      it "should set the element on the to_one side" do
        @person.occupying = @car
        @person.occupying.should == @car
      end
      
      it "should add elements on the to_many side" do
        @car.add_occupant(@person)
        @car.occupants.should =~ [@person]
        @car.add_occupant(@mechanic)
        @car.occupants.should =~ [@person, @mechanic]
      end
      
      # it "should not make any changes when adding the currently assigned element" do
      #   @person.occupying = @car
      #   ChangeTracker.commit
      #   ChangeTracker.start
      #   @person.occupying = @car
      #   ChangeTracker.unit_of_work.changes.should =~ []
      #   @car.add_occupant(@person)
      #   ChangeTracker.unit_of_work.changes.should =~ []
      # end
    end
  end
  # ============================================================
  # END ChangeTracked many-to-one section
  # ============================================================
end
  
