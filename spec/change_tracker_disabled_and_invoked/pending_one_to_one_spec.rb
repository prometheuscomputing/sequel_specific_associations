require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled and Invoked ChangeTracker pending associations meta information" do
  before :each do
    ChangeTracker.disabled = true 
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # Example: Mechanic (chief_mechanic) ----- RepairShop (chief_for)
  context "confirm pending association listings one-to-one" do
    before :each do
      @mechanic = Automotive::Mechanic.create
      @repair_shop = Automotive::RepairShop.create
      ChangeTracker.commit
      ChangeTracker.start
    end
    
    it "should be able to list the pending association additions" do
      @mechanic.chief_for = @repair_shop
      paas = @mechanic.send(:pending_association_additions)
      paas.length.should == 0
      paas.first.should be(nil)
      @mechanic.send(:pending_association_addition_objects).should =~ []
      @mechanic.send(:pending_association_addition_objects, :chief_for).should =~ []
      ChangeTracker.commit
      @mechanic.send(:pending_association_additions).should =~ []
      @mechanic.send(:pending_association_addition_objects).should =~ []
      @mechanic.send(:pending_association_addition_objects, :chief_for).should =~ []
    end
    
    it "should be able to list the pending association removals" do
      # Create association so removal can be tested
      @mechanic.chief_for = @repair_shop
      ChangeTracker.commit
      ChangeTracker.start
      
      # Test removal of chief_for
      @mechanic.chief_for = nil
      pars = @mechanic.send(:pending_association_removals)
      pars.length.should == 0
      pars.first.should be(nil)
      @mechanic.send(:pending_association_removals).should =~ []
      @mechanic.send(:pending_association_removal_objects).should =~ []
      @mechanic.send(:pending_association_removal_objects, :chief_for).should =~ []
    end
  end
end
