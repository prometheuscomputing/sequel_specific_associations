require 'spec_helper'

describe "Manipulating primitive attributes while ChangeTracking" do
  before(:each) do
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
    @student = ClassRoom::Student.create
    ChangeTracker.commit
    ChangeTracker.start
  end
  context "domain object with primitive attributes" do
    it "should be able to initialize attributes that are base primitives" do
      @student.first_name = 'Hugo'
      @student.favorite_number = '5'
      ChangeTracker.commit
    end
  
    it "should be able to initialize attributes that are subtypes of primitives" do
      @student.last_name = 'Faughn'
      @student.student_number = '1369'
      ChangeTracker.commit
    end
  end
  
  context "domain object with primitive attributes/associations with multiplicty greater than one" do
    it "should be able to add and remove attributes that are base primitives" do
      @student.add_stage_name "Bozozo"
      @student.add_stage_name "Mustard Butt"
      ChangeTracker.commit; ChangeTracker.start
      @student.stage_names.should == ["Bozozo", "Mustard Butt"]
      @student.remove_stage_name "Bozozo"
      ChangeTracker.commit
      @student.stage_names.should == ["Mustard Butt"]
    end

    it "should be able to add and remove attributes that are subtypes of primitives" do
      @student.add_nickname "Punk"
      @student.add_nickname "Crash"
      ChangeTracker.commit; ChangeTracker.start
      @student.nicknames.should == ["Punk", "Crash"]
      @student.remove_nickname "Punk"
      ChangeTracker.commit; ChangeTracker.start
      @student.nicknames.should == ["Crash"]
      @student.add_test_grade 100
      @student.add_test_grade 98
      @student.add_test_grade 44
      ChangeTracker.commit; ChangeTracker.start
      @student.test_grades.should == [100, 98, 44]
      @student.remove_test_grade 98
      ChangeTracker.commit
      @student.test_grades.should == [100, 44]
    end
  
    it "should be able to add and remove associations that are subtypes of primitives" do
      @student.add_middle_name "Samuel"
      @student.add_middle_name "Dingaling"
      @student.add_middle_name "Ulysses"
      ChangeTracker.commit; ChangeTracker.start
      @student.middle_names.should == ["Samuel", "Dingaling", "Ulysses"]
      @student.remove_middle_name "Dingaling"
      ChangeTracker.commit
      @student.middle_names.should == ["Samuel", "Ulysses"]
    end
  
    it "should not raise an error when attempting to remove a non-existant primitive" do
      @student.add_stage_name "Bozozo"
      ChangeTracker.commit; ChangeTracker.start
      @student.remove_stage_name "Mustard Butt"
      ChangeTracker.commit; ChangeTracker.start
      @student.stage_names.should == ["Bozozo"]
      @student.add_middle_name "Samuel"
      ChangeTracker.commit; ChangeTracker.start
      @student.remove_middle_name "Dingaling"
      ChangeTracker.commit; ChangeTracker.start
      @student.middle_names.should == ["Samuel"]
      @student.add_nickname "Crash"
      ChangeTracker.commit; ChangeTracker.start
      @student.remove_nickname "Punk"
      ChangeTracker.commit; ChangeTracker.start
      @student.nicknames.should == ["Crash"]
      @student.add_test_grade 100
      @student.add_test_grade 44
      ChangeTracker.commit; ChangeTracker.start
      @student.remove_test_grade 98
      ChangeTracker.commit
      @student.test_grades.should == [100, 44]
    end
  
    it "should completely remove the primitive from the database when dissociated" do
      @student.add_stage_name "Bozozo"
      ChangeTracker.commit; ChangeTracker.start
      @student.remove_stage_name "Bozozo"
      ChangeTracker.commit
      @student.stage_names.should == []
      primitive_type = @student.stage_names_type.first
      primitive_type[:stage_name => "Bozozo"].should be_nil
      primitive_type.count.should == 0
    end
  end
end