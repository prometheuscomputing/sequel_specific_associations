require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "ChangeTracker pending associations meta information" do
  before :each do
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # Example: Application::Person *----* Vehicles (repairshops)
  context "confirm pending association listings polymorphic many-to-many polymorphic for a committed item" do
    before :each do
      @person1 = Automotive::Mechanic.create
      @person2 = People::Person.create
      @person3 = People::Person.create
      @car1 = Automotive::Car.create
      @car2 = Automotive::Car.create
      @car3 = Automotive::Car.create
      @moto1 = Automotive::Motorcycle.create
      ChangeTracker.commit
      ChangeTracker.start
    end

    it "should be able to list the pending association additions" do
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3

      @car1.add_owner(@person1)
      @car1.send(:pending_association_additions).collect { |h| h[:object] }.should =~ [@person1]
      @car1.owners.should =~ [@person1]
      @car1.owners_unassociated.should =~ [@person2, @person3]
      @car1.owners_count.should == 1
      @car1.owners_unassociated_count.should == 2

      @car1.add_owner(@person2)
      # Try collecting?
      @car1.send(:pending_association_additions).collect { |h| h[:object] }.should =~ [@person1, @person2]
      @car1.owners.should =~ [@person1, @person2]
      @car1.owners_unassociated.should =~ [@person3]
      @car1.owners_count.should == 2
      @car1.owners_unassociated_count.should == 1

      @car1.add_owner(@person3)
      @car1.send(:pending_association_additions).collect { |h| h[:object] }.should =~ [@person1, @person2,  @person3]
      @car1.owners.should =~ [@person1, @person2,  @person3]
      @car1.owners_unassociated.should =~ []
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0

      ChangeTracker.commit
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0
    end
    
    it "should be able to list the pending association additions after save is called" do
      @car1.add_owner(@person1)
      @car1.owners.should =~ [@person1]
      @car1.owners_unassociated.should =~ [@person2, @person3]
      @car1.save
      @car1.owners.should =~ [@person1]
      @car1.owners_unassociated.should =~ [@person2, @person3]
    end
    
    it "should be able to list the pending association additions for new objects (before and after saving)" do
      uni = People::Clown::Unicycle.new(:vehicle_model => 'Un1')
      p = People::Person.new(:name => 'P')
      m = Automotive::Mechanic.new(:name => 'M')
      uni.add_occupant(p)
      uni.occupants.should =~ [p]
      uni.save
      uni.occupants.should =~ [p]
      uni.add_occupant(m)
      uni.occupants.should =~ [p, m]
      uni.save
      uni.occupants.should =~ [p, m]
      ChangeTracker.commit
      uni.occupants.should =~ [p.refresh, m.refresh]
    end
    
    it "should be able to list the pending association removals" do
      @car1.add_owner(@person1)
      @car1.add_owner(@person2)
      @car1.add_owner(@person3)

      ChangeTracker.commit
      ChangeTracker.start

      @car1.send(:pending_association_removals).should =~ []
      @car1.owners.should =~ [@person1, @person2,  @person3]
      @car1.owners_unassociated.should =~ []
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0

      @car1.remove_owner(@person1)
      @car1.send(:pending_association_removals).collect { |h| h[:object] }.should =~ [@person1]
      @car1.owners.should =~ [@person2, @person3]
      @car1.owners_unassociated.should =~ [@person1]
      @car1.owners_count.should == 2
      @car1.owners_unassociated_count.should == 1

      @car1.remove_owner(@person2)
      @car1.send(:pending_association_removals).collect { |h| h[:object] }.should =~ [@person1, @person2]
      @car1.owners.should =~ [@person3]
      @car1.owners_unassociated.should =~ [@person1, @person2]
      @car1.owners_count.should == 1
      @car1.owners_unassociated_count.should == 2


      @car1.remove_owner(@person3)
      @car1.send(:pending_association_removals).collect { |h| h[:object] }.should =~ [@person1, @person2, @person3]
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3

      ChangeTracker.commit
      ChangeTracker.start

      @car1.send(:pending_association_removals).should =~ []
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3
    end
    
    it "should be able to list the pending association removals after save is called" do
      @car1.add_owner(@person1)
      @car1.add_owner(@person2)
      @car1.add_owner(@person3)
      
      @car1.remove_owner(@person3)
      @car1.owners.should =~ [@person1, @person2]
      @car1.owners_unassociated.should =~ [@person3]
      @car1.save
      @car1.owners.should =~ [@person1, @person2]
      @car1.owners_unassociated.should =~ [@person3]
    end
  end

  # Example: Application::Person *----* Vehicles (repairshops)
  context "confirm pending association listings polymorphic many-to-many polymorphic for an uncommitted item" do
    before :each do
      @person1 = Automotive::Mechanic.create
      @person2 = People::Person.create
      @person3 = People::Person.create
      @car2 = Automotive::Car.create
      @car3 = Automotive::Car.create
      ChangeTracker.commit
      ChangeTracker.start
      @car1 = Automotive::Car.create
      @moto1 = Automotive::Motorcycle.create
    end

    it "should be able to list the pending association additions" do
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ []
      @car1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 0
      @car1.owners_unassociated_count.should == 3

      @car1.add_owner(@person1)
      @car1.send(:pending_association_additions).collect { |h| h[:object] }.should =~ [@person1]
      @car1.owners.should =~ [@person1]
      @car1.owners_unassociated.should =~ [@person2, @person3]
      @car1.owners_count.should == 1
      @car1.owners_unassociated_count.should == 2

      @car1.add_owner(@person2)
      # Try collecting?
      @car1.send(:pending_association_additions).collect { |h| h[:object] }.should =~ [@person1, @person2]
      @car1.owners.should =~ [@person1, @person2]
      @car1.owners_unassociated.should =~ [@person3]
      @car1.owners_count.should == 2
      @car1.owners_unassociated_count.should == 1

      @car1.add_owner(@person3)
      @car1.send(:pending_association_additions).collect { |h| h[:object] }.should =~ [@person1, @person2,  @person3]
      @car1.owners.should =~ [@person1, @person2,  @person3]
      @car1.owners_unassociated.should =~ []
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0

      ChangeTracker.commit
      @car1.send(:pending_association_additions).should =~ []
      @car1.owners.should =~ [@person1, @person2, @person3]
      @car1.owners_count.should == 3
      @car1.owners_unassociated_count.should == 0
    end

    it "should be able to list the pending association removals" do
      @moto1.add_owner(@person1)
      @moto1.add_owner(@person2)
      @moto1.add_owner(@person3)

      ChangeTracker.commit
      ChangeTracker.start

      @moto1.send(:pending_association_removals).should =~ []
      @moto1.owners.should =~ [@person1, @person2,  @person3]
      @moto1.owners_unassociated.should =~ []
      @moto1.owners_count.should == 3
      @moto1.owners_unassociated_count.should == 0

      @moto1.remove_owner(@person1)
      @moto1.send(:pending_association_removals).collect { |h| h[:object] }.should =~ [@person1]
      @moto1.owners.should =~ [@person2, @person3]
      @moto1.owners_unassociated.should =~ [@person1]
      @moto1.owners_count.should == 2
      @moto1.owners_unassociated_count.should == 1

      @moto1.remove_owner(@person2)
      @moto1.send(:pending_association_removals).collect { |h| h[:object] }.should =~ [@person1, @person2]
      @moto1.owners.should =~ [@person3]
      @moto1.owners_unassociated.should =~ [@person1, @person2]
      @moto1.owners_count.should == 1
      @moto1.owners_unassociated_count.should == 2


      @moto1.remove_owner(@person3)
      @moto1.send(:pending_association_removals).collect { |h| h[:object] }.should =~ [@person1, @person2, @person3]
      @moto1.owners.should =~ []
      @moto1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @moto1.owners_count.should == 0
      @moto1.owners_unassociated_count.should == 3

      ChangeTracker.commit
      ChangeTracker.start

      @moto1.send(:pending_association_removals).should =~ []
      @moto1.owners.should =~ []
      @moto1.owners_unassociated.should =~ [@person1, @person2, @person3]
      @moto1.owners_count.should == 0
      @moto1.owners_unassociated_count.should == 3
    end
  end

  # Example: Application::Person (customers) *----* RepairShop (repairshops)
  context "confirm pending association listings polymorphic many-to-many" do
    before :each do
      @customer1 = Automotive::Mechanic.create
      @customer2 = People::Person.create
      @customer3 = People::Person.create
      @repair_shop = Automotive::RepairShop.create
      ChangeTracker.commit
      ChangeTracker.start
    end
    
    it "should be able to list the pending association additions" do
      @repair_shop.customers_associations.count.should == 0
      @repair_shop.send(:pending_association_additions).count.should == 0
      @repair_shop.add_customer(@customer1)
      @repair_shop.send(:pending_association_additions).count.should == 1
      @repair_shop.customers_associations.count.should == 1
      @repair_shop.add_customer(@customer2)
      @repair_shop.send(:pending_association_additions).count.should == 2
      @repair_shop.customers_associations.count.should == 2
      @repair_shop.add_customer(@customer3)
      @repair_shop.send(:pending_association_additions).count.should == 3
      @repair_shop.customers_associations.count.should == 3
      ChangeTracker.commit
      @repair_shop.send(:pending_association_additions).count.should == 0
      @repair_shop.customers_associations.count.should == 3
    end
    
    it "should be able to list the pending association removals when removing one side of an association class association (side a)" do
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join = klass.create
      join.customer = @customer1
      join.repair_shop = @repair_shop
      join.save
      ChangeTracker.commit
      ChangeTracker.start
      @repair_shop.customers_associations.count.should == 1
      @repair_shop.send(:pending_association_additions).count.should == 0
      join.repair_shop = nil
      join.save
      @repair_shop.send(:pending_association_additions).count.should == 0
      @repair_shop.send(:pending_association_removals).count.should == 1
      @repair_shop.customers.count.should == 0
      @repair_shop.customers_associations.count.should == 0
    end
    
    it "should be able to list the pending association removals when removing one side of an association class association (side b)" do
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join  = klass.create
      join.customer    = @customer1
      join.repair_shop = @repair_shop
      join.save
      ChangeTracker.commit
      ChangeTracker.start
      @repair_shop.customers_associations.count.should == 1
      @repair_shop.send(:pending_association_additions).count.should == 0
      join.customer = nil
      join.save
      @repair_shop.send(:pending_association_additions).count.should == 0
      # @repair_shop.send(:pending_association_removals).count.should == 1 # this method has no knowledge of operations concerning the other side of the join class
      @repair_shop.customers.count.should == 0
      @repair_shop.customers_associations.count.should == 0
    end
    
    it "should be able to list the pending associations when removing both sides of an association class association" do
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join = klass.create
      join.customer = @customer1
      join.repair_shop = @repair_shop
      join.save
      ChangeTracker.commit
      ChangeTracker.start
      @repair_shop.customers_associations.count.should == 1
      @repair_shop.send(:pending_association_additions).count.should == 0
      join.customer = nil
      join.repair_shop = nil
      join.save
      @repair_shop.send(:pending_association_additions).count.should == 0
      @repair_shop.send(:pending_association_removals).count.should == 1
      @repair_shop.customers.count.should == 0
      @repair_shop.customers_associations.count.should == 0
    end
    
    it "should be able to list the pending associations when adding one side of an association class association (side a)" do
      @repair_shop.customers_associations.count.should == 0
      @repair_shop.send(:pending_association_additions).count.should == 0
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join = klass.create
      join.customer = @customer1
      ChangeTracker.commit
      ChangeTracker.start
      join.repair_shop = @repair_shop
      @repair_shop.send(:pending_association_additions).count.should == 1
      @repair_shop.customers.should == [@customer1]
      @repair_shop.customers_associations.count.should == 1
    end
      
    it "should be able to list the pending associations when adding one side of an association class association (side b)" do
      @repair_shop.customers_associations.count.should == 0
      @repair_shop.send(:pending_association_additions).count.should == 0
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join = klass.create
      join.repair_shop = @repair_shop
      join.save
      ChangeTracker.commit
      ChangeTracker.start
      join.customer = @customer1
      join.save
      # @repair_shop.send(:pending_association_additions).count.should == 1 # this method has no knowledge of operations concerning the other side of the join class
      @repair_shop.customers.should == [@customer1]
      @customer1.should == @repair_shop.customers_associations.first[:to]
      join.should == @repair_shop.customers_associations.first[:through]
      @repair_shop.customers_associations.should =~ [{:to => @customer1, :through => join}]
      @repair_shop.customers_associations.count.should == 1
    end
    
    it "should be able to list the pending association additions when adding both sides of an association class association" do
      @repair_shop.customers_associations.count.should  == 0
      @customer1.repair_shops_associations.count.should == 0
      @repair_shop.send(:pending_association_additions).count.should == 0
      @customer1.send(:pending_association_additions).count.should   == 0
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join  = klass.create
      join.customer    = @customer1
      join.repair_shop = @repair_shop
      @repair_shop.send(:pending_association_additions).count.should == 1
      @customer1.send(:pending_association_additions).count.should   == 1
      $debuggit = true
      @customer1.repair_shops.should == [@repair_shop]
      @repair_shop.customers.should  == [@customer1]
      @repair_shop.customers_associations.count.should  == 1
      @customer1.repair_shops_associations.count.should == 1
      $debuggit = false
    end
    
    it "should be able to list the pending association additions when repeatedly adding and removing both sides of an association class association" do
      @repair_shop2 = Automotive::RepairShop.create(:name => 'ABC Garage')
      @repair_shop.customers_associations.count.should == 0
      @repair_shop.send(:pending_association_additions).count.should == 0
      klass = Automotive::RepairShop.associations[:customers][:through].to_const
      join = klass.create
      join.customer = @customer2
      join.repair_shop = @repair_shop2
      
      ChangeTracker.commit
      ChangeTracker.start
      # join.customer = nil
      # join.repair_shop = nil
      join.customer.should == @customer2
      join.repair_shop.should == @repair_shop2
      join.send(:pending_association_additions).count.should == 0
      join.customer = @customer1
      join.customer = @customer2
      join.customer = @customer3
      join.customer = @customer2
      join.repair_shop = @repair_shop

      join.customer.should == @customer2
      join.repair_shop.should == @repair_shop

      @repair_shop.customers.should == [@customer2]
      @customer1.repair_shops.should == []
      @customer3.repair_shops.should == []
      @customer2.repair_shops.should == [@repair_shop]
      
      @repair_shop.send(:pending_association_additions).count.should == 1
      @repair_shop.customers_associations.count.should == 1
    end
    
    it "should be able to list the pending association removals" do
      @repair_shop.add_customer(@customer1)
      @repair_shop.add_customer(@customer2)
      @repair_shop.add_customer(@customer3)
      ChangeTracker.commit
      ChangeTracker.start
      @repair_shop.customers_associations.count.should == 3
      @associations = @repair_shop.customers_associations.collect { |assoc| assoc }
      association1 = @associations.first
      association2 = @associations.last
      association2[:through].customer = nil
      
      ChangeTracker.commit
      ChangeTracker.start
      @repair_shop.customers_associations.count.should == 3
      
      @repair_shop.remove_customer(@customer2)
      @repair_shop.send(:pending_association_removals).count.should == 1
      @repair_shop.customers_associations.count.should == 2
      
      @repair_shop.remove_customer(@customer1)
      @repair_shop.send(:pending_association_removals).count.should == 2
      @repair_shop.customers_associations.count.should == 1
      
      @repair_shop.add_customer(@customer3)
      @repair_shop.remove_customer(@customer3)
      @repair_shop.send(:pending_association_removals).count.should == 2
      @repair_shop.customers_associations.count.should == 1
    end
  end
end
