require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "ChangeTracker one to one associations" do
  
  before :each do
    ChangeTracker.cancel
    reset_db
    ChangeTracker.start
  end
  
  # TODO: Need to check multiple users interacting with the data simultaneously
  
  # ============================================================
  # BEGIN polymorphic-to-nonpolymorphic one-to-one section -- ChangeTracker
  # ============================================================
  # Test that re-adding an existing association does not destroy existing association
  # ============================================================
  # END polymorphic-to-nonpolymorphic one-to-one section -- ChangeTracker
  # ============================================================

  # ============================================================
  # BEGIN ChangeTracked polymorphic-to-nonpolymorphic one-to-one section
  # ============================================================
  
  context "polymorphic-to-nonpolymorphic relationship (association attribute) using ChangeTracker -" do
    # Example: Vehicle <>--- VIN (vin)
    before :each do
      @car = Automotive::Car.create(:vehicle_model => 'Thunderbird')
      @hybrid = Automotive::HybridVehicle.create(:vehicle_model => 'Avalon')
      @vin1 = Automotive::VIN.create(:vin => 101010)
      @vin2 = Automotive::VIN.create(:vin => 101011)
      @vin3 = Automotive::VIN.create(:vin => 101100)
      @vin4 = Automotive::VIN.create(:vin => 101101)
      ChangeTracker.commit
      ChangeTracker.start
    end
    
    context "adding an element" do
      it "should not commit changes necessary for the association until a commit is made, but it should show in the method" do
        @car.vin = @vin1
        @car.vin_id.should be_nil
        @car.vin.should == @vin1
        ChangeTracker.commit
        @car.vin_id.should == @vin1.id
        @car.vin.should == @vin1
      end
      
      it "should set the element for the association via the first class" do
        # Set the associations
        @car.vin = @vin1
        @hybrid.vin = @vin2
        
        # Check values before commit
        @car.vin.should == @vin1
        @hybrid.vin.should == @vin2
        
        # Check values after commit
        ChangeTracker.commit
        @car.vin.should == @vin1
        @hybrid.vin.should == @vin2
      end
      
      it "should set the element for the association via the second class" do
        # Set the associations
        @vin1.vehicle = @car
        @vin2.vehicle = @hybrid
        
        # Check values before commit
        @vin1.vehicle.should == @car
        @vin2.vehicle.should == @hybrid
        
        # Check values after commit
        ChangeTracker.commit
        @vin1.vehicle.should == @car
        @vin2.vehicle.should == @hybrid
      end
      
      it "should create associations on objects that have not been committed yet" do
        # Create uncommitted objects
        @new_car = Automotive::Car.create
        @new_hybrid = Automotive::HybridVehicle.create
        @new_vin1 = Automotive::VIN.create
        @new_vin2 = Automotive::VIN.create
        
        # Set the associations
        @new_car.vin = @new_vin1
        @new_hybrid.vin = @new_vin2
        
        # Check values before commit
        @new_car.vin.should == @new_vin1
        @new_hybrid.vin.should == @new_vin2
        
        # Check values after commit
        ChangeTracker.commit
        @new_car.vin.should == @new_vin1
        @new_hybrid.vin.should == @new_vin2
      end
      
      it "should reject an element that is not of the polymorphic type" do
        # Create invalidly typed object
        @component = Automotive::Component.create
        
        # Attempt to set the association to the invalid object (Component is not of type Vehicle)
        lambda {@vin1.vehicle = @component}.should raise_error(Sequel::Error, /Invalid type for association/i)
        @vin1.vehicle.should be_nil
      end
    end
    
    context "removing an element(s)" do
      before :each do
        @car.vin = @vin1
        @hybrid.vin = @vin2
        ChangeTracker.commit
        ChangeTracker.start
      end
      
      it "should remove the association via the first class" do
        # Remove the associations
        @car.vin = nil
        @hybrid.vin = nil
        
        # Check values before commit
        @car.vin.should == nil
        @hybrid.vin.should == nil
        
        # Check values after commit
        ChangeTracker.commit
        @car.vin.should == nil
        @hybrid.vin.should == nil
      end
      
      it "should remove the association via the second class" do
        # Remove the associations
        @vin1.vehicle = nil
        @vin2.vehicle = nil
        
        # Check values before commit
        @vin1.vehicle.should == nil
        @vin2.vehicle.should == nil
        
        # Check values after commit
        ChangeTracker.commit
        @vin1.vehicle.should == nil
        @vin2.vehicle.should == nil
      end
      
      it "should allow reassociation in a second transation if the association is removed" do
        # Remove the associations and commit
        @car.vin = nil
        @hybrid.vin = nil
        ChangeTracker.commit
        ChangeTracker.start
        
        # Reassociate
        @car.vin = @vin1
        @hybrid.vin = @vin2
        
        # Check values before commit
        @car.vin.should == @vin1
        @hybrid.vin.should == @vin2
        
        # Check values after commit
        ChangeTracker.commit
        @car.vin.should == @vin1
        @hybrid.vin.should == @vin2
      end

      it "should allow reassociation in the same transaction" do
        # Remove the associations
        @car.vin = nil
        @hybrid.vin = nil
        
        # Reassociate
        @car.vin = @vin1
        @hybrid.vin = @vin2
        
        # Check values before commit
        @car.vin.should == @vin1
        @hybrid.vin.should == @vin2
        
        # Check values after commit
        ChangeTracker.commit
        @car.vin.should == @vin1
        @hybrid.vin.should == @vin2
      end

      it "should allow removal in the same transaction" do
        # Clear out current associations
        @car.vin = nil
        @hybrid.vin = nil
        ChangeTracker.commit
        ChangeTracker.start
        
        # Set the associations
        @car.vin = @vin1
        @hybrid.vin = @vin2
        
        # Remove the associations
        @car.vin = nil
        @hybrid.vin = nil
        
        # Check values before commit
        @car.vin.should == nil
        @hybrid.vin.should == nil
        
        # Check values after commit
        ChangeTracker.commit
        @car.vin.should == nil
        @hybrid.vin.should == nil
      end
      
      it "dissociate the old association if a new association is set" do
        # Associate to different objects
        @vin3.vehicle = @car
        @vin4.vehicle = @hybrid
        
        # Check values before commit
        @vin3.vehicle.should == @car
        @vin4.vehicle.should == @hybrid
        
        # Check values after commit
        ChangeTracker.commit
        @vin3.vehicle.should == @car
        @vin4.vehicle.should == @hybrid
      end
    end
    
    context "combination of actions" do
      it "should be able to process many changes to the association before a commit" do
        @car.vin = @vin1
        @car.vin = @vin2
        @car.vin = nil
        @car.vin.should == nil
        ChangeTracker.commit
        @car.vin.should == nil
      end
      
      it "should be able to process association changes in the same context as a creation with one object being created" do
        @new_vin = Automotive::VIN.create(:vin => 101110)
        @car.vin = @new_vin
        @car.vin.should == @new_vin
        ChangeTracker.commit
        @car.vin.should == @new_vin
      end
      
      it "should be able to process association changes in the same context as a creation with the other object being created" do
        @new_car = Automotive::Car.create
        @new_car.vin = @vin1
        @new_car.vin.should == @vin1
        ChangeTracker.commit
        @new_car.vin.should == @vin1
      end
      
      it "should be able to process association changes in the same context as a creation with both objects being created" do
        @new_car = Automotive::Car.create
        @new_vin = Automotive::VIN.create(:vin => 101110)
        @new_car.vin = @new_vin
        @new_car.vin.should == @new_vin
        ChangeTracker.commit
        @new_car.vin.should == @new_vin
      end
    end # End combination of actions context
  end
  # ============================================================
  # END ChangeTracked polymorphic-to-nonpolymorphic one-to-one section
  # ============================================================
end
  
