require 'spec_helper'

# These tests modify the domain object classes, so the sequel tests must be run last

describe "Disabled ChangeTracker pending associations meta information" do
  before :each do
    ChangeTracker.disabled = true 
    # ChangeTracker.cancel
    reset_db
    # ChangeTracker.start
  end
  
  # Example: Vehicle ----* Component
  context "confirm pending association listings one-to-many for a committed item" do
    before :each do
      @car = Automotive::Car.create
      @component1 = Automotive::Component.create
      @component2 = Automotive::Component.create
      @component3 = Automotive::Component.create
      @component4 = Automotive::Component.create
      # ChangeTracker.commit
      # ChangeTracker.start
    end
    
    it "should be able to list the pending association removals and apply them when retrieving associated/unassociated objects" do
      @car.add_component(@component1)
      @car.add_component(@component2)
      @car.add_component(@component3)
      # ChangeTracker.commit
      # ChangeTracker.start
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 3
      @car.components_unassociated.count.should == 1
      @car.components_unassociated_count.should == 1
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
      
      @car.remove_component(@component1)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 2
      @car.components_unassociated.count.should == 2
      @car.components_unassociated_count.should == 2
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
      
      @car.remove_component(@component2)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 1
      @car.components_unassociated.count.should == 3
      @car.components_unassociated_count.should == 3
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
      
      @car.remove_component(@component3)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 0
      @car.components_unassociated.count.should == 4
      @car.components_unassociated_count.should == 4
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
    end
    
    it "should be able to list the pending association additions and apply them when retrieving associated/unassociated objects" do
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ []
      @car.components_unassociated.should =~ [@component1, @component2, @component3, @component4]
      @car.components_count.should == 0
      @car.components_unassociated_count.should == 4

      @car.add_component(@component1)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1]
      @car.components_unassociated.should =~ [@component2, @component3, @component4]
      @car.components_count.should == 1
      @car.components_unassociated_count.should == 3

      @car.add_component(@component2)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1, @component2]
      @car.components_unassociated.should =~ [@component3, @component4]
      @car.components_count.should == 2
      @car.components_unassociated_count.should == 2

      @car.add_component(@component3)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1, @component2,  @component3]
      @car.components_unassociated.should =~ [@component4]
      @car.components_count.should == 3
      @car.components_unassociated_count.should == 1

      @car.add_component(@component4)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1, @component2, @component3, @component4]
      @car.components_count.should == 4
      @car.components_unassociated_count.should == 0
    end
  end

  # Example: Vehicle ----* Component
  context "confirm pending association listings one-to-many for an uncommitted item" do
    before :each do
      @component1 = Automotive::Component.create
      @component2 = Automotive::Component.create
      @component3 = Automotive::Component.create
      @component4 = Automotive::Component.create
      # ChangeTracker.commit
      # ChangeTracker.start
      @car = Automotive::Car.create
    end
    
    it "should be able to list the pending association removals and apply them when retrieving associated/unassociated objects" do
      @car.add_component(@component1)
      @car.add_component(@component2)
      @car.add_component(@component3)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 3
      @car.components_unassociated.count.should == 1
      @car.components_unassociated_count.should == 1
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
      
      @car.remove_component(@component1)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 2
      @car.components_unassociated.count.should == 2
      @car.components_unassociated_count.should == 2
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
      
      @car.remove_component(@component2)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 1
      @car.components_unassociated.count.should == 3
      @car.components_unassociated_count.should == 3
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
      
      @car.remove_component(@component3)
      @car.send(:pending_association_removals).count.should == 0
      @car.components_count.should == 0
      @car.components_unassociated.count.should == 4
      @car.components_unassociated_count.should == 4
      @car.components_unassociated.each { |unassoc| unassoc.should be_a(Automotive::Component)}
    end
    
    it "should be able to list the pending association additions and apply them when retrieving associated/unassociated objects" do
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ []
      @car.components_unassociated.should =~ [@component1, @component2, @component3, @component4]
      @car.components_count.should == 0
      @car.components_unassociated_count.should == 4

      @car.add_component(@component1)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1]
      @car.components_unassociated.should =~ [@component2, @component3, @component4]
      @car.components_count.should == 1
      @car.components_unassociated_count.should == 3

      @car.add_component(@component2)
      # Try collecting?
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1, @component2]
      @car.components_unassociated.should =~ [@component3, @component4]
      @car.components_count.should == 2
      @car.components_unassociated_count.should == 2

      @car.add_component(@component3)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1, @component2,  @component3]
      @car.components_unassociated.should =~ [@component4]
      @car.components_count.should == 3
      @car.components_unassociated_count.should == 1

      @car.add_component(@component4)
      @car.send(:pending_association_additions).should =~ []
      @car.components.should =~ [@component1, @component2, @component3, @component4]
      @car.components_count.should == 4
      @car.components_unassociated_count.should == 0
    end
  end
end
