require 'spec_helper'

describe "SSA-defined extensions to Ruby and Sequel" do
  before(:all) do
    reset_db
    ChangeTracker.start
    @p1 = People::Person.create(:name => 'Alice')
    @p2 = People::Person.create
    @p3 = People::Person.create(:name => 'Bob')
    ChangeTracker.commit
  end
  
  it "should allow filtering datasets on nil values in an array" do
    People::Person.filter(:name => nil).should =~ [@p2]
    People::Person.filter(:name => [nil]).should =~ [@p2]
    People::Person.filter(:name => ['Alice', nil]).should =~ [@p1, @p2]
    People::Person.filter(:name => ['Alice', nil, 'Bob']).should =~ [@p1, @p2, @p3]
  end
  
  it "should allow correct checks on equality that are concerned only with class and id" do
    ChangeTracker.start
    p1a = People::Person.where(:name => 'Alice').first
    p1a.weight = 600
    @p1.weight.should == nil
    p1a.weight.should == 600
    @p1.should_not == p1a
    @p1.is?(p1a).should == true
    p1a.is?(@p1).should == true
    # make sure we don't return false positives
    p2a = People::Person.create
    @p2.is?(p2a).should == false
    p2a.is?(@p2).should == false
    ChangeTracker.commit
    @p2.is?(p2a).should == false
    p2a.is?(@p2).should == false
    p2a.refresh
    @p2.is?(p2a).should == false
    p2a.is?(@p2).should == false
    # check equality of when there is no pk
    another_p1 = People::Person.new
    another_p2 = People::Person.new
    another_p1.pk.should == nil
    another_p2.pk.should == nil
    another_p1.is?(another_p1).should == true
    another_p1.is?(another_p2).should == false
    another_p2.is?(another_p1).should == false
  end
  
  it "should allow correct checks on equality that are concerned only with class and id for collections of objects" do
    ChangeTracker.start
    p1a = People::Person.where(:name => 'Alice').first
    p1a.weight = 600
    SpecificAssociations.models_match?([@p1], [p1a]).should == true
    # ordering and size must be the same
    SpecificAssociations.models_match?([@p2, @p1, @p3], [@p2, p1a, @p3]).should == true
    SpecificAssociations.models_match?([@p2, @p1], [@p2, p1a, @p3]).should == false
    SpecificAssociations.models_match?([@p2, @p1, @p3], [@p2, p1a]).should == false
    SpecificAssociations.models_match?([@p2, @p1, @p3], [p1a, @p2, @p3]).should == false
    
    # make sure we don't return false positives
    p2a = People::Person.create
    SpecificAssociations.models_match?([@p2], [p2a]).should == false
    ChangeTracker.commit
    SpecificAssociations.models_match?([@p2], [p2a]).should == false
    # check equality of when there is no pk
    another_p1 = People::Person.new
    another_p2 = People::Person.new
    SpecificAssociations.models_match?([another_p1], [another_p1]).should == true
    SpecificAssociations.models_match?([another_p1], [another_p2]).should == false
    SpecificAssociations.models_match?([another_p1, another_p2], [another_p1, another_p2]).should == true
    SpecificAssociations.models_match?([another_p1, another_p2], [another_p2, another_p1]).should == false
    # Empty arrays are equal! FIXME should it be like this?
    SpecificAssociations.models_match?([], []).should == true    
  end
end

describe String do
  describe ".to_const" do
    context "no namespace argument passed" do
      it "returns constants that match strings expressing the fully qualified name of the constant" do
        "Qux::Foo::Bar".to_const.should == Qux::Foo::Bar
        "Qux::Foo::Foo".to_const.should == Qux::Foo::Foo
        "Qux::Foo".to_const.should == Qux::Foo
        "Qux".to_const.should == Qux
        "Qux::Baz".to_const.should == Qux::Baz
        lambda{"Baz".to_const}.should raise_error(/Could not find constant Baz in namespace: Object/)        
        lambda{"Qux::Foo::Foo::Foo::Bar".to_const}.should raise_error(/Could not find constant Qux::Foo::Foo::Foo::Bar in namespace: Object/)
        "Qux::Foo::Foo::VAL".to_const.should == Qux::Foo::Foo::VAL
      end
    end
    context "namespace argument passed" do
      it "returns constants that match strings expressing name of the constant" do
        "Qux::Foo::Bar".to_const(Qux).should == Qux::Foo::Bar
        "Qux::Foo::Foo".to_const(Qux).should == Qux::Foo::Foo
        "Qux::Foo".to_const(Qux::Foo).should == Qux::Foo
        "Qux::Foo".to_const(Qux::Baz).should == Qux::Foo
        "Foo::Bar".to_const(Qux).should == Qux::Foo::Bar
        "Foo::Foo".to_const(Qux).should == Qux::Foo::Foo
        "Bar".to_const(Qux::Foo).should == Qux::Foo::Bar
        "Foo".to_const(Qux::Foo).should == Qux::Foo::Foo
        "Foo".to_const(Qux).should == Qux::Foo
        "Bar".to_const(Qux::Foo::Foo).should == Qux::Foo::Bar
        "Foo".to_const(Qux::Foo::Foo).should == Qux::Foo::Foo
        "Qux::Foo::Foo::VAL".to_const(Qux).should == Qux::Foo::Foo::VAL
        "Foo::Foo::VAL".to_const(Qux).should == Qux::Foo::Foo::VAL
        "Foo::Foo::VAL".to_const(Qux::Foo).should == Qux::Foo::Foo::VAL
        "Foo::VAL".to_const(Qux::Foo).should == Qux::Foo::Foo::VAL
        "Foo::VAL".to_const(Qux::Foo::Foo).should == Qux::Foo::Foo::VAL
        "VAL".to_const(Qux::Foo::Foo).should == Qux::Foo::Foo::VAL
        "Qux::Foo::Foo::VAL".to_const(Qux::Baz).should == Qux::Foo::Foo::VAL
      end
    end
  end
end