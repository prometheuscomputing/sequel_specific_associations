require 'spec_helper'

describe "defining constructors on models" do
  before(:all) do
    reset_db
    ChangeTracker.disabled = true
  end
  
  before(:each) do
    DefinitionExampleModule::Adult.reset_constructors
  end
  
  after(:all) do
    reset_db
    ChangeTracker.disabled = false
  end
  
  it "should allow the definition of a constructor" do
    DefinitionExampleModule::Adult.constructor(:father) do |adult|
      adult.name = 'Dad'
      adult.add_dependent(DefinitionExampleModule::Child.create(:allowance => 10))
      # Returned value should be ignored.
      nil
    end
  end
  
  it "should allow the instantiation of a class using the constructor" do
    DefinitionExampleModule::Adult.constructor(:father) do |adult|
      adult.name = 'Dad'
      adult.add_dependent(DefinitionExampleModule::Child.create(:allowance => 10))
      # Returned value should be ignored.
      nil
    end
    father = DefinitionExampleModule::Adult.new_father
    father.name.should == 'Dad'
    father.dependents.length.should == 1
    dependent = father.dependents.first
    dependent.allowance.should == 10
  end
  
  it "should allow arguments when invoking the constructor" do
    DefinitionExampleModule::Adult.constructor(:father) do |adult|
      adult.name = 'Dad'
      adult.add_dependent(DefinitionExampleModule::Child.create(:allowance => 10))
      # Returned value should be ignored.
      nil
    end
    father = DefinitionExampleModule::Adult.new_father(:name => 'NotSeen', :identifier => '123abc')
    father.name.should == 'Dad'
    father.identifier.should == '123abc'
    father.dependents.length.should == 1
    dependent = father.dependents.first
    dependent.allowance.should == 10
  end
  
  it "should be able to list the defined constructors for a class" do
    # Define another constructor
    DefinitionExampleModule::Adult.constructor(:mother) { |adult| adult.name = 'Mom'}  
    constructors = DefinitionExampleModule::Adult.constructors
    constructors.keys.should =~ [:mother, :new]
    constructors[:mother][:method].should be_a(Proc)
    constructors[:new][:method].should == nil
    # Ensure that Procs are functional
    mother = constructors[:mother][:method].call
    mother.name.should == 'Mom'
  end
  
  
  #TODO replace the new constructor
  it "should be able to replace the new constructor" do
    # Define another constructor
    DefinitionExampleModule::Adult.constructor(:new, 'Parent Constructor') { |adult| adult.name = 'Type of Parent'}   
    constructors = DefinitionExampleModule::Adult.constructors
    constructors.keys.should =~ [:new]
    constructors[:new][:method].should be_a(Proc)
    constructors[:new][:name].should == 'Parent Constructor'
    adult = constructors[:new][:method].call
    adult.name.should == 'Type of Parent'
  end
  
  #TODO use the construct method
  it "should allow the instantiation of a class using the construct method" do
    DefinitionExampleModule::Adult.constructor(:mother) { |adult| adult.name = 'Mom'}
    default_new_adult = DefinitionExampleModule::Adult.construct(:new)
    mother = DefinitionExampleModule::Adult.construct(:mother)
    default_new_adult.name.should == nil
    mother.name.should == 'Mom'
  end
  
  it "should allow arguments when invoking the construct method" do
    DefinitionExampleModule::Adult.constructor(:mother) { |adult| adult.name = 'Mom'}
    mother = DefinitionExampleModule::Adult.construct(:mother, {:name => 'NotSeen', :identifier => 'xyz321'})
    mother.name.should == 'Mom'
    mother.identifier.should == 'xyz321'
  end
  
  it "should be able to reset the constructors to only contain new" do
    DefinitionExampleModule::Adult.constructor(:father) do |adult|
      adult.name = 'Dad'
      adult.add_dependent(DefinitionExampleModule::Child.create(:allowance => 10))
      # Returned value should be ignored.
      nil
    end
    DefinitionExampleModule::Adult.constructor(:mother) { |adult| adult.name = 'Mom'} 
    constructors = DefinitionExampleModule::Adult.constructors
    constructors.keys.should =~ [:father, :mother, :new]
    
    #TODO This should probably clear the constructor singleton methods as well somehow in order to be a true reset -TB
    DefinitionExampleModule::Adult.reset_constructors
    constructors = DefinitionExampleModule::Adult.constructors
    constructors.keys.should =~ [:new]
  end
  
  
end