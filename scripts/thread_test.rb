require 'sequel'
require 'sqlite3'
DB = Sequel.sqlite


DB.create_table(:foos) do
  primary_key :id
  String :name
  String :bar
end

class Foo < Sequel::Model
  set_dataset :foos
  # plugin :specific_associations
end

a = Thread.new do
  DB.transaction do
    puts "starting a at #{Time.now}"
    sleep 6
    puts "ending a at #{Time.now}"
  end
end
b = Thread.new do
  DB.transaction do
    puts "starting b at #{Time.now}"
    f = Foo.new(:name => "foo1")
    puts "ending b at #{Time.now}"
    f.save
  end
end
a.join
b.join
puts Foo.count