require 'sequel'
require 'sqlite3'
DB = Sequel.sqlite


DB.create_table(:foos) do
  primary_key :id
  String :name
  String :bar
end

Sequel::Model.db = DB

class Foo < Sequel::Model
  # self.db = DB
  set_dataset :foos
  # plugin :specific_associations
end
f = Foo.new
f.save
