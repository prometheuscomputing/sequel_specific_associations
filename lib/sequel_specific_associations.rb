require 'sequel'
require 'model_metadata'
require 'model_metadata/module_documentation'
require 'sequel_specific_associations/sequel_specific_associations'
require 'sequel_specific_associations/ssa_module'
require 'rainbow'
require 'pp'

module SpecificAssociations
  # Fixnum is deprecated as of Ruby 2.4.0, and is now aliased to Integer
  # Use Integer for Ruby 2.4.0 and later, or Fixnum for prior versions
  major, minor, _ = RUBY_VERSION.split('.').map(&:to_i)
  if major < 3 && minor < 4
    INT_TYPE = Fixnum
  else
    INT_TYPE = Integer
  end
  
  #Datatype Conversion method
  def self.convert_object_to_type(obj_to_convert, new_class, opts = {})
    # getter_mapping    = opts[:getter_mapping] # TODO Implement the getter mapping
    old_class         = obj_to_convert.class
    old_properties    = old_class.properties
    new_class         = new_class.to_const
    new_properties    = new_class.properties
    # This is somewhat strict in that the properties must be of the same type.  It is possible, for example, that the new_class's property could be typed as a super_class of the type of the old_class property and things would still be fine but we just haven't implemented that.
    properties_in_common = old_properties.collect { |k,v| [v[:getter], v[:class]]} & new_properties.collect { |k,v| [v[:getter], v[:class]]}
    getters_in_common = properties_in_common.collect { |a| a.first }

    attributes_hash        = {}
    association_hash       = {}
    complex_attribute_hash = {}
    
    ChangeTracker.start if defined?(ChangeTracker) && !ChangeTracker.started?
    getters_in_common.each do |getter|
      info = old_class.info_for(getter)
      unless info[:derived] # unless it is derived
        if info[:complex] # it is a complex attribute
          # TODO: this is bad....
          # FIXME: This won't handle complex attributes with multiplicity
          complex_attribute_hash[getter] = [{:to => obj_to_convert._get_property(getter)}]
        elsif info[:association] # it is an association
          if info[:through]
            associations = Array(obj_to_convert.send("#{getter}_associations"))
            # puts associations
          else
            associations = obj_to_convert._get_property(getter)
            associations = Array(associations).collect { |assoc| {:to => assoc}}
          end
          association_hash[getter] = associations.collect do |assoc|
            if info[:opp_is_ordered]
              assoc[:position] = assoc[:through] ? assoc[:through].send("#{info[:singular_opp_getter]}_position") : obj_to_convert.send("#{getter}_position")
            end
            assoc
          end
          # Remove existing associations to obj_to_convert
          associations.each do |assoc| 
            if assoc[:through]
              assoc[:through].send("#{info[:singular_opp_getter]}_remove", obj_to_convert)
            else
              assoc[:to].send("#{info[:opp_getter]}_remove", obj_to_convert)
            end
          end
        else # it ought to be an attribute
          attributes_hash[getter] = obj_to_convert._get_property(getter)
        end
      end
    end
    # puts attributes_hash.inspect
    # change_information_hash = {}
    begin
      new_object = new_class.new(attributes_hash)
    rescue SpecificAssociations::UniquenessError => e
      offending_constraints = e.failures.collect { |f| f.constraint }
      old_class.uniqueness_constraints.each { |c| offending_constraints.delete c }
      unless offending_constraints.any?
        raise e
      end
      SpecificAssociations.disable_uniqueness_while {new_object = new_class.create(attributes_hash)}
    end
    association_hash.merge(complex_attribute_hash).each do |getter, associations|
      # We now need to check and make sure that this is really ok to do
      # Type checking
      new_property_data = new_properties.find { |k,v| v[:getter] == getter }.last
      old_property_data = old_properties.find { |k,v| v[:getter] == getter }.last
      next unless associations.inject(true){ |truth, assoc| truth && assoc[:to].kind_of?(new_property_data[:class].to_const)}
      # FIXME: well, really we probably need to check and see if there is an association class associated with both associations as well and, if so, is it the same class of association class.
      
      # Multiplicity checking
      # If there is only one object then it doesn't matter if the new property is to-many or to-one
      # If property[:type] is the same then things are fine
      # Otherwise check to see if they are both to-many or not
      # NOTE -- this does mean that there could be instances where, when going from a to-many to a to-one association, that some of the new objects could transfer the association (if there was only one associated) and other attempts at the same sort of transformation would result in the association not transferring (when association to more than one).  This could be confusing to users of this functionality.
      next unless associations.size == 1 || (old_property_data[:type] == new_property_data[:type]) || (to_many?(old_property_data[:type]) == to_many?(new_property_data[:type]))
      begin
        if to_many?(new_property_data)
          if new_property_data[:through]
            associations.each do |assoc|
              assoc[:through].send("add_#{new_property_data[:singular_opp_getter]}", new_object)
              # assoc[:to].send("move_#{new_property_data[:singular_opp_getter]}", new_object, assoc[:position]) if assoc[:position] && new_property_data[:opp_is_ordered]
            end
          else
            new_object.send("#{getter}=", associations.collect { |assoc| assoc[:to]})
          end
        else # to_one
          # Note: Not handling to_one association class case for now
          assoc = associations.first
          next unless assoc
          new_object.send("#{getter}=", assoc[:to])
          # assoc[:to].send("#{new_property_data[:opp_getter]}_move", new_object, assoc[:position]) if assoc[:position] && new_property_data[:opp_is_ordered]
        end
      rescue SpecificAssociations::UniquenessError => e
        offending_constraints = e.failures.collect { |f| f.constraint }
        old_class.uniqueness_constraints.each { |c| offending_constraints.delete c }
        unless offending_constraints.any?
          raise e
        end
      end
    end
    new_object.save
    # ChangeTracker.commit if defined?(ChangeTracker)
    # ChangeTracker.start if defined?(ChangeTracker)
    obj_to_convert.destroy
    ChangeTracker.commit if defined?(ChangeTracker)
          
    new_object
  end

  def self.models_match?(objs1, objs2)
    return false unless objs1.count == objs2.count
    (objs1.zip(objs2)).each do |array_pair|
      arrays_are_the_same = array_pair.first.is?(array_pair.last)
      return false unless arrays_are_the_same
    end
    true
  end
end