require 'thread'

module SpecificAssociations
  SEQUEL_COLUMN_CLASSES = [Integer, String, File, Float, BigDecimal, Date, DateTime, Time, Numeric, TrueClass, FalseClass]
  
  # Fixnum and Bignum are deprecated as of Ruby 2.4.0
  # Use Integer for Ruby 2.4.0 and later
  major, minor, _ = RUBY_VERSION.split('.').map(&:to_i)
  if major < 3 && minor < 4
    SEQUEL_COLUMN_CLASSES << Fixnum
    SEQUEL_COLUMN_CLASSES << Bignum
  end
  
  def self.disable_uniqueness_warnings
    Thread.current[:uniqueness_warnings_enabled] = false
  end
  def self.enable_uniqueness_warnings
    Thread.current[:uniqueness_warnings_enabled] = true
  end
  def self.uniqueness_warnings_enabled?
    Thread.current[:uniqueness_warnings_enabled].nil? ? (Thread.current[:uniqueness_warnings_enabled] = true) : Thread.current[:uniqueness_warnings_enabled]
  end
  
  def self.disable_uniqueness_constraints
    Thread.current[:uniqueness_constraints_enabled] = false
  end
  def self.enable_uniqueness_constraints
    Thread.current[:uniqueness_constraints_enabled] = true
  end
  def self.uniqueness_constraints_enabled?
    Thread.current[:uniqueness_constraints_enabled].nil? ? (Thread.current[:uniqueness_constraints_enabled] = true) : Thread.current[:uniqueness_constraints_enabled]
  end
  
  def self.disable_uniqueness
    disable_uniqueness_constraints
    disable_uniqueness_warnings
  end
  def self.enable_uniqueness
    enable_uniqueness_constraints
    enable_uniqueness_warnings
  end
  # def self.uniqueness_enabled?
  #   uniqueness_constraints_enabled? && uniqueness_warnings_enabled? # This is tricky and perhaps should not be used.  Should both constraints and warnings have to be enabled for this to be true?
  # end
  
  def self.disable_uniqueness_while &block
    original_warning_value    = uniqueness_warnings_enabled?
    original_constraint_value = uniqueness_constraints_enabled?
    disable_uniqueness
    ret = yield
    Thread.current[:uniqueness_warnings_enabled]    = original_warning_value
    Thread.current[:uniqueness_constraints_enabled] = original_constraint_value
    ret
  end
  
  def self.disable_uniqueness_constraints_while &block
    original_value = uniqueness_constraints_enabled?
    disable_uniqueness_constraints
    ret = yield
    Thread.current[:uniqueness_constraints_enabled] = original_value
    ret
  end
  
  def self.disable_uniqueness_warnings_while &block
    original_value = uniqueness_warnings_enabled?
    disable_uniqueness_warnings
    ret = yield
    Thread.current[:uniqueness_warnings_enabled] = original_value
    ret
  end
  
  def self.get_base_attr_class attr_class
    attr_class = attr_class.to_const
    return nil if attr_class == Object
    if SEQUEL_COLUMN_CLASSES.include? attr_class
      attr_class
    else
      get_base_attr_class(attr_class.superclass)
    end
  end
  # def gripe; puts "I AM SO GRUMPY!"; end
  # module_function :get_base_attr_class
  
end