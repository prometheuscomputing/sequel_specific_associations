# TODO: parse this file into appropriate model files.
class Sequel::Model
  # Returns a concrete implementation of this class (guesses a child class if the class is abstract)
  def self.concrete_class
    return self unless abstract? || interface?
    if interface?
      implementor = implementors.select { |i| !i.abstract? }.first
      return implementor if implementor
    else
      child_klass = children.select { |ck| !ck.abstract? }.first
      return child_klass if child_klass
    end
    raise SpecificAssociations::NoConcreteClassFound, "Could not find concrete implementation of #{self} among #{([self] + children).inspect}"
  end
  
  # ============================================================
  # Methods to obtain the names of getters
  
  
  $DEFAULT_RICH_TEXT_GETTER_TYPE=:content # or :complete
  
  # Identical to local_attribute_names, but includes inherited attribute names.
  # This is similar to #attributes.keys, but:
  #   * Works for any Sequel::Model (a few do not respond to :attributes)
  #   * Does not include foreign keys
  #   * Does not include class names that are used with foreign keys when inheritance is required 
  #   * Lets you specify if you want getters for RichText objects, or the content of RichText objects
  def self.all_attribute_names(rich_text_getter_type=$DEFAULT_RICH_TEXT_GETTER_TYPE)
     return [] unless respond_to?(:attributes) # Sometimes does not respond to #attributes (when sent to a superclass???)
     assoc_hash = associations
     attrs = attributes
     answer = attrs.keys - [:id]
     if :content==rich_text_getter_type
        rich_text_getters = attrs.select { |key, info| "Gui_Builder_Profile::RichText"== info[:type]}.keys
        rich_text_content_getters = rich_text_getters.collect { |getter| "#{getter}_content".to_sym }
        answer = answer - rich_text_getters + rich_text_content_getters
        answer.uniq!
     end
     return answer unless assoc_hash.instance_of?(Hash) # Sometimes #associations returns an Array (when sent to a superclass???)
     assocs = assoc_hash.keys
     possible_fks = assocs.collect { |assoc| "#{assoc}_id".to_sym }
     possible_cls = assocs.collect { |assoc| "#{assoc}_class".to_sym } # Occurs when multiple inheritance is present
     answer - possible_fks - possible_cls
  end
  # Returns Symbols. Includes only attributes shown in the model (not foreign keys or :id).  Does not include inherited attribute names
  def self.local_attribute_names(rich_text_getter_type=$DEFAULT_RICH_TEXT_GETTER_TYPE)
     return all_attribute_names(rich_text_getter_type) unless superclass.respond_to?(:all_attribute_names)
     all_attribute_names(rich_text_getter_type) - superclass.all_attribute_names(rich_text_getter_type)
  end
  # Identical to local_association_names, but includes inherited association role names.
  # Similar to associations.keys, but:
  #   * Works for any Sequel::Model (all respond to :associations, but some return an Array instead of a Hash)
  #   * Returns Symbols instead of Strings
  def self.all_association_names
     assoc_hash = associations
     return [] unless assoc_hash.instance_of?(Hash) # Sometimes #associations returns an Array (when sent to a superclass???)
     assoc_hash.keys.collect { |assoc| assoc.to_sym }
  end
  # Returns Symbols. Does not include inherited association role names.
  def self.local_association_names
     return all_association_names unless superclass.respond_to?(:all_association_names)
     all_association_names - superclass.all_association_names
  end
  # Returns Symbols. Includes both attribute names and association role names. Does not include inherited names.
  def self.local_property_names(rich_text_getter_type=$DEFAULT_RICH_TEXT_GETTER_TYPE)
     (local_attribute_names(rich_text_getter_type) + local_association_names).uniq   # duplicates occur for attributes that are of type RichText
  end
  # Identical to local_property_names, but includes inherited names.
  def self.all_property_names(rich_text_getter_type=$DEFAULT_RICH_TEXT_GETTER_TYPE)
     answer = (all_attribute_names(rich_text_getter_type) + all_association_names).uniq    # duplicates occur for attributes that are of type RichText
     # puts "%%% #{name} - #{answer.inspect}"
     answer
  end

  def self.get_changing_columns(changes)
    changes.collect do |getter,data|
      prop = properties.find { |k,v| v[:getter] == getter }
      if prop
        prop.last[:column]
      else
        raise "Could not find column for change with getter #{getter}"
      end
    end.uniq
  end
  
  def is?(obj)
    if defined?(ChangeTracker) && !ChangeTracker.disabled && self.class.respond_to?(:change_tracked?) && self.class.change_tracked? && obj.class.respond_to?(:change_tracked?) && obj.class.change_tracked?
      ct_identity == obj.ct_identity
    else
      (object_id == obj.object_id) || !!(pk && (self.class == obj.class) && (pk == obj.pk)) # implicit check of obj.pk by checking that self has a pk and that self and obj have same pk
    end
  end
  
  # NOTE: I've commented out this method body in its entirety, since it didn't seem to actually validate anything. -SD
  def self.validate_changes(changes)
    result = {}
    # return result if changes.nil? || changes.empty?
    # attrs         = attributes
    # assocs        = associations
    # enums         = assocs.select { |k,v| v[:enumeration]}
    # Note that we aren't removing the enums from the rest of the associations.  This means that we need to check if a change is to an enum property before we move on to checking to see whether it is an association.
    # attr_getters  = attrs.collect { |k,v| v[:getter]}
    # assoc_getters = assocs.collect { |k,v| v[:getter]}
    # enum_getters  = enums.collect { |k,v| v[:getter]}

    # uniqueness_errors  = []
    # errors             = []
    # concurrency_issues = {}
    
    # subset_constrained_properties = {}
    # assocs.select { |getter, info| info[:subset_of]}.each { |getter, info| subset_constrained_properties[:getter] = info[:subset_of]}
    # puts Rainbow('subset_constrained_properties: ').cyan + subset_constrained_properties.inspect
    
    # FIXME handle cyclic problems here too
    
    # only relevant for attributes
    # columns_to_validate = get_changing_columns(changes)
    # relevant_uniqueness_constraints = uniqueness_constraints.select { |name, constraint| (constraint.columns & columns_to_validate).any?}
    # puts Rainbow('relevant_uniqueness_constraints: ').cyan + relevant_uniqueness_constraints.inspect
    
    #
    # uniqueness_constraints.each do |name, constraint|
    #   # relevant_properties = properties.select { |k,v| constraint.getters.include?(v[:getter])}
    #   # The change key IS a getter (well, it darned well better be)
    #   relevant_changes = changes.select { |change_getter, change_data| constraint.getters.include? change_getter }
    #   if relevant_changes.any?
    #   end

    # end
      
    # FIXME resolve quetsions:   enumerations could, theoretically, be subject to both uniqueness and subset constraints if we were to implement the functionality.  What should we do?
    
    #     # Apply changes to domain_obj, and return any errors and/or updated fields
    #     attr_result       = parse_attribute_change(domain_obj, getter, data)
    #     errors            += attr_result[:errors]            if attr_result[:errors]
    #     updated_fields    += attr_result[:updated]           if attr_result[:updated]
    #     uniqueness_errors += attr_result[:uniqueness_errors] if attr_result[:uniqueness_errors]
    #     if attr_result[:concurrency_issues]
    #       concurrency_issues['attributes'] ||= {}
    #       concurrency_issues['attributes'].merge!(attr_result[:concurrency_issues])
    #     end
    #     if attr_result[:concurrency_warnings]
    #       concurrency_issues['warnings'] ||= []
    #       concurrency_issues['warnings'] += attr_result[:concurrency_warnings]
    #     end
    #     if attr_result[:concurrency_messages]
    #       concurrency_issues['messages'] ||= []
    #       concurrency_issues['messages'] += attr_result[:concurrency_messages]
    #     end
    #
    #   # Enumerations need special handling because they are associations that are treated like attributes on the page.
    #   elsif enum_getters.include? getter.to_sym
    #
    #       # this assumes that concurrency checking is not concerned with anything that the user for didn't actively change on the page.  perhaps we should warn if there is a concurrent change?  we already warn for files.
    #       next if data["data"] == data["initial_data"]
    #
    #       # Apply changes to domain_obj, and return any errors and/or updated fields
    #       enum_assocs, updated_field = parse_enumeration_change(domain_obj, getter, data, enums)
    #       new_assocs += enum_assocs
    #       updated_fields << updated_field if updated_field
    #   elsif assoc_getters.include? getter.to_sym
    #     assoc_info = assocs.find { |k,v| v[:getter] == getter.to_sym }
    #     assoc_info = assoc_info.last # it better be here, else how did we enter this block?
    #
    #     # If it came from advanced view it will be a hash.  If not, it came from clearview, which isn't currently using pending_associations
    #     if data.is_a? Hash
    #       # Parse breaking associations
    #       if data['pending_association_removals'] && data['pending_association_removals'].any?
    #         temp_assocs = parse_breaking_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
    #         broken_assocs += temp_assocs if temp_assocs.any?
    #       end
    #
    #       # Parse new associations
    #       if data['pending_association_additions'] && data['pending_association_additions'].any?
    #         temp_assocs, temp_concurrents = parse_new_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
    #         new_assocs += temp_assocs if temp_assocs.any?
    #         # Use the following returned concurrencies hash when we have only ajax submitting the association changes
    #         # concurrency_issues['new_assocs'] ||= {}
    #         # concurrency_issues['new_assocs'].merge!(temp_concurrents) if temp_concurrents.any?
    #       end
    #
    #       # Parse deletions
    #       if data['pending_association_deletions'] && data['pending_association_deletions'].any?
    #         result = parse_delete_associations(domain_obj, getter, data, assoc_info, domain_obj_cache)
    #         # NOTE: nothing is done here with result[:concurrency_issues]
    #         deleted_assocs += result[:delete_assocs] if result[:delete_assocs] && result[:delete_assocs].any?
    #       end
    #
    #       # Parse ordering changes - Must be run every time for ordered collections (e.g. when objects are deleted, added, or broken).
    #       if data['widget'] == 'collection' && assoc_info[:ordered]
    #         temp_assocs, temp_concurrents = parse_ordering_change(domain_obj, getter, data, assoc_info)
    #         reordered_assocs << temp_assocs if temp_assocs.any?
    #       end
    #     end
    #   else
    #     pp data
    #     raise "No getter #{getter} for #{domain_obj}"
    #   end
    # end
    #





    result# if self.new?
    # Things to think about
    # * are we deleting this?
    # * is there really a change here (we are trying to ensure that there is before this but have we really done a good job of it?)
    # * check concurrency -- we are looking here at #check_concurrency in SimpleWidget
    #   * no problem if domain_obj.new? == true           
    # * uniqueness -- ssa method_bodies:1345
    # * other value constraints...
  end
  
  def parse_attribute_change(getter, widget_info)
    # puts "Parse attribute #{domain_obj.class.name}.#{widget_info['getter']}"
    widget_class = Gui::Widgets.get_widget_class(widget_info['widget'])
    raise "Couldn't find widget type for #{widget_info['widget']} (labeled: #{widget_info['label']})" unless widget_class
    answer = widget_class.parse_attribute_change(domain_obj, getter, widget_info)
    # There isn't a convenient way to handle concurrency warnings any differenty than errors right now so we just combine them.
    if answer[:concurrency_warnings]
      answer[:errors] ||=[]
      answer[:errors] += answer[:concurrency_warnings]
    end
    return answer
  end
end
