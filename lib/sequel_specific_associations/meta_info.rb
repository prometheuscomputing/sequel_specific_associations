module SpecificAssociations
    
  # Required
  GEM_NAME = "sequel_specific_associations"
  VERSION  = '11.0.1'
  AUTHORS  = ["Sam Dana", "Ben Dana", "Michael Faughn"]
  SUMMARY = %q{Plugin to Ruby Sequel that allows advanced associations including polymorphism.}
  
  # Optional
  EMAILS      = ["s.dana@prometheuscomputing.com", "michael.faughn@nist.gov"]
  HOMEPAGE    = 'https://gitlab.com/prometheuscomputing/sequel_specific_associations'
  DESCRIPTION = %q{Allows associations between Sequel Models to be defined. Sequel Models
    inheriting from other Models can fulfill associations defined on the parent (polymorphism).
    Also defined in this plugin is various meta-information about the model, including: abstract!, association_class! and others}
  
  LANGUAGE         = :ruby
  LANGUAGE_VERSION = ['>= 2.7']
  RUNTIME_VERSIONS = { :mri => ['>= 2.7'] }
  TYPE     = :library
  LAUNCHER = nil
  DEPENDENCIES_RUBY = {
    # Intended for use with this version of SCT, but it is not a required dependency
    # :sequel_change_tracker => '~> 2.8',
    :sequel         => '4.41',
    :indentation    => nil,
    :model_metadata => '~> 1.1',
    :common         => '~> 1.11',
    :method_source  => '',
    :alternate_inheritance => '~> 2.1' # This is used directly by SSA and not just through model_metadata.
  }
  DEPENDENCIES_MRI   = { }
  DEPENDENCIES_JRUBY = { }
  # For those of us that actually need development dependencies listed so we can install them
  # gem install rspec -v '~> 3.11' && gem install lodepath sqlite3 car_example_generated
  DEVELOPMENT_DEPENDENCIES_RUBY = {
    :rspec                 => '~> 3.11',
    :sqlite3               => nil,
    :car_example_generated => '~> 2.7',
    :simplecov             => nil,
    :lodepath              => '>= 0.2.3'
  }
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = ['lib/.*/templates/.*']
  
end