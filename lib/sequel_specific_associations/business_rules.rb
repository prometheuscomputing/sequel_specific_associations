module BusinessRules
  class RuleError < StandardError
  end
  
  # constrained_property may refer to either an attribute or an association of constrained_class.
  class BusinessRule
    attr_accessor :constrained_class
    attr_accessor :constrained_property
  
    def initialize(klass, property)
      @constrained_class    = constantize(klass)
      @constrained_property = property
      apply
    end
    
    private
    def constantize(str)
      scope = RuleDsl.dsl_scope
      return str.to_const unless scope
      strings = str.to_s.split('::')
      strings.reduce(scope) { |constant, str| constant.const_get(str) }
    end
  end

  # this appears not to transform the data on storage but rather to mutate it upon retrieval
  class TransformRule < BusinessRule
    attr_accessor :condition
    attr_accessor :transformation
    
    def initialize(klass, property, condition, transformation)
      @condition      = condition
      @transformation = transformation
      super(klass, property)
    end
  
    def apply
      getter = constrained_property.to_sym
      # NOTE: this implementation does not allow for multiple prefill rules, but this was also not supported by the original implementation. -SD
      constrained_class.add_property_options(getter, :prefill_rules => [self])
    end
  end
  
  class PrefillingRule < TransformRule
    def initialize(klass, property, containing_association, parent_property = nil, parent_is = nil)
      governing_property    = parent_property || property
      expected_parent_klass = constantize(parent_is) if parent_is
    
      prefill_when = lambda do |input|
          # Have to directly check the values because otherwise we end up calling the new method & get infinite recursion.
          if input.values && (input.values[property] == nil || input.values[property] == '')
            association_value = input.send(containing_association)
            !association_value.nil? && association_value.respond_to?(governing_property) && (parent_is.nil? || association_value.is_a?(expected_parent_klass))            
          else
            false
          end
        end
        
      prefill_how = lambda { |input| input.send(containing_association).send(parent_property).to_s }
      super(klass, property, prefill_when, prefill_how)
    end
  end

  class ValueConstraint < BusinessRule
    def initialize(klass, property, test)
      @test = test
      super(klass, property)
    end
  
    def apply
      return if @applied
      klass    = constrained_class
      property = constrained_property
      setter   = "#{constrained_property}=".to_sym
      add      = "add_#{constrained_property.to_s.singularize}".to_sym
      test     = @test
      
      if klass.method_defined?(setter)
        original_setter = klass.instance_method(setter)
        klass.send(:define_method, setter) do |value|
          if value.is_a?(Enumerable)
            is_valid = value.all? { |val| test.call(val) }
          else
            is_valid = test.call(value)
          end
          raise RuleError, "#{value.inspect} is not a valid value for the '#{property}' property on class #{klass} according to business rules" unless is_valid
          # Call the pre-existing method without cluttering the alias namespace
          original_setter.bind(self).call(value)
        end
      end
    
      if klass.method_defined?(add)
        original_add = klass.instance_method(add)
        klass.send(:define_method, add) do |value|
          is_valid = test.call(value)
          raise RuleError, "#{value.inspect} is not valid for the '#{property}' property on class #{klass} according to business rules" unless is_valid
          # Call the pre-existing method without cluttering the alias namespace
          original_add.bind(self).call(value)
        end
      end
    
      klass.class_eval("alias :#{constrained_property}_add :#{add}") if klass.instance_methods.include? "#{constrained_property}_add".to_sym
      klass.class_eval("alias :add_to_#{constrained_property} :#{add}") if klass.instance_methods.include? "add_to_#{constrained_property}".to_sym
      @applied = true
    end
  end

  class TypeConstraint < BusinessRule
    def initialize(klass, association, valid_types)
      @valid_types = valid_types.collect { |type| constantize(type) }
      super(klass, association)
    end
  
    def apply
      return if @applied
      klass       = constrained_class
      assoc       = constrained_property
      valid_types = @valid_types

      type = "#{constrained_property}_type".to_sym
      # TODO: maybe include subclasses of valid_types
      klass.send(:define_method, type) { valid_types }

      info = klass.info_for(constrained_property.to_sym)
      if ::SpecificAssociations.to_many?(info)
        add          = "add_#{constrained_property.to_s.singularize}".to_sym
        original_add = klass.instance_method(add)
        klass.send(:define_method, add) do |value|
          is_valid = value.nil? || valid_types.any? { |type| value.is_a?(type) }
          raise RuleError, "#{value.class} is not a valid type for the '#{assoc}' association on class #{klass} according to business rules. Valid types are #{valid_types.join(', ')}" unless is_valid
          # Call the pre-existing method without cluttering the alias namespace
          original_add.bind(self).call(value)
        end
        klass.class_eval("alias :#{constrained_property}_add :#{add}") if klass.instance_methods.include? "#{constrained_property}_add".to_sym
        klass.class_eval("alias :add_to_#{constrained_property} :#{add}") if klass.instance_methods.include? "add_to_#{constrained_property}".to_sym
      end

      setter          = "#{constrained_property}=".to_sym
      original_setter = klass.instance_method(setter)
      klass.send(:define_method, setter) do |value|
        if value.is_a?(Enumerable)
          is_valid = value.all? { |assoc| valid_types.any? { |type| assoc.is_a? type}}
        else
          is_valid = value.nil? || valid_types.any? { |type| value.is_a? type }
        end
        raise RuleError, "#{value.class} is not a valid type for the '#{assoc}' association on class #{klass} according to business rules. Valid types are #{valid_types.join(', ')}" unless is_valid
        # Call the pre-existing method without cluttering the alias namespace
        original_setter.bind(self).call(value)
      end
      @applied = true
    end
  end
  
  class MultiplicityConstraint < BusinessRule
    attr_accessor :options
    def initialize(constrained_class, constrained_property, options)
      @options = options
      super(constrained_class, constrained_property)
    end
  
    def apply
      return if @applied
      klass      = constrained_class
      assoc      = constrained_property.to_s
      assoc_info = klass.associations[assoc.to_sym]
      opp_assoc  = assoc_info[:opp_getter].to_s
      raise "Unable to retrieve association info for association: #{assoc} on class: #{klass}" unless assoc_info

      add    = "add_#{assoc.singularize}".to_sym
      remove = "remove_#{assoc.singularize}".to_sym
      count  = "#{assoc}_count".to_sym
      original_count  = klass.instance_method(count)
      original_add    = klass.instance_method(add)
      original_remove = klass.instance_method(remove)
      
      max    = options[:max]
      min    = options[:min]
      filter = options[:on_subtype] ? {'type' => options[:on_subtype]} : {}
      
      # If many_to_many type association, override join class methods
      if assoc_info[:through]
        through_class               = assoc_info[:through].to_class
        assoc_setter_name           = "#{opp_assoc.singularize}=".to_sym
        assoc_getter_name           = "#{opp_assoc.singularize}".to_sym
        opp_assoc_setter_name       = "#{assoc.singularize}=".to_sym
        old_assoc_setter_method     = through_class.instance_method(assoc_setter_name)
        old_opp_assoc_setter_method = through_class.instance_method(opp_assoc_setter_name)
        
        # Setting the class from which the constraint was defined
        through_class.send(:define_method, assoc_setter_name) do |value|
          # NOTE: num_current_values does _NOT_ reflects current association here
          num_current_values = value.send(count, filter)
          raise(RuleError, "Maximum associations exceeded") if (max && (num_current_values + 1) > max)
          # If attempting to remove the association, check min constraint
          raise(RuleError, "Below minimum associations") if ((value == nil) && min && (num_current_values - 1) < min)
          old_assoc_setter_method.bind(self).call(value)
        end
        # Setting the class that is constrained
        through_class.send(:define_method, opp_assoc_setter_name) do |opp_value|
          # Get the instance of the class from which the constraint is defined
          value = send(assoc_getter_name)
          if value # Only have something to check if opp_value is defined
            num_current_values = value.send(count, filter)
            # NOTE: num_current_values reflects current association here
            raise(RuleError, "Maximum associations exceeded") if (max && num_current_values > max)
            # If attempting to remove the association, check min constraint
            raise(RuleError, "Below minimum associations") if ((opp_value == nil) && min && (num_current_values - 1) < min)
          end
          old_opp_assoc_setter_method.bind(self).call(opp_value)
        end
      end   

      klass.send(:define_method, add) do |value|
        num_current_values = original_count.bind(self).call(filter)
        raise(RuleError, "Maximum associations exceeded") if (max && num_current_values >= max)
        original_add.bind(self).call(value)
      end
      
      klass.send(:define_method, remove) do |value|
        num_current_values = original_count.bind(self).call(filter)
        raise(RuleError, "Below minimum associations") if (min && (num_current_values - 1) < min)
        original_remove.bind(self).call(value)
      end
    
      klass.class_eval("alias :#{constrained_property}_add :#{add}") if klass.instance_methods.include?("#{constrained_property}_add".to_sym)
      klass.class_eval("alias :add_to_#{constrained_property} :#{add}") if klass.instance_methods.include?("add_to_#{constrained_property}".to_sym)
      klass.class_eval("alias :#{constrained_property}_remove :#{remove}") if klass.instance_methods.include?("#{constrained_property}_remove".to_sym)
      klass.class_eval("alias :remove_from_#{constrained_property} :#{remove}") if klass.instance_methods.include?("remove_from_#{constrained_property}".to_sym)
      @applied = true
    end
  end
  
  
  module RuleDsl
    # Fake the on_type argument if it wasn't given and this method was called on a Sequel::Model
    def self.use_containing_class_as_type?(args)
      args[:on_type] = name.to_sym if !args.has_key?(:on_type) && self < Sequel::Model
    end
    private_class_method :use_containing_class_as_type?
    
    def self.check_args(args, required_keys)
      use_containing_class_as_type?(args)
      unless args.is_a?(Hash) && (required_keys - args.keys).empty?
        raise(ArgumentError, "Invalid argument hash given to #{caller_locations(1,1)[0].label}: #{args.inspect}")
      end
    end
    private_class_method :check_args

    def self.transform(args)
      check_args(args, [:on_type, :on_property, :when, :how])
      TransformRule.new(args[:on_type], args[:on_property], args[:when], args[:how])
    end

    def self.prefill(args)
      check_args(args, [:on_type, :on_property, :containing_association])      
      attribute_for_prefill = args[:parent_property] || args[:on_property]
      PrefillingRule.new(args[:on_type], args[:on_property], args[:containing_association], attribute_for_prefill, args[:parent_is])
    end

    def self.value_constraint(args, &filter)
      check_args(args, [:on_type, :on_property])      
      filter ||= args[:filter]
      ValueConstraint.new(args[:on_type], args[:on_property], filter)
    end

    def self.type_constraint(args)
      check_args(args, [:on_type, :on_property, :valid_types])      
      TypeConstraint.new(args[:on_type], args[:on_property], args[:valid_types])
    end
    
    def self.multiplicity_constraint(constrained_class, constrained_property, options = {})
      MultiplicityConstraint.new(constrained_class, constrained_property, options)
    end
    
    def self.set_scope(scope)
      warn "#set_scope is deprecated.  Stop using #set_scope and just give the fully qualified names of everything."
      @dsl_scope = scope.to_const
    end
    
    def self.dsl_scope
      @dsl_scope
    end
  end
end
