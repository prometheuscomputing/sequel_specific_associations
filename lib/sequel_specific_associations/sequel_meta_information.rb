module SpecificAssociations
  module SequelMetaInformation
    module ClassMethods

      # FIXME calling something polymorphic because it is a complex attribute is wrong.  Please fix this and don't do it again.
      def is_polymorphic?
        interface? || immediate_children.any? || complex_attribute?
      end
      
      def associates?
        @associates
      end
 
      def immediate_child_classes
        Kernel.warn "immediate_child_classes is deprecated. Please use 'immediate_children' instead."
        immediate_children
      end
 
      # Return all instances of this type, including child types
      def all(&block)
        # TODO: this is not a good implementation. If there is multiple inheritance between this class's children,
        #       then .all will be called twice on a class, and duplicates are returned (this is currently handled by a .uniq call)
        #       This is hard to fix since it isn't easy to call the original definition of .all, given Sequel's mixin structure
        instances = immediate_children.reduce([]) { |all_obj, klass| all_obj + klass.all(&block) }
        instances = super + instances unless abstract?
        instances.uniq
      end
      
      # Return all instances of this type, including child types
      def count_all
        total = count
        immediate_children.each do |klass|
          total += klass.count_all
        end
        total
      end
      
      # Add an instantiate method that creates a new instance of a class like 'create', but it accepts
      # both attributes and associations as arguments
      def instantiate(hash)
        # partition the hash into two, one for attributes, the other for associations (because #create does not support associations)
        # This test is not detailed enough (we know there are other kinds of simple things).
        # It's better to err on the side of presuming something is complex, since that will work anyway (admitedly with lower efficiency)
        attributes = hash.select { |key, value| columns.include?(key) }
        attributes_used = attributes.keys
        associations = hash.reject { |key, value| attributes_used.member?(key) }
        inst = create(attributes)
        return inst if associations.empty?
        associations.each { |getter, value|
          setter = "#{getter}=".to_sym
          inst.send(setter, value)
          }
        inst.save
        inst
      end
      
      # Override new to make sure abstract classes can't be instantiated
      def new(*args)
        raise Sequel::Error, "Cannot instantiate abstract class: #{self}" if abstract?
        super
      end
      
      def constructor(constructor_key, constructor_name=nil, &block)
        raise "A block must be passed to create a constructor." unless block_given?
        constructor_proc = Proc.new { |*args|
          instance = new(*args)
          yield(instance)
          instance
        }
        constructor_name ||= constructor_key
        #This singleton_method is still used for advanced view right now and also left in for some backwards compatibility
        define_singleton_method("new_#{constructor_key}", constructor_proc)
        constructors[constructor_key.to_sym] = {:method => constructor_proc, :name => constructor_name, :classifier => self.to_const}
      end
        
      def constructors
        @constructors
      end
      
      def reset_constructors
        @constructors = {:new => {:method => nil, :name => self.to_s, :classifier => self.to_const}}
      end
      
      def construct(key = nil, *args)
        key ||= :new
        constructor_proc = constructors[key.to_sym][:method]
        if constructor_proc
          constructor_proc.call(*args)
        else
          self.new(*args)
        end
      end
      
      def complex_attribute!
        return if complex_attribute?
        @attribute_users = []
        send(:define_singleton_method, :attribute_users) { @attribute_users }
        # Ensure that #save and #create do not save this class to DB
        save_method_name = respond_to?(:change_tracked?) && change_tracked? ? :save_original : :save
        send(:define_method, save_method_name) do |*args|
          # Call before/after/around save hooks but do nothing else (do not create/update)
          around_save do
            raise_hook_failure(:save) if before_save == false
            after_save
          end
          self
        end
        # Completely override #destroy method (It would crash due to lack of ID)
        send(:define_method, :destroy) do |*args|
          # Call before/after/around save hooks but do nothing else (do not create/update)
          around_destroy do
            raise_hook_failure(:before_destroy) if before_destroy == false
            # actual destruction has no meaning since complex attributes aren't persisted
            after_destroy
            true
          end
          self
        end
        @complex_attribute = true
      end
      
      def complex_attribute?
        @complex_attribute == true
      end

    end # ClassMethods
  end # SequelMetaInformation
end
