require 'model_metadata/primitive_extensions'
module SpecificAssociations
  module PrimitiveExtensions
    def composer; nil; end # this could be implemented...maybe
    def compositions(immediate_only = true); []; end
    def id; nil; end
    def destroy; nil; end
  end
end
ModelMetadata::BASE_PRIMITIVES.each do |klass|
  klass.class_eval {include SpecificAssociations::PrimitiveExtensions}
end