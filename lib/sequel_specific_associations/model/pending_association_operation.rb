module SpecificAssociations
  class PendingAssociationOperation
    attr_accessor :model # model that will be associated
    attr_accessor :association # association information hash
    attr_accessor :pk_field # Primary key column for associated model
    attr_accessor :type_field # Type column of associated model (Stores type of associated class in case of polymorphic association)
    attr_accessor :position_field
    attr_accessor :complex_getter_field
    attr_accessor :second_model
    attr_accessor :second_pk_field # second_ vars are for many to many associations which track two models per entry
    attr_accessor :second_type_field
    attr_accessor :second_position_field
    attr_accessor :through_class
    # Items that need to be reordered. The var is set up by #apply, and is used by #persist_reordering (unless using SCT)
    attr_accessor :items_to_reorder
    # The complex_getter that this association relates to
    attr_accessor :complex_getter
    
  
    def initialize(model, association, pk_field, type_field, position_field, second_model, second_pk_field = nil, second_type_field = nil, second_position_field = nil, through_class = nil, complex_getter_field = nil, complex_getter = nil)
      @model = model
      @association = association
      @pk_field = pk_field
      @type_field = type_field
      @position_field = position_field
      @complex_getter_field = complex_getter_field
      @second_model = second_model
      @second_pk_field = second_pk_field
      @second_type_field = second_type_field
      @second_position_field = second_position_field
      @through_class = through_class
      @items_to_reorder = []
      @complex_getter = complex_getter
    end
    
    def columns
      columns = []
      columns << @pk_field
      columns << @type_field if @type_field
      columns << @position_field if @position_field
      columns << @complex_getter_field if @complex_getter_field
      columns << @second_pk_field if @second_pk_field
      columns << @second_type_field if @second_type_field
      columns << @second_position_field if @second_position_field
      columns
    end
    
    alias_method :through_klass, :through_class
    
    def inspect
      "#{self} -- @model: #{model.inspect}, @pk_field: #{pk_field}, @type_field: #{type_field}, @position_field: #{position_field}, @second_model: #{second_model}, @second_pk_field: #{second_pk_field}, @second_type_field: #{second_type_field}, @second_position_field: #{second_position_field}, @through_class: #{through_class}, association: #{association}"
    end
    
    # A PendingAssociationOperation is processable if all referenced objects are saved (and thus have ids)
    def is_processable?
      referenced_models.all?{ |m| !m.new? }
    end
    
    # The models that referenced, meaning that their ids are recorded to persist this association.
    def referenced_models
      # For many-to-one and one-to-one associations, only 'second_model' is referenced (its id is recorded in a column of 'model')
      models = [@second_model]
      # For many-to-many associations, both 'model' and 'second_model' are referenced by the join entry
      models << @model if @through_class
      models
    end
    
    def persist
      persist_changes_to_join
    end
  end

  class PendingAssociationAddition < PendingAssociationOperation
    def join
      @join ||= @through_class.is_a?(Class) ? @through_class.new : @through_class
    end
    
    # Persist the changes made to the join entry.
    # This method uses @join (not #join) because it should not be called before @join is initialized
    def persist_changes_to_join
      @join.save if @join
    end
    
    def apply
      modified = []
      if @through_class
        # through_class is either the class of the join table entry or an instance of a join table entry.
        # Should be renamed.
        join[@second_pk_field] = @model.pk
        join[@second_type_field] = @model.class.to_s if @second_type_field
        # If association is ordered, set to last position
        if @second_position_field
          max_position_query = join.class.filter(@second_pk_field => @model.pk)
          max_position_query = max_position_query.filter(@second_type_field => @model.class.to_s) if @second_type_field
          max_position = max_position_query.max(@second_position_field) || -1
          join[@second_position_field] = max_position + 1
        end
        join[@pk_field] = @second_model.pk
        join[@type_field] = @second_model.class.to_s if @type_field
        if @position_field
          max_position_query = join.class.filter(@pk_field => @second_model.pk)
          max_position_query = max_position_query.filter(@type_field => @second_model.class.to_s) if @type_field
          max_position = max_position_query.max(@position_field) || -1
          join[@position_field] = max_position + 1
        end
        # NOTE: At this point, join needs to be saved to complete the association.
        #       Not saving here, because this would upset SCT's usage of #apply,
        #       so save is executed separately by #persist
        modified << join
      else
        @model[@pk_field] = @second_model.pk
        @model[@type_field] = @second_model.class.to_s if @type_field
        @model[@complex_getter_field] = @complex_getter if @complex_getter_field
        if @position_field
          if @association[:associates] # @model is a join class instance
            # NOTE: If association classes are allowed to have children, this should be changed
            max_position_query = @model.class.filter(@pk_field => @second_model.pk)
            max_position_query = max_position_query.filter(@type_field => @second_model.class.to_s) if @type_field
            max_position = max_position_query.max(@position_field) || -1
          else # Normal association. Use count method to determine max_position
            # TODO: place in and retrieve from association information, and remove this line
            # - Line is removed, now using the opp_getter. 
            # count_method_name = @second_pk_field.to_s.sub(/_id$/,'').pluralize + '_count'
            count_method_name = @association[:opp_getter].to_s + '_count'
            max_position = @second_model.send(count_method_name, {'ignore_pending' => true}) - 1
          end
          @model[@position_field] = max_position + 1
        end
        modified << @model
      end
      modified
    end
  end

  class PendingAssociationRemoval < PendingAssociationOperation
    def join
      # puts;puts "Pending assoc removal: #{@model.class}[#{@model.pk}] -- #{@second_model.class}[#{@second_model.pk}] -- through: #{@through_class.inspect}"
      @join ||= begin
        if @through_class.is_a?(Class)
          join_table_filter = {}
          join_table_filter[@pk_field]          = @second_model.pk
          join_table_filter[@type_field]        = @second_model.class.to_s if @type_field
          join_table_filter[@second_pk_field]   = @model.pk
          join_table_filter[@second_type_field] = @model.class.to_s if @second_type_field
          query  = @through_class.filter(join_table_filter)
          result = query.first
          # puts Rainbow(query.inspect).yellow unless result
          result
        else
          @through_class
        end
      end
      @join
    end
    
    # Have #persist also persist reordering changes
    def persist
      super
      persist_reordering
    end
    
    # Persist the changes made to the join entry.
    # This method uses @join (not #join) because it should not be called before @join is initialized
    def persist_changes_to_join
      @join.destroy if @join
    end
    
    def persist_reordering
      @items_to_reorder.each { |i| i.save }
    end
    
    def apply
      modified = []
      
      # Check validity of removal
      if @through_class
        valid = join
      else
        valid = @second_model.pk == @model[@pk_field] && (@type_field.nil? || @second_model.class.to_s == @model[@type_field])
      end
      raise(Sequel::Error, "Could not find '#{@association[:getter]}' association between #{@model.class.to_s} and #{@second_model.class.to_s} for this PendingAssociationRemoval:\n#{pretty_inspect}") unless valid
      
      if @through_class
        modified += reorder_positions_without(join, @position_field, @pk_field, @type_field) if @position_field
        modified += reorder_positions_without(join, @second_position_field, @second_pk_field, @second_type_field) if @second_position_field
        join
      else # Not a many-to-many
        modified += reorder_positions_without(@model, @position_field, @pk_field, @type_field) if @position_field
        @model[@pk_field] = nil
        @model[@type_field] = nil if @type_field
        @model[@position_field] = nil if @position_field
        @model[@complex_getter_field] = nil if @complex_getter_field
        modified << @model
      end
      modified
    end
    
    def reorder_positions_without(item_to_remove, position_col, pk_col, type_col)
      item_to_remove.refresh
      position_to_remove = item_to_remove[position_col]
      reorder_items = []
      if @association[:through]
        klass = ::SpecificAssociations.through_class_for(@association)
        classes = [klass]
      else
        klass = ::SpecificAssociations.as_class_for(@association)
        classes = [klass] + klass.children
      end
      classes.each do |c|
        next if c.abstract?
        items_to_reorder_query = c.filter(pk_col => item_to_remove[pk_col])
        items_to_reorder_query = items_to_reorder_query.filter(type_col => item_to_remove[type_col]) if type_col
        items_to_reorder_query = items_to_reorder_query.filter(Sequel.+(position_col, 0) > position_to_remove)
        reorder_items += items_to_reorder_query.all
      end
      
      reorder_items.each do |item|
        item[position_col] -= 1
      end
      
      @items_to_reorder += reorder_items
      reorder_items
    end
  end
end