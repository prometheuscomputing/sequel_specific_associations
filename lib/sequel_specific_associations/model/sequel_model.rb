# Modifications to Sequel::Model

AlternateInheritance.extend_ruby_inheritance = true
AlternateInheritance.top_level_ruby_const_exclusive = Sequel::Model
AlternateInheritance.populate_ruby_children_on_initialization = false

module Sequel
  class Model
    extend ModelMetadata # which is going to give us the appropriate AlternateInheritance inclusions
    # Require after_initialize and scissors plugins
    plugin :after_initialize
    plugin :scissors
    
    def before_association_add(assoc, object)
      Array(assoc[:before_add]).all?{ |p| p.call(object, self) != false} if assoc && assoc[:before_add]
    end
    def after_association_add(assoc, object)
      Array(assoc[:after_add]).all?{ |p| p.call(object, self) != false} if assoc && assoc[:after_add]
    end
    def before_association_remove(assoc, object)
      Array(assoc[:before_remove]).all?{ |p| p.call(object, self) != false} if assoc && assoc[:before_remove]
    end
    def after_association_remove(assoc, object)
      Array(assoc[:after_remove]).all?{ |p| p.call(object, self) != false} if assoc && assoc[:after_remove]
    end
  end
end