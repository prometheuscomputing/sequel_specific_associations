module SpecificAssociations
  # Given information about an association, get the opposite association specification
  def self.get_opposite_association(assoc_info)
    return nil if assoc_info[:attribute]
    # Explicitly state that nil is returned in the case of an :associates relationship
    if assoc_info[:type] == :associates
      puts "Can not get opposite info for a through class"
      return nil
    end
    klass = ::SpecificAssociations.class_for(assoc_info)
    klass.properties[assoc_info[:opp_getter]]
  end
  
  def self.class_for(getter_or_info)
    info = getter_or_info.is_a?(Hash) ? getter_or_info : info_for(getter_or_info)
    # Invalidate cache if needed
    # info[:class_const] = nil if info[:class_const] && info[:class_const].to_s != info[:class]
    info[:class_const] ||= info[:class].to_const
  end

  def self.through_class_for(getter_or_info)
    info = getter_or_info.is_a?(Hash) ? getter_or_info : info_for(getter_or_info)
    info[:through_const] ||= info[:through].to_const
  end
  
  def self.as_class_for(getter_or_info)
    info = getter_or_info.is_a?(Hash) ? getter_or_info : info_for(getter_or_info)
    info[:as_const] ||= info[:as].to_const
  end
  
  # Returns type restrictions for the associations, or nil for no restrictions
  def self.type_classes_for(getter_or_info)
    info = getter_or_info.is_a?(Hash) ? getter_or_info : info_for(getter_or_info)
    return nil unless info[:types]
    info[:type_consts] ||= info[:types].collect{|t| t.to_const }
  end
  
  # Helper method that adds a new filter to an existing filter
  def self.add_to_filter(filter, new_filter)
    return [new_filter] if filter == {}
    filter = filter.is_a?(Array) ? filter : [filter]
    filter << new_filter
    filter
  end
  
  # Helper to 'trim' a filter to only apply to a specific class
  def self.filter_for_class(filter, klass)
    return {} if filter == {}
    filter = filter.is_a?(Array) ? filter : [filter]
    trimmed_filter = []
    filter.each do |f|
      case f
      when Sequel::SQL::BooleanExpression
        keep_filter = true
        new_args = []
        f.args.each_with_index do |arg, i|
          if arg.is_a?(Sequel::SQL::BooleanExpression)
            new_args[i] = filter_for_class(arg, klass).first
          elsif arg.is_a?(Symbol) || arg.is_a?(Sequel::SQL::Identifier)
            if arg.is_a?(Symbol)
              col = arg.to_s.gsub(/#{klass.table_name}__/, '')
            else
              col = arg.value
            end
            # TODO this may not pass muster if the assoc class has a column w/ same name as one of the associated classes
            if klass.columns.include?(col.to_sym)
              if (arg !~ /#{klass.table_name}__/)
                new_args[i] = "#{klass.table_name}__#{col}".to_sym # qualified_col
              else
                new_args[i] = arg
              end
            else
              keep_filter = false
            end
          else
            new_args[i] = arg
          end
        end
        if keep_filter
          trimmed_filter << Sequel::SQL::BooleanExpression.new(f.op, *new_args)
        end
      when Hash
        valid_filter = f.select { |k,v|
          col = k.to_s.gsub(/#{klass.table_name}__/, '')
          klass.columns.include?(col.to_sym)
        }
        qualified_filter = {}
        valid_filter.each do |k, v|
          k = "#{klass.table_name}__#{k}".to_sym unless k.to_s.include?('__')
          qualified_filter[k] = v
        end
        trimmed_filter << qualified_filter if qualified_filter.any?
      else
        puts "Unexpected filter: #{f.inspect}. Not parsing on association class."
      end
    end
    trimmed_filter.any? ? trimmed_filter : {}
  end
  
  def self.filter_join(filter1, filter2)
    case
    when filter1.is_a?(Array) && filter2.is_a?(Array)
      filter1 + filter2
    when filter1.is_a?(Array) && filter2.is_a?(Hash)
      filter1 + [filter2]
    when filter2.is_a?(Array)
      [filter1] + filter2
    else
      [filter1, filter2]
    end
  end
  
  # TODO is it possible to make this more DRY
  def self.transform_enum_search_filter(klass, filter)
    return filter unless filter.is_a?(Hash)
    replacement = {}
    filter.each do |search_getter, search_value|
      if search_getter == :id
        replacement[search_getter] = search_value
        next
      end
      info = klass.info_for(search_getter.to_sym)
      if info.nil? && search_getter.to_s.match?(/__/)
        # Assuming we are searching on an association class column.  Perhaps this is a bad assumption?
        through_getter, through_search_getter = search_getter.to_s.split('__').map { |x| x.to_sym }
        info = klass.info_for(through_getter)
        if info.nil?
          # TODO what should we really do in this case??
          replacement[search_getter] = search_value
          next
        end
        through_klass = ::SpecificAssociations.through_class_for(info)
        info = through_klass.properties[through_search_getter]
        if through_info.nil?
          # TODO what should we really do in this case??
          replacement[search_getter] = search_value
          next
        end
      end
      if info.nil?
        # TODO what should we really do in this case??
        replacement[search_getter] = search_value
        next
      end
      if info[:enumeration]
        enum = nil
        parent_enum_class = ::SpecificAssociations.class_for(info)
        enum_classes = [parent_enum_class] + parent_enum_class.children
        # FIXME there is a case where this would be a problem.  There is currently nothing preventing an instance of an enum class having the same value as does an instance of a child/parent/sibling enum class.
        enum_classes.each do |enum_class|
          enum = enum_class.where(:value => search_value).first
          break if enum
        end
        if enum
          replacement[(search_getter.to_s + '_id').to_sym] = enum.id
        else
          # TODO make a better way of indicating that the filter was bogus.
          return nil
        end
      else
        replacement[search_getter] = search_value
      end
    end
    replacement
  end
  
  # Determine whether a property should be treated as having an array of values
  def self.to_many?(property)
    [:many_to_many, :one_to_many].include?(property[:type])
  end
  
  # Determine whether a property should be treated as having a single value
  def self.to_one?(property)
    [:many_to_one, :one_to_one].include?(property[:type])
  end
  
  # Determine whether the inverse relationship should be treated as having an array of values
  def self.from_many?(property)
    [:many_to_one, :many_to_many].include?(property[:type])
  end
  
  # Determine whether the inverse relationship should be treated as having a single value
  def self.from_one?(property)
    [:one_to_one, :one_to_many].include?(property[:type])
  end
end
