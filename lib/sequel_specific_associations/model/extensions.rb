require 'indentation'
require 'sequel/extensions/inflector'
require 'common/constants'
# Fix for problematic association
String.inflections.irregular 'binary_data', 'binary_data'

=begin
class Array
  # Appends a given separator(s) to all but the last element in an array of Strings
  # or if given an array of arrays, inserts the separator element between each array element,
  # unless the element (array or string) is empty
  def append_separator(*separators)
    len = length
    each_with_index do |element, i|
      unless i == len - 1
        separators.each do |separator|
          element << separator unless element.empty?
        end
      end
    end
  end
end
=end

class Proc
  # Using method_source gem, return a string representation of the proc's definition
  # The context parameter is currently unused, but will be used in future to help determine
  #   which proc is indicated in the case of multiple proc definitions on a line.
  def to_proc_definition(context = nil)
    # Get method_source source. This source is really all of the lines containing the proc/block
    #   definition, and is not the definition itself.
    # Raises MethodSource::SourceNotFoundError if the source could not be determined
    src = self.source
    
    # Look for Proc.new/proc
    definition = ProcHelper.proc_definition_from_string(src, context)
    
    unless definition
      # Look for do ... end block
      block_match = src.scan(/\s+do\s+(.*)\s+end/m)
      definition = "Proc.new { #{block_match[0][0]} }" if block_match.any?
    end
    
    # Failed to determine proc definition from source. Quietly return 'nil'
    return nil.inspect unless definition
    
    definition
  rescue MethodSource::SourceNotFoundError
    # Could not find source for proc. Quietly return 'nil'
    return nil.inspect
  end
end

module ProcHelper
  # Search a string for a proc definition
  # If a context is passed, then the proc definition is assumed to follow the context
  def self.proc_definition_from_string(source, context = nil)
    closure_stack = []
    definition = ''
    context_index = context ? source.index(context.to_s) : 0
    context_source = context_index ? source[context_index..-1] : source
    proc_index = context_source.index(/(proc|Proc.new)\s*\{/m)
    # Return nil if there is no proc in the source
    return nil unless proc_index
    closure_started = false
    context_source[proc_index..-1].each_char{ |c|
      definition << c
      case c
      when '{'
        closure_stack.push '{'
        closure_started = true
      when '}'
        last_closure = closure_stack.pop
        # Return nil if we do not find a matching beginning bracket in the stack
        return nil unless last_closure == '{'
      end
      # Break if the closure is complete
      break if closure_started && closure_stack.empty?
    }
    definition
  end
end

class Symbol
  def to_class; to_s.to_class; end
  def pluralize; to_s.pluralize; end
end

class String
  
  # Alias to #to_const for backwards compatibility
  def to_class(namespace = Object)
    puts "#to_class is deprecated in favor of #to_const. Please update your code."
    to_const(namespace)
  end

  # Capitalize first letter, leave the rest alone
  def capital
    self[0..0].upcase + self[1..length].to_s
  end
  
  # Replaces the current string with one that has had its first letter capitalized.
  def capital!
    # NOTE: jruby fails to correctly interpret self[0], and returns the Fixnum (or Integer if jruby is following Ruby 2.4.0 and replacing Fixnum with Integer) representation of the char
    #       Using a range forces jruby to return a String
    self[0] = self[0..0].upcase
    self
  end
  
  # Convert from CamelCase or under_score to a title
  #
  # 'myNewTitle'.to_title # => 'My New Title'
  # 'my_new_title'.to_title # => 'My New Title'
  #
  #  Small words are not capitalized (unless first in the title)
  # 'Cloudy with a chance of rain' # => 'Cloudy with a Chance of Rain'
  # 'the first item' # => 'The First Item'
  def to_title
    # Parse camel casing to spaces
    gsub(/([a-z0-9])([A-Z])/, '\1 \2').gsub(/([A-Z]+)([A-Z][a-z0-9])/, '\1 \2').
    # Convert all underscores to spaces
    tr('_', ' ').
    # Remove leading and trailing whitespace
    strip
    # Capitalize each word in the parsed string except for small words (prepositions, etc.)
    # .titleize
  end
end

class Hash
  def difference(other)
    reject { |k, v| other.key?(k) }
  end
  alias :- :difference
end
# Override Sequel::SQL::BooleanExpression.from_value_pairs()
# Added ability to specify nil in an array of possible column values in a filter (e.g. filter = {:name => ['Bob', '', nil]})
module Sequel
  module SQL
    class BooleanExpression < ComplexExpression
      # Take pairs of values (e.g. a hash or array of two element arrays)
      # and converts it to a +BooleanExpression+.  The operator and args
      # used depends on the case of the right (2nd) argument:
      #
      # * 0..10 - left >= 0 AND left <= 10
      # * [1,2] - left IN (1,2)
      # * nil - left IS NULL
      # * true - left IS TRUE
      # * false - left IS FALSE
      # * /as/ - left ~ 'as'
      # * :blah - left = blah
      # * 'blah' - left = 'blah'
      #
      # If multiple arguments are given, they are joined with the op given (AND
      # by default, OR possible).  If negate is set to true,
      # all subexpressions are inverted before used.  Therefore, the following
      # expressions are equivalent:
      #
      #   ~from_value_pairs(hash)
      #   from_value_pairs(hash, :OR, true)
      def self.from_value_pairs(input_pairs, op=:AND, negate=false)
        if input_pairs.empty?
          puts caller
          raise
        end
        pairs = input_pairs.collect do |l,r|
          ce = case r
          when Range
            new(:AND, new(:>=, l, r.begin), new(r.exclude_end? ? :< : :<=, l, r.end))
          when ::Array, ::Sequel::Dataset
            # This is the only line that has been changed (from Sequel v3.34.1)
            # If the array includes nil, assume that we want to check if the value is either in the range of listed values or if it is nil
            if r.include?(nil)
              new(:OR, new(:IN, l, r.reject { |e| e.nil?}), new(:IS, l, nil))
            else
              new(:AND, new(:IN, l, r), new(:"IS NOT", l, nil))
            end
          when NegativeBooleanConstant
            new(:"IS NOT", l, r.constant)
          when BooleanConstant
            new(:IS, l, r.constant)
          when NilClass, TrueClass, FalseClass
            new(:IS, l, r)
          when Regexp
            StringExpression.like(l, r)
          else
            new(:'=', l, r)
          end
          negate ? invert(ce) : ce
        end
        pairs.length == 1 ? pairs.at(0) : new(op, *pairs)
      end
    end
  end
end
