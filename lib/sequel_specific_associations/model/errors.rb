module SpecificAssociations
  class Error < StandardError
  end

  class SchemaMismatchError < Error
  end 
  
  class InvalidIndexError < Error
  end
  
  class AssociationInfoMismatchError < Error
  end
  
  class UniquenessError < Error
    attr_accessor :failures
    def initialize(failures)
      @failures = failures
    end
    def failure_message
      @failures.collect { |f| f.message }.join("\n")
    end
  end  
  
  class NonUniqueError < UniquenessError
    def is_error?; true; end
  end
  
  class NonUniqueWarning < UniquenessError
    def is_error?; false; end
  end
  
  class CyclicAssociationError < Sequel::Error
  end
  
  class InvalidAssociationTypeError < Sequel::Error
  end
  
  class FailedSubsetConstraint < Error
  end
  
  class NoConcreteClassFound < Error
  end
  
  class SchemaConflict < Error
  end
  
  class PrimaryKeyConflict < SchemaConflict
  end
end
