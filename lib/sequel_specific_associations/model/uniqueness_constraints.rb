module SpecificAssociations
  class UniquenessConstraint
    attr_accessor :klass
    attr_accessor :getters
    attr_accessor :columns
    attr_accessor :options
    attr_accessor :label
    attr_accessor :identifiers
    attr_accessor :show_value_in_message
    
    def initialize(klass, *getters)
      @options = {}
      # options and their defaults are 
      #   :check_null => false              # Check null values for uniqueness (not implemented fully -- don't use)
      #   :check_empty => false             # Check empty strings for uniqueness (i.e. only allow one unique empty string)
      #   :label => <derived from cols>     # Option to manually set the label for the attribute(s) constrained by this rule
      #   :show_value_in_message => true    # Show/hide the non-unique value that was set. Important because you don't want to 
                                            # necessarily print large values or ids that won't mean anything to the user
      #   :identifiers => <same as cols>    # Option to specify how conflicts are identified to the user.
                                            # Specify the column(s) that should identify a conflict.
      @klass = klass
      @options = getters.pop if getters.last.is_a?(Hash)
      @getters = getters.collect { |g| g.to_sym }
      @columns = @getters.collect do |getter|
        prop = @klass.properties.find { |k,v| v[:getter].to_sym == getter }
        if prop
          prop.last[:column]
        else
          # NOTE: if the user specified a getter in the uniqueness constraint that does not map properly to an attribute getter then we are just ignoring it!  We could instead throw an exception.  Should we?
          nil
        end
      end.compact
      @label                 = @options[:label] || @getters.collect { |c| c.to_s.to_title }.english_join
      @identifiers           = @options[:identifiers] || @columns # or should this be @getters???????
      @show_value_in_message = @options[:show_value_in_message].nil? ? true : @options[:show_value_in_message]
    end
    
    def will_be_unique(object, getter_values)
      # return unique if the getter in question is not constrained by this UniquenessConstraint
      return true if @getters.none?{|g| getter_values.key?(g)}
      column_values = {}
      getter_values.each do |getter, value|
        info = object.class.info_for getter
        column_values[info[:column]] = value
      end
      # return unique if not checking null values for uniqueness and new_value is null
      return true if !@options[:check_null] && column_values.value?(nil)
      # return unique if not checking empty strings for uniqueness and new_value is an empty string
      return true if !@options[:check_empty] && column_values.value?('')
      new_hash = column_values.dup
      @columns.each do |col|
        new_hash[col] = object[col] unless new_hash.key?(col) # don't do 'unless new_hash[col]' because it could be there but be nil, in which case we would overwrite the nil value and we don't want to do that.  The idea is that the user submitted nil (or empty string) as the new value.
      end
      
      klasses = klass.interface? ? klass.implementors : ([klass] + klass.children)
      klasses.reject!  { |k| k.abstract? }
      
      query = nil
      klasses.each_with_index do |k, i|
        dataset = k.dataset.select(:id, *new_hash.keys).where(new_hash).select_append(Sequel.as(k.to_s, '_class_for_row_'))
        dataset = dataset.exclude(:id => object.id) if object.class == k
        if i == 0
          query = dataset
        else
          query = query.union(dataset, :from_self => false)
        end
      end
      
      if query.count > 0
        # Return instances that conflict
        # TODO: replace usage of additional DB query with Model.call -SD
        query.row_proc = Proc.new{ |values| values[:_class_for_row_].to_const[values[:id]] }
        query.all
      else
        true
      end
    end
  end
  
  class IsUnique < UniquenessConstraint
    # Check that the given object will be unique after the given column is updated with the given value
    # def check(object, column, new_value)
    def check(object, getter_values)
      result = SpecificAssociations.uniqueness_constraints_enabled? ? will_be_unique(object, getter_values) : true
      if result.is_a?(TrueClass)
        true
      else
        UniquenessViolation.new(result, object, getter_values, self)
      end
    end
  end
  
  class ShouldBeUnique < UniquenessConstraint
    # Check that the given object will be unique after the given column is updated with the given value
    def check(object, getter_values)
      result = SpecificAssociations.uniqueness_warnings_enabled? ? will_be_unique(object, getter_values) : true
      if result.is_a?(TrueClass)
        true
      else
        UniquenessViolation.new(result, object, getter_values, self)
      end
    end
  end
  
  class UniquenessViolation
    attr_accessor :conflicts
    attr_accessor :object
    attr_accessor :columns
    attr_accessor :getter_values
    attr_accessor :is_error
    attr_accessor :object_label
    attr_accessor :constraint
    
    
    def initialize(conflicts, object, getter_values, constraint)
      @conflicts     = conflicts
      @object        = object
      @getter_values = getter_values
      @is_error      = constraint.is_a?(IsUnique)
      @constraint    = constraint
      @object_label  = object.class.name.demodulize.to_title
    end
    
    def is_error?
      @is_error
    end
    
    def columns;               @constraint.columns; end
    def show_value_in_message; @constraint.show_value_in_message; end
    def label;                 @constraint.label; end
    def identifiers;           @constraint.identifiers; end
    
    def message
      message = ''
#      message << (is_error? ? 'Error: ' : 'Warning: ')
      if show_value_in_message
        message << "Saving would result in this #{object_label} having the following attribution: "
        attrs = []
        constraint.getters.each do |getter|
          if getter_values[getter]
            attrs << "#{getter} => '#{getter_values[getter]}'"
          else
            attrs << "#{getter} => '#{object._get_property(getter)}'"
          end
        end
        message << attrs.english_join
        message << ", as the #{label} of this #{object_label}. This "
      else
        message << "Attempted to assign a non-unique value as the #{label} of this #{object_label}. This "
      end
      message << (is_error? ? 'conflicts with ' : 'is the same as ')
      # message << (@conflicts.count > 1 ? " #{object_label.pluralize}: " : " #{object_label}: ")
      conflict_strings = []
      @conflicts.each do |conflict_obj|
        id_strings = []
        id_strings << "#{conflict_obj.class}:"
        constraint.getters.each do |getter|
          conflicting_value = conflict_obj._get_property(getter)
          # Add conflicting value for this conflict to list unless the identifier is nil, doesn't respond to to_s, or is empty.
          id_strings << "#{getter} => '#{conflicting_value}'" unless conflicting_value.nil? || !conflicting_value.respond_to?(:to_s) || conflicting_value.to_s.empty?
        end
        # columns.each do |col|
        #   conflicting_value = conflict_obj[col]
        #   # Add conflicting value for this conflict to list unless the identifier is nil, doesn't respond to to_s, or is empty.
        #   id_strings << conflicting_value.to_s unless conflicting_value.nil? || !conflicting_value.respond_to?(:to_s) || conflicting_value.to_s.empty?
        # end
        conflict_strings << (id_strings.empty? ? "ID => #{conflict_obj.id}" : id_strings.join(' '))
      end
      message << conflict_strings.english_join
      message
    end
  end
end
        
