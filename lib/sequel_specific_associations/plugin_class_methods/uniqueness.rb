module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        # Adds a "IsUnique" uniqueness constraint to one or more columns
        def is_unique(*getters)
          constraint = SpecificAssociations::IsUnique.new(self, *getters)
          constraint_name_separator = constraint.columns.length > 1 ? "are" : "is"
          constraint_name = "#{constraint.columns.collect { |c| c.to_s }.join('_')}_#{constraint_name_separator}_unique"
          @is_unique_constraints[constraint_name.to_sym] = constraint
        end

        # Adds a "ShouldBeUnique" uniqueness constraint to one or more columns
        def should_be_unique(*getters)
          constraint = SpecificAssociations::ShouldBeUnique.new(self, *getters)
          constraint_name = "#{constraint.columns.collect { |c| c.to_s }.join('_')}_should_be_unique"
          @should_be_unique_constraints[constraint_name.to_sym] = constraint
        end
  
        # In the case of multiple inheritance these methods leave open the possibility of ambiguity if two parents at the same level both define a constraint with the same key.
        def should_be_unique_constraints
          ret = {}
          immediate_parents.each { |p| ret.merge! p.should_be_unique_constraints }
          ret.merge! @should_be_unique_constraints
        end
        def is_unique_constraints
          ret = {}
          immediate_parents.each { |p| ret.merge! p.is_unique_constraints }
          ret.merge! @is_unique_constraints
        end
        def uniqueness_constraints
          ret = {}
          immediate_parents.each { |p| ret.merge!(p.should_be_unique_constraints).merge!(p.is_unique_constraints)}
          ret.merge!(@should_be_unique_constraints).merge!(@is_unique_constraints)
        end
      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel

