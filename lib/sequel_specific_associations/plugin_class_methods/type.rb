module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        # Return instantiable types that are allowed for the given association getter
        def type_of(getter)
          # getter could be chained
          getters = getter.to_s.split(".")
          if getters.count == 1
            info = info_for getter
            to_klass = ::SpecificAssociations.class_for(info)
            if to_klass.ancestors.include?(Sequel::Model) && to_klass.plugins.include?(Sequel::Plugins::SpecificAssociations)
              if to_klass.interface?
                types = to_klass.implementors
              elsif to_klass.complex_attribute?
                # NOTE: attribute_users can possibly contain subclasses of to_klass, so uniq must be called on result
                types = (to_klass.attribute_users.collect { |au| au.interface? ? au.implementors : au.children }.flatten + to_klass.attribute_users).uniq
              else
                types = to_klass.children << to_klass
              end
              # Handle :types constraints. Currently these are only being enforced for non-primitive attributes and associations
              # Types that this class is allowed to associate with
              allowed_types = ::SpecificAssociations.type_classes_for(info)
              # If types are specified in info, then only use the intersection of types and allowed_types
              types = allowed_types ? types & allowed_types : types
              # Also ensure that these types are allowed to associate with this class
              types.reject! { |type|
                opp_info = type.properties[info[:opp_getter]]
                opp_info && opp_info[:types] && !opp_info[:types].include?(self.to_s)
              }
              types
            else # Handle base primitive types (i.e. String, Integer)
              [to_klass]
            end
          elsif getters.count > 1
            next_types = type_of(getters.shift)
            next_types.collect { |t| t.type_of(getters.join("."))}.flatten.uniq
          else
            raise "problem with #{getter} in #type_of"
          end
        end
      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel

