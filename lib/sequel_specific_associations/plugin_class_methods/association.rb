module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        attr_writer :associations
  
        # TODO: conversion from assoc_info into options is no longer required. Consider deleting this method
        def create_association_from_info(assoc_info)
          associate(assoc_info)
        end
  
        # FIXME: resolve this stuff! We don't wan't to have to be doing this.
        # Attempt to fix association naming mismatches
        # This can be removed if we are willing to sacrifice backwards compatibility with code 
        #   where :opp_getter has been accidentally ommitted (but was not previously needed).
        def fix_association_name_mismatches!(options)
          to_klass = options[:class].to_const rescue nil
          return options unless to_klass && to_klass.ancestors.include?(Sequel::Model) && !to_klass.complex_attribute?
          # Hard coded ignore of BinaryData and RichTextImage because they are properties of complex attributes and we can't tell that.
          
          return if options[:for_complex_attribute]

          inverse_info_via_opp_getter = to_klass.associations[options[:opp_getter]]
          # Reject if associates doesn't match options
          inverse_info_via_opp_getter = nil if inverse_info_via_opp_getter && inverse_info_via_opp_getter[:associates] != options[:associates]
          inverse_infos_via_getter = to_klass.associations.values.select { |info| options[:associates] == info[:associates] && info[:opp_getter] == options[:getter] && info[:class] == to_s }
          # Get inverse info via getter or opp_getter
          inverse_info = inverse_info_via_opp_getter
          # Only get inverse info via getter if there is only one match
          inverse_info ||= inverse_infos_via_getter.first if inverse_infos_via_getter && inverse_infos_via_getter.count == 1

          if inverse_info
            unless inverse_info[:getter] == options[:opp_getter]
              # Mismatch detected
              default_opp_getter = name.demodulize.downcase
              default_opp_getter = ::SpecificAssociations.from_many?(options) ? default_opp_getter.pluralize : default_opp_getter
              correct_name = (default_opp_getter == inverse_info[:getter].to_s) ? options[:opp_getter] : inverse_info[:getter]
              # $stderr.puts "Association naming mismatch for #{to_s} and #{options[:class]}: #{inverse_info[:getter]} != #{options[:opp_getter]}. Choosing: #{correct_name}"
              inverse_info[:getter] = options[:opp_getter] = correct_name
            end
            unless inverse_info[:opp_getter] == options[:getter]
              # Mismatch detected
              default_getter = options[:class].demodulize.downcase
              default_getter = default_getter.pluralize if ::SpecificAssociations.to_many?(options)
              correct_name = (default_getter == inverse_info[:opp_getter].to_s) ? options[:getter] : inverse_info[:opp_getter]
              # $stderr.puts "Association naming mismatch for #{to_s} and #{options[:class]}: #{inverse_info[:opp_getter]} != #{options[:getter]}. Choosing: #{correct_name}"
              inverse_info[:opp_getter] = options[:getter] = correct_name
            end
          end
          options
        end
  
        def transform_legacy_options!(options)
          other_prefix = options[:other_prefix]
          if other_prefix
            options.delete(:other_prefix)
            options[:getter] = other_prefix
          end
          prefix = options[:prefix]
          if prefix
            options.delete(:prefix)
            options[:opp_getter] = prefix
          end
    
          # Alias other_getter to opp_getter
          # Not really a legacy option. Should move this.
          other_getter = options[:other_getter]
          if other_getter
            options.delete(:other_getter)
            options[:opp_getter] = other_getter
          end
    
          # Alias gotten_by to opp_getter
          # Not really a legacy option. Should move this.
          other_getter = options[:gotten_by]
          if other_getter
            options.delete(:other_getter)
            options[:opp_getter] = other_getter
          end
    
          # Change order and is_ordered to ordered and opp_is_ordered
          order = options[:order]
          if order
            options.delete(:order)
            options[:ordered] = order
          end
          is_ordered = options[:is_ordered]
          if is_ordered
            options.delete(:is_ordered)
            options[:opp_is_ordered] = is_ordered
          end
    
          # Alias other_ordered to opp_is_ordered
          # Not really a legacy option. Should move this.
          other_ordered = options[:other_ordered]
          if other_ordered
            options.delete(:other_ordered)
            options[:opp_is_ordered] = other_ordered
          end
    
          # Change one_to_one legacy association to be a true one_to_one type and set holds key to true if it was a many_to_one
          one_to_one = options[:one_to_one]
          if one_to_one
            options[:holds_key] = true if options[:type] == :many_to_one
            options[:type] = :one_to_one if options[:type] == :many_to_one || options[:type] == :one_to_many
            options.delete(:one_to_one)
          end
        end
  
        # Defines the classes that an association class relates to
        #
        # Usage:
        # - using default role (association class name)
        #   - associates :ClassOne; associates :ClassTwo
        # - using explicit role
        #   - associates :ClassOne => :class_one_role; associates :ClassTwo => :class_two_role
        #
        # Provides the following methods for the association class:
        #
        # *  <class_name>_<association_class_name or role> - Returns the object for given side of the
        #    association.
        #
        # *  <class_name>_<association_class_name or role>= - Sets the object for the given side of the
        #    association.
        def associates(class_arg, options = {})
          role = nil
          case
          when class_arg.is_a?(Hash)
            klass, role = class_arg.shift
            # Allow user to specify options as part of one hash, or separate hashes
            options = class_arg.merge(options)
          when class_arg.is_a?(Symbol)
            klass = class_arg
          else
            raise(Sequel::Error, 'Invalid association argument. Should be Hash or Symbol')
          end
          raise "An invalid class was specified for an association defined on: #{self}" unless klass && !klass.empty?
    
          # Convert klass to a string
          klass = klass.to_s
    
          # Add namespacing to klass
          unless klass.index('::') || ['Object', 'Kernel'].include?(namespace.to_s)
            klass = "#{namespace}::#{klass}"
          end
          
          singular_role = role.to_sym if role && !role.empty?
          
          parameters = options.clone
          parameters[:getter] = singular_role if singular_role
          parameters[:associates] = true
          many_to_one klass, parameters
          @associates = true
        end
  
        def one_to_one(to_klass, options = {}, &block)
          associate(options.merge(:class => to_klass.to_s, :type => :one_to_one), &block)
        end
  
        def many_to_one(to_klass, options = {}, &block)
          associate(options.merge(:class => to_klass.to_s, :type => :many_to_one), &block)
        end
  
        alias :belongs_to :many_to_one

        def one_to_many(to_klass, options = {}, &block)
          associate(options.merge(:class => to_klass.to_s, :type => :one_to_many), &block)
        end
        alias :has_many :one_to_many
  
        # Defines a many_to_many relationship
        #
        # a relationship may fall into one of three categories
        #  - both classes are polymorphic
        #    syntax:
        #     - many_to_many :<associated_to_role_name>, :through => :<join_class_name>
        #     example:
        #     class Person
        #     - many_to_many :throwable, :through => :ThrowableItem
        #     class Frisbee
        #     - many_to_many :thrower, :through => :ThrowableItem
        #
        #  - this class is non-polymorphic and the other is polymorphic
        #    syntax:
        #     - many_to_many :<associated_to_role_name>, :through => :<join_class_name>
        #     - many_to_many :<associated_class_name>, :through => :<join_class_name>
        #     example:
        #     class Person (non-polymorphic)
        #     - many_to_many :throwable, :through => :ThrowableItem
        #     class Frisbee (polymorphic)
        #     - many_to_many :Person, :through => :ThrowableItem
        #
        #  - this class is polymorphic and the other is non-polymorphic
        #    syntax is the same as 2nd category
        def many_to_many(to_klass, options = {}, &block)
          associate(options.merge(:class => to_klass.to_s, :type => :many_to_many), &block)
        end
  
        def parse_associate_options!(options)
          # Legacy options that need to be transformed: [:prefix, :other_prefix]
          transform_legacy_options!(options)
          raise ArgumentError, "You must specify the type of association" unless options[:type]
          raise ArgumentError, "You must specify the class to associate to" unless options[:class]
          # Valid options: [:class, :type, :getter, :opp_getter, :associates, :holds_key, ]
    
          # Qualify the to_klass with this class's namespace if needed
          # TODO: Find a better method of qualification -SD
          options[:class] = "#{namespace}::#{options[:class]}" unless options[:class].index('::') || namespace == Object
    
          # If :acyclic option is set, ensure that this is a self-association
          # TODO: make this allow for subclass -> superclass associations
          # TODO: make this option work for multiple associations:
          #       example, given class A which associates to class B which associates to A
          #         allowable: a1->b1->a2
          #         disallowed: a1->b1->a1 (cycle)
          raise "Currently can't specify non-self-associations as acyclic: #{options.inspect}" if options[:acyclic] && options[:class] != to_s
    
          # Set options[:getter] (and possibly options[:singular_getter])
          # Default getter to to_klass_name if unspecified
          to_many = ::SpecificAssociations.to_many?(options)
          unless options[:getter]
            default_getter = downsnake(options[:class].demodulize)
            options[:getter] = to_many ? default_getter.pluralize : default_getter
          end
          options[:getter] = options[:getter].to_sym
          if to_many && !options[:singular_getter]
            options[:singular_getter] = options[:getter].to_s.singularize.to_sym
          end
    
          # Set options[:opp_getter] (and possibly options[:singular_opp_getter])
          # Default getter to klass_name if unspecified
          from_many = ::SpecificAssociations.from_many?(options)
          unless options[:opp_getter]
            default_opp_getter = downsnake(name.demodulize)
            options[:opp_getter] = from_many ? default_opp_getter.pluralize : default_opp_getter
          end
          options[:opp_getter] = options[:opp_getter].to_sym
          if from_many && !options[:singular_opp_getter]
            options[:singular_opp_getter] = options[:opp_getter].to_s.singularize.to_sym
          end
    
          fix_association_name_mismatches!(options)

          # Check for bad logic in ordering options
          options.delete(:ordered) if options[:ordered] && !to_many
          options.delete(:opp_is_ordered) if options[:opp_is_ordered] && !from_many
    
          parse_additive_options!(options)
        end
  
        # Parse options that may be modified later (such as multiplicity or type restrictions)
        # In addition to association definition, this is also run when options are added
        # via #add_association_options
        def parse_additive_options!(options)
          # For now, qualify :types in the same manner as :class
          if options[:types]
            options[:types] = options[:types].collect do |type|
              (type.to_s.index('::') || namespace == Object) ? type.to_s : "#{namespace}::#{type}"
            end
          end
          options
        end
        
        # TODO: this probably belongs in an enumeration mixin
        def literal_values
          raise "Cannot call #literal_values on a non-enumeration" unless self.enumeration?
          classes = [self] + self.children
          classes.collect{|c| c.select_map(:value) }.flatten
        end
        
        def associate(options, &block)
          parse_associate_options!(options)
  
          options[:association] = true
          options[:as]          = name

          # For many_to_many associations, get the model name for the join table
          if options[:through] || options[:join_class_name]
            raise "No join table class specified for many-to-many association between #{name} and #{options[:class]}. Please specify one using the :through option" unless options[:through] || options[:join_class_name]
            through_class = (options[:through] || options[:join_class_name]).to_s
            unless through_class.index('::') || ['Object', 'Kernel'].include?(namespace.to_s)
              through_class = "#{namespace}::#{through_class}"
            end
            options[:through] = through_class
          end
          
          define_association_methods(options)
          
          # Save the association info to the @properties collection
          add_immediate_property(options[:getter].to_sym, options)
        end # End associate

        # TODO: move this to somewhere common to both associations and attributes
        def define_property_method(general_method, getter, association_methods, options = {})
          primary_method = association_methods.shift
          self.send(:define_method, primary_method) do |*args|
            # If any options weren't specified, use arg_defaults when available
            args = args + options[:arg_defaults].slice(args.length..-1) if options[:arg_defaults]
            send(general_method, getter, *args)
          end
          private primary_method if options[:private]
          
          association_methods.each do |meth|
            self.send(:alias_method, meth, primary_method)
          end
        end
        
        def define_association_methods(info)
          getter = info[:getter]
          singular_getter = info[:singular_getter] || info[:getter]

          # Add method - private if this is a to_one association
          define_property_method(:_add, getter, ["add_#{singular_getter}", "add_to_#{getter}", "#{getter}_add"], :private => (info[:type] == :one_to_one || info[:type] == :many_to_one))
          
          # Type method
          define_property_method(:_type_of, getter, ["#{getter}_type", "type_of_#{getter}"])

          # Remove method
          define_property_method(:_remove, getter, ["remove_#{singular_getter}", "remove_from_#{getter}", "#{getter}_remove"])

          # Remove all method
          define_property_method(:_remove_all, getter, ["remove_all_#{getter}", "#{getter}_remove_all"])
          
          # Getter methods
          define_property_method(:_get_property, getter, [getter])
          # NOTE: this method must be safe to use when a change_tracker browse time is set
          define_property_method(:_get_unassociated, getter, ["#{getter}_unassociated"])
          
          # Associations getter method
          if info[:through]
            define_property_method(:_get_property, getter, ["#{getter}_associations"], :arg_defaults => [filter = {}, limit = nil, offset = 0, return_associations_hash = true])
          end

          # Count methods
          define_property_method(:_count, getter, ["#{getter}_count"])
          define_property_method(:_unassociated_count, getter, ["#{getter}_unassociated_count"])
          
          # Query methods
          if (info[:type] == :one_to_many) || ((info[:type] == :one_to_one) && !info[:holds_key]) || (info[:type] == :many_to_many) # to_many
            define_property_method(:_get_many_query, getter, ["#{getter}_query", "get_many_query_#{getter}"])
          end
          define_property_method(:_unassoc_get_many_query, getter, ["#{getter}_unassociated_query", "unassoc_get_many_query_#{getter}"])
  
          # Records method (alias to get_many method when many_to_many)
          # Formerly aliased as get_many_<property>
          # NOTE the _records methods are not preferred since they do not clearly describe what this method does
          # using 'through' instead of 'throughs' because these may be to-one associations.  Better to have uniformity than to check?  Could be changed later I suppose -- MF
          if info[:through]
            define_property_method(:_get_many, getter, ["#{getter}_through", "through_for_#{getter}", "#{getter}_records", "records_for_#{getter}"])
          end

          # Setter method
          define_property_method(:_set_property, getter, ["#{getter}="])
          
          if info[:ordered] # Implies to_many
            # Move Item method
            # Moves an item in an ordered association to the specified position
            define_property_method(:_move, getter, ["move_#{singular_getter}"])
    
            # Get Item At Position method
            define_property_method(:_get_at_position, getter, ["get_#{singular_getter}_at_position"])
          end

          # Is Polymorphic method
          # TODO: deprecate this. We probably don't need acccessor methods for every option setting
          define_property_method(:_is_polymorphic?, getter, ["#{singular_getter}_is_polymorphic?"])
  
          # Info method
          # Returns information about this association
          define_property_method(:_get_info, getter, ["#{getter}_info"])
  
          # Opposite Info method
          # Returns information about the opposite (reverse direction) of this association
          define_property_method(:_get_opp_info, getter, ["#{getter}_opp_info"])
  
          if info[:enumeration]
            define_property_method(:_enum_literals, getter, ["#{getter}_literals"])
          end
        end
         
        def add_association_getter(name, options = {}, &block)
          add_association_options(name, options.merge(:custom_getter => block))
        end
  
        def add_association_getter_for_each(name, options = {}, &block)
          add_association_options(name, options.merge(:custom_getter_for_each => block))
        end
  
        def add_association_setter(name, options = {}, &block)
          add_association_options(name, options.merge(:custom_setter => block))
        end
  
        def add_association_setter_for_each(name, options = {}, &block)
          add_association_options(name, options.merge(:custom_setter_for_each => block))
        end
  
        def add_association_adder(name, options = {}, &block)
          add_association_options(name, options.merge(:custom_adder => block))
        end
  
        def add_association_remover(name, options = {}, &block)
          add_association_options(name, options.merge(:custom_remover => block))
        end
  
        # Define additional options for a given existing association
        def add_association_options(name, options = {})
          add_property_options(name, options)
        end
        
        # Define a 'derived' asssociation.
        # Derived associations do not create any methods, but instead reference pre-existing methods on the object.
        def derived_association(name, classifier = nil, options = {})
          derived_options = {:getter => name.to_sym, :singular_getter => name.to_s.singularize.to_sym,
            :derived => true, :class => classifier.to_s, :association => true}
          options = derived_options.merge(options)
          add_immediate_property(name.to_sym, options)
        end
        
        # TODO: move this to a more appropriate location -SD
        # Define additional options for a given existing property
        def add_property_options(name, options = {})
          parse_additive_options!(options)
          info = properties[name.to_sym]
          raise "Could not find property named #{name} when defining additional property options" unless info
          # Check for setting hook options on alias association, and raise error if detected
          # NOTE: :custom_getter appears to be absent intentionally, since there was additional handling code in _get_attribute.
          #       This was probably a poor decision, since it leads to surprising behavior for that one particular hook. -SD
          hook_options = [:custom_setter_for_each, :custom_getter_for_each, :custom_adder, :custom_setter, :custom_remover, :before_add, :after_add, :before_remove, :after_remove]
          raise "Cannot define hooks #{(options.keys & hook_options).inspect} on an alias association: #{name}" if info[:alias_of] && (options.keys & hook_options).any?
          # NOTE: always modify local @properties (creating if necessary). This means that if an option is added to a subclass, it only affects the subclass.
          #       However, once an association is modified by a subclass, changes to the superclass's options will not affect the subclass.
          #       This could be fixed by merging association hashes according to inheritance, if needed.
          add_immediate_property(name.to_sym, info.merge(options))
        end
      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel

