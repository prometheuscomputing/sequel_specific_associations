module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        attr_writer :attributes
        # Add an attribute hash to @properties
        def attribute(name, type, options = {})
          name = name.to_sym
          attr_class = type.is_a?(Class) ? type : type.to_s.to_const
          type = type.to_s
          is_complex = attr_class.respond_to?(:complex_attribute?) && attr_class.complex_attribute?
          attr_class.attribute_users << self if is_complex
          defaults = {:attribute => true, :getter => name, :class => type, :type => :attribute, :store_value => true, :column => name, :store_on_get => false}
          info = defaults.merge(options)
          if is_complex
            info[:complex] = true
            info[:type]    = :complex_attribute
            # If complex, there is typically no column with name matching attribute <-- this does not seem to be true at all.  In fact, I'm pretty sure it is totally false or that I don't understand at all (MF).
            info[:store_value] = false
          end
    
          # Check for bad logic in attribute definition
          raise "Error: Attribute #{name} of class #{to_s} has no value storage, no defined getter, and is not complex." if info[:custom_getter].nil? && !info[:store_value] && !info[:complex]
    
          # the #id method is currently reserved to retrieve the primary key.  In future we should use #pk instead of #id to retrieve the primary key
          if name == :id
            message = "You can not specify an attribute named 'id'.  You did this with #{name}."
            raise ::SpecificAssociations::PrimaryKeyConflict, message
          end
    
          # id column is reserved for primary key.
          if info[:column] == :id
            message = "You can not specify an attribute with column :id.  You did this with #{name}'s attribute named '#{name}'"
            raise ::SpecificAssociations::PrimaryKeyConflict, message
          end
    
          # Ignore attributes for which there is already a defined column
          # TODO: Revise this check when generators no longer incorrectly specify foreign_keys as attributes.
          # Note: this code is using unsophisticated string concatenation to get association columns since we can't assume that the necessary tables or
          #       associated Models exist (and thus can't use get_association_columns).
          problem = associations.find { |key, assoc_info| g = assoc_info[:getter]; ["#{g}_id", "#{g}_class", "#{g}_position"].include?(name.to_s) }
          if problem
            # I am not sure this is foolproof.  I think this check depends on the order of processing of properties.
            pg = problem.last[:getter]
            message = "In class #{name}, the attribute named '#{name}' conflicts with association named '#{pg}'\n  #{associations[problem.first]}"
            raise ::SpecificAssociations::SchemaConflict, message
          end
          
          define_attribute_methods(info)
          
          # Cancel attribute processing if an attribute (with the exact same information) already exists in this class or a parent
          # This prevents complications in case a model is loaded multiple times accidentally
          return false if properties[name] == info
          info[:as] = name
          # If a complex attribute is being made, check for associations and create copies of those associations on this class.
          # TODO: rethink this. By 'copying' the association, any updates made to the original association on the complex attribute class will not be propagated to the copies.
          if info[:complex]
            attr_class.associations(false).each_value do |assoc_data|
              # Replace getter name with prefixed version
              assoc_data      = assoc_data.dup
              original_getter = assoc_data[:getter]
              # Derive the association getter from the name of the complex attribute and the name of the association
              assoc_data[:getter] = "#{info[:column]}_#{original_getter}"
              to_many = ::SpecificAssociations.to_many?(assoc_data)
              # Derive the singular association getter from the name of the complex attribute and the name of the association
              singular_getter = to_many ? (assoc_data[:singular_getter] || (original_getter.to_s.singularize)) : original_getter
              assoc_data[:singular_getter] = "#{info[:column]}_#{singular_getter}"
              assoc_data[:for_complex_attribute] = info[:getter]
              create_association_from_info(assoc_data)
            end
          end
    
          # Save the attribute info to @properties
          add_immediate_property(name, info)
        end
  
        def define_attribute_methods(info)
          getter = info[:getter]
          define_property_method(:_get_property, getter, [getter])
          define_property_method(:_set_property, getter, ["#{getter}="])
          define_property_method(:_type_of, getter, ["#{getter}_type"])
        end
  
        # Define a derived attribute with some typical options
        def derived_attribute(name, type, options = {})
          # Old behavior (versions prior to v6) kept here for reference. This would create methods and a column by default for derived attributes.
          # attribute(name, type, options.merge(:derived => true, :store_on_get => true))
    
          # New behavior for derived attributes: Do not automatically create methods or column for attribute.
          defaults = {:getter => name, :class => type.to_s, :store_value => false, :column => name, :store_on_get => false, :derived => true, :attribute => true}
          add_immediate_property(name, defaults.merge(options))
        end
  
        # Define a getter method for a given existing attribute
        def add_attribute_getter(name, options = {}, &block)
          add_attribute_options(name, options.merge(:custom_getter => block))
        end
  
        # Define a setter method for a given existing attribute
        def add_attribute_setter(name, options = {}, &block)
          add_attribute_options(name, options.merge(:custom_setter => block))
        end
  
        # Define additional options for a given existing attribute
        def add_attribute_options(name, options = {})
          add_property_options(name, options)
        end
  
      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel

