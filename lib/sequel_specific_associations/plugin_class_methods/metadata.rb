module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        def many_to_manys(include_derived = true)
          properties(include_derived).select { |getter, info| info[:type] == :many_to_many }.values
        end
        def many_to_ones(include_derived = true)
          properties(include_derived).select { |getter, info| info[:type] == :many_to_one }.values
        end
        def one_to_manys(include_derived = true)
          properties(include_derived).select { |getter, info| info[:type] == :one_to_many }.values
        end
        def one_to_ones(include_derived = true)
          properties(include_derived).select { |getter, info| info[:type] == :one_to_one }.values
        end
        def associating(include_derived = true)
          properties(include_derived).select { |getter, info| ::SpecificAssociations.to_one?(info) && info[:associates]}.values
        end
        def composers(include_derived = true)
          properties(include_derived).select { |getter, info| opp_info = ::SpecificAssociations.get_opposite_association(info) || {}; opp_info[:composition]}.values
        end
        def composer_getters(include_derived = true)
          composers(include_derived).collect { |c| c[:getter]}
        end
        def aggregators(include_derived = true)
          properties(include_derived).select { |getter, info| opp_info = ::SpecificAssociations.get_opposite_association(info) || {}; opp_info[:composition] || opp_info[:shared]}.values
        end
        def aggregator_getters(include_derived = true)
          aggregators(include_derived).collect { |c| c[:getter]}
        end

        # ----------- Inherited properties -----------
        # def uniqueness_constraints; get_property 'uniqueness_constraints'; end
        # When inheriting through ruby, make sure to change the dataset from the parent table's dataset to an implied table name.
        def inherited(subclass)
          subclass.set_dataset(subclass.implicit_table_name)
          super
        end

        # NOTE: this can be run on already initialized classes, so make sure not to overwrite pre-initialized vars
        def on_inherit(subclass)
          super
          # Initialize variables
          subclass.setup
          # Define appropriate methods on subclass for properties inherited from this class
          self.properties.each do |prop, info|
            next if respond_to?(info[:getter]) # Skip definitions if getter is already defined
            info[:attribute] ? subclass.define_attribute_methods(info) : subclass.define_association_methods(info)
          end
          
          # Propagate singleton property to subclasses
          subclass.singleton! if singleton?
        end

        # When implementing a new interface, check for necessary method additions
        def on_interface_addition(interface)
          super
          # Define appropriate methods on this implementor for properties inherited from specified interface
          interface.properties.each do |prop, info|
            info[:attribute] ? define_attribute_methods(info) : define_association_methods(info)
          end
        end

        def setup
          @is_unique_constraints ||= {}
          @should_be_unique_constraints ||= {}
          @constructors ||= {:new => {:method => nil, :name => self.to_s, :classifier => self.to_const}}
        end 

        # FIXME several of these methods need to be moved to model_metadata
        def primitive_attributes
          attributes.select { |_,v| v[:type] == :attribute }
        end
  
        def complex_attributes
          attributes.select { |_,v| v[:type] == :complex_attribute }
        end
  
        def info_for getter
          getter = getter.to_sym if getter.is_a?(String)
          properties[getter]
        end

        def opp_info_for getter_or_info
          info = getter_or_info.is_a?(Hash) ? getter_or_info : info_for(getter_or_info)
          ::SpecificAssociations.get_opposite_association(info)
        end
  
        def additional_data_for getter
          info_for(getter)[:additional_data]
        end
      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel

