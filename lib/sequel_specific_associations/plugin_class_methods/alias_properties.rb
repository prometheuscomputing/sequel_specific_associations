# FIXME: this file is called "alias_properties.rb", but the non-alias equivalent is "association.rb", not "properties.rb". This is very confusing. -SD
module Sequel
  module Plugins
    module SpecificAssociations
      module ClassMethods
        # TODO == send better info to method bodies
        
        # Define an 'alias' association.
        # Alias associations are derived associations that are backed directly by an existing association.
        # They may additionally apply a filter to the association being aliased.
        def alias_association(alias_getter, type_filter_klass = nil, options = {})
          alias_getter = alias_getter.to_sym
          options.delete(:type) # it is (currently) NOT OK to have a type that is different than that of the aliased association
          # Prevent setting of :ordered on alias associations (can't order a subset)
          raise "Cannot define ordered alias association: #{alias_getter}" if options[:ordered]
          
          # Disallow setting hooks on alias associations. This could eventually be supported, but prevent use for now.
          # TODO: should not have to duplicate this code in add_association_options method
          hook_options = [:custom_setter_for_each, :custom_getter_for_each, :custom_adder, :custom_setter, :custom_remover, :before_add, :after_add, :before_remove, :after_remove]
          raise "Cannot define hooks #{(options.keys & hook_options).inspect} on an alias association: #{alias_getter}" if (options.keys & hook_options).any?
          
          # Again we have the uncomfortable possibility for differences between the property key and the getter in the property info
          # The property key and the :getter value in the hash are defined to be synonymous. There will be no difference -SD
          proto_info = properties[options[:alias_of].to_sym]
          # Add type_filter_klass to the specified alias filter
          options[:filter] = ::SpecificAssociations.add_to_filter(options[:filter] || {}, {'type' => type_filter_klass.to_s}) if type_filter_klass
          
          classifier = type_filter_klass.to_s || proto_info[:class]
          
          alias_options = {
            :getter          => alias_getter,
            :singular_getter => alias_getter.to_s.singularize.to_sym,
            :derived         => true,
            :class           => classifier,
            :alias           => true,
            :association     => true,
            :as              => proto_info[:as],
            :type            => proto_info[:type]
          }
          # alias_options are non-negotiable! Do not overwrite them.
          options = options.merge(alias_options)
          options[:through] ||= proto_info[:through] unless options[:hide_through_class]
          
          define_association_methods(options)
          
          # Save the association info to the @properties collection (via derived_association)
          add_immediate_property(options[:getter], options)
        end
        
        def alias_attribute(alias_getter, getter)
          getter       = getter.to_sym
          alias_getter = alias_getter.to_sym
          info = attributes[getter]
          raise "#{self} has no #{getter} attribute that can be aliased" unless info
          # TODO should this check properties or immediate_properties?  Is it ok to override an alias defined on a generalization (i.e. parent class or interface)?
          # Allowed to overwrite the alias if you are re-aliasing the same property
          existing_alias_of = properties.dig(alias_getter, :alias_of)
          raise "#{alias_getter} is not available for #{self} because it is already in use." if existing_alias_of && existing_alias_of != getter
          alias_info = {
            :getter   => alias_getter,
            :alias_of => info[:column]# , # the potential for a difference b/w info[:column] and info[:getter] makes me uneasy but we gotta do what we gotta do
          }
          define_attribute_methods(alias_info)
          alias_complex_attribute_columns(alias_getter, getter, info) if info[:complex]
          add_immediate_property(alias_getter, info.merge(alias_info))
        end
        
        # TODO it would be nice if there were some way to already know what klass is without having to pass it in.  There is currently no metainfo for columns that do not directly map to getters so it is a bit tough.
        def alias_column(alias_col, col, klass)
          col       = col.to_sym
          alias_col = alias_col.to_sym
          info = properties[col]
          raise "#{self} has a '#{col}' property that can be aliased.  Please use #alias_attribute instead of #alias_column." if info
          alias_info = {:getter => alias_col, :alias_of => col, :type => :column_alias, :class => "#{klass}", :attribute => true, :store_value => true, :store_on_get => false, :column => col}
          define_attribute_methods(alias_info)
          raise "#{self} has no column '#{col}' that can be aliased." unless columns.include?(col)
          add_immediate_property(alias_col, alias_info)
        end
        
        def alias_complex_attribute_columns(alias_getter, getter, getter_info)
          ::SpecificAssociations.class_for(getter_info).get_attribute_columns(false).each do |col|
            complex_attr_col = "#{getter}_#{col[:column]}".to_sym
            if columns.include?(complex_attr_col)
              alias_col = "#{alias_getter}_#{col[:column]}".to_sym
              add_immediate_property(alias_col, {:getter => alias_col, :alias_of => complex_attr_col, :type => :column_alias, :class => col[:type]})
            else
              raise "Expected to find column '#{complex_attr_col}' for #{self} while creating alias.  Column was not found."
            end
          end
        end        
      end
    end
  end
end