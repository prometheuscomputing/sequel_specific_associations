module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        attr_accessor :schema_definition

        def using_change_tracker_for_associations?
          respond_to?(:change_tracked?) && change_tracked? && !::ChangeTracker.disabled
        end

        def get_schema_columns(include_primary_keys = true)
          assoc_cols = get_association_columns
          attribute_cols = get_attribute_columns(include_primary_keys)

          columns = assoc_cols + attribute_cols
          # Add ID column unless a primary key was specified
          if !columns.any?{ |c| c[:primary_key]} && include_primary_keys
            columns << {:primary_key => true, :column => :id, :type => SpecificAssociations::INT_TYPE}
          end
          if  defined?(Sequel::Plugins::Timestamps) && self.plugins.include?(Sequel::Plugins::Timestamps)
            columns << {:column => :updated_at, :type => Time}
            columns << {:column => :created_at, :type => Time}
          end

          columns
        end

        def get_association_columns
          columns = []
          key_holding_associations = many_to_ones + one_to_ones.select { |a| a[:holds_key] }
          key_holding_associations.each do |a|
            getter = a[:getter].to_s
            # Add id column
            id_column = "#{getter}_id"
            a[:holds_key] = [id_column.to_sym] if !a[:through]
            columns << {:column => id_column, :type => SpecificAssociations::INT_TYPE} if !a[:through]

            to_class = ::SpecificAssociations.class_for(a)
            # FIXME: This seems really out of place. I think this was done here since you can't assume that all classes
            #        have been processed earlier, and thus can't know if the to_class is a complex_attribute. 
            #        However, we shouldn't just toss things like this into the schema creation code. -SD
            a[:to_complex_attribute] = to_class.complex_attribute?
            if (to_class.interface? || to_class.has_children? || a[:to_complex_attribute]) && !a[:through]
              # Add class column if needed
              class_column = "#{getter}_class"
              columns << {:column => class_column, :type => String}
              a[:holds_key] << class_column.to_sym
            end
            # Add complex attribute getter column if needed
            getter_column = "#{getter}_complex_getter"
            columns << {:column => getter_column, :type => String} if a[:to_complex_attribute]
            # Add order column if needed
            position_column = "#{getter}_position"
            columns << {:column => position_column, :type => SpecificAssociations::INT_TYPE} if a[:opp_is_ordered] && !a[:through]
          end
          columns
        end

        def get_attribute_columns(include_primary_keys = true)
          columns = []
          # Iterate over attributes, collection columns
          attributes.values.each do |attribute|
            # Skip unstored attributes that are not complex
            next unless attribute[:store_value] || attribute[:complex]
            attr_class = attribute[:class].is_a?(Class) ? attribute[:class] : ::SpecificAssociations.class_for(attribute)
            base_attr_class = ::SpecificAssociations.get_base_attr_class attr_class
            if !attribute[:complex]
              col = {:column => attribute[:column].to_s, :type => base_attr_class}
              if attribute[:primary_key]
                next unless include_primary_keys
                col[:primary_key] = true
              end
              columns << col
            else # Complex attribute type
              # Get the attribute columns for the complex attribute type, prepend the attribute's name to each column, and add to columns of this class
              # Not worrying about association columns here since those are covered when get_association_columns is run, since
              #   there is really a direct association between the complex attribute's associations and this class which was set up when
              #   the attribute was defined.
              columns += attr_class.get_attribute_columns(false).collect { |csc| csc[:column] = "#{attribute[:column]}_#{csc[:column]}"; csc }
              # Add existance column
              columns << {:column => attribute[:column], :type => TrueClass}
            end
          end
          columns
        end

        # Create a schema based off the Sequel Model's associations and attributes
        def _create_schema(tname, schema_columns = nil)
          return if abstract? || interface?
          columns = schema_columns || get_schema_columns
          column_names = columns.collect { |c| c[:column]}
          without_dups = (column_names.length == column_names.uniq.length)
          raise "Duplicate columns found in: #{column_names} specified in #{self} definition" unless without_dups
          primary_keys = columns.select { |c| c[:primary_key]}
          raise "No primary keys defined for #{tname}" unless primary_keys.count > 0
          single_primary_key = (primary_keys.count == 1)

          # puts "About to create table: #{tname}"
          db.create_table(tname) do
            columns.each do |col|
              if col[:primary_key] && single_primary_key
                if col[:type] == SpecificAssociations::INT_TYPE
                  # method(:primary_key).call(col[:column])
                  primary_key(col[:column])
                else
                  # method(:primary_key).call(col[:column], :type => col[:type])
                  primary_key(col[:column], :type => col[:type])
                end
              else
                method(col[:type].to_s).call(col[:column])
              end
            end
            # Deal with composite key situation
            if !single_primary_key
              # method(:primary_key).call(primary_keys.collect { |k| k[:column]})
              primary_key(primary_keys.collect { |k| k[:column]})
            end
          end
          # Reset the model's information about the dataset since the dataset has changed
          @db_schema = nil
          db_schema
        end
        
        def _create_schema_definition(tname, schema_columns = nil)
          if abstract? || interface?
            @schema_definition = ''
            return @schema_definition
          end
          columns      = schema_columns || get_schema_columns
          primary_keys = columns.select { |c| c[:primary_key] }
          single_primary_key = (primary_keys.count == 1)

          schema = ["#{self}.db.create_table :#{tname} do"] # this is for debugging only.. not parsed          
          columns.each do |col|
            if col[:primary_key] && single_primary_key
              if col[:type] == SpecificAssociations::INT_TYPE
                schema << "  primary_key :#{col[:column]}"
              else
                schema << "  primary_key :#{col[:column]}, :type => :#{col[:type]}"
              end
            else
              schema << "  #{col[:type]} :#{col[:column]}"
            end
          end
          # Deal with composite key situation
          unless single_primary_key
            schema << "  primary_key [:#{primary_keys.collect { |k| k[:column].to_s }.join(', :')}]"
          end
          schema << 'end'
          # Return the string representation of the created schema
          @schema_definition = schema.join("\n")
        end
        
        # Create a schema based off the Sequel Model's associations and attributes
        def _update_schema(tname, columns = nil)
          return if abstract? || interface?
          columns ||= get_schema_columns
          new_columns = []
          columns.each do |col|
            sym = col[:column].to_sym
            existing = @db_schema[sym]
            # TODO check to see if the type is the same.  If it isn't then raise.
            unless existing
              new_columns << col
            end
          end
          if new_columns.any?
            # puts Rainbow("#{self.name} should have the following columns that are not in the #{table_name} table:\n#{new_columns.pretty_inspect}").orange
            db.alter_table(tname) do
              new_columns.each do |col|
                raise SpecificAssociations::SchemaConflict, "You shouldn't be adding a primary key while altering the #{tname} table." if col[:primary_key]
                add_column(col[:column].to_sym, col[:type].to_s)
              end
            end
            # Reset the model's information about the dataset since the dataset has changed
            @db_schema = nil
            db_schema
          end
        end
        
        # Create a schema unless one already exists
        # FIXME this should call create_schema so that there is a single point on entry.  Either that or #create_schema needs to be deprecated and die.
        # Well, #create_schema can die now I suppose...or at least alias to this method
        def create_schema?
          # NOTE: I've changed this to always recalculate schema columns so that this runs at every SSA startup whether a database is present or not.
          #       This is because options-altering code has been injected into get_association_columns.
          #       Without this change, the options would be different on the first launch (when the DB is created) from subsequent launches. -SD
          columns = get_schema_columns
          if db.table_exists?(table_name)
            _update_schema(table_name, columns)
            @schema_definition = _create_schema_definition(table_name, columns)
          else
            _create_schema(table_name, columns)
            @schema_definition = _create_schema_definition(table_name, columns)
          end
          if using_change_tracker_for_associations? && !LAZY_DELETED_TABLE_CREATION
            if db.table_exists?(deleted_table_name)
              _update_schema(deleted_table_name)
            else
              _create_schema(deleted_table_name)
            end
          end
          @schema_definition
        end
        
        def create_schema
          create_schema?
          # schema_defn = _create_schema(table_name)
          # _create_schema(deleted_table_name) if using_change_tracker_for_associations? && !LAZY_DELETED_TABLE_CREATION
          # schema_defn
        end

        # Remove any existing schema and create a new one
        def create_schema!
          db.drop_table(table_name) if db.table_exists?(table_name)
          db.drop_table(deleted_table_name) if using_change_tracker_for_associations? && db.table_exists?(deleted_table_name)
          create_schema
        end

      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
