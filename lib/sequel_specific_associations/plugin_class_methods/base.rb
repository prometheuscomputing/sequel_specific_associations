module Sequel
  module Plugins
    module SpecificAssociations
      include ::SpecificAssociations
      module ClassMethods
        
        # include BusinessRules::RuleDsl
        include SpecificAssociations::SequelMetaInformation::ClassMethods
                
        # Stuck here in a moment of laziness.  Should go away when the naming service is done and is utilized in this code.
        def name_parts str
          str.split(/-|_|(?=[A-Z][a-z])|(?<=[a-z])(?=[A-Z0-9]+$)/)
        end
        # Stuck here in a moment of laziness.  Should go away when the naming service is done and is utilized in this code.
        def downsnake str
          name_parts(str).join("_").downcase
        end
      
      end # ClassMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
