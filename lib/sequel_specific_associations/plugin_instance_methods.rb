files = %w(associations change_tracking cloning composition constraints count filtering get hooks info pending_associations remove set uniqueness display)
files.each { |file| require_relative "plugin_instance_methods/#{file}" }
