module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        def _remove(getter, argument)
          return unless argument
          info     = _get_info(getter)
          if info[:alias_of]
            return _remove(info[:alias_of], argument)
          end
          
          raise '_remove is not implemented for complex_attributes' if self.class.complex_attribute?
          
          opp_info = _get_opp_info(getter)
          
          singular_getter     = (_singular_getter(info))
          singular_opp_getter = (_singular_opp_getter(info))

          # For many_to_many associations, get the model name for the join table
          # through_class = info[:through].to_const if info[:type] == :many_to_many
          through_class = info[:through].to_const if info[:through]
          
          if info[:primitive]
            unless argument.is_a?(info[:class].to_const)
              argument = _get_property(getter, {info[:primitive] => argument}, 1, 0, true)
              argument = argument[:to] if argument.is_a?(Hash) && argument[:to]
            end
            if info[:type] == :many_to_many || info[:type] == :one_to_many
              return if argument.empty?
              argument = argument.first[:to]
            else
              return if argument.nil?
            end
          end
          
          raise(Sequel::Error, "Invalid type for association when removing from #{getter} for #{inspect}. Should have type #{info[:class]}, got type: #{argument.class.to_s.demodulize}") unless self.class.type_of(info[:getter]).include?(argument.class)
          # Call before_association_add hooks. Return nil if value returned by hook is false (same behavior as Sequel hooks)
          raise_hook_failure(:before_association_remove) if (before_association_remove(info, argument) == false) || (argument.before_association_remove(opp_info, self) == false)

          to_one = (info[:type] == :many_to_one) || (info[:type] == :one_to_one && info[:holds_key])
          
          owning_model  = to_one ? self : argument
          calling_model = owning_model
    
          index_to_delete = nil
          if _tracked_changes?
            ct_unit_of_work.changes.each_with_index do |chng, i|
              if chng.is_a?(::ChangeTracker::ChangeAssociation) && 
                chng.association_operation.is_a?(SpecificAssociations::PendingAssociationAddition) && 
                chng.applies_to?(self, _singular_getter(info), argument)
                index_to_delete = i
                break
              end
            end
          end

          if index_to_delete
            ct_unit_of_work.changes.delete_at(index_to_delete)
          else
            owning_klass     = to_one ? self.class : info[:class].to_const
            non_owning_klass = to_one ? info[:class].to_const : self.class
            # Could label the position column more accurately: :position_in_<getter>
            if to_one
              owning_id           = "#{singular_getter}_id".to_sym
              non_owning_id       = "#{singular_opp_getter}_id".to_sym
              owning_position     = "#{singular_getter}_position".to_sym if info[:opp_is_ordered]
              non_owning_position = "#{singular_opp_getter}_position".to_sym if info[:ordered]
              non_owning_model    = argument
            else
              owning_id           = "#{singular_opp_getter}_id".to_sym
              non_owning_id       = "#{singular_getter}_id".to_sym
              owning_position     = "#{singular_opp_getter}_position".to_sym if info[:ordered]
              non_owning_position = "#{singular_getter}_position".to_sym if info[:opp_is_ordered]
              non_owning_model    = self
            end
            owning_type     = (to_one ? "#{singular_getter}_class" : "#{singular_opp_getter}_class").to_sym if non_owning_klass.is_polymorphic?
            non_owning_type = (to_one ? "#{singular_opp_getter}_class" : "#{singular_getter}_class").to_sym if owning_klass.is_polymorphic?
            owning_info = owning_model == self ? info : opp_info
            
            removal = SpecificAssociations::PendingAssociationRemoval.new(owning_model, 
                                                                          owning_info,
                                                                          owning_id,
                                                                          owning_type,
                                                                          owning_position,
                                                                          non_owning_model,
                                                                          non_owning_id,
                                                                          non_owning_type,
                                                                          non_owning_position,
                                                                          through_class)
            calling_model.add_pending_association_operation(removal)
          end
          custom_remover = info[:custom_remover]
          calling_model = instance_exec(calling_model, &custom_remover) if custom_remover
          calling_model.save if calling_model.is_a?(Sequel::Model)
    
          after_association_remove(info, argument)
          argument.after_association_remove(opp_info, self)
          if info[:primitive]
            original_primitive_argument = argument[info[:primitive]]
            argument.destroy
            return original_primitive_argument
          end
          calling_model
        end
        
        def _remove_all(getter)
          all_associations = Array(_get_property(getter))
          # Remove in reverse of association order. This prevents reordering of positions for ordered associations
          # Return the number of associations removed.
          all_associations.reverse_each { |association| _remove(getter, association) }.length
        end

      end
    end
  end
end