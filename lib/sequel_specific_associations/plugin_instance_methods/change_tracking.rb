module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        def using_change_tracker_for_associations?
          self.class.using_change_tracker_for_associations?
        end
        
        def browse_time
          if using_change_tracker_for_associations? && self.class.browse_time
            self.class.browse_time.is_a?(Time) ? self.class.browse_time : nil
          end
        end
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel