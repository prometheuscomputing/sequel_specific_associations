module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        
        def _get_uniqueness_warnings(getter_values)
          _get_uniqueness_violations(getter_values, :should_be_unique_constraints)
        end
        
        def _get_uniqueness_errors(getter_values)
          _get_uniqueness_violations(getter_values, :is_unique_constraints)
        end
        
        def _get_uniqueness_violations(getter_values, constraints_method = :uniqueness_constraints)
          klass = self.class
          # Check constraints on this column and collect results (a result can either be 'true' or a UniquenessViolation)
          results = klass.send(constraints_method).collect do |constraint_name, constraint|
            constraint.check(self, getter_values) 
          end
          results.select { |r| r.is_a?(SpecificAssociations::UniquenessViolation) }
        end
        
        def _violates_subset_constraint?(object_to_check, getter, info)
          # Notice here that we can only restrict something to being a member of a single superset...kind of an oversight when this was first implemented...should be easy-ish to fix
          superset_getter_path = info[:subset_of]
          if superset_getter_path && !_superset_includes?(superset_getter_path, object_to_check)
            return "Could not add #{object_to_check} to the '#{getter}' association because it was not found in the '#{superset_getter_path}' superset association"
          end
          false
        end
        
        # FIXME this is problematic when object_to_check is a primitive and klass.type_of(getter) returns subclasses of that primitive.
        # What really needs to be happening is that an primitive object that comes in needs to have been properly cast before it gets here. 
        def _violates_type_constraint?(object_to_check, getter, opp_info)
          klass     = self.class
          chk_class = object_to_check.class
          
          legal_types = klass.type_of(getter)
          if legal_types.any?
            matching_klass = legal_types.find { |t| chk_class == t }
            unless matching_klass
              # # This is probably an imperfect check.  As such, we'll call this a HACK and actually screws things up downstream.  It does provide some insight into the problem so I'll leave it here for now.
              # primitive_legal_types = legal_types.select { |t| t.primitive?}
              # matching_klass = primitive_legal_types.find { |t| t <= chk_class }
              # unless matching_klass
                return "#{object_to_check.class} is an invalid type for association when adding to #{klass}##{getter} association.  The valid types are: #{legal_types}."
              # end
            end
          end
          
          if opp_info[:getter]
            legal_types = chk_class.type_of(opp_info[:getter])
            unless legal_types.find { |t| klass == t }
              return "Invalid type when adding a #{object_to_check.class} for the #{klass}##{getter} association. The reciprocal association, '#{object_to_check.class}##{opp_info[:getter]}', can not accept a #{klass}.  The valid types are: #{legal_types}."
            end
          end
          false
        end
        
        def _violates_acyclic_constraint?(object_to_check, getter, info, opp_info)
          klass = self.class
          if info[:acyclic] && _addition_would_cause_cycle(object_to_check, info[:opp_getter])
            return "The #{getter} association must be acyclic and adding this #{object_to_check.class} would cause a cycle."
          end
          if opp_info[:acyclic] && object_to_check._addition_would_cause_cycle(self, info[:getter])
            return "The #{opp_info[:getter]} association must be acyclic and adding this #{self.class} would cause a cycle"
          end
          false
        end
        
        # Maybe this should go in pending associations?  Those methods really seem to deal with collecting pending association operations while this one is ultimately concerned with objects...
        def _get_pending_superset(superset_getter_path)
          existing_superset = _get_association_path(superset_getter_path) || []
          pending_assocs    = superset_pending_association_operations(superset_getter_path)
          pending_adds      = pending_assocs.select { |pa| pa.is_a? SpecificAssociations::PendingAssociationAddition }.collect { |pa| _get_other_model(pa)}
          pending_removes   = pending_assocs.select { |pa| pa.is_a? SpecificAssociations::PendingAssociationRemoval }.collect { |pa| _get_other_model(pa)}
          # TODO this comparison right here needs to be refactored into something reusable or replaced with something that already does the job
          (existing_superset + pending_adds).reject { |obj| pending_removes.find { |r| (r.id == obj.id) && (r.class == obj.class)}}
        end

        def _superset_includes?(superset_getter_path, obj)
          superset = _get_pending_superset(superset_getter_path)
          superset.include?(obj)
        end        
        
        # Check if the addition of element to the association with the specified opp getter will cause a cycle
        # A hash mapping classes to getters may be passed instead of a getter
        def _addition_would_cause_cycle(element, opp_getter_or_hash)
          # Check for self-association cycle
          return true if element == self
          # Can't add as a child anything listed as a parent
          _get_property_recursively(opp_getter_or_hash).include?(element)
        end
        
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
