module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods

        def _add(getter, object_to_add, options = {})
          return if object_to_add.nil?
          klass    = self.class
          info     = _get_info(getter)
          if info[:alias_of]
            raise(Sequel::Error, "Invalid type for association when adding to #{getter} for #{inspect}\n.Should have type #{info[:class]}, got type: #{object_to_add.class}") unless self.class.type_of(info[:getter]).include?(object_to_add.class)
            # Either raise an error or set attributes to ensure that object_to_add matches filter
            alias_filter = info[:filter]
            # Set any nil attributes according to filter if possible
            object_to_add.match_filter(alias_filter)
            # Check that object_to_add matches alias filter
            object_matches_alias_filter = object_to_add.match?(alias_filter)
            raise(Sequel::Error, "Added object #{object_to_add.inspect} does not match filter #{alias_filter.inspect}") unless object_matches_alias_filter
            return _add(info[:alias_of], object_to_add, options)
          end
          
          # Handle enumeration string value to model instance
          if info[:enumeration] && object_to_add.is_a?(String)
            enum_class = info[:class].to_const
            object_to_add = _literal_for_string enum_class, object_to_add
          end
          
          # TODO: See if _set_property can call _add for this function
          # Handle setting complex attribute properties
          if self.class.complex_attribute?
            if info[:type] == :many_to_many || info[:type] == :one_to_many
              current = _get_property(info[:getter])
              instance_variable_set('@' + info[:getter].to_s, current + [object_to_add])
            else
              instance_variable_set('@' + info[:getter].to_s, object_to_add)
            end
            return object_to_add
          end
          
          opp_info = klass.opp_info_for(getter) || {}
          to_klass = info[:class].to_const

          if info[:primitive]
            if object_to_add.is_a?(Enumerable)
              object_to_add.map! { |arg| arg.is_a?(to_klass) ? arg : to_klass.create(info[:primitive] => arg) }
            else
              object_to_add = to_klass.create(info[:primitive] => object_to_add) unless object_to_add.is_a?(to_klass)
            end
          end
  
          # If subset_of option is defined, ensure that requirement is met.
          violation_message = _violates_subset_constraint?(object_to_add, getter, info)
          if violation_message
            raise ::SpecificAssociations::FailedSubsetConstraint, violation_message
          end

          violation_message = _violates_type_constraint?(object_to_add, getter, opp_info)
          if violation_message
            raise(SpecificAssociations::InvalidAssociationTypeError, violation_message)
          end
          
          violation_message = _violates_acyclic_constraint?(object_to_add, getter, info, opp_info)
          if violation_message
            raise(SpecificAssociations::CyclicAssociationError, violation_message)
          end
   
          # # Retrieve the current value of the association
          # current_values = Array(_get_property(getter))
          # # Check for multiple existing entries, and warn if found
          # existing_entry = current_values.find do |entry|
          #   if info[:primitive]
          #     entry == object_to_add
          #   else
          #     entry.pk && entry.pk == object_to_add.pk && entry.class == object_to_add.class
          #   end
          # end
          
          # FIXME: _get_many_query does not work for all to_one associations!
          has_existing_entry = 
            if object_to_add.pk.nil?
              false
            elsif ::SpecificAssociations.to_many?(info) || (info[:type] == :one_to_one && !info[:holds_key])
              if info[:through]
                id_col = (info[:singular_getter].to_s + '_id').to_sym
                filter_hash = {id_col => object_to_add.id}
                if _is_polymorphic?(getter)
                  type_col = (info[:singular_getter].to_s + '_class').to_sym
                  filter_hash[type_col] = object_to_add.class.to_s
                end
                query = _get_many_query(getter, {:id => object_to_add.id, 'exact_type' => object_to_add.class})
                # Handling for non-query values here, because sometimes _get_many_query doesn't return a query!
                if query == []
                  false
                else
                  query.any?
                end
              else
                _get_many_query(getter, {:id => object_to_add.id, 'exact_type' => object_to_add.class}).any?
              end
            else # :many_to_one, and :one_to_one where holds_key is defined
              keys = info[:holds_key]
              # Relying on id being the first key. Should ensure this is always true
              (self[keys[0]] == object_to_add.id) && (keys[1].nil? || self[keys[1]] == object_to_add.class.to_s) if keys
            end
          
          # FIXME: this code is too simplistic and makes incorrect assumptions. It should be replaced with code abstracted from _get_property
          if _tracked_changes? && object_to_add.pk
            if has_existing_entry
              has_existing_entry = false if _pending_associations(info[:getter], {})[:removals].any?{|a| a[:to].id == object_to_add.id && a[:to].class == object_to_add.class}
            else
              has_existing_entry = true if _pending_associations(info[:getter], {})[:additions].any?{|a| a[:to].id == object_to_add.id && a[:to].class == object_to_add.class}
            end
          end

          if has_existing_entry
            puts Rainbow("Warning! ").yellow + "While calling #{self.class}#_add(#{getter}): Ignoring attempt to add multiple associations from #{klass.name}[#{id}] to #{object_to_add.class.to_s}[#{object_to_add.id}]." + Rainbow(" Please debug.").yellow
            # Commenting out this section due to performance issues. -SD
            # unless caller.join =~ /spec\.rb/
            #   puts Rainbow("self:").magenta + " #{pretty_inspect}"; puts Rainbow("object_to_add:").magenta + " #{object_to_add.pretty_inspect}" + Rainbow("existing_entry:").magenta + " #{existing_entry.pretty_inspect}"
            # end
          # TODO: rethink existing_entry/calling_model setup
          else
            # Define the association creation
            # Call before_association_add hooks. Return nil if value returned by hook is false (same behavior as Sequel hooks)
            
            if (before_association_add(info, object_to_add) == false) || (!object_to_add.class.primitive? && (object_to_add.before_association_add(opp_info, self) == false))
              raise_hook_failure(:before_association_add)
            end
            
            custom_adder  = info[:custom_adder]
            object_to_add = self.instance_exec(object_to_add, &custom_adder) if custom_adder
            
            if object_to_add.nil?
              raise(Sequel::Error, "custom_adder for '#{getter}' returned nil. Please debug extensions.")
            end
    
            g     = _singular_getter(info)
            opp_g = _singular_opp_getter(info)
            
            id_col       = "#{g}_id".to_sym
            opp_id_col   = "#{opp_g}_id".to_sym
            type_col     = "#{g}_class".to_sym
            opp_type_col = "#{opp_g}_class".to_sym
            opp_complex_getter_col = "#{opp_g}_complex_getter".to_sym
            
            position_col     = info[:opp_is_ordered] ? "#{g}_position".to_sym     : nil
            # Could label this column more accurately: :position_in_<getter>
            opp_position_col = info[:ordered]        ? "#{opp_g}_position".to_sym : nil
            if info[:type] == :many_to_one || (info[:type] == :one_to_one && info[:holds_key])
              key_holding_model            = self
              key_holding_klass            = klass
              non_key_holding_model        = object_to_add
              non_key_holding_klass        = to_klass
              key_holding_id_col           = id_col
              key_holding_type_col         = type_col
              key_holding_position_col     = position_col
              # It is not necessary or possible to match a complex_getter_col for one_to_one complex attribute associations
              key_holding_complex_getter_col = nil
              non_key_holding_id_col       = opp_id_col
              non_key_holding_type_col     = opp_type_col
              non_key_holding_position_col = opp_position_col
            else
              key_holding_model            = object_to_add
              key_holding_klass            = to_klass
              non_key_holding_model        = self
              non_key_holding_klass        = opp_info[:class] ? opp_info[:class].to_const : nil
              key_holding_id_col           = opp_id_col
              key_holding_type_col         = opp_type_col
              key_holding_position_col     = opp_position_col
              # Inform the pending addition that it must also match the complex getter col, to ensure that the associated objects are for the 
              #   correct complex attribute. This is relevant when there is more than one complex attribute of the same type on an object.
              # Note that the check to ensure that the columns include the complex_getter_col allows backwards compatibility with older datasets.
              #   This may be removed at a later date. -SD
              key_holding_complex_getter_col = opp_complex_getter_col if info[:for_complex_attribute] && key_holding_klass.columns.include?(opp_complex_getter_col)
              non_key_holding_id_col       = id_col
              non_key_holding_type_col     = type_col
              non_key_holding_position_col = position_col
            end
            
            through_class_instance  = info[:through] ? info[:through].to_const.new : nil
            
            complex_getter = info[:for_complex_attribute]
            
            # Define change to be made
            key_holding_type_col     = non_key_holding_klass.is_polymorphic? ? key_holding_type_col     : nil
            non_key_holding_type_col = key_holding_klass.is_polymorphic?     ? non_key_holding_type_col : nil
            key_holding_info         = key_holding_model == self ? info : opp_info
            addition = SpecificAssociations::PendingAssociationAddition.new(key_holding_model, key_holding_info, key_holding_id_col, key_holding_type_col, key_holding_position_col, non_key_holding_model, non_key_holding_id_col, non_key_holding_type_col, non_key_holding_position_col, through_class_instance, key_holding_complex_getter_col, complex_getter)
            calling_model = key_holding_model
            calling_model.add_pending_association_operation(addition)
          end
          calling_model.save if calling_model # TESTING

          # Call after_association_add hooks.
          after_association_add(info, object_to_add)
          object_to_add.after_association_add(opp_info, self)
  
          return object_to_add
        end 
          
        # TODO: Remove duplication with _add
        def _set_property(getter, argument)
          getter = getter.to_sym
          info   = _get_info(getter)
          
          # TODO: remove duplication with _add
          if info[:enumeration]
            enum_class = info[:class].to_const
            if argument.is_a?(String)
              argument = _literal_for_string(enum_class, argument)
            elsif argument.is_a?(Array)
              argument = argument.collect do |member|
                if member.is_a?(String)
                  _literal_for_string(enum_class, argument)
                else
                  member
                end
              end
            end
          end
          
          # Handle setting complex attribute associations
          # Note that attributes aren't handled here and instead get set normally. This has the advantage of
          #   allowing easy comparison and inspection of complex attributes. -SD
          return instance_variable_set('@' + info[:getter].to_s, argument) if self.class.complex_attribute? && info[:association]
          
          # Handle setting attributes
          return _set_attribute(getter, argument) if info[:attribute]
          
          to_one = ::SpecificAssociations.to_one?(info)
          # Remove existing association(s)
          current_entry = _get_property(getter)
          unless Array(current_entry).empty? # don't bother if there was no current entry
            if to_one
              if current_entry.respond_to?(:is?)
                return if current_entry.is?(argument)
              else # primitives don't respond to #is?
                return if (current_entry.class == argument.class) && (current_entry.pk == argument.pk)
              end
              _remove(getter.to_sym, current_entry)
            else
              return if ::SpecificAssociations.models_match?(current_entry, argument) # only should compare ct_identity.
              raise "Error: Attempted to assign non-array #{argument.class} to to-many association: #{getter}." unless argument.is_a?(Array)
              _remove_all(getter)
            end
          end

          # Handle custom_setter_for_each
          custom_setter_for_each = info[:custom_setter_for_each]
          if custom_setter_for_each
            if argument.is_a?(Enumerable)
              argument = argument.collect { |a| instance_exec(a, &custom_setter_for_each)}
            else
              argument = instance_exec(argument, &custom_setter_for_each)
            end
          end
          # Handle custom_setter
          custom_setter = info[:custom_setter]
          argument = instance_exec(argument, &custom_setter) if custom_setter
          # Perform add
          if to_one
            _add(info[:getter], argument)
          else
            argument.collect { |model| _add(info[:getter], model) } if argument
          end
        end
        
        def _set_attribute(getter, value)
          getter = getter.to_sym
          # original args are name(i.e. getter) and attr_class, which we should be able to get from metainfo
  
          # get current attribute information from meta-information since options may have changed since attribute definition time (with the exception of 'name' and 'attr_class')
          klass = self.class
          info  = klass.info_for getter

          raise "Error: Attribute meta-information for #{getter} has been removed from the @properties of #{self.class}" unless info
          return nil if info[:disable_setter]

          # Call on_assign hook if available
          value.on_assign(self) if value.respond_to?(:on_assign)

          # Execute the setter block if specified
          # Instance exec changes the scope of the setter block to the scope of this method and passes a value to the block
          value = instance_exec(value, &info[:custom_setter]) if info[:custom_setter]

          # FIXME are we not doing uniqueness checks on complex attributes?  Should we?
          # Proceed to uniqueness check and storage if store_value is specified
          if info[:store_value]
            # Check the value for uniqueness
            check_for_uniqueness({getter => value})
            # Store the value in the column
            self[info[:column]] = value
          end
          if info[:complex]
            # Set the existance flag
            self[info[:column].to_sym] = !value.nil?
    
            attr_class = info[:class].to_const
            attr_class.attributes.values.each do |complex_attr|
              next if complex_attr[:primary_key]
              self["#{info[:column]}_#{complex_attr[:column]}".to_sym] = value ? value[complex_attr[:column]] : nil
            end

            attr_class.associations(false).values.each do |complex_assoc|
              new_value = value.send(complex_assoc[:getter]) if value
              # old_value = send("#{info[:column]}_#{complex_assoc[:getter]}") if complex_assoc[:composition]
              old_value = _get_property("#{info[:column]}_#{complex_assoc[:getter]}".to_sym) if complex_assoc[:composition]
              if ::SpecificAssociations.to_many?(complex_assoc)
                # Assign new value(s)
                #TODO This is a hack so that the old association values aren't automatically deleted when a new complex attribute object is created
                # e.g. old rich text images being deleted if a seperate rich text getter is set to a nil image array.  It is a change to be on the safe side of preserving data.
                new_value ||= []
                new_value = new_value + old_value if complex_assoc[:composition]
                _set_property("#{info[:column]}_#{complex_assoc[:getter]}".to_sym, new_value)
                #Old assign new values which assumed if you passed it an empty array which is the default for a new object that it should delete all the old assocations which loses data in some cases.
                # _set_property("#{info[:column]}_#{complex_assoc[:getter]}".to_sym, (new_value || []))
                # removed_items = new_value ? old_value - new_value : old_value
                # removed_items.each { |ri| ri.destroy }
              else
                # Assign new value
                _set_property("#{info[:column]}_#{complex_assoc[:getter]}".to_sym, new_value)
                # Destroy old_value unless it is the same as new_value
                if old_value # && complex_assoc[:composition]
                  new_value_is_different = new_value.nil? || old_value.id != new_value.id
                  old_value.destroy if new_value_is_different
                end
              end
            end
          end
          value
        end
        private :_set_attribute
        
        def _literal_for_string(enum_class, str)
          enum_class = enum_class.to_const
          enum_classes = [enum_class] | enum_class.children
          literal = nil
          enum_classes.each do |ec|
            # puts "#{ec.name} instances: #{ec.all.inspect}"
            literal = ec.where(:value => str).first
            break if literal
          end
          literal ||= enum_class.create(:value => str)
          # puts "Literal: #{literal.inspect}"
          literal
        end
           
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
