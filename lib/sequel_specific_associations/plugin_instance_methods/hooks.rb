module Sequel
  module Plugins
    module SpecificAssociations
        module InstanceMethods
        # TODO: This should be changed to before_initialize (doesn't exist) so as to allow associations to be set via #create and #new
        def after_initialize
          super
          @pending_association_operations = []
        end

        # The below definitions are very confusing, read carefully!
        # When using SCT:
        #   #before_change_tracker_destroy is called during #destroy, and #before_destroy is called during commit
        # Without SCT:
        #   #before_change_tracker_destroy is never called, and #before_destroy is called during #destroy
        def before_change_tracker_destroy
          before_destroy(false)
        end
        def before_destroy(is_actual_destroy = true)
          return if using_change_tracker_for_associations? && is_actual_destroy
          composition_associations, other_associations = self.class.associations(false).values.partition{ |info| info[:composition]}
  
          # Delete all composition objects
          composition_associations.each do |info|
            # next if info[:enumeration] # though commented out, UML does allow for attributes to be compositions and enumerations are implemented as associations (even when modeled as attributes)
            objs = _get_property(info[:getter])
            objs = objs ? [objs] : [] if ::SpecificAssociations.to_one?(info)
            objs.each { |o| o.destroy }
          end
  
          # Remove from all normal associations
          other_associations.each do |info|
            next if info[:associates] # No need to dissociate items from association class, also it causes an issue.
            next if info[:class].to_const.complex_attribute?
            ::SpecificAssociations.to_many?(info) ? _remove_all(info[:getter]) : _set_property(info[:getter], nil)
          end
  
          super()
        end

        def before_save
          # Perform any pending association operations
          # NOTE: @pending_association_operations should always be empty here if using
          #       change tracker for associations
          @pending_association_operations ||= []
          if @pending_association_operations.any? && using_change_tracker_for_associations?
            raise "CT Didn't empty pending_association_operations for instance of #{self.class}:\n   @pending_association_operations: #{@pending_association_operations.inspect}"
          end
  
          # NOTE: skipping associations processing for new objects
          if @pending_association_operations.any? && !using_change_tracker_for_associations?
            # Iterate over pending_association_operations, removing them from the array if they can be applied
            @pending_association_operations.reject! do |pending_operation|
              # TODO: simplify this by having pending_operation.apply return a true/false success value
              if pending_operation.is_processable?
                pending_operation.apply
                pending_operation.persist
                true
              else # Can't be applied due to new object, defer apply until after_save
                false
              end
            end
          end
  
          super
        end

        def after_save
          super
          # Process any deferred pending associations
          if @pending_association_operations.any? && !using_change_tracker_for_associations?
            # Save unsaved referenced models (excluding self, since it was just saved)
            referenced_models = @pending_association_operations.collect { |op| op.referenced_models }.flatten.uniq - [self]
            found_unsaved_change_tracked_reference = false
            referenced_models.each { |m| 
              m.save if m.new?
              found_unsaved_change_tracked_reference = true if m.new? && m.using_change_tracker_for_associations?
            }
            # Save self again to re-process pending association operations unless an unsaved, change tracked reference was found.
            # In that case, saving again wouldn't help, since a change tracked object is only really saved on commit.
            # Instead, the pending association operation will be left in place to be handled by the commit later.
            self.save unless found_unsaved_change_tracked_reference
          end
        end 
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel