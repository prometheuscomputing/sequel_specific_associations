require_relative 'get/get_property'
require_relative 'get/query'

module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods

        # Recursively get all properties, either via a getter or hash mapping classes to getters
        # If getter is not specified for an encountered class, empty array is returned
        # TODO: Implement at query level for improved efficiency
        def _get_property_recursively(*args)
          getter_or_hash = args.first
          getter = nil
          case getter_or_hash
          when String, Symbol
            getter = getter_or_hash
          else # Hash
            types = ([self.class] + self.class.parents(:traversal => :breadth)).collect { |t| t.to_s.to_sym }
            types.each do |type|
              getter = getter_or_hash[type]
              break if getter
            end
          end
          return [] unless getter
          properties = _get_property(getter, *args[1..-1])
          return [] unless properties
          # Convert #to_one associations to arrays
          properties = Array(properties)
          properties + properties.collect { |d| d._get_property_recursively(*args)}.flatten
        end # def _get_property_recursively
        
        def _handle_query_results(query)
          if query.nil?
            []
          elsif query.is_a?(Array)
            query
          else
            query.all
          end
        end
        private :_handle_query_results
        
        def _get_many(getter, filter = {}, limit = nil, offset = 0, ignore_objs = [], order = {}, return_mode = :through)
          query = _get_many_query(getter, filter, limit, offset, ignore_objs, order, return_mode)
          _handle_query_results(query)
        end
        
        def _unassoc_get_many(getter, filter = {}, limit = nil, offset = 0, ignore_objs = [], order = {})
          query = _unassoc_get_many_query(getter, filter, limit, offset, ignore_objs, order)
          _handle_query_results(query)
        end
        
        def _type_of(getter)
          self.class.type_of(getter)
        end
        
        def _is_polymorphic?(getter)
          info = _get_info(getter)
          info = _get_info(info[:alias_of]) if info[:alias_of]
          ::SpecificAssociations.class_for(info).is_polymorphic?
        end
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
