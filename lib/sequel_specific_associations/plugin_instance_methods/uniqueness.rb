module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods

        # TODO: make this look into the current SCT commit to see if there is violation within the to-be-created/modified items
        def check_for_uniqueness(getter_values)
          violations = _get_uniqueness_violations(getter_values)
          return true if violations.empty?
          # It is a bit lame to just hand back the first of what is potentially multiple errors.
          if violations.any? { |v| v.is_error?}
            exception = SpecificAssociations::NonUniqueError.new(violations)
          else
            exception = SpecificAssociations::NonUniqueWarning.new(violations)
          end
          raise exception, exception.failure_message
        end
        
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel