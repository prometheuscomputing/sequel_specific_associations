module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods

        # Follow an association path
        # Returns nil if a node in the path is missing
        def _get_association_path(path, filter = {}, limit = nil, offset = 0)
          path = path.split('.') if path.is_a?(String)
          getter = path.shift
          assoc_info = self.class.associations[getter.to_sym]
          unless path.length == 0
            raise "Cannot traverse path: '#{path.unshift(getter)}' from: #{inspect}" unless assoc_info && ::SpecificAssociations.to_one?(assoc_info)
            next_object = _get_property(getter)
            # If the path can't be traversed due to an unpopulated association, just return nil
            return nil unless next_object
            next_object._get_association_path(path, filter, limit, offset)
          else
            assoc_info = self.class.associations[getter.to_sym]
            raise "Cannot access association: '#{getter}' from: #{inspect}" unless assoc_info
            _get_property(getter, filter, limit, offset)
          end
        end

      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel