module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        # Get the value of a given property
        # Filter, limit, and offset can be used to return a subset of associated objects for associations
        # Options accepts:
        #   :custom_getter - overrides the custom getter defined by the property's info. Used by alias attributes, where :type != :column_alias exclusively.
        def _get_property(getter, filter = {}, limit = nil, offset = 0, return_associations_hash = false, order = {}, options = {})
          info = _get_info(getter)
          
          if info[:prefill_rules]
            # If any prefill rule is found whose condition evaluates to true, immediately return the value of the transformation
            info[:prefill_rules].each do |rule|
              should_transform = rule.condition.call(self)
              return rule.transformation.call(self) if should_transform
            end
          end
          
          if info[:alias_of] && info[:type] != :column_alias
            filter = ::SpecificAssociations.filter_join(filter, info[:filter])
            # Add :custom_getter override option
            # TODO: Deprecate this option. It is very surprising that one hook gets treated differently than all others. -SD
            return _get_property(info[:alias_of], filter, limit, offset, return_associations_hash, order, :custom_getter => info[:custom_getter])
          end
          
          # Handle returning complex attribute associations
          # Note that attributes aren't handled here and instead get returned normally -SD
          if self.class.complex_attribute? && info[:association]
            val = instance_variable_get("@#{getter}")
            val ||= [] if ::SpecificAssociations.to_many?(info)
            return val
          end
          return _get_attribute(getter, info, options) if info[:attribute]
          
          # TODO: disambiguate this usage of 'to_one' and 'to_many' with the normal usage which refers to returned multiplicity of the property. -SD
          to_one  = (info[:type] == :one_to_one && info[:holds_key]) || info[:type] == :many_to_one
          to_many = !to_one

          return (to_many ? [] : nil) if limit == 0
          
          singular_getter = _singular_getter(info)
          id_column       = "#{_singular_getter(info)}_id".to_sym
          class_column    = "#{_singular_getter(info)}_class".to_sym
          
          change_tracker = _tracked_changes?
          if change_tracker
            pending_assocs = _pending_associations(info[:getter], filter)
            # Modify filter to exclude join entries that are listed in pending_assocs[:additions]. This occurs when setting one side of a 'broken' join entry association.
            if info[:type] == :many_to_many
              added_through_objs    = pending_assocs[:additions].select { |obj| obj[:through] && obj[:through].pk }
              added_through_obj_ids = added_through_objs.collect { |obj| obj[:through].pk }
              qualified_through_id_col = (info[:through].to_const.table_name.to_s + '__id').to_sym
              filter = ::SpecificAssociations.add_to_filter(filter, Sequel.~({qualified_through_id_col => added_through_obj_ids}))
            end
          end

          if info[:through] && !return_associations_hash
            table_name = info[:through].to_const.table_name
            qualified_id_col   = ("#{table_name}__#{id_column}").to_sym 
            # qualified_id_col   = ("_join_table__#{id_column}").to_sym
            qualified_type_col = ("#{table_name}__#{class_column}").to_sym
            # qualified_type_col = ("_join_table__#{class_column}").to_sym
            filter = ::SpecificAssociations.add_to_filter(filter, Sequel.~({qualified_id_col => nil}))
            filter = ::SpecificAssociations.add_to_filter(filter, Sequel.~({qualified_type_col => nil})) if info[:class].to_const.is_polymorphic?
          end

          ret = nil          
          if to_one && !info[:through]
            if change_tracker && pending_assocs[:additions].any?
              # Not sure if this is ever run (pending_assocs[:additions] always seems to be empty)
              # Not sure who commented the above, but it definitely runs. -SD
              first_added = pending_assocs[:additions].first
              ret = first_added[:to] if first_added
            elsif self[id_column]
              if info[:class].to_const.is_polymorphic?
                if self[class_column]
                  return_klass = self[class_column].to_const
                else
                  return nil
                end
              else
                return_klass = info[:class].to_const
              end
              ret = return_klass[self[id_column]]
              ret = nil if change_tracker && pending_assocs[:removals].any? { |ro| ro[:to] == ret }
            else
              return nil
            end
          else # to_many
            offset ||= 0
            added_objs_to_return = []
            query_args = [filter]
            if change_tracker
              final_index    = offset + limit - 1 if limit
              ignore_objs    = pending_assocs[:removals].collect { |ro| ro[:to] }
              num_added_objs = pending_assocs[:additions].count
              if num_added_objs > offset
                index_of_last_added_obj_to_return = (limit.nil? || final_index > num_added_objs-1) ? -1 : final_index
                added_objs_to_return = pending_assocs[:additions].slice(offset .. index_of_last_added_obj_to_return) || []
                need_x_more_objs = limit ? limit - added_objs_to_return.count : nil
                query_args += [need_x_more_objs, 0, ignore_objs]
              else
                query_args += [limit, offset - num_added_objs, ignore_objs]
              end
            else
              query_args += [limit, offset, []]
            end
            query_args << order
            # Set return_mode to :both if return_associations_hash is true, or :to otherwise
            query_args << (return_associations_hash ? :both : :to)
            entries = _get_many(getter, *query_args)
            added_objs_to_return = added_objs_to_return.collect{|o| o[:to] } unless return_associations_hash
            ret = added_objs_to_return + entries
            ret = ret.first if to_one
          end
  
          # TODO: rewrite so that primitive and return_associations_hash are not mutually exclusive
          if info[:primitive]
            if ret.is_a?(Array) && !return_associations_hash
              ret.map! { |r| r.send(info[:primitive]) }
            elsif ret && !return_associations_hash
              ret = ret.send(info[:primitive])
            end
          end
          # Handle custom_getter_for_each
          custom_getter_for_each = info[:custom_getter_for_each]
          if custom_getter_for_each && !return_associations_hash
            if ret.is_a?(Enumerable)
              ret = ret.collect { |r| instance_exec(r, &custom_getter_for_each) }
            else
              ret = instance_exec(ret, &custom_getter_for_each)
            end
          end
          # Handle custom_getter
          # NOTE: I'm intentionally not checking options[:custom_getter] for an override here, because this wasn't already 
          #       supported, and I don't want to expand on this behavior. -SD
          custom_getter = info[:custom_getter]
          ret = instance_exec(ret, &custom_getter) if custom_getter && !return_associations_hash
          ret = ret.first if ret.is_a?(Array) && info[:type] == :one_to_one
          ret
        end # def _get_property

        # Options accepts:
        #   :custom_getter - overrides the custom getter defined by the property's info. Used by alias attributes, where :type != :column_alias exclusively.
        def _get_attribute(getter, info = nil, options = {})
          info ||= _get_info(getter)
          
          custom_getter ||= options[:custom_getter] || info[:custom_getter]
          value = self[info[:column]] if info[:store_value]
          if info[:complex]
            # Check the existance flag
            exists = self[info[:column]]
            if exists
              attr_class = info[:class].to_const
              value      = attr_class.new
              attr_class.attributes.each_value do |complex_attr|
                next if complex_attr[:primary_key]
                value[complex_attr[:column]] = self["#{info[:column]}_#{complex_attr[:column]}".to_sym]
              end
              attr_class.associations(false).each_value do |complex_assoc|
                # This might cause problems if the assoc in question is a composition.  Just sayin...
                value.send("#{complex_assoc[:getter]}=", send("#{info[:column]}_#{complex_assoc[:getter]}"))
              end
            end
          end
        
          # Call on_retrieve hook if available
          value.on_retrieve(self) if value.respond_to?(:on_retrieve)
          if custom_getter
            # Execute the getter block if specified
            # Instance exec changes the scope of the getter block to the scope of this method and passes a value to the block
            value = instance_exec(value, &custom_getter)
            # Store the value in the column if store_on_get is specified (and we are storing the value at all)
            # TODO: check if values are equal before assignment?
            self[info[:column]] = value if info[:store_value] && info[:store_on_get]
          end
          value
        end
        private :_get_attribute
        
        def _get_unassociated(getter, filter = {}, limit = nil, offset = 0, order = {})
          info = _get_info(getter)
          if info[:alias_of]
            filter = ::SpecificAssociations.filter_join(filter, info[:filter])
            return _get_unassociated(info[:alias_of], filter, limit, offset, order)
          end
          
          to_one  = (info[:type] == :one_to_one && info[:holds_key]) || info[:type] == :many_to_one
          to_many = !to_one
          return []  if limit == 0 && to_many
          return nil if limit == 0
          final_index = offset + limit - 1 if limit
  
          ret = nil

          offset ||= 0
          
          added_objs, removed_objs = _parse_pending_additions_and_removals(info, filter)
      
          # This is opposite of normal getter.
          # TODO: Currently if an added_obj's id is 'nil', this will ignore other objects of the same class with a 'nil' id and the same attributes
          #       Could perhaps counter this by checking object_id's in this case.
          ignore_objs = added_objs.collect { |ro| ro[:to] }
      
          num_removed_objs = removed_objs.count
          if num_removed_objs > offset
            index_of_last_removed_obj_to_return = (limit.nil? || final_index > num_removed_objs - 1) ? -1 : final_index
            removed_objs_to_return = removed_objs.slice(offset .. index_of_last_removed_obj_to_return)
            need_x_more_objs = limit ? limit - removed_objs_to_return.count : nil
            entries = _unassoc_get_many(getter, filter, need_x_more_objs, 0, ignore_objs, order)
            removed_objs_to_return.collect! { |ro| ro[:to] }
            ret = removed_objs_to_return + entries
          else
            # Note: ignore_objs here has some overlap with those filtered out by the exclusion query in _unassoc_get_many_query.
            #       Review is needed to see if we are duplicating work unnecessarily -SD
            ret = _unassoc_get_many(getter, filter, limit, offset - num_removed_objs, ignore_objs, order)
          end
      
          if info[:primitive]
            if ret.is_a?(Enumerable)
              ret.map! { |r| r.send(info[:primitive]) }
            else
              ret = ret.send(info[:primitive])
            end
          end
          ret
        end
        
        def _parse_pending_assoc_changes(info, filter)
          return [[],[]] unless _tracked_changes?
          singular_getter = _singular_getter(info)
          assoc_changes   = ct_unit_of_work.changes.select { |c| c.is_a?(::ChangeTracker::ChangeAssociation) }
          assoc_changes.select! do |c|
            op = c.association_operation
            unless op.second_model.respond_to?(:ct_identity)
              raise "Object of type: #{op.second_model.class} does not have ChangeTracker applied! Object: #{op.second_model.inspect}"
            end
            if (op.model.pk && op.second_model.pk && op.model == self) || op.model.ct_identity.equal?(ct_identity)
              op.pk_field.to_s == "#{singular_getter}_id"
            elsif (op.model.pk && op.second_model.pk && op.second_model == self) || op.second_model.ct_identity.equal?(ct_identity)
              op.second_pk_field.to_s == "#{singular_getter}_id"
            else
              false
            end
          end
          # TODO: Make sure that this catches association additions done with join tables.
          new_assocs_changes = assoc_changes.select do |c|
            c.association_operation.is_a?(SpecificAssociations::PendingAssociationAddition)
          end

          new_assocs = new_assocs_changes.collect do |c|
            op = c.association_operation
            if (op.model.pk && op.second_model.pk && op.model == self) || op.model.ct_identity.equal?(ct_identity)
              {:to => op.second_model, :through => op.through_class}
            else
              {:to => op.model, :through => op.through_class}
            end
          end
          new_assocs.select! { |na| na[:to].match?(filter) }
    
          removed_assocs_changes = assoc_changes.select do |c|
            c.association_operation.is_a?(SpecificAssociations::PendingAssociationRemoval)
          end
          
          removed_assocs = removed_assocs_changes.collect do |c|
            op = c.association_operation
            if (op.model == self) || op.model.ct_identity.equal?(ct_identity)
              {:to => op.second_model, :through => op.through_class}
            else
              {:to => op.model, :through => op.through_class}
            end
          end
          removed_assocs.select! { |ra| ra[:to].match?(filter) }
          [new_assocs, removed_assocs]
        end
        
        def _parse_pending_additions_and_removals(info, filter)
          new_assocs, removed_assocs = _parse_pending_assoc_changes(info, filter)
          # TODO: Replay the associations in order in case something was removed and then added back in the same transaction
          assoc_hash = Hash.new(0)
          new_assocs.each do |a|
            assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] += 1
          end
          removed_assocs.each do |a|
            assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] -= 1
          end
          added_objs   = new_assocs.select { |a| assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] > 0 }
          removed_objs = removed_assocs.select { |a| assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] < 0 }
          [added_objs, removed_objs]
        end # end of _parse_pending_additions_and_removals
      
        def _get_at_position(getter, position)
          info = _get_info(getter)
          return nil unless info[:ordered]
          opp_getter = (info[:singular_opp_getter] || info[:opp_getter])
          opp_position = "#{opp_getter}_position".to_sym
          items = _get_property(getter).select { |item| item[opp_position] == position }
          puts Rainbow('WARNING:').red + ' Multiple items found at same position in ordered association. Please debug' if items.length > 1
          items.first
        end
        
        def _singular_getter(getter_or_info)
          return (getter_or_info[:singular_getter] || getter_or_info[:getter]) if getter_or_info.is_a?(Hash)  
          info ||= _get_info(getter_or_info)
          info[:singular_getter] || info[:getter]
        end
        
        def _singular_opp_getter(getter_or_info)
          return (getter_or_info[:singular_opp_getter] || getter_or_info[:opp_getter]) if getter_or_info.is_a?(Hash)  
          info ||= _get_info(getter_or_info)
          info[:singular_opp_getter] || info[:opp_getter]
        end
        
        def _move(getter, item, new_position)
          info = _get_info(getter)
          item = item.dup.refresh if item.respond_to?(:refresh)
          all_items = _get_many(getter)
          sg = _singular_getter(info)
          if info[:type] == :many_to_many
            all_objects = all_items.collect { |r| r._get_property(sg) }
            item_object = item
            # re-assign item to the record
            current_index = all_objects.index(item)
            item = all_items[current_index] if current_index
          end
      
          valid_item = all_items.include?(item)
      
          if item.nil? || !valid_item || !new_position.is_a?(SpecificAssociations::INT_TYPE)
            raise ArgumentError, "Invalid arguments to move_#{getter} given. Item must be a part of the #{getter} collection, and the position must be of type #{SpecificAssociations::INT_TYPE}. Received item: #{item.inspect}, and position: #{new_position.inspect}"
          end
          opp_getter   = (info[:singular_opp_getter] || info[:opp_getter])
          opp_position = "#{opp_getter}_position".to_sym
          # item_at_new_position = all_items.select { |item| item[opp_position] == new_position }.first
          item_at_new_position = all_items.find { |item| item[opp_position] == new_position }

          unless item_at_new_position
            raise SpecificAssociations::InvalidIndexError, "Specified index: #{new_position} is out of bounds."
          end
          # One implementation of move is to swap with the item at the new position. This is cheaper, but unintuitive to the user.
          #swap_#{getter_name}(item, item_at_new_position)"

          # Implement move function
          old_position = item[opp_position]
          return item if old_position == new_position
          move_forward  = (new_position > old_position)
          item_range    = move_forward ? (old_position + 1)..new_position : new_position..(old_position - 1)
          items_to_move = item_range.collect do |pos|
            all_items.select { |item| item[opp_position] == pos }.first
          end
          items_to_move.each do |current_item|
            # If moving an item forward, subtract one from position to compensate; Add one if moving an item backward
            current_item[opp_position] += move_forward ? -1 : 1
            current_item.save
          end
          item[opp_position] = new_position
          item.save
          return item if info[:type] == :one_to_many
          return item_object if info[:type] == :many_to_many
        end # def _move
        
        
        def _enum_literals(getter)
          info = _get_info(getter)
          raise "Cannot call _enum_literals on a non-enumeration association" unless info[:enumeration]
          info[:class].to_const.literal_values
        end # def _enum_literals
      end
    end
  end
end