module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        def _process_filter_for_types(filters, klasses)
          klasses.collect { |klass| _process_filter_for_type(filters, klass) }.compact
        end
        
        def _process_filter_for_type(filters, klass)
          if klass
            replacement_filters = filters.collect do |filter|
              if filter.is_a?(Hash) && filter.any?
                new_hash = filter.dup
                filter.each_key do |getter|
                  unless klass.columns.include?(getter) || getter.to_s.match?(/__/)
                    aliased_col = klass.properties.dig(getter, :alias_of)
                    if aliased_col
                      new_hash[aliased_col] = new_hash.delete(getter)
                    else
                      return nil # tried to filter on a non-existant column so this type is excluded from the query
                    end
                  end
                end
                new_hash if new_hash.any?
              elsif filter.is_a?(Sequel::SQL::Expression)
                new_exp = _resolve_columns_in_expression(klass, filter)
                return nil unless new_exp
                new_exp
              else
                filter
              end
            end
          else
            replacement_filters = filters.dup
          end
          replacement_filters.compact!
          replacement_filters.delete_if { |filter| filter.is_a?(Hash) && filter.empty? }
          [klass, replacement_filters]
        end
        
        def _resolve_columns_in_expression(klass, exp)
          raise unless exp.is_a?(Sequel::SQL::Expression)
          if exp.is_a?(Sequel::SQL::ComplexExpression)
            new_args = exp.args.collect do |arg|
              # if arg is nil or an empty array we just want to keep it as nil but if it wasn't nil and comes back as nil then we have an invalid query and we want to return nil so the caller can remove this klass from the queried_types
              if arg.nil? || arg == false || arg == true || (arg.is_a?(Array) && arg.empty?)
                arg
              else
                new_arg = _resolve_column_in_expression(klass, arg)
                return nil unless new_arg
                new_arg
              end
            end
            replacement_expression = exp.class.new(exp.op, *new_args)
          elsif exp.is_a?(Sequel::SQL::Identifier)
            new_value = _resolve_column_in_expression(klass, exp.value)
            replacement_expression = exp.class.new(new_value)
          end
          replacement_expression
        end
        
        def _resolve_column_in_expression(klass, arg)
          if arg.is_a?(Sequel::SQL::Expression)
            new_arg = _resolve_columns_in_expression(klass, arg)
            return nil unless new_arg
            new_arg
          elsif arg.is_a?(Symbol)
            # if it is a qualified column then don't mess with it because it isn't referencing an attribute column on this klass
            if klass.columns.include?(arg) || arg.to_s.match?(/__/)
              arg
            else
              aliased_col = klass.properties.dig(arg, :alias_of)
              if aliased_col
                aliased_col
              else
                return nil # tried to filter on a non-existant column so this type is excluded from the query
              end
            end
          else
            # If we don't know what it is, then don't mess with it and just return it as is.
            arg
          end
        end
        
        # return_mode may be
        #   - :through_or_to - Returns the 'through' instances if available, or 'to' instances if not (this is the default behavior since it is the way the original method operated)
        #   - :through       - Returns the 'through' instances (many_to_many only)
        #   - :to            - Returns the 'to' instances without through instances
        #   - :both          - Retuns both 'through' and 'to' instances (if available) in an array of hashes
        def _get_many_query(getter, filter = {}, limit = nil, offset = 0, ignore_objs = [], order = {}, return_mode = :through_or_to)
          return [] if pk.nil? || limit == 0
          info = _get_info(getter)
          if info[:alias_of]
            filter = ::SpecificAssociations.filter_join(filter, info[:filter])
            return _get_many_query(info[:alias_of], filter, limit, offset, ignore_objs, order)
          end
          return_through_instances_for = [:through, :both]
          return_through_instances_for << :through_or_to if info[:through]
          return_to_instances_for = [:to, :both]
          return_to_instances_for << :through_or_to unless info[:through]
          # Whether or not this query needs to construct through instances
          return_through_instances = return_through_instances_for.include?(return_mode)
          return_to_instances = return_to_instances_for.include?(return_mode)
          
          singular_getter = _singular_getter(info)
          opp_getter      = _singular_opp_getter(info)
          opp_position    = "#{opp_getter}_position".to_sym if info[:ordered]
          
          match = {"#{opp_getter}_id".to_sym => pk}
          if info[:for_complex_attribute]
            # Handle backwards compatibility with databases created prior to SSA 9.8.x by matching both nil and correct class
            backwards_compatible_class_match = {"#{opp_getter}_class".to_sym => [nil, self.class.to_s]}
            match.merge!(backwards_compatible_class_match)
            # Handle backwards compatibility with databases created prior to SSA 9.8.x by only matching correct complex 
            #  getter if getter column is present, and by matching both the correct getter and nil if the column is present
            complex_getter_column = "#{opp_getter}_complex_getter".to_sym
            complex_getter_match = {complex_getter_column => [nil, info[:for_complex_attribute].to_s]}
            match.merge!(complex_getter_match) if info[:class].to_const.columns.include?(complex_getter_column)
          elsif info[:as].to_const.is_polymorphic?
            class_match = {"#{opp_getter}_class".to_sym => self.class.to_s}
            match.merge!(class_match)
          end
  
          # if browse_time
          #   record_holding_klass = info[:through].to_const if info[:through]
          #   record_holding_klass ||= (info[:type] == :many_to_one) ? self.class : info[:class].to_const
          #   matches = record_holding_klass.all
          #   matches = matches.select { |entry| entry.match?(match) }
          #   return case
          #          when limit && info[:ordered]
          #            matches.select { |entry| (entry[opp_position] >= offset) && (entry[opp_position] < offset + limit) }
          #          when info[:ordered]
          #            matches.select { |entry| (entry[opp_position] >= offset) }
          #          when limit # FIXME this should break b/c opp_position will be nil unless info[:ordered]
          #            matches.select { |entry| (entry[opp_position] < offset + limit)}
          #          else
          #            matches
          #          end
          # end
            
          # Skip abstract classes and interfaces, they cannot be instanitated.
          queried_types = Array(self.class.type_of(getter)).reject { |klass| klass.abstract? || klass.interface? }
          
          filter = [filter] unless filter.is_a?(Array)
          # We're about to modify the filter hashes (to remove 'type'). We must duplicate them to avoid modifying the original filter.
          filter = filter.collect { |f| f.is_a?(Hash) ? f.dup : f }
          # Capture, then delete 'type' from filter. Not allowed in filter, we have to filter on class type manually.
          filter_on_type_names = filter.collect { |f| f.is_a?(Hash) ? f.delete('type') : nil }.flatten.compact
          if filter_on_type_names.any?
            filter_types = filter_on_type_names.collect do |type_name|
              klass_constant = type_name.to_const
              [klass_constant] + klass_constant.children
            end
            queried_types  = queried_types & filter_types.flatten.uniq
          end
          # Same as above, but use exact type matching instead of checking child classes
          filter_on_exact_type_names = filter.collect{|f| f.is_a?(Hash) ? f.delete('exact_type') : nil }.flatten.compact
          if filter_on_exact_type_names.any?
            exact_filter_types = filter_on_exact_type_names.collect do |type_name|
              type_name.to_const
            end
            queried_types = queried_types & exact_filter_types.flatten.uniq 
          end
          # Pass nil into class types to capture associations that are pointing to nil or deleted objects
          queried_types += [nil] if info[:through]
          queried_types_with_filters = _process_filter_for_types(filter, queried_types)
          
          # Initialize ordering values
          order_by_column = order[:column]
          # Special case where we want to order by type, regardless of pre-existing ordering
          if order_by_column == 'type'
            order_by_type   = true
            order_by_column = false
          elsif order_by_column.nil? && !info[:ordered]
            # Order by type if no other order is specified
            order_by_type = true
          end

          other_col  = "#{singular_getter}_id".to_sym
          other_type = "#{singular_getter}_class".to_sym

          # we are depending on association class tables having <getter>_class columns for both sides of the association (i.e. acting as many_to_many implementations)
          if info[:through]
            # Build Sequel query for many_to_many
            through_class   = info[:through].to_const
            join_table_name = through_class.table_name
            query           = through_class
            
            # Check to see if the column to order by was manually specified as being on the association class.
            on_association_class = order[:on_association_class]
            # Check if the association class table has the column to order by
            association_class_contains_order_column = order_by_column && (on_association_class != false) && through_class.columns.include?(order_by_column)
            # Select through_class id as _id_for_row
            # TODO: see if this is necessary -SD
            query = query.select(Sequel.as(through_class.primary_key, '_id_for_row_'))
            # Select append all columns for the through_class
            through_class.columns.each do |col|
              query = query.select_append(col)
            end
            # If association does have the ordering column (and not specified otherwise), then append it to selection
            # NOTE: Creating a virtual column helps us to order without knowing whether the column is off the 'association class' or the 'to class'.
            #       Also helps us to avoid having to qualify the select from possibly multiple tables.
            if association_class_contains_order_column
              query = query.select_append(Sequel.as(order_by_column, '_column_for_order_'))
              query = query.select_append(Sequel.as(Sequel.case([[{order_by_column=>nil},1]],0), '_column_for_sort_null_last_'))
            end
            query = query.filter(match).qualify
            
            # Find the set of all columns including those from subtypes
            all_columns = queried_types_with_filters.collect{|klass, _| klass.columns if klass }.compact.flatten.uniq
            
            type_queries = queried_types_with_filters.collect do |klass, klass_filter|
              type_query = query
              type_query = type_query.select_append(Sequel.as(klass ? klass.to_s : nil, '_class_for_row_'))
              if klass
                # Join query (of join table entries) with class (or subclass)
                # Filter on types if info[:class].to_const is polymorphic.
                if info[:class].to_const.is_polymorphic? || (info[:proto_info] && info[:proto_info][:class].to_const.is_polymorphic?)
                  type_query = type_query.filter(other_type => klass.to_s)
                end
                type_query = type_query.exclude(other_col => nil).qualify
                # Exclude joins where id is in ignore_objs
                ignore_ids_for_klass = ignore_objs.select{ |i| i.class == klass }.collect{ |i| i.id }
                type_query = type_query.exclude({other_col => ignore_ids_for_klass}).qualify

                if return_mode == :to
                  type_query = type_query.join(klass.table_name, {klass.primary_key => "#{through_class.table_name}__#{other_col}".to_sym})
                else
                  type_query = type_query.left_join(klass.table_name, {klass.primary_key => "#{through_class.table_name}__#{other_col}".to_sym})
                end
                
                # Select append all type columns, creating null aliases for those not used by this type
                # This allows us to use SQL union later since that requires that subqueries have the same number of columns
                all_columns.each do |col|
                  col_alias = "_to_klass_#{col}".to_sym
                  select_col = klass.columns.include?(col) ? Sequel.as(col, col_alias) : Sequel.as(nil, col_alias)
                  type_query = type_query.select_append(select_col)
                end
                
                type_query = type_query.filter(klass_filter).qualify(klass.table_name)
              else # Nil case (no right side row to join to)
                # skip this case unless we need to return through classes
                next nil unless return_through_instances
                # Select append null aliases for all possible columns
                # This allows us to use SQL union later since that requires that subqueries have the same number of columns
                # TODO: refactor so this code is not duplicated. -SD
                all_columns.each_with_index do |col, i|
                  col_alias = "_to_klass_#{col}".to_sym
                  select_col = Sequel.as(nil, col_alias)
                  type_query = type_query.select_append(select_col)
                end
                type_query = type_query.filter(::SpecificAssociations.filter_for_class(klass_filter, through_class))
                nil_match  = {other_col => nil}
                if info[:class].to_const.is_polymorphic?
                  nil_match = Sequel.|(nil_match, {other_type => nil})
                end
                type_query = type_query.filter(nil_match)
              end
              # Append column for ordering (if it's not on the association class)
              unless association_class_contains_order_column
                if order_by_column
                  # Still need to order, but can't append non-existent column when klass is nil. Fake it here (and for type), insert nil.
                  obc   = klass ? order_by_column : nil
                  type_query = type_query.select_append(Sequel.as(obc, '_column_for_order_'))
                  cfsnl = klass ? Sequel.case([[{order_by_column=>nil},1]],0) : nil
                  type_query = type_query.select_append(Sequel.as(cfsnl, '_column_for_sort_null_last_'))
                end
              end
              type_query = type_query.qualify(klass.table_name) if klass
              type_query
            end
          else # not a many to many
            # Build Sequel query for one_to_many or many_to_one
            all_columns = queried_types_with_filters.collect{|klass, klass_filter| klass.columns }.flatten.uniq
            
            type_queries = queried_types_with_filters.collect do |klass, klass_filter|
              type_query = klass.dataset
              # Select or select append all type columns, creating null aliases for those not used by this type
              # This allows us to use SQL union later since that requires that subqueries have the same number of columns
              # TODO: refactor so this code is not duplicated. -SD
              all_columns.each_with_index do |col, i|
                select_col = klass.columns.include?(col) ? col : Sequel.as(nil, col)
                if i == 0
                  type_query = type_query.select(select_col)
                else
                  type_query = type_query.select_append(select_col)
                end
              end
              
              # Append order_by_column to select
              type_query = type_query.select_append(Sequel.as(order_by_column, '_column_for_order_')) if order_by_column
              type_query = type_query.select_append(Sequel.as(Sequel.case([[{order_by_column=>nil},1]],0), '_column_for_sort_null_last_')) if order_by_column
              type_query = type_query.filter(match).qualify
              # Creating virtual column here to save class type.
              type_query = type_query.select_append(Sequel.as(klass.to_s, '_class_for_row_'))
              type_query = type_query.select_append(Sequel.as(klass.primary_key, '_id_for_row_'))
              type_query = type_query.filter(klass_filter).qualify(klass.table_name)
              type_query = type_query.exclude(klass.primary_key => ignore_objs.select { |i| i.class == klass }.collect { |i| i.pk }) if ignore_objs.any?
              type_query = type_query.qualify(klass.table_name) if klass
              type_query
            end
          end
          union_query = nil
          type_queries.compact.each do |q|
            if union_query.nil?
              union_query = q
            else
              # Must have 'from_self' set to false, to avoid unusual syntax wrapping of columns in generated SQL
              union_query = union_query.union(q, :from_self => false)
            end
          end
          if union_query
          # TODO: Do I need to change this back??? We may have overridden proc for Classes
          # Restoring class type from virtual column. For every row, we are doing a database lookup.
            # Do a database lookup for each item to get values. Seems innefficient, but Sequel does not support unioning of different classes correctly.
            # Restoring class type from association table.
            if info[:through]
              through_klass = info[:through].to_const
              
              union_query.row_proc = Proc.new{ |values| 
                to_klass = values[:_class_for_row_].to_const if values[:_class_for_row_]
                
                to = if to_klass && return_to_instances
                  to_klass_aliased_values = values.select{|k,v|
                    unaliased_key_str = k.to_s[10..-1]
                    to_klass.columns.include?(unaliased_key_str.to_sym) if unaliased_key_str
                  }
                  to_klass_values = {}
                  to_klass_aliased_values.each {|key, value|
                    actual_key = key.to_s[10..-1].to_sym
                    # Fix weird case where complex attribute boolean is sometimes (???) not properly typecast when retrieved from DB
                    # TODO: Discover why this case occurs and prevent, so this fix isn't needed. -SD
                    prop_info = to_klass.properties[actual_key]
                    value = (value == 1) if !value.nil? && prop_info && prop_info[:type] == :complex_attribute && value.is_a?(Integer)
                    to_klass_values[actual_key] = value
                  }
                  to_klass.call(to_klass_values) if to_klass_values[to_klass.primary_key]
                end
                
                # TODO: replace this with klass.call(values.slice(klass.columns)) when it's ok to drop support for Ruby < 2.5 -SD
                through = if return_through_instances
                  through_klass.call(values.select{|k,v| through_klass.columns.include?(k)})
                end
                
                case return_mode
                when :through, :through_or_to
                  through
                when :to
                  to
                else # return_mode == :both
                  {:to => to, :through => through}
                end
              }
            else # Not a many_to_many
              union_query.row_proc = Proc.new{ |values|
                klass = values[:_class_for_row_].to_const
                # TODO: replace this with klass.call(values.slice(klass.columns)) when it's ok to drop support for Ruby < 2.5 -SD
                to = klass.call(values.select{|k,v| klass.columns.include?(k)})
                case return_mode
                when :both
                  {:to => to}
                else
                  to
                end
              }
            end
            # Apply ordering and limiting, if either.
            # Sort by position if info[:ordered], ascending (by default) or descending, and only if manual ordering is not implemented.
            union_query = 
              case
              when order_by_type
                order[:descending_order] ? union_query.order(Sequel.desc(:_class_for_row_)) : union_query.order(:_class_for_row_)
              when order_by_column
                if order[:descending_order]
                   union_query.order(Sequel.desc(:_column_for_sort_null_last_)).order_append(Sequel.desc(:_column_for_order_))
                else
                  union_query.order(:_column_for_sort_null_last_).order_append(:_column_for_order_)
                end
              when info[:ordered]
                order[:descending_order] ? union_query.order(Sequel.desc(opp_position)) : union_query.order(opp_position)
              else
                union_query
              end
            union_query = union_query.limit(limit, offset) if limit
            # Prevents wrapping each union in SELECT statements. Could trigger a 'parser stack overflow' if too many unions.
            # union_query = union_query.from_self
          end
          union_query
        end
        
        def _unassoc_get_many_query(getter, filter = {}, limit = nil, offset = 0, ignore_objs = [], order = {})
          return [] if limit == 0
          info = self.class.info_for(getter.to_sym)
          if info[:alias_of]
            filter = ::SpecificAssociations.filter_join(filter, info[:filter])
            return _unassoc_get_many_query(info[:alias_of], filter, limit, offset, ignore_objs, order)
          end
          
          if info[:subset_of]
            current_values  = Array(_get_property(getter))
            pas             = superset_pending_association_operations(info[:subset_of])
            pending_adds    = pas.select { |pa| pa.is_a? SpecificAssociations::PendingAssociationAddition }
            pending_removes = pas.select { |pa| pa.is_a? SpecificAssociations::PendingAssociationRemoval }
            # puts "Pending Additions: #{pending_adds.inspect}"
            # puts "Pending Removals: #{pending_removes.inspect}"
            available_subset = ((_get_association_path(info[:subset_of], filter, limit, offset) || []) - pending_adds + pending_removes - current_values)
            return available_subset
          end
  
          singular_getter = _singular_getter(info)
          opp_getter      = _singular_opp_getter(info)
          opp_position    = "#{opp_getter}_position".to_sym if info[:ordered]
          
          if info[:type] == :many_to_one
            match = {:id => self[:id]}
          elsif info[:type] == :many_to_many || info[:type] == :one_to_many
            match = {"#{opp_getter}_id".to_sym => pk}
            match["#{opp_getter}_class".to_sym] = self.class.to_s if info[:as].to_const.is_polymorphic?
          elsif info[:type] == :one_to_one && info[:holds_key]
            match = {:id => self["#{singular_getter}_id".to_sym]}
          elsif info[:type] == :one_to_one
            match = {"#{opp_getter}_id".to_sym => pk}
            match["#{opp_getter}_class".to_sym] = self.class.to_s if info[:as].to_const.is_polymorphic?
          end
  
          # if browse_time
          #   record_holding_klass   = info[:through].to_const if info[:through]
          #   record_holding_klass ||= (info[:type] == :many_to_one) ? self.class : info[:class].to_const
          #   matches = record_holding_klass.all
          #   matches = matches.select { |entry| entry.match?(match) }
          #   return case
          #          when limit && info[:ordered]
          #            matches.select { |entry| (entry[opp_position] >= offset) && (entry[opp_position] < offset + limit) }
          #          when info[:ordered]
          #            matches.select { |entry| (entry[opp_position] >= offset) }
          #          when limit # FIXME this should break b/c opp_position will be nil unless info[:ordered]
          #            matches.select { |entry| (entry[opp_position] < offset + limit)}
          #          else
          #            matches
          #          end
          # end
            
          # Skip abstract classes and interfaces, they cannot be instanitated.
          queried_types = Array(self.class.type_of(getter)).reject { |klass| klass.abstract? || klass.interface? }
          
          filter = [filter] unless filter.is_a?(Array)
          # We're about to modify the filter hashes (to remove 'type'). We must duplicate them to avoid modifying the original filter.
          filter = filter.collect { |f| f.is_a?(Hash) ? f.dup : f }
          # Capture, then delete 'type' from filter. Not allowed in filter, we have to filter on class type manually.
          filter_on_type_names = filter.collect { |f| f.is_a?(Hash) ? f.delete('type') : nil }.flatten.compact
          if filter_on_type_names.any?
            filter_types = filter_on_type_names.collect do |type_name|
              klass_constant = type_name.to_const
              unless klass_constant < Sequel::Model
                # somebody is trying to filter on a type that is invalid
                return ::SpecificAssociations.to_one?(info) ? nil : []
              end
              [klass_constant] + klass_constant.children
            end
            queried_types  = queried_types & filter_types.flatten.uniq
          end          
          queried_types_with_filters = _process_filter_for_types(filter, queried_types)
          # Initialize ordering values
          order_by_column = order[:column]
          # Special case where we want to order by type, regardless of pre-existing ordering
          if order_by_column == 'type'
            order_by_type   = true
            order_by_column = false
          elsif order_by_column.nil?
            # Order by type if no other order is specified
            order_by_type = true
          end

          other_col  = "#{singular_getter}_id".to_sym
          other_type = "#{singular_getter}_class".to_sym
          all_columns = queried_types_with_filters.collect{|klass, klass_filter| klass.columns }.flatten.uniq

          # we are depending on association class tables having <getter>_class columns for both sides of the association (i.e. acting as many_to_many implementations)
          if info[:through]
            # Build Sequel query for many_to_many
            
            # Build base for exclusion query by selecting through class rows that match the given match hash
            # This returns through class rows that should be excluded since they are already associated with the current object
            exclusion_query = info[:through].to_const.select_all(info[:through].to_const.table_name).filter(match).qualify
            
            type_queries = queried_types_with_filters.collect do |klass, klass_filter|
              # Since SQL union requires that subqueries have the same number of columns, pad with nulls as needed
              type_query = klass.dataset
              all_columns.each_with_index do |col, i|
                select_col = klass.columns.include?(col) ? col : Sequel.as(nil, col)
                if i == 0
                  type_query = type_query.select(select_col)
                else
                  type_query = type_query.select_append(select_col)
                end
              end
              
              # Sequel union does not allow multiple types to be joined.
              type_query = type_query.select_append(Sequel.as(klass.to_s, '_class_for_row_'))
              type_query = type_query.select_append(Sequel.as(order_by_column, '_column_for_order_')) if order_by_column
              
              if info[:class].to_const.is_polymorphic?
                # If the to_class is polymorphic, use exclusion query base to build type-specific exclusion for this klass
                exclusion_type_query = exclusion_query.filter(other_type => klass.to_s)
              else
                exclusion_type_query = exclusion_query
              end
              # Remove rows where the ID of the associated item is nil from the exclusion query
              exclusion_type_query = exclusion_type_query.filter(Sequel.~(other_col => nil))
              type_query = type_query.filter(klass_filter)
              type_query = type_query.qualify(klass.table_name)
              type_query = type_query.exclude(klass.primary_key => ignore_objs.select { |i| i.class == klass }.collect { |i| i.id }) if ignore_objs.any?
              type_query = type_query.exclude(klass.primary_key => exclusion_type_query.select(other_col) ) if pk && exclusion_type_query
              type_query = type_query.qualify(klass.table_name)
              type_query
            end
          end
          if !info[:through]
            # Build Sequel query for one_to_many or many_to_one
            type_queries = queried_types_with_filters.collect do |klass, klass_filter|
              apply_exclusion = true
              # TODO: Much of this exclusion code does not need to be run for every klass. Move out of loop code -SD
              if info[:type] == :many_to_one
                # An exclusion only needs to be applied if there is a current association and then only for the currently held instance klass
                apply_exclusion = self[other_col] && (!info[:class].to_const.is_polymorphic? || klass.to_s == self[other_type])
                if apply_exclusion
                  exclusion_query = self.class.select_all(self.class.table_name).filter(match).qualify
                  excluded_ids = exclusion_query.select(other_col).qualify(self.class.table_name)
                end
              elsif info[:type] == :one_to_many
                apply_exclusion = !pk.nil?
                if apply_exclusion
                  exclusion_query = klass.select_all(klass.table_name).filter(match).qualify
                  excluded_ids = exclusion_query.select(klass.primary_key).qualify(klass.table_name)
                end
              elsif info[:type] == :one_to_one && info[:holds_key]
                apply_exclusion = self[other_col] && (!info[:class].to_const.is_polymorphic? || klass.to_s == self[other_type])
                if apply_exclusion
                  exclusion_query = klass.select_all(klass.table_name).filter(match)# if klass.to_s == self[other_type]
                  excluded_ids = exclusion_query.qualify.select(klass.primary_key).qualify(klass.table_name)
                end
              else # info[:type] == :one_to_one
                apply_exclusion = !pk.nil?
                if apply_exclusion
                  exclusion_query = klass.select_all(klass.table_name).filter(match).qualify
                  excluded_ids = exclusion_query.select(klass.primary_key).qualify(klass.table_name)
                end
              end
              
              # Since SQL union requires that subqueries have the same number of columns, pad with nulls as needed
              type_query = klass.dataset
              all_columns.each_with_index do |col, i|
                select_col = klass.columns.include?(col) ? col : Sequel.as(nil, col)
                if i == 0
                  type_query = type_query.select(select_col)
                else
                  type_query = type_query.select_append(select_col)
                end
              end
              
              # Sequel glitch, can't return multiple types when queries are unioned.
              # Creating virtual column here to save class type.
              type_query = type_query.select_append(Sequel.as(klass.to_s, '_class_for_row_'))
              # NOTE: Creating a virtual column helps us to avoid having to qualify the select from possibly multiple tables.
              type_query = type_query.select_append(Sequel.as(order_by_column, '_column_for_order_')) if order_by_column
              
              type_query = type_query.filter(klass_filter) if klass_filter.any?
              type_query = type_query.exclude(klass.primary_key => ignore_objs.select { |i| i.class == klass }.collect { |i| i.id }) if ignore_objs.any?
              type_query = type_query.exclude(klass.primary_key => excluded_ids) if apply_exclusion
              type_query = type_query.qualify(klass.table_name)
              type_query
            end
          end
          union_query = nil
          type_queries.compact.each do |query|
            if union_query.nil?
              union_query = query
            else
              # Must have 'from_self' set to false, to avoid unusual syntax wrapping of columns in generated SQL
              union_query = union_query.union(query, :from_self => false)
            end
          end
          if union_query
          # TODO: Do I need to change this back??? We may have overridden proc for Classes
          # Restoring class type from virtual column. For every row, we are doing a database lookup.
            # Do a database lookup for each item to get values. Seems innefficient, but Sequel does not support unioning of different classes correctly.
            # Restoring class type from association table.
            union_query.row_proc = Proc.new{ |values| 
              klass = values[:_class_for_row_].to_const
              # TODO: replace this with klass.call(values.slice(klass.columns)) when it's ok to drop support for Ruby < 2.5 -SD
              klass.call(values.select{|k,v| klass.columns.include?(k)})
            }
            
            
            # Apply ordering and limiting, if either.
            # Sort by position if info[:ordered], ascending (by default) or descending, and only if manual ordering is not implemented.
            union_query =         
              case
              when order_by_type
                order[:descending_order] ? union_query.order(Sequel.desc(:_class_for_row_)) : union_query.order(:_class_for_row_)
              when order_by_column
                if order[:descending_order]
                   union_query.order(Sequel.desc(:_column_for_order_))
                else
                  union_query.order(:_column_for_order_)
                end
              else
                union_query
              end  
            union_query = union_query.limit(limit, offset) if limit
            # Prevents wrapping each union in SELECT statements. Could trigger a 'parser stack overflow' if too many unions.
            union_query = union_query.from_self
          end
          union_query
        end
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
