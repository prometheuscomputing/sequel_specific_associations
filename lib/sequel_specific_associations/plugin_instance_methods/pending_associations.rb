module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        
        def _pending_associations(getter, filter)
          info = _get_info(getter)
          info = _get_info(info[:alias_of]) if info[:alias_of]
          sgetter       = _singular_getter(info)
          sopgetter     = _singular_opp_getter(info)
          through_class = info[:through].to_const if info[:through]

          relevant_objects = {:additions => [], :removals => []}
          return relevant_objects unless _tracked_changes?
  
          change_ops = ct_unit_of_work.changes.collect do |change|
            next nil unless change.is_a?(::ChangeTracker::ChangeAssociation)
            change.association_operation
          end
          change_ops.compact! # faster than select then collect?
  
          addition_ops = []
          removal_ops  = []
          
          # Get all of the ChangeAssociations that pertain to the association specified by getter
          change_ops.each_with_index do |operation, i|
            model1 = operation.model
            model2 = operation.second_model
            if !model2.respond_to?(:ct_identity)
              raise "Object of type: #{model2.class} does not have ChangeTracker applied! Object: #{model2.inspect}"
            end
    
            if through_class && model1.is_a?(through_class)
              change_getter = operation.association[:getter]

              # model1 is the join class instance.  model2 is self.  
              if (change_getter == sopgetter) && self.is?(model2)
                _sort_assoc_operation(operation, addition_ops, removal_ops)
                
              # model1 is the join class instance.  model2 is the object on the other side of the join from self.  
              elsif change_getter == sgetter
                to_obj = model1.send(:pending_association_removals, sopgetter).first || model1.send(sopgetter)
                _sort_assoc_operation(operation, addition_ops, removal_ops) if self.is?(to_obj)
              end
            # No join class between model1 and model2
            else
              if (model1.is?(self) && operation.pk_field.to_s == "#{sgetter}_id") || (model2.is?(self) && operation.second_pk_field.to_s == "#{sgetter}_id")
                _sort_assoc_operation(operation, addition_ops, removal_ops)
              end
            end
          end
          
          new_assocs           = []
          ignored_addition_ops = []
          addition_ops.each do |operation|
            model1 = operation.model
            model2 = operation.second_model
            if through_class && model1.is_a?(through_class)
              next if ignored_addition_ops.include?(operation)
              if model2.is?(self)
                second_pending_addition = model1.send(:pending_association_additions, model1.class.associations[sgetter][:getter]).first
                ignored_addition_ops << second_pending_addition[:operation] if second_pending_addition
                if second_pending_addition
                  to_obj = second_pending_addition[:object]
                else
                  to_obj = model1.send(sgetter)
                end
                new_assocs << {:to => to_obj, :through => model1}
              else
                second_pending_addition = model1.send(:pending_association_additions, model1.class.associations[sopgetter][:getter]).first
                ignored_addition_ops << second_pending_addition[:operation] if second_pending_addition
                new_assocs << {:to => model2, :through => model1}
              end
            elsif model1.is?(self)
              new_assocs << {:to => model2, :through => operation.through_class}
            else # model2.is?(self)
              new_assocs << {:to => model1, :through => operation.through_class}
            end
          end

          removed_assocs      = []
          ignored_removal_ops = []
          removal_ops.each do |operation|
            model1 = operation.model
            model2 = operation.second_model
            if through_class && model1.is_a?(through_class)
              next if ignored_removal_ops.include?(operation)
              if model2.is?(self)
                second_pending_removal = model1.send(:pending_association_removals, model1.class.associations[sgetter][:getter]).first
                if second_pending_removal
                  to_obj = second_pending_removal[:object]
                else
                  to_obj = model1.send(sgetter)
                end
                removed_assocs << {:to => to_obj, :through => model1}
              else
                second_pending_removal = model1.send(:pending_association_removals, model1.class.associations[sopgetter][:getter]).first
                removed_assocs << {:to => model2, :through => model1}
              end
              ignored_removal_ops << second_pending_removal[:operation] if second_pending_removal
            elsif model1.is?(self)
              removed_assocs << {:to => model2, :through => operation.through_class}
            else # model2.is?(self)
              removed_assocs << {:to => model1, :through => operation.through_class}
            end
          end
          # get only the assocs that match the filter
          new_assocs.select! { |a| a[:to].match?(filter) }
          removed_assocs.select! { |a| a[:to] && a[:to].match?(filter) }
          
          # TODO: Replay the associations in order in case something was removed and then added back in the same transaction
          assoc_hash = Hash.new(0)  
          new_assocs.each do |a|
            assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] += 1
          end
          removed_assocs.each do |a|
            assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] -= 1
          end

          relevant_objects[:additions] += new_assocs.select { |a| assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] > 0 }
          relevant_objects[:removals]  += removed_assocs.select { |a| assoc_hash[a[:to].pk ? a[:to] : a[:to].object_id] < 0 }
          relevant_objects
        end
        
        def _sort_assoc_operation(operation, addition_ops, removal_ops)
          if operation.is_a?(SpecificAssociations::PendingAssociationAddition)
            addition_ops << operation
          elsif operation.is_a?(SpecificAssociations::PendingAssociationRemoval)
            removal_ops  << operation
          end
        end
        
        #  Does this get weird if it is a self association?
        def _get_other_model(pending_assoc)
          if pending_assoc.model == self
            pending_assoc.second_model
          else
            model
          end
        end
        
        def add_pending_association_operation(association_operation)
          @pending_association_operations ||= []
          @pending_association_operations << association_operation
        end

        # These methods provide a way to get a list of pending association operations or their associated objects
        def pending_association_operations(association = nil)
          if using_change_tracker_for_associations?
            return [] unless ::ChangeTracker.started?
            changes = ::ChangeTracker.unit_of_work.changes.select do |change|
              next false unless change.is_a?(::ChangeTracker::ChangeAssociation)
              op = change.association_operation
              # A bit of a kludge here. Using the pk field of the association_operation to get the getter of the object type (trimming off '__id') that we're querying the associations for?
              getter = op.pk_field[0..-4].to_sym
              if op.association[:getter] == getter
                opp_getter = op.association[:opp_getter]
              else
                opp_getter = op.association[:getter]
              end
              ((op.model.ct_identity == ct_identity && (association.nil? || getter == association)) ||
               (op.second_model.ct_identity == ct_identity && (association.nil? || opp_getter == association)))
            end
            changes.collect { |change| change.association_operation }
          else
            paos = @pending_association_operations ||= []
            paos.select { |pao| association ? (pao.association[:getter] == association) : true }
          end
        end

        def pending_association_additions(association = nil)
          paas = pending_association_operations(association).select { |pao| pao.is_a?(SpecificAssociations::PendingAssociationAddition)}
          paas.collect do |paa|
            {
              :object      => paa.model == self ? paa.second_model : paa.model,
              :through     => paa.through_class,
              :association => paa.association,
              :operation   => paa
            }
          end
        end

        def pending_association_removals(association = nil)
          pars = pending_association_operations(association).select { |pao| pao.is_a?(SpecificAssociations::PendingAssociationRemoval)}
          pars.collect do |par|
            {
              :object      => par.model == self ? par.second_model : par.model,
              :through     => par.through_class,
              :association => par.association,
              :operation   => par
            }
          end
        end

        def pending_association_addition_objects(association = nil)
          pending_association_additions(association).collect { |addition| addition[:object] }
        end

        def pending_association_removal_objects(association = nil)
          pending_association_removals(association).collect { |removal| removal[:object] }
        end
        
        # Gets the pending associations relevant within the context of a subset constraint
        def superset_pending_association_operations(subset_getter_path)
          # If subset_getter_path is a chanined getter then get the target of the pending associations and the simple getter, otherwise the target is self and the subset_getter_path is the getter we want.
          if subset_getter_path.include?('.')
            path = subset_getter_path.split('.')
            target_getter = path.pop.to_sym
            pending_assoc_target = _get_association_path(path)
          else
            target_getter = subset_getter_path.to_sym
            pending_assoc_target = self
          end
          return [] unless pending_assoc_target
          # Could using both getter and opp_getter lead to problems in the case where there the pending_assoc_target has roles with both names?
          pas = pending_assoc_target.send(:pending_association_operations).select do |pa|
            pa.association[:getter] == target_getter || pa.association[:opp_getter] == target_getter
          end
          pas
        end
        
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel