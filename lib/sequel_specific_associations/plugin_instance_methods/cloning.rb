module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods        
        # Deep clone all attributes and composition associations
        def deep_clone(options = {})
          # Account for legacy option :ignore_composers (renamed to ignore_composer)
          puts ":ignore_composers is a deprecated option for #deep_clone. Please use :ignore_composer" if options[:ignore_composers]
          options[:ignore_composer] = options[:ignore_composers] if options[:ignore_composers]
          # Give the user the option to not clone composer associations
          defaults = {:ignore_composer => false}
          options = defaults.merge(options)
          # Perform a shallow clone, ignoring compositions for efficiency (They need to be processed separately)
          # Composers are also ignored for this first item if the option :ignore_composer is specified
          clone = shallow_clone(options.merge(:ignore_compositions => true))
          self.class.compositions(false).each do |ca|
            next if ca[:derived] # Do not clone any derived composition associations
            getter = ca[:getter]
            setter = "#{getter}="
            # Get the current value of the association
            value = _get_property(getter)
            # Deep clone the compositions
            if ::SpecificAssociations.to_many?(ca)
              clone_value = value.collect { |v| v.deep_clone }
            else
              clone_value = value.deep_clone if value
            end
            # Set the cloned composition values to the clone of the current class
            clone.send(setter, clone_value)
          end
          clone
        end
        
        # Create a clone of the current instance with the same attributes and associations
        def shallow_clone(options = {})
          puts ":ignore_composers is a deprecated option for #shallow_clone. Please use :ignore_composer" if options[:ignore_composers]
          # Account for legacy option :ignore_composers (renamed to ignore_composer)
          options[:ignore_composer] = options[:ignore_composers] if options[:ignore_composers]
          # Whether or not to process composition/composer associations. Used by deep_clone
          defaults = {:ignore_compositions => false, :ignore_composer => false, :duplicate_unlinkable_objects => false, :cloned_objects => {}}
          options = defaults.merge(options)
          
          # Check for clone in options[:cloned_objects] and return if found
          already_cloned = options[:cloned_objects][self]
          return already_cloned if already_cloned
          
          # Create a new object of the same Class
          clone = self.class.create
          
          # Save clone into options[:cloned_objects][self]
          options[:cloned_objects][self] = clone
          
          # Get list of uniqueness constrained columns
          uniqueness_constrained_columns = self.class.uniqueness_constraints.collect { |name, uc| uc.columns }.flatten.uniq
          
          # Iterate over attributes and duplicate values into clone if appropriate
          self.class.attributes.each do |name, info|
            next if info[:derived] || info[:alias_of] # Do not clone derived attributes
            getter = info[:column]
            next if getter == :id # Ignore 'id'. This shouldn't be an attribute, but currently is specified as one by generation code.
            next if uniqueness_constrained_columns.include?(getter) # Ignore any uniqueness constrained columns
            clone._set_property(getter, _get_property(getter))
          end
          
          # Iterate over associations and duplicate values into clone if appropriate
          composition_assocs = self.class.compositions(false)
          composer_assocs    = self.class.composers(false)
          self.class.associations(false).each do |name, info|
            next if info[:derived] # Do not clone derived associations
            next if options[:ignore_compositions] && composition_assocs.include?(info)
            next if options[:ignore_composer] && composer_assocs.include?(info)
            next if composer_assocs.include?(info) && !options[:ignore_compositions]
            # begin
            #   next unless send("#{info[:getter]}_count") > 0
            # rescue Sequel::DatabaseError => e
            #   # puts Rainbow(self.class.info_for(info[:getter]).pretty_inspect).yellow
            #   op_klass = info[:class].to_const
            #   raise unless op_klass.complex_attribute? && op_klass.properties.key?(info[:opp_getter])
            # end
            getter = info[:getter]
            new_value = 
              case info[:type]
              when :one_to_one, :one_to_many
                # We are unable to associate to the existing items here since they would be 'stolen' from the original object being cloned.
                if options[:duplicate_unlinkable_objects]
                  if info[:type] == :one_to_one
                    _get_property(getter).shallow_clone(options)
                  else
                    _get_property(getter).collect { |obj| obj.shallow_clone(options) }
                  end
                else
                  (info[:type] == :one_to_one) ? nil : []
                end
              else # Ok to link to existing objects
                _get_property(getter)
              end
            clone._set_property(getter, new_value)
          end
          clone.save
          clone
        end
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
