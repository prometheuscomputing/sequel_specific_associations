module Sequel
  module Plugins
    module SpecificAssociations
        module InstanceMethods

        # Return the first found composer
        # TODO: add logic that prevents an instance from having multiple composers
        # As well, cycles of compositions should be prevented as well.
        def composer
          self.class.composers.each do |composer_assoc|
            composer = _get_property(composer_assoc[:getter])
            return composer if composer
          end
          return nil
        end
        
        # This might not be of great utility?
        # It would be faster if we designated properties as being on the aggregated side
        # FIXME: No tests for this code, and it looks like it is broken! -SD
        def aggregators
          aggs = []
          self.class.aggregators.each do |aggregator_assoc|
            agg += [_get_property(aggregator_assoc[:getter])]
          end
          agg.flatten!
          agg.uniq!
          agg.any? ? agg : nil
        end

        # Return pending composer TODO: go over this with Sam!!!!!!!!!!!! Do we need to worry about opp_getter? or singular_opp_getter?  Will it always be the second model?  Kinda thinking not.  opp_getter should match up to pending_association#model -- but if we are looking at things from that side then we are the composer and we won't find the composition within this method..Hmmmm
        def pending_composer
          composer_getters = self.class.composers.collect { |c| c[:getter]}
          pending_association_operations.each do |pa|
            if composer_getters.include?(pa.association[:getter])
              return pa.second_model
            end
          end
          return nil
        end

        # Return contained compositions of an instance
        # Does not account for composition cycles
        def compositions(immediate_only = true)
          comps = []
          self.class.compositions(false).each do |composition_assoc|
            value = _get_property(composition_assoc[:getter])
            if ::SpecificAssociations.to_many?(composition_assoc)
              comps += value
              value.each { |v| comps += v.compositions(false)} unless immediate_only
            elsif value
              comps << value
              comps += value.compositions(false) unless immediate_only
            end
          end
          comps
        end
        
        # FIXME: No tests for this code! -SD
        def aggregations(immediate_only = true)
          aggs = []
          self.class.aggregations(false).each do |aggregation_assoc|
            value = _get_property(aggregation_assoc[:getter])
            if ::SpecificAssociations.to_many?(aggregation_assoc)
              aggs += value
              value.each { |v| aggs += v.aggregations(false)} unless immediate_only
            elsif value
              aggs << value
              aggs += value.aggregations(false) unless immediate_only
            end
          end
          aggs
        end

        # Return the association info for the first composer defined for this instance
        def composer_association
          self.class.composers.each do |composer_assoc|
            composer = _get_property(composer_assoc[:getter])
            return composer_assoc if composer
          end
          return nil
        end

        # currently only possible to do this with immediate compositions
        def compositions_with_data
          compositions = {}
          self.class.compositions(false).each do |composition_property|
            value = _get_property(composition_property[:getter])
            value = Array(value)
            if value.any?
              compositions[composition_property[:getter]] = {:value => value, :data => composition_property}
            end
          end
          compositions
        end
        
        # currently only possible to do this with immediate aggregations
        def immediate_aggregations_with_data
          aggs = {}
          self.class.aggregations(false).each do |agg_property|
            value = Array(_get_property(agg_property[:getter]))
            if value.any?
              aggs[agg_property[:getter]] = {:value => value, :data => agg_property}
            end
          end
          aggs
        end
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel
