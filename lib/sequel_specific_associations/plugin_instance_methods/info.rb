# TODO is this OK to do?  Caching info on the instance??
module Sequel::Plugins::SpecificAssociations::InstanceMethods
  def _get_info(getter, fail_silently = false)
    info = self.class.properties[getter.to_sym]
    # info = self.class.info_for(getter.to_sym) # uncomment for debugging
    unless info || fail_silently
      message = "#{self.class} does not have property: #{getter.inspect}\n#{self.class} has the following property keys: #{self.class.properties.keys.sort.inspect}"
      raise message
    end
    info
  end
  private :_get_info
  
  def _get_opp_info(getter, fail_silently = false)
    ::SpecificAssociations.get_opposite_association(_get_info(getter))
  end
  private :_get_opp_info
  
  def _tracked_changes?
    self.class.respond_to?(:change_tracked?) && self.class.change_tracked? && ct_unit_of_work
  end
end
