module Sequel::Plugins::SpecificAssociations::InstanceMethods
  # Get the number of associated objects
  # NOTE: In the case of a many-to-many, this method returns the number of association class instance
  #       This may differ from the number of objects returned by getter, if some associations are not
  #       complete.
  def _count(getter, filter = {})
    ignore_pending = filter.delete('ignore_pending')
    info = _get_info(getter)
    if info[:alias_of]
      filter = ::SpecificAssociations.filter_join(filter, info[:filter])
      return _count(info[:alias_of], filter)
    end
    
    if self.class.complex_attribute?
      return Array(_get_property(info[:getter])).count
    end
    
    if info[:attribute]
      if info[:derived] 
        # FIXME this could probably better in some way
        return send(getter) ? 1 : 0
      else # info[:type] == :attribute || info[:type] == :complex_attribute
        return self[info[:column]] ? 1 : 0
      end
    elsif info[:type] == :many_to_one || (info[:type] == :one_to_one && info[:holds_key])
      ret = self["#{getter}_id".to_sym] ? 1 : 0
    else
      query_results = _get_many_query(getter, filter, nil, 0, [], {}, :to)
      ret = query_results.nil? ? 0 : query_results.count
    end

    if !ignore_pending
      add, subtract = _parse_pending_assoc_changes(info, filter).collect(&:count)
      ret += add
      ret -= subtract
    end
    ret
  end
  
  # Get the number of unassociated objects
  # NOTE: In the case of a many-to-many, this method returns the number of association class instance
  #       This may differ from the number of objects returned by getter, if some associations are not
  #       complete.
  def _unassociated_count(getter, filter = {})
    ignore_pending = filter.delete('ignore_pending')
    info = _get_info(getter)
    if info[:alias_of]
      filter = ::SpecificAssociations.filter_join(filter, info[:filter])
      return _unassociated_count(info[:alias_of], filter)
    end
    
    query = _unassoc_get_many_query(getter, filter)
    ret   = query ? query.count : 0
    # For unassoc associations, swapped the - and + signs when changing the amount with the new and removed assoc changes.
    if !ignore_pending
      add, subtract = _parse_pending_assoc_changes(info, filter).collect(&:count)
      ret -= add
      ret += subtract
    end
    ret
  end
end
