module Sequel
  module Plugins
    module SpecificAssociations
      module InstanceMethods
        # Determine whether this instance matches a given filter
        # This could eventually be expanded to support advanced filters like 'Sequel.ilike', but it doesn't currently
        def match?(filter)
          filter = [filter] unless filter.is_a?(Array)
          !filter.find do |f|
            next unless f.is_a?(Hash) # Currently filters other than hashes aren't processed
            f.find do |attribute, filter_value|
              if attribute == 'type'
                # NOTE: Replaced this segment with hopefully more efficient version. Need to test speed.
                # filter_type = filter_value.to_const
                # !((self.class == filter_type) || self.class.inherits_from?(filter_type))
                !((self.class.to_s == filter_value) || self.class.parents.any?{|p| p.to_s == filter_value})
              elsif filter_value.is_a?(Range)
                !filter_value.cover?(self[attribute])
              elsif filter_value.is_a?(Array)
                !filter_value.include?(self[attribute])
              else
                self[attribute] != filter_value
              end
            end
          end
        end
          
        # Force any nil attributes on this instance to match a given filter
        # Only hash filters are supported
        def match_filter(filter)
          filter = [filter] unless filter.is_a?(Array)
          filter.each do |f|
            next unless f.is_a?(Hash)
            f.each do |attribute, filter_value|
              next if attribute == 'type' # Can't fix 'type' filter, so ignore it
              value_to_assign = case filter_value
              when String, SpecificAssociations::INT_TYPE, Float
                filter_value
              when Range
                filter_value.min
              else
                puts "Currently can't enforce #{filter_value.class} filter requirements when adding an item."
                next
              end
              # TODO: see if there is a case where _set_attribute/_get_property is required and handle it here
              # self._set_property(attribute, value_to_assign) if self._get_property(attribute).nil?
              self[attribute] = value_to_assign if self[attribute].nil?
            end
          end
        end
      end # End InstanceMethods
    end # SpecificAssociations
  end # Plugins
end # Sequel