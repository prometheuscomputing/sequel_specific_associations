module Sequel::Plugins::SpecificAssociations::InstanceMethods
  def to_title
    self.class.to_title
  end
end
