files = %w(base alias_properties association attribute metadata schemas type uniqueness)
files.each { |file| require_relative "plugin_class_methods/#{file}" }
