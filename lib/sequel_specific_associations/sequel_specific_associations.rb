require 'sequel_specific_associations/model/sequel_model'
require 'sequel_specific_associations/model/pending_association_operation'
require 'sequel_specific_associations/model/helpers'
require 'sequel_specific_associations/model/errors'
require 'sequel_specific_associations/model/uniqueness_constraints'
require 'sequel_specific_associations/model/extensions'
require 'sequel_specific_associations/sequel_meta_information'
require 'sequel_specific_associations/business_rules'
require 'sequel_specific_associations/meta_info'
require 'sequel_specific_associations/additional_extensions'
require 'sequel_specific_associations/plugin_instance_methods'
require 'sequel_specific_associations/plugin_class_methods'

# Information about association. Currently used only by QueryExt.
# Fix:
#  * Not currently storing the getter for ordering information
#  * Not currently storing getters / columns used by join table
#  * Probably does not work for association classes
#  * This does not currently have an association to the instance that describes the inverse
#  * Would prefer to build code from this (rather than extracting info from options twice)
#  * Not currently using this to store info about attributes.  Attributes should be treated as similarly as possible to association.
#  * Not much confidance in any of this code
class Assoc_MetaInfo
  attr_accessor :getter,         # Getter used at this end of association to obtain a single instance of type_name (at other end of association), unless collection? in which case getter returns an Array
                :fk_name,        # non-nil only if the table for the Model holding this Assoc_MetaInfo has (for this association) a foreign key pointing to the table for type_name
                :inverse_getter, # Getter used from an instance at other end of association
                :type_name,      # The class at the other end of the association (not the join or association class name)
                :is_collection,  # If true, getter returns an Array. Owning class has a <getter>_add method.
                :is_ordered,     # Meaningful only if is_collection is true. If true, objects in Array returned by getter are ordered.
                :join_class_name # non-nil only when a join table is needed
                
  def initialize(getter, fk_name, inverse_getter, type_name, is_collection = false, is_ordered = false, join_class_name = nil)
    @getter          = getter
    @fk_name         = fk_name
    @inverse_getter  =inverse_getter
    @type_name       =type_name
    @is_collection   =is_collection
    @is_ordered      =is_ordered
    @join_class_name =join_class_name
  end
  def self.from_options(association_type, other_class_name, options)
    Kernel.warn("The Assoc_MetaInfo#from_options method is deprecated and probably does not work correctly!")
    is_ordered = options[:order] || false  # There is also an :is_ordered option which appears to apply to the inverse association
    getter = options[:other_getter]
    inverse_getter = options[:getter]
    is_one_to_one  = options[:one_to_one] || false
    is_collection  = !is_one_to_one
    case association_type
      when :many_to_one
        inverse_getter = is_one_to_one ? inverse_getter : inverse_getter.pluralize
        new(getter, "#{getter}_id", inverse_getter, other_class_name)
      when :one_to_many
        getter = is_collection ? getter.pluralize : getter
        new(getter, nil, inverse_getter, other_class_name, is_collection, is_ordered)
      when :many_to_many
        raise "Did not expect :many_to_many to be :one_to_one!" unless is_collection
        new(getter.pluralize, nil, inverse_getter.pluralize, other_class_name, is_collection, is_ordered, options[:through])
      else "Unrecognized association type: #{association_type.inspect}"
    end
  end
  def join_fk; "#{getter.singularize}_id"; end # column used to get other end from join table
  def join_inverse_fk; "#{inverse_getter.singularize}_id"; end # column used to get this end from join table
  def type; type_name.to_const; end
  def join_class; join_class_name.to_const; end
  def join?; nil!=join_class_name; end
  def fk?; nil!=fk_name; end
  alias collection? is_collection
  alias ordered? is_ordered
end

module Sequel
  module Plugins
    module SpecificAssociations
      # This is used if ChangeTracker is also used. It is required to be false for Postgres support.
      LAZY_DELETED_TABLE_CREATION = false
      
      def self.configure(model)
        # Initialize association and attribute arrays
        model.setup
      end
      
    end # SpecificAssociations
  end # Plugins
end # Sequel